// eslint-disable-next-line @typescript-eslint/no-require-imports
const _ = require('lodash')

// These two schema for roles are enums but are missing the type field
const transformer = (input) => {
    _.set(
        input,
        'components.schemas.mantik_api__database__project__ProjectRole.type',
        'number'
    )
    _.set(
        input,
        'components.schemas.mantik_api__database__role_details__ProjectRole.type',
        'number'
    )

    return input
}

module.exports = {
    'mantik-api-spec': {
        output: {
            workspace: 'src/queries/',
            target: './MantikQueries.ts',
            schemas: './models',
            client: 'react-query',
            mode: 'tags-split',
            override: {
                mutator: {
                    path: '../../src/modules/auth/mantikApi.ts',
                    name: 'mantikApiInstance',
                },
            },
            mock: {
                type: 'msw',
                baseUrl: 'https://api.dev2.cloud.mantik.ai',
            },
            clean: true,
        },
        input: {
            target: 'https://api.dev2.cloud.mantik.ai/openapi.json',
            override: {
                transformer,
            },
        },
    },
}
