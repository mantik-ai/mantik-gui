import { ReactNode } from 'react'

export interface Route {
    name: string
    positions?: string[]
    icon?: ReactNode
    subRoutes?: Route[]
    path: string
    target?: '_blank' | string
}
