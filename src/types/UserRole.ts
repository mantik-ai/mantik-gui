import { MantikApiDatabaseRoleDetailsProjectRole } from '../queries'

export enum UserRole {
    Visitor = MantikApiDatabaseRoleDetailsProjectRole.NUMBER_MINUS_1,
    Guest = MantikApiDatabaseRoleDetailsProjectRole.NUMBER_1,
    Reporter = MantikApiDatabaseRoleDetailsProjectRole.NUMBER_2,
    Researcher = MantikApiDatabaseRoleDetailsProjectRole.NUMBER_3,
    Maintainer = MantikApiDatabaseRoleDetailsProjectRole.NUMBER_4,
    Owner = MantikApiDatabaseRoleDetailsProjectRole.NUMBER_5,
}
