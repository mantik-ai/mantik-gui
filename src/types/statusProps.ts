export interface StatusProps {
    status: boolean
    type: string
    text: string
}
