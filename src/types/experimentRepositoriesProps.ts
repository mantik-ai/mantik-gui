interface LabelProps {
    labelId: string
    scope: string
    name: string
    value: string
}

export interface ExperimentRepositoryProps {
    experimentRepositoryId: string
    mlflowExperimentId: number
    name: string
    artifactLocation: string
    lifecycleStage: string
    lastUpdateTime: number
    creationTime: number
    labels: LabelProps[]
}
