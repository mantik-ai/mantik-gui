export interface CodeRepositoryProps {
    codeRepositoryId: string
    codeRepositoryName: string
    uri: string
    accessToken: string
    description: string
    labels: { [key: string]: any }
}
