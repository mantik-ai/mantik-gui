import { JSX } from 'react'
import { RenderElement } from '../utils/renderResourceActions'

export type MenuItemProps = {
    type: RenderElement
    action?: (idx: number, type: RenderElement) => void
    icon: JSX.Element
    tooltipText: string
    color?: 'primary' | 'error' | 'info'
}
