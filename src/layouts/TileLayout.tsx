import {
    Box,
    Card,
    CardActions,
    CardContent,
    CardHeader,
    Chip,
    Typography,
    useTheme,
} from '@mui/material'
import React, { JSX } from 'react'
import { Label } from '../queries'
import { Spacing } from '../components/Spacing'

interface TileLayoutProps {
    logo?: React.ReactNode
    title?: string
    bodyContent?: React.ReactNode
    bodyText?: string
    labels?: Label[]
    alignTextCenter?: boolean
    footer?: JSX.Element
    subHeading?: JSX.Element
    actions?: JSX.Element[]
}
export const TileLayout = (props: TileLayoutProps) => {
    const theme = useTheme()
    return (
        <Card
            data-testid="card-content"
            sx={{
                width: '100%',
                minHeight: '300px',
                height: '100%',
            }}
        >
            <CardHeader
                action={props.actions ? props.actions : <></>}
            ></CardHeader>
            <CardHeader
                style={{
                    textAlign: props.alignTextCenter ? 'center' : 'left',
                }}
                avatar={props.logo}
                title={props?.title && props?.title}
                subheader={props.subHeading ? props.subHeading : <></>}
            ></CardHeader>
            <CardContent
                style={{
                    textAlign: props.alignTextCenter ? 'center' : 'left',
                    paddingTop: '0px',
                }}
            >
                {props.bodyText && (
                    <Typography
                        variant="body1"
                        sx={{
                            wordBreak: 'break-all',
                            lineHeight: '1.1',
                        }}
                    >
                        <small>{props.bodyText}</small>
                    </Typography>
                )}
                <Spacing value={theme.spacing(5)} />
                {props.bodyContent}
            </CardContent>
            <CardActions>
                {props.labels?.length ? (
                    <Box>
                        {props.labels?.map((label, idx) => (
                            <Chip
                                key={idx}
                                label={label.name}
                                style={{ margin: '2px' }}
                            />
                        ))}
                    </Box>
                ) : (
                    ''
                )}
            </CardActions>
            {props.footer && (
                <Typography
                    variant="caption"
                    style={{
                        marginRight: '1em',
                        marginTop: 'auto',
                        paddingBottom: '0.5em',
                        textAlign: 'right',
                        color: theme.palette.grey[700],
                    }}
                >
                    {props.footer}
                </Typography>
            )}
        </Card>
    )
}
