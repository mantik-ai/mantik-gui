import React from 'react'
import { Box } from '@mui/material'
import { Footer } from '../components/footer/Footer'
import Navbar from '../components/navbar/Navbar'
import { Route } from '../types/route'
import { MAIN_LAYOUT_MARGIN_TOP } from '../constants'

const routes: Route[] = [
    { name: 'Projects', path: '/projects', positions: ['navbar', 'drawer'] },
    {
        name: 'Docs',
        path: 'https://mantik-ai.gitlab.io/mantik/', ///docs/Getting_Started/Welcome.md
        positions: ['navbar', 'drawer'],
        target: '_blank',
    },
    { name: 'About', path: '/about', positions: ['navbar', 'drawer'] },
    { name: 'Contact', path: '/contact', positions: ['navbar', 'drawer'] },
]

interface MainLayoutProps {
    children: React.ReactNode
}

const Layout: React.FC<MainLayoutProps> = (props) => {
    return (
        <Box
            sx={{
                position: 'relative',
                display: 'flex',
                flexFlow: 'column',
                height: '100vh',
                backgroundColor: 'background.default',
            }}
        >
            <Navbar routes={routes} />
            <Box
                sx={{
                    overflowY: 'visible',
                    flexGrow: 1,
                }}
            >
                <Box
                    sx={{
                        width: '100%',
                        overflowX: 'scroll',
                        height: '100%',
                        paddingTop: MAIN_LAYOUT_MARGIN_TOP,
                    }}
                >
                    {' '}
                    {props.children}
                </Box>
            </Box>

            <Footer />
        </Box>
    )
}

export default Layout
