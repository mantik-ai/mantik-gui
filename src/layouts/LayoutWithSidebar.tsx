import { Box, useTheme } from '@mui/material'
import { ReactNode } from 'react'
import {
    DetailsSideBar,
    DetailsSidebarProps,
} from '../components/DetailsSideBar'

export const LayoutWithSidebar = (
    props: DetailsSidebarProps & { children: ReactNode }
) => {
    const theme = useTheme()

    return (
        <Box sx={{ display: 'flex', height: '100%' }}>
            <DetailsSideBar {...props} />
            <Box
                sx={{
                    flex: 4,
                    display: 'flex',
                    flexDirection: 'column',
                    height: '100%',
                    p: theme.spacing(2),
                }}
            >
                {props.children}
            </Box>
        </Box>
    )
}
