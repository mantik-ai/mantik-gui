import React, { useState } from 'react'
import Tabs from '@mui/material/Tabs'
import Tab from '@mui/material/Tab'
import Box from '@mui/material/Box'

interface TabData {
    label: string
    index: number
    component: React.ReactNode
}

interface TabLayoutProps {
    tabsData: TabData[]
}

export const TabLayout: React.FC<TabLayoutProps> = ({ tabsData }) => {
    const [value, setValue] = useState(0)

    const handleChange = (event: React.SyntheticEvent, newValue: number) => {
        setValue(newValue)
    }

    return (
        <Box sx={{ width: '100%' }}>
            <Tabs value={value} onChange={handleChange} aria-label="tabs">
                {tabsData.map((tab) => (
                    <Tab
                        key={tab.index}
                        label={tab.label}
                        {...tabProps(tab.index)}
                    />
                ))}
            </Tabs>
            {tabsData.map((tab) => (
                <TabPanel key={tab.index} value={value} index={tab.index}>
                    {tab.component}
                </TabPanel>
            ))}
        </Box>
    )
}
function tabProps(index: number) {
    return {
        id: `tab-${index}`,
        'aria-controls': `tabPanel-${index}`,
    }
}
interface TabPanelProps {
    value: number
    index: number
    children: React.ReactNode
}

const TabPanel: React.FC<TabPanelProps> = ({ value, index, children }) => {
    return (
        <Box role="tabpanel" hidden={value !== index}>
            {value === index && <Box p={3}>{children}</Box>}
        </Box>
    )
}
