export function renderApiErrorMessage(error: any): string {
    const response = error.response?.data
    return response?.message
        ? `${response.message}`
        : response?.detail
          ? `${response.detail}`
          : `${error?.message}`
            ? `${error.message}`
            : 'Error'
}
