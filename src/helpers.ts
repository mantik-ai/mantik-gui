import {
    EMAIL_REGEX,
    LOWERCASE_REGEX,
    NO_WHITESPACE_REGEX,
    NUMBER_REGEX,
    SYMBOL_REGEX,
    UPPERCASE_REGEX,
    UUID_REGEX,
} from './constants'
import yaml from 'js-yaml'

export const assertEnv = (value: unknown) => {
    if (typeof value === 'string') {
        return value
    }

    throw Error(`env variable is not defined`)
}

export const formRulesChecker = (
    name: string,
    value: string,
    stateObj: { [key: string]: string },
    givenPassword: string
) => {
    switch (name) {
        case 'email':
            if (!EMAIL_REGEX.test(value)) {
                stateObj[name] = 'Please enter a valid Email'
            }
            break
        case 'username':
            if (value === '') {
                stateObj[name] = 'Username cannot be empty'
            }
            if (EMAIL_REGEX.test(value)) {
                stateObj[name] = 'Username connot be an email'
            }
            if (!NO_WHITESPACE_REGEX.test(value)) {
                stateObj[name] += 'Username connot contain whitespaces'
            }
            break
        case 'password':
            stateObj[name] = ''
            if (value === '') {
                stateObj[name] += 'Password cannot be empty'
            }
            if (value.length < 8) {
                stateObj[name] ? (stateObj[name] += ', ') : null
                stateObj[name] += 'Minimum length of 8 characters'
            }
            if (!UPPERCASE_REGEX.test(value)) {
                stateObj[name] ? (stateObj[name] += ', ') : null
                stateObj[name] += 'Minimum one uppercase letter'
            }
            if (!LOWERCASE_REGEX.test(value)) {
                stateObj[name] ? (stateObj[name] += ', ') : null
                stateObj[name] += 'Minimum one lowercase letter'
            }
            if (!NUMBER_REGEX.test(value)) {
                stateObj[name] ? (stateObj[name] += ', ') : null
                stateObj[name] += 'Minimum one number/digit'
            }
            if (!SYMBOL_REGEX.test(value)) {
                stateObj[name] ? (stateObj[name] += ', ') : null
                stateObj[name] += 'Minimum one special symbol'
            }
            if (!NO_WHITESPACE_REGEX.test(value)) {
                stateObj[name] ? (stateObj[name] += ', ') : null
                stateObj[name] += 'Password cannot contain whitespaces'
            }
            break
        case 'confirmPassword':
            if (value !== givenPassword) {
                stateObj[name] = 'Password does not match'
            }
            break
        default:
            break
    }
}

function base64UrlDecode(base64Url: string): string {
    let base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/')
    while (base64.length % 4) {
        base64 += '='
    }
    return atob(base64)
}
export function generateUserIdFromAccessToken(
    accessToken: string | undefined
): string | null {
    const tokenParts = accessToken?.split('.')
    if (tokenParts?.length !== 3) {
        console.error('Invalid access token format.')
        return null
    }
    const encodedPayload = tokenParts[1]
    const decodedPayload = base64UrlDecode(encodedPayload)
    try {
        const payloadObj = JSON.parse(decodedPayload)
        return payloadObj.sub || null
    } catch (error) {
        console.error('Error decoding access token:', error)
        return null
    }
}

export const debounce = (func: Function, timeout = 300) => {
    let timer: ReturnType<typeof setTimeout>
    return (...args: any) => {
        clearTimeout(timer)
        timer = setTimeout(() => {
            func.apply(this, args)
        }, timeout)
    }
}
export const isISO8601DateTime = (date: string) => {
    const isoRegex =
        /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}(\.\d{1,})?(\+\d{2}:\d{2})?$/
    return isoRegex.test(date)
}
const formatTime = (date: Date) => {
    const year = date.getFullYear()
    const month = String(date.getMonth() + 1).padStart(2, '0')
    const day = String(date.getDate()).padStart(2, '0')
    const hours = String(date.getHours()).padStart(2, '0')
    const minutes = String(date.getMinutes()).padStart(2, '0')
    const seconds = String(date.getSeconds()).padStart(2, '0')

    return `${year}-${month}-${day} ${hours}:${minutes}:${seconds}`
}

export const formatUnixTimestamp = (timestamp: number | undefined) => {
    if (timestamp) {
        const date = new Date(timestamp * 1000) // Convert to milliseconds
        return formatTime(date)
    } else return 'Invalid timestamp'
}

export const formatTimestamp = (timestamp: string | undefined) => {
    if (timestamp) {
        if (isISO8601DateTime(timestamp)) {
            const parsedTimestamp = Date.parse(timestamp)
            if (!isNaN(parsedTimestamp)) {
                const date = new Date(timestamp)
                return formatTime(date)
            }
        } else {
            return formatUnixTimestamp(Number(timestamp))
        }
    }
    return 'Invalid timestamp'
}
export function parseYaml(data: any) {
    try {
        const parsedData = yaml.load(data)
        return parsedData
    } catch (error) {
        console.error(error)
    }
}
export function createYamlFromObject(obj: any) {
    try {
        const yamlFile = yaml.dump(obj)
        return yamlFile
    } catch (error) {
        console.error(error)
    }
}

export function capitalizeFirstLetter(str: string) {
    return str.charAt(0).toUpperCase() + str.slice(1)
}

export function removeIdFromPath(path: string) {
    const pathArray = path.split('/')
    const filteredPathArray = pathArray.filter((item) => {
        return !UUID_REGEX.test(item) && item !== '[id]'
    })
    return filteredPathArray.join('/')
}

export function replaceTimeValueInCronExpression(
    cronExpression: string,
    idx: number,
    value: string
) {
    return cronExpression
        .split(' ')
        .reduce(
            (cron, param, index) =>
                index === idx ? cron + ' ' + value : cron + ' ' + param,
            ''
        )
        .trim()
}
function decodeBase64(str: string): string {
    const decoded = Buffer.from(str, 'base64').toString('utf-8')
    return decoded
}

export function extractExpiryTime(token: string) {
    if (!token || token === 'undefined') return null
    const tokenParts = token.split('.')
    if (tokenParts.length < 2) {
        throw new Error('Invalid token format')
    }

    const payload = JSON.parse(decodeBase64(tokenParts[1]))

    if (payload.exp) {
        const expirationTimestamp = payload.exp * 1000
        return new Date(expirationTimestamp)
    } else {
        return null
    }
}

export function isTokenExpired(token: string) {
    const expiryTime = extractExpiryTime(token)
    const currentTime = new Date()
    if (expiryTime) {
        return currentTime >= expiryTime
    }
    return false
}

export function getIndex<T>(
    givenValue: T,
    givenKey: string,
    arr: Record<string, any>[]
) {
    if (givenValue && arr && givenKey) {
        let idx = -1
        arr.forEach((el, i) => {
            Object.entries(el).map(([currentKey, currentValue]) => {
                if (currentKey === givenKey && currentValue == givenValue) {
                    if (typeof givenValue === typeof currentValue) {
                        idx = i
                    } else {
                        console.error(
                            'types of given value and current value must match'
                        )
                    }
                }
            })
        })
        return idx
    }
    return -1
}
export function removeHyphens(data: string) {
    return data.replace(/-/g, '')
}

export function generateCamelCaseKeyFromString(str: string) {
    return (
        str[0].toLowerCase() +
        str.slice(1).replace(/\s\S+/g, (s) => s[1].toUpperCase() + s.slice(2))
    )
}

export function generateTitleFromCamelCaseKey(key: string) {
    return (
        key[0].toUpperCase() +
        key.slice(1).replace(/[A-Z]/g, (str) => ' ' + str.toLowerCase())
    )
}
export const handlePagination = (
    type: 'rowsPerPage' | 'page',
    num: number,
    setRowsPerPage: React.Dispatch<React.SetStateAction<number>>,
    setPage: React.Dispatch<React.SetStateAction<number>>
): void => {
    if (type === 'rowsPerPage') {
        setRowsPerPage(num)
    } else if (type === 'page') {
        setPage(num)
    }
}

export const shortenString = (
    str: string,
    min: number,
    max: number,
    windowWidth: number
) => {
    if (str.length < min) return str
    const len =
        Math.min(
            max - min,
            Math.max(Math.floor((windowWidth - 1600) / 50), 0)
        ) + min
    const shortenedStr = str.slice(0, len)
    return shortenedStr.length < str.length
        ? shortenedStr + '...'
        : shortenedStr
}

export function sanitzeRepoUrl(url: string): string | null {
    url = url
        .replace('https://', '')
        .replace('git@', '')
        .replace('.com:', '.com/')
        .replace('.git', '')
        .replace(/\/?browse\/?$/gm, '')
    return `https://${url}`
}
export function toTitleCase(str: string) {
    return str
        .replace(/([A-Z])/g, ' $1') // Insert a space before all caps
        .replace(/_/g, ' ') // Replace underscores with spaces
        .replace(
            /\w\S*/g,
            (txt) => txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase()
        ) // Capitalize each word
        .trim()
}
export const downloadFileInBackground = (url: string) => {
    const a = document.createElement('a')
    a.style.display = 'none'
    a.href = url
    document.body.appendChild(a)
    a.click()
    window.URL.revokeObjectURL(url)
}
