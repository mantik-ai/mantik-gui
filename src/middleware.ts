import { NextResponse } from 'next/server'
import type { NextRequest } from 'next/server'

export async function middleware(req: NextRequest) {
    const { cookies } = req
    const allCookies = cookies.getAll()
    const authCookies = allCookies.find(
        (cookie) =>
            cookie.name === '__Secure-next-auth.session-token.0' ||
            cookie.name === 'next-auth.session-token.0'
    )

    if (!authCookies) {
        return NextResponse.rewrite(new URL('/login', req.url))
    }

    return NextResponse.next()
}

// Supports both a single string value or an array of matchers
export const config = {
    matcher: ['/settings/:path*', '/collaborations/:path*'],
}
