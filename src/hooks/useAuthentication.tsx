import { useSession } from 'next-auth/react'
import { AXIOS_INSTANCE } from '../modules/auth/mantikApi'
import { useEffect } from 'react'

export const useAuthentication = () => {
    const session = useSession()

    useEffect(() => {
        if (session?.status === 'authenticated') {
            AXIOS_INSTANCE.interceptors.request.use((config) => {
                if (config.headers) {
                    config.headers['Authorization'] =
                        `Bearer ${session?.data?.user.accessToken}`
                    config.headers['Content-Type'] = 'application/json'
                }
                return config
            })
        }
    }, [session])

    return session?.status
}
