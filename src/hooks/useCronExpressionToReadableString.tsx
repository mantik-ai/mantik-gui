import awsCronParser from 'aws-cron-parser'
import { useEffect, useState } from 'react'

interface CronExpressionToReadableStringProps {
    cronExpression: string
    nextOccurrances?: number
    timeZone?: string
}
export function useCronExpressionToReadableString({
    cronExpression,
    nextOccurrances = 1,
    timeZone = 'Europe/Berlin',
}: CronExpressionToReadableStringProps) {
    const [nextRunsStrings, setNextRunsStrings] = useState<string[]>([])
    const [cronError, setCronError] = useState<boolean>(false)

    useEffect(() => {
        try {
            // first we need to parse the cron expression
            // AWS crons have one more place for year. The API
            // only allows 5 places for Cron strings as well as the form.
            const cron = awsCronParser.parse(`${cronExpression} *`)

            // to get the first occurrence from now
            let occurrence = awsCronParser.next(cron, new Date())!

            const lang = 'en-GB'
            const config: Intl.DateTimeFormatOptions = {
                weekday: 'long',
                year: 'numeric',
                month: 'long',
                day: 'numeric',
                hour: 'numeric',
                minute: 'numeric',
                timeZone: 'Etc/UTC',
            }
            const configForTimeZone: Intl.DateTimeFormatOptions = {
                timeZoneName: 'short',
                timeZone: timeZone,
            }
            const readableStrings = [
                `${occurrence.toLocaleDateString(lang, config)} ${
                    occurrence
                        .toLocaleDateString(lang, configForTimeZone)
                        .split(' ')[1]
                }`,
            ]

            for (let i = 1; i < nextOccurrances; i++) {
                // to get the next occurrence following the previous one
                occurrence = awsCronParser.next(cron, occurrence)!
                readableStrings.push(
                    `${occurrence.toLocaleDateString(lang, config)} ${
                        occurrence
                            .toLocaleDateString(lang, configForTimeZone)
                            .split(' ')[1]
                    }`
                )
            }

            setCronError(false)
            setNextRunsStrings(readableStrings)
            // eslint-disable-next-line unused-imports/no-unused-vars
        } catch (e) {
            setCronError(true)
            return
        }
    }, [cronExpression, timeZone, nextOccurrances])

    return !cronError ? nextRunsStrings : ['Invalid cron expression']
}
