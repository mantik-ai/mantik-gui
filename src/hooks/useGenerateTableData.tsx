import { useCallback, useEffect, useMemo, useState } from 'react'
import { refactorDataForTableView } from '../components/TablesLayout/extractTableData'

export const useGenerateTableData = (data: any, prop: string) => {
    const dataObj = useMemo(
        () => (data && data[prop] ? data[prop] : []),
        [data, prop]
    )

    const memoizedRefactorDataForTableView: (data: any[], prop: string) => any =
        useCallback(refactorDataForTableView, [dataObj])

    const [tableData, setTableData] = useState<any>([])

    useEffect(() => {
        const tData: Array<{
            data: {
                [key: string]: {
                    data?: string
                    component?: React.ReactElement
                    type?: string
                    link?: string
                    hiddenTableColumn?: boolean
                }
            }
        }> = dataObj ? memoizedRefactorDataForTableView(dataObj, prop) : []
        setTableData(tData)
    }, [dataObj, memoizedRefactorDataForTableView, prop])

    return tableData
}
