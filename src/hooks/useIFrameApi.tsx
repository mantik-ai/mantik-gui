import { useEffect } from 'react'
import { MLFLOW_UI_URL } from '../constants'

export interface MantikGuiPayload {
    message: string
    path?: string
    method?: string
    payload?: any
}
export interface MantikGuiMessage {
    data: MantikGuiPayload
    uuid: string
    type: string
}

export interface MantikGuiResponseBody {
    data: any
}

export interface MantikGuiResponse {
    response: MantikGuiResponseBody
    uuid: string
    type: string
    message: string
}
export interface MessageHandler {
    (payload: MantikGuiPayload): Promise<MantikGuiResponseBody>
}

interface MantikGuiResponder {
    (source: MessageEventSource, message: MantikGuiResponse): void
}
export const useIFrameApi = (iframeMessageHandler: MessageHandler) => {
    useEffect(() => {
        const sendMessageToChild: MantikGuiResponder = (source, data) => {
            try {
                source.postMessage(data, { targetOrigin: '*' })
            } catch (e) {
                console.error(e)
            }
        }

        const processMessage: MessageHandler = async (message) => {
            return iframeMessageHandler(message)
        }

        const handleMessage = async (event: MessageEvent<MantikGuiMessage>) => {
            try {
                const { source, origin, data } = event
                if (origin === MLFLOW_UI_URL && source !== null) {
                    const { uuid, type } = data
                    const response = await processMessage(data.data)
                    const responseMessage = {
                        uuid,
                        type,
                        response,
                        message: 'message',
                    }
                    sendMessageToChild(source, responseMessage)
                }
            } catch (e) {
                console.error('error handling message', e)
            }
        }

        window.addEventListener('message', handleMessage)
        return () => {
            window.removeEventListener('message', handleMessage)
        }
    })
}
