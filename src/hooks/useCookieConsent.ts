import { useEffect } from 'react'
import * as CookieConsent from 'vanilla-cookieconsent'
import 'vanilla-cookieconsent/dist/cookieconsent.css'

function configureCookieConsent({
    enableAnalytics,
    disableAnalytics,
}: {
    enableAnalytics: () => void
    disableAnalytics: () => void
}): CookieConsent.CookieConsentConfig {
    const handleAnalytics = () => {
        if (CookieConsent.acceptedCategory('analytics')) {
            enableAnalytics()
        } else {
            disableAnalytics()
        }
    }
    return {
        guiOptions: {
            consentModal: {
                layout: 'box',
                position: 'bottom right',
                equalWeightButtons: true,
                flipButtons: false,
            },
            preferencesModal: {
                layout: 'box',
                position: 'left',
                equalWeightButtons: true,
                flipButtons: false,
            },
        },
        onConsent: handleAnalytics,
        onChange: handleAnalytics,
        disablePageInteraction: true,

        categories: {
            necessary: {
                readOnly: true,
                enabled: true,
            },
            analytics: {
                autoClear: {
                    cookies: [
                        {
                            name: /^(cwr_s|cwr_u)/,
                        },
                    ],
                },
            },
        },

        language: {
            default: 'en',

            translations: {
                en: {
                    consentModal: {
                        title: 'We value your privacy',
                        description:
                            'We use cookies to enhance your browsing experience, analyze site traffic, and serve personalized content. By clicking "Accept All," you consent to our use of cookies. You can also <a href="#privacy-policy" data-cc="show-preferencesModal" class="cc__link">manage your preferences</a> or reject cookies entirely. For more details, please review our <a href="/privacy" class="cc__link">Privacy policy</a>.',
                        acceptAllBtn: 'Accept all',
                        acceptNecessaryBtn: 'Reject all',
                        showPreferencesBtn: 'Manage preferences',
                        closeIconLabel: 'Close',
                        footer: `<a href="/privacy">Privacy Policy</a>`,
                    },
                    preferencesModal: {
                        title: 'Cookie preferences',
                        acceptAllBtn: 'Accept all',
                        acceptNecessaryBtn: 'Reject all',
                        savePreferencesBtn: 'Save preferences',
                        closeIconLabel: 'Close',
                        sections: [
                            {
                                title: 'Cookie Usage',
                                description:
                                    'We use cookies to ensure the proper functioning of our website and to enhance your browsing experience. You can customize your cookie preferences for each category at any time. For more detailed information regarding cookies and how we handle your data, please refer to our <a href="/privacy" class="cc__link">privacy policy</a>.',
                            },
                            {
                                title: 'Strictly necessary cookies',
                                description:
                                    'These cookies are essential for securely managing your login session. They help keep you signed in as you navigate through the website and ensure secure authentication processes. Authentication cookies also protect against unauthorized access and security threats, such as cross-site request forgery (CSRF) attacks. Since these cookies are required for the website to function properly, they cannot be disabled.',
                                linkedCategory: 'necessary',
                            },
                            {
                                title: 'Performance and Analytics cookies',
                                linkedCategory: 'analytics',
                                cookieTable: {
                                    headers: {
                                        name: 'Name',
                                        domain: 'Service',
                                        description: 'Description',
                                    },
                                    body: [
                                        {
                                            name: 'cwr_s',
                                            domain: 'AWS CloudWatch RUM',
                                            description:
                                                'Tracks session information for AWS CloudWatch RUM to monitor performance.',
                                        },
                                        {
                                            name: 'cwr_u',
                                            domain: 'AWS CloudWatch RUM',
                                            description:
                                                'Tracks user identifiers for AWS CloudWatch RUM to analyze user behavior.',
                                        },
                                    ],
                                },
                            },
                        ],
                    },
                },
            },
        },
    }
}

export function useCookieConsent({
    enableAnalytics,
    disableAnalytics,
}: {
    enableAnalytics: () => void
    disableAnalytics: () => void
}) {
    useEffect(() => {
        const pluginConfig = configureCookieConsent({
            enableAnalytics,
            disableAnalytics,
        })
        CookieConsent.run(pluginConfig).catch((error) => {
            console.error('Error running CookieConsent:', error)
        })
    }, [disableAnalytics, enableAnalytics])
}
