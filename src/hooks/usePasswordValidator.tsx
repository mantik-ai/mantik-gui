import { useEffect, useState } from 'react'
import {
    LOWERCASE_REGEX,
    NO_WHITESPACE_REGEX,
    NUMBER_REGEX,
    SYMBOL_REGEX,
    UPPERCASE_REGEX,
} from '../constants'

export const usePasswordValidator = (value: string, reset = false) => {
    const [passwordRequirements, setPasswordRequirements] = useState<
        {
            requirement: string
            error: boolean
        }[]
    >([])
    const [error, setError] = useState(false)
    const [initial, setInitial] = useState(true)
    useEffect(() => {
        const pwRequirements = {
            'Password cannot be empty': /.+/,
            'Minimum length of 8 characters': /.{8,}/,
            'Minimum one uppercase letter': UPPERCASE_REGEX,
            'Minimum one lowercase letter': LOWERCASE_REGEX,
            'Minimum one number/digit': NUMBER_REGEX,
            'Minimum one special symbol': SYMBOL_REGEX,
            'Password cannot contain whitespaces': NO_WHITESPACE_REGEX,
        }

        const pwRequirementsArr: {
            requirement: string
            error: boolean
        }[] = []

        for (const requirement in pwRequirements) {
            pwRequirementsArr.push({
                requirement: requirement,
                error: !pwRequirements[
                    requirement as keyof typeof pwRequirements
                ].test(value),
            })
        }

        // initial state ensures that requirements list ist only shown after user has typed
        // it also prevents the error state from being set to true initially so the field is not red
        if (value !== '' && initial) {
            setInitial(false)
        }

        const isError = pwRequirementsArr.some(
            (requirement) => requirement.error
        )
        if (isError && !initial && !reset) {
            setError(true)
        } else {
            setError(false)
        }
        setPasswordRequirements(pwRequirementsArr)
    }, [value, initial, reset])
    return {
        passwordRequirements: initial || reset ? [] : passwordRequirements,
        error,
    }
}
