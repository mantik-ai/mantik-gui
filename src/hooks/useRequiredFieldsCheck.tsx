import { useEffect, useState } from 'react'

export const useRequiredFieldsCheck = (
    form: HTMLElement | null,
    formDataState: { [key: string]: unknown }
): boolean => {
    const [requiredFieldValuesGiven, setRequiredFieldValuesGiven] =
        useState<boolean>(false) // this is the optimum setting to disable button initially, but led to bugs in some cases --> should be resolved with timeout hack

    useEffect(() => {
        const timeout = setTimeout(() => {
            // wait for the form to be rendered -- this is a hack. Necessary for edit form data passed to input fields after render. Makes validation more reliable, but sluggish. A better solution should be found.
            if (!form) return
            const requiredFields = Array.from(
                form.querySelectorAll<HTMLInputElement>('input[required]')
            )
            const isFormValid = requiredFields.every(
                (field) => field.value.trim() !== ''
            )
            setRequiredFieldValuesGiven(isFormValid)
        }, 200)
        return () => clearTimeout(timeout)
    }, [form, formDataState])
    return requiredFieldValuesGiven
}
