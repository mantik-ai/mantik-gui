# Custom Hooks

- ### Authentication

    The useAuthentication is used to avoid redundant effects hooks if authentication is needed for fetching data in each component: _`useAuthentication.tsx`_

- ### Cron Expression Strings

    Returns a list of human readable strings transcripted from Cron Expressions: _`useCronExpressionToReadableString.tsx`_

- ### Debounce

    _To Do: Add usecase for_: _`useDebounce.tsx`_

- ### Filter Errors

    Accepts an object with boolean props and returns true if one true value is found: _`useFilterErrors.tsx`_

- ### Generate Table Data

    Accepts a data object from database and returns renderable table data: _`useGenerateTableData.tsx`_

- ### Basic Modal Functionality

    Returns a boolean state and functions to open and close modal windows: _`useModal.tsx`_

- ### Check form for missing required data

    Accepts a form wrapper (html element) and a trigger state and returns true if all required fields have entries: _`useRequiredFieldsCheck.tsx`_

## Deprecated (to be replaced/deleted)

The following hooks should be deleted after their file references have been updated with new functionality from the new endpoint providing user information.
