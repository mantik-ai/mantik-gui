import { ChangeEvent, useState } from 'react'
import { EMAIL_REGEX } from '../constants'
import { ConfirmationCodeUserDetails } from '../queries'

export const useSetUsernameOrEmail = () => {
    const [helperText, setHelperText] = useState('')
    const [userNameOrEmail, setUserNameOrEmail] = useState('')
    const [confirmationType, setConfirmationType] =
        useState<keyof ConfirmationCodeUserDetails>('userName')

    const onChange = (
        e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
    ) => {
        const { value } = e.target
        if (EMAIL_REGEX.test(value)) {
            setConfirmationType('email')
        } else {
            setConfirmationType('userName')
        }
        setUserNameOrEmail(value)
        setHelperText(!value ? 'Username or email is required' : '')
    }

    return {
        confirmationType,
        helperText,
        userNameOrEmail,
        onChange,
    }
}
