import { useEffect, useState } from 'react'

export const useFilterErrors = (errorState: { [key: string]: any }) => {
    const [formError, setFormError] = useState(false)
    useEffect(() => {
        const findBoolErrors = (error: any) =>
            typeof error === 'boolean' && error === true
        const flattenArray = (arr: any) =>
            arr.reduce((arr: any, el: any) => arr.concat(el), [])
        const hasErrors =
            Object.values(errorState).filter((e) => {
                if (typeof e === 'boolean' && e === true) return true
                else if (typeof e === 'object') {
                    return (
                        flattenArray(Object.values(e)).filter((el: any) =>
                            findBoolErrors(el)
                        ).length > 0
                    )
                }
            }).length > 0

        setFormError(hasErrors)
    }, [errorState])

    return formError
}
