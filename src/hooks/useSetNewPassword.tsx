import { ChangeEvent, useEffect, useState } from 'react'

export const useSetNewPassword = (resetPassword: boolean) => {
    const [password, setPassword] = useState('')
    const [showPassword, setShowPassword] = useState(false)
    const [showConfirmPassword, setShowConfirmPassword] = useState(false)
    const [confirmPassword, setConfirmPassword] = useState('')
    const [confirmPasswordErrorMessage, setConfirmPasswordErrorMessage] =
        useState('')

    const handleShowPassword = (target: string) => {
        if (target === 'password') {
            setShowPassword((prev) => !prev)
        } else {
            setShowConfirmPassword((prev) => !prev)
        }
    }

    const onChange = (e: ChangeEvent<HTMLInputElement>) => {
        const { target } = e
        if (target.name === 'password') {
            setPassword(target.value)
            setConfirmPasswordErrorMessage(
                !!confirmPassword && confirmPassword !== target.value
                    ? 'Passwords do not match'
                    : ''
            )
        } else {
            setConfirmPassword(target.value)
            setConfirmPasswordErrorMessage(
                !!password && password !== target.value
                    ? 'Passwords do not match'
                    : ''
            )
        }
    }

    useEffect(() => {
        if (resetPassword) {
            setPassword('')
            setConfirmPassword('')
            setConfirmPasswordErrorMessage('')
            setShowConfirmPassword(false)
            setShowPassword(false)
        }
    }, [resetPassword])

    return {
        password,
        confirmPassword,
        confirmPasswordErrorMessage,
        showPassword,
        showConfirmPassword,
        handleShowPassword,
        handleChange: onChange,
    }
}
