import { FormControl, FormGroup, TextField, useTheme } from '@mui/material'
import React, { useEffect, useMemo, useState } from 'react'
import { Spacing } from '../../../components/Spacing'
import {
    ExperimentRepository,
    Label,
    useProjectsProjectIdExperimentsExperimentRepositoryIdPutProjectsProjectIdExperimentsExperimentRepositoryIdPut,
    useProjectsProjectIdExperimentsPostProjectsProjectIdExperimentsPost,
} from '../../../queries'
import { renderApiErrorMessage } from '../../../errors'
import { FormDialogLayout } from '../../../components/FormDialogLayout'
import { LabelSelector } from '../../projects_overview/components/LabelSelector'
import { useAuthentication } from '../../../hooks/useAuthentication'

interface ExperimentRepeatDialogProps {
    open: boolean
    projectId: string
    setOpen: React.Dispatch<React.SetStateAction<boolean>>
    editData?: ExperimentRepository
    isEdit?: boolean
    setIsEdit: Function
    refetchData?: () => void // Callback function to be invoked when code repository is added
}

export const ExperimentDialog = (props: ExperimentRepeatDialogProps) => {
    const theme = useTheme()
    const authentication = useAuthentication()

    const [name, setName] = useState('')
    const [labels, setLabels] = useState<Label[]>([])
    const [status, setStatus] = useState({ status: false, type: '', text: '' })
    const [nameErrorText, setNameErrorText] = useState('')

    const { mutate, isPending: isLoading } =
        useProjectsProjectIdExperimentsPostProjectsProjectIdExperimentsPost({
            mutation: {
                onSuccess: () => {
                    setStatus({
                        status: true,
                        type: 'success',
                        text: 'Experiment details were saved successfully!',
                    })
                    if (props.refetchData) {
                        props.refetchData() // Invoke the callback function if it's defined
                    }
                },
                onError: (error) => {
                    setStatus({
                        status: true,
                        type: 'error',
                        text: renderApiErrorMessage(error),
                    })
                },
            },
        })

    const updateExperimentsMutation =
        useProjectsProjectIdExperimentsExperimentRepositoryIdPutProjectsProjectIdExperimentsExperimentRepositoryIdPut(
            {
                mutation: {
                    onSuccess: () => {
                        setStatus({
                            status: true,
                            type: 'success',
                            text: 'Experiment was updated successfully!',
                        })
                        if (props.refetchData) {
                            props.refetchData() // Invoke the callback function if it's defined
                        }
                    },
                    onError: (error) => {
                        setStatus({
                            status: true,
                            type: 'error',
                            text: renderApiErrorMessage(error),
                        })
                    },
                },
            }
        )

    function handleSubmit() {
        if (!props.projectId) return

        const labelIds = labels.map((label) => label.labelId)

        if (authentication === 'authenticated') {
            if (props.isEdit) {
                updateExperimentsMutation.mutate({
                    projectId: props.projectId,
                    experimentRepositoryId:
                        props.editData?.experimentRepositoryId || '',
                    data: {
                        name,
                        labels: labelIds,
                    },
                })
            } else {
                mutate({
                    projectId: props.projectId,
                    data: {
                        name,
                        labels: labelIds,
                    },
                })
            }
        } else {
            setStatus({
                status: true,
                type: 'error',
                text: 'Authentication error. Please check your inputs and try again.',
            })
        }
    }

    function resetData() {
        setName('')
        setNameErrorText('')
        setLabels([])
    }

    function handleClose(): void {
        props.setOpen(false)
        setStatus({ status: false, type: '', text: '' })
        props.setIsEdit(false)
        resetData()
    }

    function handleNameChange(value: string) {
        setName(value)
        setStatus({ status: false, type: '', text: '' })
        setNameErrorText(!value ? 'Please enter name' : '')
    }

    function handleLabelSelect(value: any) {
        setLabels(value)
    }
    const memoizedEditData = useMemo(() => props.editData, [props.editData])
    useEffect(() => {
        if (props.isEdit) {
            setName(props.editData?.name || '')
            setLabels(props.editData?.labels || [])
        }
    }, [memoizedEditData])

    return (
        <FormDialogLayout
            formDataState={{ name, error: !!nameErrorText }}
            status={status}
            open={props.open}
            title={!props.isEdit ? `Create an Experiment` : `Edit Experiment`}
            content="Configure the experiment details"
            handleSubmit={handleSubmit}
            handleClose={handleClose}
            isEdit={props.isEdit}
            loading={
                props.isEdit ? updateExperimentsMutation.isPending : isLoading
            }
        >
            <FormGroup>
                <FormControl>
                    <TextField
                        key="name"
                        name="name"
                        id="name"
                        label="Name"
                        size="small"
                        value={name}
                        error={!!nameErrorText}
                        onChange={(e) => {
                            handleNameChange(e.target.value)
                        }}
                        required
                    />
                </FormControl>
                <Spacing value={theme.spacing(2)} />
                <LabelSelector
                    currentLabels={labels}
                    queryString="experiment"
                    handleLabelSelect={handleLabelSelect}
                />
            </FormGroup>
        </FormDialogLayout>
    )
}
