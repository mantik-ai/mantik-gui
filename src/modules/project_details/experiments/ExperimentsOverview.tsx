import { Add } from '@mui/icons-material'
import { Button, useTheme } from '@mui/material'
import { useRouter } from 'next/router'
import React, { useState } from 'react'

import { DataStateIndicator } from '../../../components/DataStateIndicator'
import { DeleteDialogLayout } from '../../../components/DeleteDialogLayout'
import { Spacing } from '../../../components/Spacing'
import {
    ActionProps,
    TablesLayout,
} from '../../../components/TablesLayout/TablesLayout'
import { PAGE_LENGTH_OPTIONS } from '../../../constants'
import { renderApiErrorMessage } from '../../../errors'
import { useGenerateTableData } from '../../../hooks/useGenerateTableData'
import {
    ExperimentRepository,
    useProjectsProjectIdExperimentsExperimentRepositoryIdDeleteProjectsProjectIdExperimentsExperimentRepositoryIdDelete,
    useProjectsProjectIdExperimentsGetProjectsProjectIdExperimentsGet,
} from '../../../queries'
import { StatusProps } from '../../../types/statusProps'
import {
    RenderElement,
    renderResourceActions,
    ResourceType,
} from '../../../utils/renderResourceActions'
import { parseUserRole } from '../../../utils/userRole'
import { DetailsToolbar } from '../overview/ProjectDetailsToolbar'
import { ExperimentDialog } from './ExperimentDialog'

export const ExperimentsOverview = () => {
    const router = useRouter()
    const theme = useTheme()
    const { id } = router.query
    const [openExperimentDialog, setOpenExperimentDialog] = useState(false)
    const [experimentDeleteDialogOpen, setExperimentDeleteDialogOpen] =
        useState<boolean>(false)

    const [rowsPerPage, setRowsPerPage] = useState<number>(
        PAGE_LENGTH_OPTIONS[0]
    )
    const [page, setPage] = useState<number>(0)
    const openDialog = () => {
        setOpenExperimentDialog(true)
    }
    const initialEditData: ExperimentRepository = {
        experimentRepositoryId: '',
        mlflowExperimentId: 0,
        name: '',
        createdAt: '',
        labels: [],
    }
    const [editItem, setEditItem] = useState<ExperimentRepository>()
    const [isEdit, setIsEdit] = useState<boolean>(false)
    const { data, status, refetch, error } =
        useProjectsProjectIdExperimentsGetProjectsProjectIdExperimentsGet(
            id as string,
            {
                pagelength: rowsPerPage,
                startindex: page * rowsPerPage,
            },
            {
                // async fix: triggers data refetch when authorization changes
                // fixes issue where incorrect user role is returned
                /* request: {
                    headers: {
                        Authorization: `Bearer ${user?.accessToken}`,
                    },
                },*/
            }
        )
    const tableData = useGenerateTableData(data, 'experimentRepositories')

    const userRole = parseUserRole(data?.userRole)
    const userActions = renderResourceActions(
        ResourceType.EXPERIMENTS,
        userRole
    )

    const handleRowsChange = (
        type: 'rowsPerPage' | 'page',
        num: number
    ): void => {
        if (type === 'rowsPerPage') {
            setRowsPerPage(num)
        } else if (type === 'page') {
            setPage(num)
        }
    }
    const handleDataRefetch = () => {
        refetch() // Refresh the data
    }

    const { mutate, isPending: isLoading } =
        useProjectsProjectIdExperimentsExperimentRepositoryIdDeleteProjectsProjectIdExperimentsExperimentRepositoryIdDelete(
            {
                mutation: {
                    onSuccess: () => {
                        setDeleteStatus({
                            status: true,
                            type: 'success',
                            text: 'Experiment was deleted successfully!',
                        })
                        handleDataRefetch()
                    },
                    onError: (error) => {
                        setDeleteStatus({
                            status: true,
                            type: 'error',
                            text: renderApiErrorMessage(error),
                        })
                    },
                },
            }
        )

    const [deleteData, setDeleteData] = useState({
        name: '',
        projectId: id,
        experimentRepositoryId: '',
    })

    const [deleteStatus, setDeleteStatus] = useState<StatusProps>({
        status: false,
        type: '',
        text: '',
    })

    /**
     * Edit experiment functionality
     */

    function handleIsEdit() {
        setEditItem(initialEditData)
        setIsEdit(false)
    }

    function handleEditData(idx = 0) {
        setOpenExperimentDialog(true)
        const row =
            data && data.experimentRepositories
                ? data.experimentRepositories[idx]
                : null
        if (!!row) {
            setEditItem(row)
        }
        setIsEdit(true)
    }

    /**
     * Delete experiment functionality
     */

    function handleDeleteOpen() {
        setExperimentDeleteDialogOpen(true)
    }

    function handleDeleteClose() {
        setExperimentDeleteDialogOpen(false)
        setDeleteStatus({ status: false, type: '', text: '' })
    }

    function handleDelete(idx: number) {
        handleDeleteOpen()
        const row =
            data && data.experimentRepositories
                ? data.experimentRepositories[idx]
                : null
        if (!!row) {
            setDeleteData((prev) => ({
                ...prev,
                name: String(row.name),
                experimentRepositoryId: String(row.experimentRepositoryId),
            }))
        }
    }

    function handleDeleteSubmit() {
        const projectId = String(id)
        const experimentRepositoryId = deleteData?.experimentRepositoryId
        mutate(
            { projectId, experimentRepositoryId },
            {
                // Optional: You can provide extra configuration here if needed
                // e.g., onSuccess, onError, onSettled, etc.
            }
        )
    }

    const action: ActionProps = {}

    if (userActions.includes(RenderElement.EDIT)) {
        action.edit = handleEditData
    }

    if (userActions.includes(RenderElement.DELETE)) {
        action.delete = handleDelete
    }
    return (
        <>
            <DataStateIndicator
                status={status}
                text="Loading Experiment..."
                usePaper
                errorMessage={
                    error?.response?.status === 401
                        ? "The project you're trying to access is private and you don't have the required permissions!"
                        : 'Error'
                }
            >
                <DetailsToolbar
                    title={'Experiments'}
                    tool={
                        userActions.includes(RenderElement.ADD) ? (
                            <Button variant="text" onClick={() => openDialog()}>
                                <Add></Add>Add
                            </Button>
                        ) : (
                            <></>
                        )
                    }
                />
                <Spacing value={theme.spacing(1)}></Spacing>
                <TablesLayout
                    data={tableData}
                    action={action}
                    page={{
                        rowsPerPage: rowsPerPage,
                        page: page,
                        totalRecords: data?.totalRecords || 0,
                        handleRowsChange,
                    }}
                    tableFor={'experiments'}
                />
            </DataStateIndicator>
            <ExperimentDialog
                projectId={String(id)}
                open={openExperimentDialog}
                setOpen={setOpenExperimentDialog}
                setIsEdit={handleIsEdit}
                isEdit={isEdit}
                editData={editItem}
                refetchData={handleDataRefetch}
            ></ExperimentDialog>
            <DeleteDialogLayout
                open={experimentDeleteDialogOpen}
                title="Delete Experiment"
                text={
                    <>
                        Are you sure you want to delete experiment{' '}
                        <strong>{deleteData?.name}</strong>?
                    </>
                }
                warning="All related runs and their tracked parameters, metrics, artifacts, models, etc., will be deleted!"
                handleSubmit={handleDeleteSubmit}
                handleClose={handleDeleteClose}
                isLoading={isLoading}
                status={deleteStatus}
            ></DeleteDialogLayout>
        </>
    )
}
