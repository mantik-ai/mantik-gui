import { Paper, useTheme } from '@mui/material'
import React, { useContext } from 'react'
import { MarkdownWrapper } from '../../../docs/MarkdownWrapper'
import { DetailsToolbar } from '../ProjectDetailsToolbar'
import { Spacing } from '../../../../components/Spacing'
import { useProject } from '../../../../context/ProjectContext'
import { DataStateIndicator } from '../../../../components/DataStateIndicator'
import DataContext from '../../../../context/DataContext'

export const DetailsOverview = () => {
    const theme = useTheme()
    const project = useProject()
    const {
        projectDetailsDataStatus: status,
        projectDetailsErrorResponseStatus: errorResponse,
    } = useContext(DataContext)
    const markdown = project.detailedDescription ?? ''
    const markdownWithLineBreaks = markdown.replace(/\\n/gi, ' \n')
    return (
        <DataStateIndicator
            status={status}
            text="Loading Overview..."
            errorMessage={
                errorResponse === 401 // This should be changed to 403 once the correct status code is returned from backend
                    ? "The project you're trying to access is private and you don't have the required permissions!"
                    : 'Error'
            }
            usePaper
        >
            <DetailsToolbar title="Overview" />
            <Paper className="details-content">
                <MarkdownWrapper
                    markdown={markdownWithLineBreaks}
                ></MarkdownWrapper>
            </Paper>
            <Spacing value={theme.spacing(1)}></Spacing>
        </DataStateIndicator>
    )
}
