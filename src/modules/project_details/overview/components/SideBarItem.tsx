import { ListItemButton, ListItemIcon, ListItemText } from '@mui/material'
import Link from 'next/link'
import React, { ReactNode } from 'react'
import { useRouter } from 'next/router'
import { removeIdFromPath } from '../../../../helpers'
import { useTheme } from '@mui/material'

export default function SideBarItem({
    name,
    path,
    icon,
    subItem = false,
}: {
    name: string
    path: string
    icon: ReactNode
    subItem?: boolean
}) {
    const router = useRouter()
    const theme = useTheme()
    const isSelected =
        removeIdFromPath(router.pathname) === removeIdFromPath(path)

    return (
        <Link key={name} href={path} passHref legacyBehavior>
            <ListItemButton
                key={name}
                selected={isSelected}
                sx={
                    subItem
                        ? {
                              paddingLeft: theme.spacing(4),
                          }
                        : {}
                }
            >
                <ListItemIcon>{icon}</ListItemIcon>
                <ListItemText>{name}</ListItemText>
            </ListItemButton>
        </Link>
    )
}
