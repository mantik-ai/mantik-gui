import React, { useContext } from 'react'
import DataContext from '../../../context/DataContext'
import Toolbar, {
    DetailsToolbarProps,
} from '../../../components/DetailsToolbar'

export const DetailsToolbar = ({
    title = '',
    center = <></>,
    tool = <></>,
}: DetailsToolbarProps) => {
    const { projectDetailsData: data } = useContext(DataContext)

    return <Toolbar title={title} center={center} tool={tool} data={data} />
}
