import { Info } from '@mui/icons-material'
import { Box, Grid, Typography, useTheme } from '@mui/material'
import { useCallback, useState } from 'react'
import { QueryStatus, useQueryClient } from '@tanstack/react-query'
import { strings, urls } from '../../../../public/locales/en/translation'
import { ExternalLink } from '../../../components/ExternalLink'
import { Spacing } from '../../../components/Spacing'
import { useProject } from '../../../context/ProjectContext'
import {
    AddDataRepository,
    Connection,
    DataRepository,
    useProjectsProjectIdDataGetV02ProjectsProjectIdDataGet as useGetDataRepository,
    useProjectsProjectIdDataDataRepositoryIdConnectionProjectsProjectIdDataDataRepositoryIdConnectionPost as usePostConnection,
    useProjectsProjectIdDataPostProjectsProjectIdDataPost as usePostDataRepository,
} from '../../../queries'
import { DataRepositoryCard } from './DataRepositoryCard'
import { AddRepositoryDialog } from './Dialogs/AddRepositoryDialog'
import { AddResourceToolbar } from './Dialogs/AddResourceToolbar'
import { CreateDataStatusDialog } from './Dialogs/CreateDataRepositoryStatusDialog'
import { SuccessDialog } from './Dialogs/SuccessDialog'
import { Action } from './actions'

export type StatusForAction =
    | {
          status: QueryStatus
          type: Action
          open?: boolean
      }
    | {
          status: 'idle'
          type?: Action
          open?: boolean
      }

export const DataRepositoriesOverview = () => {
    const [isSuccessDialogOpened, setSuccessDialogOpened] = useState(false)
    const [isAddDialogOpened, setAddDialogOpened] = useState(false)

    const [
        dataRepositoryConnectionFromForm,
        setDataRepositoryConnectionFromForm,
    ] = useState<Connection | null>(null)
    const [dvcConnectionFromForm, setDvcConnectionFromForm] =
        useState<Connection | null>(null)

    const { projectId } = useProject()
    const queryClient = useQueryClient()
    const { data, queryKey } = useGetDataRepository(projectId)
    const {
        mutate: addConnection,
        error: addConnectionError,
        reset: resetAddConnection,
        isIdle: isIdleAddConnection,
        isPending: isLoadingAddConnection,
    } = usePostConnection()
    const {
        mutate: addDvcConnection,
        error: addDvcConnectionError,
        reset: resetAddDvcConnection,
        isIdle: isIdleAddDvcConnection,
        isPending: isLoadingAddDvcConnection,
    } = usePostConnection()

    const {
        mutate,
        isSuccess: isSuccessPostDataRepository,
        error: createDataRepositoryError,
        isPending: isLoadingPostDataRepository,
    } = usePostDataRepository({
        mutation: {
            async onSuccess(data) {
                if (dataRepositoryConnectionFromForm?.connectionId) {
                    addConnection({
                        projectId,
                        dataRepositoryId: data.dataRepositoryId,
                        data: {
                            connectionId:
                                dataRepositoryConnectionFromForm?.connectionId,
                        },
                    })
                }
                if (dvcConnectionFromForm?.connectionId) {
                    addDvcConnection({
                        projectId,
                        dataRepositoryId: data.dataRepositoryId,
                        data: {
                            connectionId: dvcConnectionFromForm.connectionId,
                        },
                    })
                }

                await queryClient.invalidateQueries({ queryKey })
            },
            onSettled() {
                setAddDialogOpened(false)
                setSuccessDialogOpened(true)
            },
        },
    })

    const repos: DataRepository[] = data?.dataRepositories ?? []
    const [queryStatus, setQueryStatus] = useState<StatusForAction>({
        status: 'idle',
        open: false,
    })

    const theme = useTheme()

    const onModalClose = useCallback(() => {
        resetAddConnection()
        resetAddDvcConnection()
        setDataRepositoryConnectionFromForm(null)
        setDvcConnectionFromForm(null)
    }, [resetAddConnection, resetAddDvcConnection])

    const onCancel = () => {
        setAddDialogOpened(false)
        onModalClose()
    }

    const onSubmit = (
        payload: AddDataRepository & { dvcConnectionId: string | null }
    ) => {
        const {
            connectionId: _connectionId,
            dvcConnectionId: _dvcConnectionId,
            ...rest
        } = payload

        mutate({
            projectId,
            data: rest,
        })
    }

    const getStatusMessages = () => {
        const messages: {
            title: string
            severity: 'success' | 'error'
            error?: string
        }[] = [
            {
                title: 'Data Repository',
                severity: isSuccessPostDataRepository ? 'success' : 'error',
                error: JSON.stringify(
                    createDataRepositoryError?.response?.data.detail
                ),
            },
        ]
        if (!isIdleAddConnection) {
            messages.push({
                title: 'Connection to the data repository',
                severity: addConnectionError ? 'error' : 'success',
                error: JSON.stringify(
                    addConnectionError?.response?.data.detail
                ),
            })
        }
        if (!isIdleAddDvcConnection) {
            messages.push({
                title: 'Dvc connection',
                severity: addDvcConnectionError ? 'error' : 'success',
                error: JSON.stringify(
                    addDvcConnectionError?.response?.data.detail
                ),
            })
        }
        return messages
    }

    const isLoading =
        isLoadingPostDataRepository ||
        isLoadingAddDvcConnection ||
        isLoadingAddConnection

    return (
        <>
            <SuccessDialog queryStatus={queryStatus} />
            {isSuccessDialogOpened && (
                <CreateDataStatusDialog
                    close={() => {
                        onModalClose()
                        setSuccessDialogOpened(false)
                    }}
                    isLoading={isLoading}
                    statusMessages={getStatusMessages()}
                />
            )}
            <AddResourceToolbar
                title="Data Repositories"
                onClick={() => {
                    setAddDialogOpened(true)
                }}
            />
            <AddRepositoryDialog
                open={isAddDialogOpened}
                onCancel={onCancel}
                onSubmit={onSubmit}
                dataRepositoryConnectionFromForm={
                    dataRepositoryConnectionFromForm
                }
                setDataRepositoryConnectionFromForm={
                    setDataRepositoryConnectionFromForm
                }
                dvcConnectionFromForm={dvcConnectionFromForm}
                setDvcConnectionFromForm={setDvcConnectionFromForm}
            />
            <Spacing value={theme.spacing(1)} />
            <Box sx={{ display: 'flex', gap: 1, m: 2 }}>
                <Info />
                <Typography>
                    {strings.DataRepositories.externalDataTransferInfo}
                    <ExternalLink {...urls.externalDataTransferDocs} />
                </Typography>
            </Box>
            <Grid container spacing={4} mt={0}>
                {repos.map((data) => (
                    <DataRepositoryCard
                        key={data.dataRepositoryId}
                        initialData={data}
                        id={data.dataRepositoryId}
                        parentQueryKey={queryKey}
                        setStatus={setQueryStatus}
                    />
                ))}
            </Grid>
        </>
    )
}
