import { IconButton, Tooltip } from '@mui/material'
import {
    ActionableResource,
    colorOverride,
    getMenubarActions,
    iconForAction,
    hasPermission,
    tooltipForAction,
    menubarActions,
} from '../actions'
import { FC } from 'react'
import { usePermission } from '../../../../context/ProjectContext'
import { SickSharp } from '@mui/icons-material'

type MenuBarActions = (typeof menubarActions)['DataRepository'][number]
type MenuDispatch = (action: { type: MenuBarActions }) => void

interface ActionMenubarItemProps {
    action: MenuBarActions
    dispatch: MenuDispatch
    title?: string
    disabled?: boolean
    rbacOverride?: boolean
}
export const ActionMenubarItem: FC<ActionMenubarItemProps> = ({
    action,
    dispatch,
    title = tooltipForAction[action] ?? 'TOOLTIP MISSING',
    disabled = false,
    rbacOverride = false,
}) => {
    const Icon = iconForAction[action] ?? (() => <SickSharp />)
    const role = usePermission()
    const forbidden = !(rbacOverride || hasPermission(role, action))
    const color = colorOverride[action] ?? 'primary'
    const toggle = () => {
        dispatch({ type: action })
    }

    return (
        <Tooltip title={title} color={'primary'} placement="top">
            <span>
                <IconButton
                    size="small"
                    color={color}
                    onClick={toggle}
                    disabled={disabled || forbidden}
                    title={title}
                >
                    <Icon />
                </IconButton>
            </span>
        </Tooltip>
    )
}

export const getDefaultMenubar = (
    resource: ActionableResource,
    dispatch: MenuDispatch
) => {
    return getMenubarActions(resource).map((action, key) => (
        <span key={key}>
            <ActionMenubarItem action={action} dispatch={dispatch} />
        </span>
    ))
}
