import { QueryKey } from '@tanstack/react-query'
import { IconButtonProps, SvgIconOwnProps } from '@mui/material'
import {
    Add,
    DeleteOutline,
    EditOutlined,
    VisibilityOutlined,
} from '@mui/icons-material'
import { DataRepository } from '../../../../queries/models'
import { SemanticUserRole } from '../../../../utils/userRole'
import { StatusForAction } from '../DataRepositoriesOverview'

export type ActionableResource = DataRepository

export const isDataRepository = (
    data: ActionableResource
): data is DataRepository => {
    return (data as DataRepository) !== undefined
}

// I don't think there's a better way to (currently) do this in TypeScript (as of 4.7)
export const nameOf = (resource: ActionableResource): string => {
    if (isDataRepository(resource)) {
        return 'DataRepository'
    } else {
        return 'Unknown'
    }
}

export type SubResource<T> = {
    id: string
    initialData: T
    parentQueryKey: QueryKey
    setStatus?: (status: StatusForAction) => void
}

type CREATE = 'CREATE'
type READ = 'READ'
type UPDATE = 'UPDATE'
type DELETE = 'DELETE'
type UNUSED_ACTION = 'UNUSED_ACTION'

export type Action = CREATE | READ | UPDATE | DELETE | UNUSED_ACTION

export type ActionIcon = { [T in Action]: React.FC<SvgIconOwnProps> }

export const iconForAction: ActionIcon = {
    CREATE: () => <Add fontSize="small" />,
    READ: () => <VisibilityOutlined fontSize="small" />,
    UPDATE: () => <EditOutlined fontSize="small" />,
    DELETE: () => <DeleteOutline fontSize="small" />,
    UNUSED_ACTION: undefined as never,
}

export const tooltipForAction: Partial<Record<Action, string>> = {
    CREATE: 'Add',
    READ: 'Details',
    UPDATE: 'Edit',
    DELETE: 'Delete',
}

export const colorOverride: Record<string, IconButtonProps['color']> = {
    DELETE: 'error',
}

export type ActionableResourceName = 'DataRepository' | 'Unknown'

export type ActionMap = { [T in ActionableResourceName]: Action[] }

export const actionMap: ActionMap = {
    DataRepository: ['CREATE', 'READ', 'UPDATE', 'DELETE'],
    Unknown: [],
}

export const menubarActions = {
    DataRepository: ['READ', 'UPDATE', 'DELETE'],
} as const

export const getActions = (data: ActionableResource): Action[] => {
    const resourceName = nameOf(data) ?? 'Unknown'
    return actionMap[resourceName as ActionableResourceName]
}

export const getMenubarActions = (data: ActionableResource) => {
    if (isDataRepository(data)) {
        return menubarActions.DataRepository
    }
    return []
}

// This is an implementation of the business logic defined here:
// https://mantik-ai.gitlab.io/mantik/user_ui/roles_permissions.html#project-roles-and-permission
// Written in this style to make it obvious at a glance whether the logic is correct
const YES = true
const ___ = false

// prettier-ignore
export const permissionTable = [
  ['Action'       , 'VISITOR', 'GUEST', 'REPORTER', 'RESEARCHER', 'MAINTAINER', 'OWNER'],
  ['READ'         ,    YES   ,   YES  ,    YES    ,    YES      ,    YES      ,   YES  ], 
  ['CREATE'       ,    ___   ,   ___  ,    ___    ,    YES      ,    YES      ,   YES  ], 
  ['UPDATE'       ,    ___   ,   ___  ,    ___    ,    YES      ,    YES      ,   YES  ], 
  ['DELETE'       ,    ___   ,   ___  ,    ___    ,    YES      ,    YES      ,   YES  ], 
  ['UNUSED_ACTION',    ___   ,   ___  ,    ___    ,    ___      ,    ___      ,   ___  ] // Here for test purposes
]

export const hasPermission = (
    role: SemanticUserRole,
    action: Action
): boolean => {
    try {
        const roleColumn = permissionTable[0].indexOf(role)
        const actionRow = permissionTable.map((r) => r[0]).indexOf(action)
        return permissionTable[actionRow][roleColumn] as boolean
    } catch (error) {
        console.error(
            `Error  looking up permission to ${action} when user is ${role}: ${error}`
        )
        // I return true here because the general case is probably an arbitrary action that doesn't exist in the table but the developer has defined
        return true
    }
}
