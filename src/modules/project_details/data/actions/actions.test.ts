import { Action, hasPermission } from './'

describe('hasPermission', () => {
    it('should return true for VISITOR and READ action', () => {
        expect(hasPermission('VISITOR', 'READ')).toBe(true)
    })

    it('should return false for VISITOR and CREATE action', () => {
        expect(hasPermission('VISITOR', 'CREATE')).toBe(false)
    })

    it('should return true for GUEST and READ action', () => {
        expect(hasPermission('GUEST', 'READ')).toBe(true)
    })

    it('should return false for GUEST and CREATE action', () => {
        expect(hasPermission('GUEST', 'CREATE')).toBe(false)
    })

    it('should return true for MAINTAINER and CREATE action', () => {
        expect(hasPermission('MAINTAINER', 'CREATE')).toBe(true)
    })

    it('should return true for MAINTAINER and DELETE action', () => {
        expect(hasPermission('MAINTAINER', 'DELETE')).toBe(true)
    })

    it('should return true for OWNER and all actions', () => {
        const actions: Action[] = ['READ', 'CREATE', 'UPDATE', 'DELETE']
        actions.forEach((action) => {
            expect(hasPermission('OWNER', action)).toBe(true)
        })
    })
})
