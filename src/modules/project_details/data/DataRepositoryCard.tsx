import {
    Box,
    Card,
    CardActions,
    CardContent,
    CardHeader,
    Chip,
    Grid,
    Typography,
} from '@mui/material'
import { FC, useCallback, useEffect, useState } from 'react'
import { useQueryClient } from '@tanstack/react-query'
import { TimestampCaption } from '../../../components/TimestampCaption'
import { useProject } from '../../../context/ProjectContext'
import {
    DataRepository,
    UpdateDataRepository,
    useProjectsProjectIdDataDataRepositoryIdConnectionProjectsProjectIdDataDataRepositoryIdConnectionPost as useAddConnection,
    useProjectsProjectIdDataDataRepositoryIdConnectionIdDeleteProjectsProjectIdDataDataRepositoryIdConnectionConnectionIdDelete as useDeleteConnection,
    useProjectsProjectIdDataDataRepositoryIdDeleteProjectsProjectIdDataDataRepositoryIdDelete as useDeleteData,
    useProjectsProjectIdDataDataRepositoryIdGetProjectsProjectIdDataDataRepositoryIdGet as useGetDataRepositoryData,
    useProjectsProjectIdDataDataRepositoryIdPutProjectsProjectIdDataDataRepositoryIdPut as useUpdateDataRepository,
} from '../../../queries'
import { SubResource } from './actions'
import { DeleteRepositoryDialog } from './Dialogs/DeleteRepositoryDialog'
import { DetailsViewDialog } from './Dialogs/DetailsViewDialog'
import { EditRepositoryDialog } from './Dialogs/EditRepositoryDialog'
import { getDefaultMenubar } from './menubar/ResourceMenubar'
import { CreateDataStatusDialog } from './Dialogs/CreateDataRepositoryStatusDialog'

function DataRepositoryLabels({
    labels,
}: {
    labels: DataRepository['labels']
}) {
    if (!labels) {
        return null
    }

    return (
        <CardActions>
            <Box>
                {labels?.map((label, idx) => (
                    <Chip
                        key={idx}
                        label={label.name}
                        style={{ margin: '2px' }}
                    />
                ))}
            </Box>
        </CardActions>
    )
}

const DataRepositoryCard: FC<SubResource<DataRepository>> = ({
    initialData,
    id: dataRepositoryId,
    parentQueryKey,
    setStatus,
}) => {
    const [isDeleteDialogOpened, setDeleteDialogOpened] = useState(false)
    const [isReadDialogOpened, setReadDialogOpened] = useState(false)
    const [isSuccessDialogOpened, setSuccessDialogOpened] = useState(false)
    const [isUpdateDialogOpened, setUpdateDialogOpened] = useState(false)
    const { projectId } = useProject()
    const queryClient = useQueryClient()

    const { data, queryKey } = useGetDataRepositoryData(
        projectId,
        dataRepositoryId
    )

    const { mutate: deleteDataRepository, status: deleteStatus } =
        useDeleteData()
    const { mutateAsync: deleteConnection } = useDeleteConnection()
    const { mutateAsync: addConnection } = useAddConnection()

    const resource = data ?? initialData

    const [currentConnection, setCurrentConnection] = useState<{
        connectionId: string
    } | null>(null)
    const [dvcConnectionFromForm, setDvcConnectionFromForm] = useState<{
        connectionId: string
    } | null>(null)

    const {
        mutate: updateDataRepository,
        isSuccess: isSuccessUpdateDataRepository,
        error: updateDataRepositoryError,
        reset,
    } = useUpdateDataRepository({
        mutation: {
            onSuccess: async (_, variables) => {
                await handleUpdateConnection()
                await handleUpdateDvcConnection(variables.data)
                setUpdateDialogOpened(false)
                onModalClose()
                setSuccessDialogOpened(true)
            },
        },
    })

    useEffect(() => {
        if (deleteStatus !== 'idle') {
            setStatus?.({ status: deleteStatus, type: 'DELETE' })
        }
    }, [deleteStatus, setStatus])

    const onSuccess = useCallback(async () => {
        await queryClient.invalidateQueries({ queryKey })
        await queryClient.invalidateQueries({ queryKey: parentQueryKey })
    }, [queryClient, queryKey, parentQueryKey])

    useEffect(() => {
        setCurrentConnection(
            resource.connectionId
                ? { connectionId: resource.connectionId }
                : null
        )
        setDvcConnectionFromForm(
            resource.dvcConnectionId
                ? { connectionId: resource.dvcConnectionId }
                : null
        )
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [data, isUpdateDialogOpened])

    const handleUpdateConnection = async () => {
        if (resource.connectionId === currentConnection?.connectionId) {
            return
        }

        if (resource.connectionId) {
            await deleteConnection({
                projectId,
                dataRepositoryId,
                connectionId: resource.connectionId,
            })
        }

        if (currentConnection) {
            await addConnection({
                projectId,
                dataRepositoryId,
                data: {
                    connectionId: currentConnection.connectionId,
                },
            })
        }
    }

    const handleUpdateDvcConnection = async (
        variables: UpdateDataRepository
    ) => {
        if (!variables.isDvcEnabled) {
            if (resource?.dvcConnectionId) {
                await deleteConnection({
                    projectId,
                    dataRepositoryId,
                    connectionId: resource.dvcConnectionId,
                })
            }
            return
        }

        if (resource?.dvcConnectionId === dvcConnectionFromForm?.connectionId) {
            return
        }

        if (resource?.dvcConnectionId) {
            await deleteConnection({
                projectId,
                dataRepositoryId,
                connectionId: resource.dvcConnectionId,
            })
        }
        if (dvcConnectionFromForm?.connectionId) {
            await addConnection({
                projectId,
                dataRepositoryId,
                data: {
                    connectionId: dvcConnectionFromForm.connectionId,
                },
            })
        }
    }

    const onModalClose = useCallback(() => {
        setCurrentConnection(null)
        setDvcConnectionFromForm(null)
    }, [])

    const onCancel = () => {
        setUpdateDialogOpened(false)
        onModalClose()
        reset()
    }

    const onSubmit = ({
        dvcConnectionId: _dvcConnectionId,
        connectionId: _connectionId,
        ...payload
    }: UpdateDataRepository & {
        dvcConnectionId: string | undefined
        connectionId: string | undefined
    }) => {
        updateDataRepository(
            {
                projectId,
                dataRepositoryId,
                data: payload,
            },
            {
                onSuccess,
                async onError() {
                    await queryClient.invalidateQueries({ queryKey })
                },
            }
        )
    }

    return (
        <>
            {isSuccessDialogOpened && (
                <CreateDataStatusDialog
                    close={() => setSuccessDialogOpened(false)}
                    isLoading={!isSuccessUpdateDataRepository}
                    statusMessages={[
                        {
                            title: 'Data Repository',
                            severity: 'success',
                            action: 'update',
                        },
                    ]}
                />
            )}
            <Grid item xs={12} md={6} lg={4} xl={3}>
                <DetailsViewDialog
                    data={resource}
                    onClose={() => setReadDialogOpened(false)}
                    open={isReadDialogOpened}
                />
                <DeleteRepositoryDialog
                    open={isDeleteDialogOpened}
                    onCancel={() => setDeleteDialogOpened(false)}
                    onSubmit={() => {
                        deleteDataRepository(
                            { projectId, dataRepositoryId },
                            { onSuccess }
                        )
                        setDeleteDialogOpened(false)
                        setStatus?.({
                            status: 'pending',
                            type: 'DELETE',
                        })
                    }}
                    status={{
                        type: deleteStatus,
                        status:
                            deleteStatus === 'success' ||
                            deleteStatus === 'error',
                        text: 'None',
                    }}
                    data={resource}
                />
                {isUpdateDialogOpened && (
                    <EditRepositoryDialog
                        data={resource}
                        currentConnection={currentConnection}
                        setCurrentConnection={setCurrentConnection}
                        currentDvcConnection={dvcConnectionFromForm}
                        setCurrentDvcConnection={setDvcConnectionFromForm}
                        updateDataRepositoryError={updateDataRepositoryError}
                        onCancel={onCancel}
                        onSubmit={onSubmit}
                    />
                )}

                <Card
                    sx={{
                        width: '100%',
                        minHeight: '300px',
                        height: '100%',
                    }}
                >
                    <CardHeader
                        action={getDefaultMenubar(resource, ({ type }) => {
                            switch (type) {
                                case 'READ':
                                    setReadDialogOpened(true)
                                    break
                                case 'UPDATE':
                                    setUpdateDialogOpened(true)
                                    break
                                case 'DELETE':
                                    setDeleteDialogOpened(true)
                                    break
                            }
                        })}
                    />
                    <CardContent data-testid="card-content">
                        <Typography
                            variant="h6"
                            gutterBottom
                            fontWeight={600}
                            sx={{
                                whiteSpace: 'normal',
                                overflow: 'hidden',
                                textOverflow: 'ellipsis',
                                wordWrap: 'break-word',
                                width: '100%',
                            }}
                        >
                            {resource?.dataRepositoryName ??
                                resource?.dataRepositoryId}
                        </Typography>
                        <Typography variant="body2" color="text.secondary">
                            {resource?.description}
                        </Typography>
                    </CardContent>
                    <DataRepositoryLabels labels={resource.labels} />
                    <TimestampCaption
                        timestamp={resource?.createdAt}
                        displayName={'Created At'}
                    />
                </Card>
            </Grid>
        </>
    )
}

export { DataRepositoryCard }
