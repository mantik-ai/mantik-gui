import { Alert, Button, Dialog, DialogContent } from '@mui/material'
import { Spacing } from '../../../../components/Spacing'
import { useEffect, useState } from 'react'
import { strings } from '../../../../../public/locales/en/translation'
import { StatusForAction } from '../DataRepositoriesOverview'

type MessageForResult = ({ status, type }: StatusForAction) => string
const messageForResult: MessageForResult = ({ status, type }) => {
    if (status === 'success')
        switch (type) {
            case 'CREATE':
                return strings['DataRepositories'].createSuccess
            case 'DELETE':
                return strings['DataRepositories'].deleteSuccess
            case 'UPDATE':
                return strings['DataRepositories'].updateSuccess
            default:
                throw new Error('Unknown action type')
        }
    // Either there's an error or there's an error
    return strings['DataRepositories'].error
}

export type SideEffectResult = {
    queryStatus: StatusForAction
}

export const SuccessDialog: React.FC<SideEffectResult> = ({
    queryStatus: { status, type },
}) => {
    const [open, setOpen] = useState(false)
    const handleClose = () => setOpen(false)
    const statusMessage = messageForResult({ status, type } as StatusForAction)
    const statusSeverity = status === 'error' ? 'error' : 'success'

    useEffect(() => {
        if (status === 'pending') setOpen(true)
    }, [status, type])

    return (
        <Dialog fullWidth maxWidth="sm" open={open} onClose={handleClose}>
            <DialogContent>
                <Alert variant="outlined" severity={statusSeverity}>
                    {statusMessage}
                </Alert>
            </DialogContent>
            <Spacing value="4em" />
            <Button
                style={{ position: 'absolute', right: 10, bottom: 10 }}
                onClick={handleClose}
            >
                Close
            </Button>
        </Dialog>
    )
}
