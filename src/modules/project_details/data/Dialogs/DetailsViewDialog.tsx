import CopyToClipboard from '../../../../components/CopyToClipboard'
import { DetailsDialogLayout } from '../../../../components/DetailsDialogLayout'
import { DetailsDialogList } from '../../../../components/DetailsDialogList'
import { CustomCellProps } from '../../../../components/TablesLayout/CustomCell'
import { toTitleCase } from '../../../../helpers'
import { DataRepository, Label } from '../../../../queries'
import { ComponentProps } from 'react'

type DataRepositoryValueTypes = DataRepository[keyof DataRepository]

export function transformData({
    versions: _versions,
    ...data
}: DataRepository) {
    const transformedData = Object.fromEntries(
        Object.entries(data).map(
            ([key, value]: [string, DataRepositoryValueTypes]): [
                string,
                CustomCellProps,
            ] => {
                const typedKey = key as keyof DataRepository
                const newKey = toTitleCase(typedKey)

                if (value === null || value === undefined) {
                    return [newKey, { data: null }]
                } else if (
                    typedKey === 'connectionId' ||
                    typedKey === 'dvcConnectionId' ||
                    typedKey === 'dataRepositoryId'
                ) {
                    return [
                        newKey,
                        {
                            data: String(value),
                            component: (
                                <CopyToClipboard
                                    title={String(value)}
                                    data={String(value)}
                                />
                            ),
                        },
                    ]
                } else if (typedKey === 'labels') {
                    return [
                        newKey,
                        { data: { data: value as Label[], type: 'labels' } },
                    ]
                } else if (typeof value === 'boolean') {
                    return [newKey, { data: value ? 'Yes' : 'No' }]
                }

                return [newKey, { data: String(value) }]
            }
        )
    )

    return transformedData
}

export const DetailsViewDialog = ({
    open,
    data,
    onClose,
}: {
    open: boolean
    onClose: () => void
    data: DataRepository
}) => {
    const transformedData = transformData(data)

    return (
        <DetailsDialogLayout
            open={open}
            setOpen={onClose}
            title={'Details'}
            toggleWidth
        >
            {/* note: type of DetailsDialogList is broken and is complicated to fix. Refactoring stops here */}
            <DetailsDialogList
                data={
                    transformedData as ComponentProps<
                        typeof DetailsDialogList
                    >['data']
                }
            />
        </DetailsDialogLayout>
    )
}
