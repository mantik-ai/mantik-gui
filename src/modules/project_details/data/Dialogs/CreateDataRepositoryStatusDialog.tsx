import {
    Alert,
    AlertTitle,
    Button,
    CircularProgress,
    Container,
    Dialog,
    DialogContent,
    Typography,
} from '@mui/material'
import { Spacing } from '../../../../components/Spacing'

export const CreateDataStatusDialog = ({
    close,
    statusMessages,
    isLoading,
}: {
    close: () => void
    statusMessages: {
        title: string
        severity: 'success' | 'error'
        error?: string
        action?: 'create' | 'update'
    }[]
    isLoading?: boolean
}) => {
    return (
        <Dialog fullWidth maxWidth="sm" open onClose={close}>
            <DialogContent>
                {isLoading ? (
                    <Container
                        sx={{
                            justifyContent: 'center',
                            display: 'flex',
                            marginTop: '1em',
                        }}
                    >
                        <CircularProgress />
                    </Container>
                ) : (
                    statusMessages.map((message, index) => {
                        return (
                            <Alert
                                variant="outlined"
                                severity={message.severity}
                                key={index}
                            >
                                <AlertTitle>{message.title}</AlertTitle>
                                {message.severity === 'success'
                                    ? `Was ${
                                          message.action ?? 'create'
                                      }d successfully!`
                                    : `Failed to be ${
                                          message.action ?? 'create'
                                      }d`}

                                {message.error && (
                                    <Typography
                                        variant="caption"
                                        gutterBottom
                                        sx={{ display: 'block' }}
                                    >
                                        {message.error}
                                    </Typography>
                                )}
                            </Alert>
                        )
                    })
                )}
            </DialogContent>
            <Spacing value="4em" />
            <Button
                style={{ position: 'absolute', right: 10, bottom: 10 }}
                onClick={close}
            >
                Close
            </Button>
        </Dialog>
    )
}
