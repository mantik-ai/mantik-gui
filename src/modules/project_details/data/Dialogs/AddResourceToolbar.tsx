import { FC } from 'react'
import { usePermission } from '../../../../context/ProjectContext'
import { DetailsToolbar } from '../../overview/ProjectDetailsToolbar'
import { hasPermission } from '../actions'
import { Button, Tooltip } from '@mui/material'
import { Add } from '@mui/icons-material'
import { strings } from '../../../../../public/locales/en/translation'

const { tooltip, tooltipDisabled } = strings['DataRepositories']

export const AddResourceToolbar: FC<{
    title: string
    onClick: () => void
}> = ({ onClick, title }) => {
    const role = usePermission()
    const canCreate = hasPermission(role, 'CREATE')
    const toolTip = canCreate ? tooltip : tooltipDisabled
    return (
        <DetailsToolbar
            title={title}
            tool={
                <Tooltip title={toolTip} color={'primary'} placement="top">
                    <span>
                        <Button
                            disabled={!canCreate}
                            variant="text"
                            onClick={onClick}
                        >
                            <Add />
                            Add
                        </Button>
                    </span>
                </Tooltip>
            }
        />
    )
}
