import { FormEvent, useReducer, useState } from 'react'
import {
    Button,
    Container,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    FormHelperText,
    Typography,
    useTheme,
} from '@mui/material'
import {
    defaultValidator,
    DisplayName,
    FormAction,
    FormState,
    formReducer,
} from '../../../../utils/forms'
import {
    BasicCheckbox,
    BasicSelector,
    BasicTextField,
} from '../../../../components/forms/BasicFields'
import {
    AddDataRepository,
    Connection,
    Label,
    Platform,
} from '../../../../queries'
import { LabelSelector } from '../../../projects_overview/components/LabelSelector'
import { Spacing } from '../../../../components/Spacing'
import { ConnectionSelection } from '../../../../components/forms/ConnectionSelection'

interface AddFormState extends FormState {
    dataRepositoryName: string
    description: string
    uri: string
    platform: Platform
    isDvcEnabled?: boolean
    connectionId?: string | null
    dvcConnectionId?: string | null
    labels: Label[]
}

const initialFormState: AddFormState = {
    dataRepositoryName: '',
    description: '',
    uri: '',
    platform: 'GitHub',
    isDvcEnabled: false,
    connectionId: null,
    labels: [],
}

const displayNames: DisplayName<AddFormState> = {
    dataRepositoryName: 'Name',
    description: 'Description',
    uri: 'Data Repository URL',
    platform: 'Platform',
    isDvcEnabled: 'Is DVC',
    labels: 'Labels',
}

const requiredFields: (keyof AddFormState)[] = [
    'dataRepositoryName',
    'uri',
    'platform',
]

// POST request expects labels as string[] of label IDs
const transformLabels = (labels: Label[]): string[] =>
    labels.map(({ labelId }) => labelId)

//  Run the basic dispatch function with initialFormState to reset the form
const clearState = (dispatch: (action: FormAction<AddFormState>) => void) =>
    Object.entries(initialFormState).map(
        ([target, payload]) => dispatch({ target, payload }) // eslint-disable-line @typescript-eslint/no-unsafe-assignment
    )

export const AddRepositoryDialog = ({
    open,
    onCancel,
    onSubmit,
    dataRepositoryConnectionFromForm,
    setDataRepositoryConnectionFromForm,
    dvcConnectionFromForm,
    setDvcConnectionFromForm,
}: {
    onSubmit: (
        payload: AddDataRepository & { dvcConnectionId: string | null }
    ) => void
    onCancel: () => void
    open: boolean
    dataRepositoryConnectionFromForm: Connection | null
    setDataRepositoryConnectionFromForm: (connection: Connection | null) => void
    dvcConnectionFromForm: Connection | null
    setDvcConnectionFromForm: (connection: Connection | null) => void
}) => {
    const theme = useTheme()
    const [userInteraction, setUserInteraction] = useState(false)

    const handleClose = () => {
        onCancel()
        resetState()
    }

    const handleSubmit = (payload: AddFormState) => {
        const labels = transformLabels(payload.labels)
        onSubmit({
            ...payload,
            connectionId: payload.connectionId ?? undefined,
            dvcConnectionId: payload.dvcConnectionId ?? null,
            labels,
        })
        resetState()
    }

    const [state, dispatch] = useReducer(formReducer<AddFormState>, {
        ...initialFormState,
    }) as [AddFormState, (action: FormAction<AddFormState>) => void]

    const resetState = () => {
        clearState(dispatch)
        setUserInteraction(false)
    }

    // Convenience function to simplify forms for simplest cases
    const formProps = (target: keyof AddFormState) => ({
        state,
        dispatch,
        target,
        label: displayNames[target],
        required: requiredFields.includes(target),
    })

    const validator = defaultValidator<AddFormState>

    const validate = () =>
        validator({
            state,
            requiredFields,
        })

    return (
        <Dialog
            onKeyDown={(_) => {
                setUserInteraction(true)
            }}
            open={open}
            onClose={handleClose} // this is needed so onBlur close works
            PaperProps={{
                sx: {
                    minHeight: '80vh',
                    minWidth: '50vw',
                },
                component: 'form',
                onSubmit: (event: FormEvent<HTMLFormElement>) => {
                    event.preventDefault()
                    handleSubmit(state)
                },
            }}
        >
            <DialogTitle>Add Data Repository</DialogTitle>
            <DialogContent>
                <BasicTextField {...formProps('dataRepositoryName')} />
                <BasicTextField {...formProps('uri')} />
                <BasicTextField {...formProps('description')} />
                <LabelSelector
                    currentLabels={state['labels']}
                    handleLabelSelect={(labels) =>
                        dispatch({ target: 'labels', payload: labels })
                    }
                />
                <BasicSelector
                    options={Object.values(Platform)}
                    {...formProps('platform')}
                    onChange={(e) => {
                        dispatch({
                            target: 'platform',
                            payload: e.target.value,
                        })
                        setDataRepositoryConnectionFromForm(null)
                    }}
                />
                <ConnectionSelection
                    currentConnection={dataRepositoryConnectionFromForm || null}
                    setCurrentConnection={setDataRepositoryConnectionFromForm}
                    isRequired={false}
                    platform={state.platform}
                />
                <Spacing value={theme.spacing(0)} />
                <Container disableGutters>
                    {state && <BasicCheckbox {...formProps('isDvcEnabled')} />}
                    {state?.isDvcEnabled && (
                        <>
                            <Typography sx={{ marginBottom: 2 }}>
                                Data Source
                            </Typography>
                            <ConnectionSelection
                                currentConnection={
                                    dvcConnectionFromForm || null
                                }
                                setCurrentConnection={setDvcConnectionFromForm}
                                isRequired={false}
                                platform={'S3'}
                            />
                        </>
                    )}
                </Container>
            </DialogContent>
            <DialogContent>
                {userInteraction && !validate() && (
                    <FormHelperText error>
                        Please ensure that all fields with * are filled out.
                    </FormHelperText>
                )}
            </DialogContent>
            <DialogActions>
                <Button onClick={handleClose}>Cancel</Button>
                <Button type="submit" disabled={userInteraction && !validate()}>
                    Create
                </Button>
            </DialogActions>
        </Dialog>
    )
}
