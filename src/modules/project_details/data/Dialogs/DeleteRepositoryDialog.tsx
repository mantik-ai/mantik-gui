import { DeleteDialogLayout } from '../../../../components/DeleteDialogLayout'
import { DataRepository } from '../../../../queries'

const strings = {
    deleteRepositoryDialog: {
        title: 'Delete Data Repository',
        text: 'Are you sure you want to delete data repository',
        warning: 'This action cannot be undone',
    },
}

export const DeleteRepositoryDialog = ({
    data,
    open,
    onCancel,
    onSubmit,
    status,
}: {
    data: DataRepository
    open: boolean
    onCancel: () => void
    onSubmit: () => void
    status: {
        type: 'idle' | 'success' | 'error' | 'pending'
        status: boolean
        text: 'None'
    }
}) => {
    return (
        <DeleteDialogLayout
            open={open}
            title={strings.deleteRepositoryDialog.title}
            handleClose={onCancel}
            handleSubmit={onSubmit}
            text={`${strings.deleteRepositoryDialog.text} ${data.dataRepositoryId}`}
            warning={strings.deleteRepositoryDialog.warning}
            isLoading={false}
            status={status}
        />
    )
}
