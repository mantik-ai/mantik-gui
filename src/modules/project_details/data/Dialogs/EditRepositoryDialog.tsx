import {
    Alert,
    Button,
    Container,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    Typography,
} from '@mui/material'
import { FormEvent, useEffect, useReducer, useState } from 'react'
import { Spacing } from '../../../../components/Spacing'
import {
    BasicCheckbox,
    BasicSelector,
    BasicTextField,
} from '../../../../components/forms/BasicFields'
import { ConnectionSelection } from '../../../../components/forms/ConnectionSelection'
import {
    DataRepository,
    HTTPError,
    HTTPValidationError,
    Label,
    Platform,
    UpdateDataRepository,
} from '../../../../queries'
import {
    defaultValidator,
    DisplayName,
    FormAction,
    formReducer,
    FormState,
} from '../../../../utils/forms'
import { ErrorType } from '../../../auth/mantikApi'
import { LabelSelector } from '../../../projects_overview/components/LabelSelector'
import { isArray } from 'lodash'

interface EditFormState extends FormState {
    dataRepositoryName: string
    description: string
    uri: string
    platform: Platform
    isDvcEnabled?: boolean
    connectionId?: string | null
    dvcConnectionId?: string | null
    labels: Label[]
}

const displayNames: DisplayName<EditFormState> = {
    dataRepositoryName: 'Name',
    description: 'Description',
    uri: 'Data Repository URL',
    platform: 'Platform',
    isDvcEnabled: 'Is DVC',
    labels: 'Labels',
}

const requiredFields: (keyof EditFormState)[] = [
    'dataRepositoryName',
    'uri',
    'platform',
]

// POST request expects labels as string[] of label IDs
const transformLabels = (labels: Label[]): string[] =>
    labels?.map(({ labelId }) => labelId)

const GENERIC_UPDATE_ERROR =
    'An error happened while updating the data repository.'
function ErrorBox({
    errors,
}: {
    errors: ErrorType<HTTPValidationError | HTTPError>
}) {
    const detail = errors.response?.data?.detail
    if (typeof detail === 'string') {
        return (
            <DialogContent>
                <Alert variant="outlined" severity={'error'}>
                    {detail}
                </Alert>
            </DialogContent>
        )
    }
    if (isArray(detail)) {
        return (
            <DialogContent>
                <Alert variant="outlined" severity={'error'}>
                    <ul>
                        {detail.map((error, index) => {
                            return (
                                <li key={`update-error-${index}`}>
                                    {error.msg}
                                </li>
                            )
                        })}
                    </ul>
                </Alert>
            </DialogContent>
        )
    }

    return (
        <DialogContent>
            <Alert variant="outlined" severity={'error'}>
                {GENERIC_UPDATE_ERROR}
            </Alert>
        </DialogContent>
    )
}

export const EditRepositoryDialog = ({
    data,
    setCurrentConnection,
    setCurrentDvcConnection,
    currentConnection,
    currentDvcConnection,
    updateDataRepositoryError,
    onSubmit,
    onCancel,
}: {
    data: DataRepository
    setCurrentConnection: (
        currentConnection: { connectionId: string } | null
    ) => void
    currentConnection: { connectionId: string } | null
    setCurrentDvcConnection: (
        currentConnection: { connectionId: string } | null
    ) => void
    currentDvcConnection: { connectionId: string } | null
    updateDataRepositoryError: ErrorType<HTTPValidationError | HTTPError> | null
    onSubmit: (
        payload: UpdateDataRepository & {
            dvcConnectionId: string | undefined
            connectionId: string | undefined
        }
    ) => void
    onCancel: () => void
}) => {
    const [userInteraction, setUserInteraction] = useState(false)

    const handleClose = () => {
        onCancel()
        resetForm()
    }

    const handleSubmit = (payload: EditFormState) => {
        const labels = transformLabels(payload.labels)

        onSubmit({
            ...payload,
            labels,
            dvcConnectionId: payload.dvcConnectionId ?? undefined,
            connectionId: payload.connectionId ?? undefined,
        })

        resetForm()
    }

    const formState = data as EditFormState

    const [state, dispatch] = useReducer(
        formReducer<EditFormState>,
        formState
    ) as [EditFormState, (action: FormAction<EditFormState>) => void]

    const resetForm = () => {
        dispatch({ target: '@reset', payload: data })
        setUserInteraction(false)
    }

    useEffect(() => {
        dispatch({ target: '@reset', payload: data })
    }, [data])

    // Convenience function to simplify forms for simplest cases
    const formProps = (target: keyof EditFormState) => ({
        state,
        dispatch,
        target,
        label: displayNames[target],
        required: requiredFields.includes(target),
    })

    const validator = defaultValidator<EditFormState>

    const validate = () =>
        !userInteraction ||
        validator({
            state,
            requiredFields,
        })

    return (
        <Dialog
            onKeyDown={(_) => {
                setUserInteraction(true)
            }}
            open
            onClose={handleClose} // this is needed so onBlur close works
            PaperProps={{
                sx: {
                    minHeight: '80vh',
                    minWidth: '50vw',
                },
                component: 'form',
                onSubmit: (event: FormEvent<HTMLFormElement>) => {
                    event.preventDefault()
                    handleSubmit(state)
                },
            }}
        >
            <DialogTitle>Edit Data Repository</DialogTitle>
            {updateDataRepositoryError && (
                <ErrorBox errors={updateDataRepositoryError} />
            )}

            <DialogContent>
                <BasicTextField {...formProps('dataRepositoryName')} />
                <BasicTextField {...formProps('uri')} />
                <BasicTextField {...formProps('description')} />
                <LabelSelector
                    currentLabels={state?.['labels']}
                    handleLabelSelect={(labels) =>
                        dispatch({ target: 'labels', payload: labels })
                    }
                />
                <BasicSelector
                    options={Object.values(Platform)}
                    {...formProps('platform')}
                    onChange={(e) => {
                        dispatch({
                            target: 'platform',
                            payload: e.target.value,
                        })
                        setCurrentConnection(null)
                    }}
                />
                <ConnectionSelection
                    currentConnection={currentConnection}
                    setCurrentConnection={setCurrentConnection}
                    isRequired={false}
                    platform={state.platform}
                />
                <Container disableGutters>
                    {state && <BasicCheckbox {...formProps('isDvcEnabled')} />}
                    {state?.isDvcEnabled && (
                        <>
                            <Typography sx={{ marginBottom: 2 }}>
                                Data Source
                            </Typography>
                            <ConnectionSelection
                                currentConnection={currentDvcConnection}
                                setCurrentConnection={setCurrentDvcConnection}
                                isRequired={false}
                                platform={'S3'}
                            />
                        </>
                    )}
                </Container>
            </DialogContent>
            <DialogContent>
                {userInteraction && !validate() && (
                    <>
                        <Spacing value={'1em'} />
                        <Typography variant="inherit" color="error">
                            {`Please ensure that all fields with * are filled out.`}
                        </Typography>
                    </>
                )}
            </DialogContent>
            <DialogActions>
                <Button onClick={handleClose}>Cancel</Button>
                <Button type="submit" disabled={userInteraction && !validate()}>
                    Update
                </Button>
            </DialogActions>
        </Dialog>
    )
}
