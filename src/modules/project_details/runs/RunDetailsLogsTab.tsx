import React, { useEffect, useRef } from 'react'
import { renderApiErrorMessage } from '../../../errors'
import InfoIcon from '@mui/icons-material/Info'
import { ButtonWithLoadingStatus } from '../../../components/ButtonWithLoadingStatus'
import { useProjectsProjectIdRunsRunIdLogsGetProjectsProjectIdRunsRunIdLogsGet } from '../../../queries'
import { RefreshButton } from '../../../components/RefreshButton'
import { useProject } from '../../../context/ProjectContext'
import { useQueryClient } from '@tanstack/react-query'
interface CustomCellProps {
    data?: string
    component?: React.ReactElement
}

export interface RunDetailsDialogLogsProps {
    data: { [key: string]: CustomCellProps }
    height: number | null
    minHeight: string
    resetScrollPosition: boolean
}
function RunDetailsLogsTab({
    data,
    height,
    minHeight,
    resetScrollPosition,
}: RunDetailsDialogLogsProps) {
    const queryClient = useQueryClient()
    const { projectId } = useProject()
    const logsContainerRef = useRef<HTMLDivElement | null>(null)
    const {
        data: logsData,
        isPending: isLoading,
        isError,
        isSuccess,
        error,
        queryKey,
    } = useProjectsProjectIdRunsRunIdLogsGetProjectsProjectIdRunsRunIdLogsGet(
        projectId,
        data?.ID?.data as string
    )

    useEffect(() => {
        if (logsContainerRef.current) {
            logsContainerRef.current.scrollTop = 0
            logsContainerRef.current.scrollLeft = 0
        }
    }, [resetScrollPosition])

    return (
        <div>
            <RefreshButton
                loading={isLoading}
                onClick={() => {
                    queryClient.invalidateQueries({ queryKey })
                }}
            />
            {isSuccess ? (
                <div
                    ref={logsContainerRef}
                    style={{
                        border: '1px solid #ccc',
                        backgroundColor: 'black',
                        color: 'white',
                        height:
                            height && minHeight
                                ? `max(${
                                      height - 50
                                  }px, calc(${minHeight} - 50px))`
                                : '100%',
                        width: '100%',
                        overflow: 'scroll',
                        padding: '0.9em',
                        whiteSpace: 'pre-wrap',
                        position: 'relative',
                        zIndex: 0,
                    }}
                >
                    {logsData.map((log, index) => (
                        <pre key={index}>{log}</pre>
                    ))}
                    <ButtonWithLoadingStatus loading={isLoading}>
                        {' '}
                    </ButtonWithLoadingStatus>
                </div>
            ) : (
                <div
                    style={{
                        display: 'flex',
                        gap: 3,
                        width: '13.75em',
                        margin: '0.5em',
                    }}
                >
                    <ButtonWithLoadingStatus loading={isLoading}>
                        {' '}
                    </ButtonWithLoadingStatus>
                    <InfoIcon />
                    {isError && renderApiErrorMessage(error)}
                </div>
            )}
        </div>
    )
}

export default RunDetailsLogsTab
