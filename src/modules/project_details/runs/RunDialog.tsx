import {
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
    FormControl,
    FormGroup,
    TextField,
    useTheme,
    InputLabel,
    Select,
    Typography,
    MenuItem,
    FormLabel,
    RadioGroup,
    FormControlLabel,
    Radio,
    Tooltip,
    SelectChangeEvent,
} from '@mui/material'
import React, {
    useEffect,
    useState,
    useRef,
    useCallback,
    ChangeEvent,
} from 'react'
import { useRouter } from 'next/router'
import { Spacing } from '../../../components/Spacing'
import axios from '../../../modules/auth/axios'
import {
    createYamlFromObject,
    debounce,
    getIndex,
    parseYaml,
} from '../../../helpers'
import {
    RunDialogProps,
    RunDialogErrorProps,
    EntryPoints,
} from './types/runDialogProps'
import {
    getFromMlprojectConfig,
    prepareParamsForRequest,
} from './mlflow/entryPoints'
import {
    AddRun,
    CodeRepository,
    Connection,
    DataRepository,
    ExperimentRepository,
    Platform,
    Run,
    useProjectsProjectIdRunsPostProjectsProjectIdRunsPost,
} from '../../../queries'
import { useRequiredFieldsCheck } from '../../../hooks/useRequiredFieldsCheck'
import { StatusProps } from '../../../types/statusProps'
import { renderApiErrorMessage } from '../../../errors'
import { useAuthentication } from '../../../hooks/useAuthentication'
import { FormLayout } from '../../../components/FormLayout'
import { ConnectionSelection } from '../../../components/forms/ConnectionSelection'
import { ButtonWithLoadingStatus } from '../../../components/ButtonWithLoadingStatus'
import { CodeRepositorySelection } from './CodeRepositorySelection'
import { ExperimentRepositorySelection } from './ExperimentRepositorySelection'
import { RenderElement } from '../../../utils/renderResourceActions'
import { Help } from '@mui/icons-material'
import { fetchRepo } from '../../../utils/fetchRepo'
import { convertToRawUrl } from '../../../utils/convertToRawUrl'
import { useFilterErrors } from '../../../hooks/useFilterErrors'
import { DataRepositorySelection } from './DataRepositorySelection'

interface RunRepeatDialogProps {
    open: boolean
    setOpen: React.Dispatch<React.SetStateAction<RenderElement | null>>
    refetchData?: () => void // Callback function to be invoked when code repository is added
    editData?: Run | null
    setEditItem: React.Dispatch<React.SetStateAction<Run | null>>
}

interface yamlDataState {
    data: { entry_points: Record<string, unknown> } | null
    type: string
}

enum BranchCommitOptions {
    BRANCH = 'branchName',
    COMMIT = 'commitHash',
    DATA_BRANCH = 'dataBranchName',
    DATA_COMMIT = 'dataCommitHash',
    CODE_REPO = 'codeRepo',
    DATA_REPO = 'dataRepo',
}

function getRepoBranchURL(
    currentRepository: CodeRepository | DataRepository,
    branchName: string
) {
    const mainBranchExtension =
        currentRepository?.platform === Platform.Bitbucket ? 'src' : 'tree'

    return `${currentRepository?.uri}/${mainBranchExtension}/${branchName}`
}

function getRepoCommitHashUrl(
    currentRepository: CodeRepository | DataRepository,
    commitHash: string
) {
    const platformSlug =
        currentRepository?.platform === Platform.Bitbucket ? 'src' : 'commit'
    return `${currentRepository?.uri}/${platformSlug}/${commitHash}`
}

const DEFAULT_BRANCH_NAME = 'main'

export const RunDialog = (props: RunRepeatDialogProps) => {
    const form: React.RefObject<HTMLElement | null> = useRef(null)
    const router = useRouter()
    const { id } = router.query
    const projectId: string =
        typeof id === 'string' ? id : id?.length ? id[0] : ''
    const theme = useTheme()
    const authentication = useAuthentication()

    const [status, setStatus] = useState<StatusProps>({
        status: false,
        type: '',
        text: '',
    })

    const initRunData = {
        name: '',
        branchName: '',
        commitHash: '',
        mlflowConfigPath: '',
        entryPoints: null,
        backendConfigPath: '',
        unicoreComputeBudgetAccount: '',
        dataBranchName: '',
        dataCommitHash: '',
    }

    const [runData, setRunData] = useState<RunDialogProps>(initRunData)

    const [runDataError, setRunDataError] = useState<RunDialogErrorProps>({
        name: false,
        codeRepo: false,
        branchName: false,
        commitHash: false,
        mlflowConfigPath: false,
        entryPoints: false,
        backendConfigPath: false,
        unicoreComputeBudgetAccount: false,
        dataRepo: false,
        dataBranchName: false,
        dataCommitHash: false,
    })

    const [unparsedYamlData, setUnparsedYamlData] = useState<string | null>(
        null
    )

    // on select change
    const [currentEntryPointIdx, setCurrentEntryPointIdx] = useState<number>(0)
    const [currentExperimentRepository, setCurrentExperimentRepository] =
        useState<ExperimentRepository | null>(null)
    const [currentConnection, setCurrentConnection] =
        useState<Connection | null>(null)
    const [currentCodeRepository, setCurrentCodeRepository] =
        useState<CodeRepository | null>(null)
    const [currentDataRepository, setCurrentDataRepository] =
        useState<DataRepository | null>(null)
    const allRequiredFieldValuesGiven: boolean = useRequiredFieldsCheck(
        form?.current,
        {
            formDataState: runData,
            currentCodeRepository,
            currentExperimentRepository,
            currentConnection,
        }
    )
    const formError = useFilterErrors(runDataError)

    const debounceDelay = 500 // amount of ms API calls are debounced
    const [selectedRadioOption, setSelectedRadioOption] = useState(
        BranchCommitOptions.BRANCH
    )
    const [selectedDataRadioOption, setSelectedDataRadioOption] = useState(
        BranchCommitOptions.DATA_BRANCH
    )
    async function handleCodeOptionChange(
        event: React.ChangeEvent<HTMLInputElement>
    ) {
        const selectedValue = event.target.value
        setSelectedRadioOption(selectedValue as BranchCommitOptions)
        const resetType =
            selectedValue === BranchCommitOptions.BRANCH
                ? BranchCommitOptions.COMMIT
                : BranchCommitOptions.BRANCH
        setRunDataError((prev) => ({
            ...prev,
            [resetType]: false,
        }))

        if (
            selectedValue === BranchCommitOptions.BRANCH &&
            currentCodeRepository
        ) {
            await debouncedFetch(
                getRepoBranchURL(currentCodeRepository, DEFAULT_BRANCH_NAME),
                BranchCommitOptions.BRANCH
            )
        }

        setRunData((prev) => ({
            ...prev,
            branchName:
                selectedValue === BranchCommitOptions.BRANCH
                    ? DEFAULT_BRANCH_NAME
                    : '',
            commitHash: '',
            mlflowConfigPath: '',
            entryPoints: null,
            backendConfigPath: '',
            unicoreComputeBudgetAccount: '',
        }))

        setUnparsedYamlData(null)
    }

    async function handleDataOptionChange(
        event: React.ChangeEvent<HTMLInputElement>
    ) {
        const selectedValue = event.target.value
        setSelectedDataRadioOption(selectedValue as BranchCommitOptions)
        const resetType =
            selectedValue === BranchCommitOptions.DATA_BRANCH
                ? BranchCommitOptions.DATA_COMMIT
                : BranchCommitOptions.DATA_BRANCH
        setRunDataError((prev) => ({
            ...prev,
            [resetType]: false,
        }))

        if (
            selectedValue === BranchCommitOptions.DATA_BRANCH &&
            currentDataRepository
        ) {
            await debouncedFetch(
                getRepoBranchURL(currentDataRepository, DEFAULT_BRANCH_NAME),
                BranchCommitOptions.DATA_BRANCH
            )
        }

        setRunData((prev) => ({
            ...prev,
            dataBranchName:
                selectedValue === BranchCommitOptions.DATA_BRANCH
                    ? DEFAULT_BRANCH_NAME
                    : '',
            dataCommitHash: '',
        }))

        setUnparsedYamlData(null)
    }
    const debouncedFetch = async (
        uri: string,
        fetchType: BranchCommitOptions
    ) => {
        return await fetchRepo(
            uri,
            () => {
                setRunDataError((prev) => ({
                    ...prev,
                    [fetchType]: false,
                }))
            },
            () => {
                setRunDataError((prev) => ({
                    ...prev,
                    [fetchType]: true,
                }))
            }
        )
    }

    // 2. Check if file path exists after branch existence has been confirmed (no restriction for file type)
    // on input change (debounced)
    const debouncedPathFetch = debounce((val: string, pathTypeKey: string) => {
        // eslint-disable-next-line @typescript-eslint/no-floating-promises
        fetchRepo(
            val,
            // on status 200 of file path request
            async () => {
                setRunDataError((prev) => ({
                    ...prev,
                    [pathTypeKey]: false,
                }))

                const fileContent = await getFileContent(val)

                if (pathTypeKey === 'backendConfigPath') {
                    setUnparsedYamlData(fileContent)
                } else if (pathTypeKey === 'mlflowConfigPath') {
                    const parsedYamlData = (await parseYaml(
                        fileContent
                    )) as yamlDataState['data']
                    if (parsedYamlData) {
                        parseAndSetEntryPoints(parsedYamlData.entry_points)
                    }
                }
            },

            () =>
                setRunDataError((prev) => ({
                    ...prev,
                    [pathTypeKey]: true,
                }))
        )
    }, debounceDelay)

    /* #############################################
                    EVENT HANDLERS
    ############################################# */

    function clearRun() {
        setRunDataError({
            name: false,
            codeRepo: false,
            branchName: false,
            commitHash: false,
            mlflowConfigPath: false,
            entryPoints: false,
            backendConfigPath: false,
            unicoreComputeBudgetAccount: false,
            dataRepo: false,
            dataBranchName: false,
            dataCommitHash: false,
        })
        setUnparsedYamlData(null)
        setCurrentEntryPointIdx(0)
        setCurrentConnection(null)
        setCurrentCodeRepository(null)
        setCurrentDataRepository(null)
        setCurrentExperimentRepository(null)
        setSelectedRadioOption(BranchCommitOptions.BRANCH)
        setSelectedDataRadioOption(BranchCommitOptions.DATA_BRANCH)
        props.setEditItem(null)
        setRunData(initRunData)
    }

    function handleClose(): void {
        props.setOpen(null)
        props.setEditItem(null)
        setStatus({ status: false, type: '', text: '' })
        clearRun()
    }

    async function handleCodeRepoChange(repo: CodeRepository) {
        setRunData((prev) => ({
            ...prev,
            branchName: DEFAULT_BRANCH_NAME,
            commitHash: '',
            mlflowConfigPath: '',
            entryPoints: null,
            backendConfigPath: '',
        }))

        setCurrentCodeRepository(repo)
        setUnparsedYamlData(null)

        setRunDataError((prev) => ({
            ...prev,
            commitHash: false,
            branchName: false,
        }))

        const { success } = await debouncedFetch(
            repo.uri,
            BranchCommitOptions.CODE_REPO
        )

        if (success) {
            await debouncedFetch(
                getRepoBranchURL(repo, DEFAULT_BRANCH_NAME),
                BranchCommitOptions.BRANCH
            )
        }
    }

    async function handleDataRepoChange(repo: DataRepository) {
        setRunData((prev) => ({
            ...prev,
            dataBranchName: DEFAULT_BRANCH_NAME,
            dataCommitHash: '',
        }))

        setCurrentDataRepository(repo)

        setRunDataError((prev) => ({
            ...prev,
            dataCommitHash: false,
            dataBranchName: false,
        }))

        const { success } = await debouncedFetch(
            repo.uri,
            BranchCommitOptions.DATA_REPO
        )

        if (success) {
            await debouncedFetch(
                getRepoBranchURL(repo, DEFAULT_BRANCH_NAME),
                BranchCommitOptions.DATA_BRANCH
            )
        }
    }

    function handleNameChange(e: React.ChangeEvent<HTMLInputElement>): void {
        const { value } = e.target

        setRunData((prev) => ({
            ...prev,
            name: value,
        }))
    }
    async function handleCommitHashChange(
        e: React.ChangeEvent<HTMLInputElement>
    ) {
        const { value } = e.target
        setRunDataError((prev) => ({
            ...prev,
            commitHash: true,
        }))

        setRunData((prev) => ({
            ...prev,
            commitHash: value,
            branchName: '',
            mlflowConfigPath: '',
        }))

        if (currentCodeRepository) {
            await debouncedFetch(
                getRepoCommitHashUrl(currentCodeRepository, value),
                BranchCommitOptions.COMMIT
            )
        }
    }

    async function handleDataCommitHashChange(
        e: React.ChangeEvent<HTMLInputElement>
    ) {
        const { value } = e.target
        setRunDataError((prev) => ({
            ...prev,
            dataCommitHash: true,
        }))

        setRunData((prev) => ({
            ...prev,
            dataCommitHash: value,
            dataBranchName: '',
        }))

        if (currentDataRepository) {
            await debouncedFetch(
                getRepoCommitHashUrl(currentDataRepository, value),
                BranchCommitOptions.DATA_COMMIT
            )
        }
    }

    async function handleBranchChange(e: React.ChangeEvent<HTMLInputElement>) {
        const { value } = e.target
        setRunDataError((prev) => ({
            ...prev,
            branchName: true,
        }))
        setRunData((prev) => ({
            ...prev,
            branchName: value,
            mlflowConfigPath: '',
        }))
        if (currentCodeRepository) {
            await debouncedFetch(
                getRepoBranchURL(currentCodeRepository, value),
                BranchCommitOptions.BRANCH
            )
        }
    }

    async function handleDataBranchChange(
        e: React.ChangeEvent<HTMLInputElement>
    ) {
        const { value } = e.target
        setRunDataError((prev) => ({
            ...prev,
            dataBranchName: true,
        }))
        setRunData((prev) => ({
            ...prev,
            dataBranchName: value,
        }))

        if (currentDataRepository) {
            await debouncedFetch(
                getRepoBranchURL(currentDataRepository, value),
                BranchCommitOptions.DATA_BRANCH
            )
        }
    }

    // check if file path exists
    // reset subsequent states and fields on file path input change
    function handlePathChange(
        e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
        pathTypeKey = 'mlflowConfigPath'
    ): void {
        const { value } = e.currentTarget
        setRunDataError((prev) => ({
            ...prev,
            [pathTypeKey]: true,
        }))
        setRunData((prev) => ({
            ...prev,
            [pathTypeKey]: value,
            entryPoints:
                pathTypeKey === 'mlflowConfigPath' ? null : prev.entryPoints, // reset entrypoints on mlflow config path change only
        }))
        const url =
            pathTypeKey === 'mlflowConfigPath' ? value + '/MLproject' : value // add file name to path if mlflow config path
        let validRawUrlPath: string | null = null
        if (currentCodeRepository?.platform) {
            validRawUrlPath = convertToRawUrl(
                currentCodeRepository?.platform,
                currentCodeRepository?.uri || '',
                runData.branchName || runData.commitHash,
                url
            )
        }
        if (!validRawUrlPath) {
            return
        }
        debouncedPathFetch(validRawUrlPath, pathTypeKey)
    }

    // set index of current select value
    function handleEntrypointChange(e: SelectChangeEvent<string>) {
        const entryPoints = runData.entryPoints
        let idx = 0
        if (!!entryPoints) {
            const item = entryPoints.find(
                (el) => el.entryPoint === e.target.value
            )

            if (item) {
                idx = entryPoints.indexOf(item)
            }
            setCurrentEntryPointIdx(idx)
        }
    }

    function handleBackendConfigChange(e: ChangeEvent<HTMLInputElement>): void {
        const { value } = e.target
        setUnparsedYamlData(value)
    }

    function handleUnicoreChange(e: ChangeEvent<HTMLInputElement>): void {
        const { value } = e.target
        setRunData((prev) => ({
            ...prev,
            unicoreComputeBudgetAccount: value,
        }))
    }

    // set nested entrypoints data on parameters change
    function handleParameterChange(
        e: ChangeEvent<HTMLTextAreaElement | HTMLInputElement>,
        idx: number
    ): void {
        const { value } = e.currentTarget
        let currentEntryPoints: EntryPoints
        if (runData.entryPoints) {
            currentEntryPoints = runData.entryPoints
            currentEntryPoints[currentEntryPointIdx].parameters[idx].value =
                value
            setRunData((prev) => ({
                ...prev,
                entryPoints: [...currentEntryPoints],
            }))
        }
    }

    async function handleSubmitRun() {
        if (
            runData.entryPoints?.length &&
            currentConnection &&
            currentCodeRepository &&
            currentExperimentRepository
        ) {
            const yamlDataParsed = (await parseYaml(unparsedYamlData)) as {
                [key: string]: string
            }
            const entryPointParams = prepareParamsForRequest(
                runData.entryPoints,
                currentEntryPointIdx
            )

            const data: AddRun = {
                name: runData.name,
                experimentRepositoryId:
                    currentExperimentRepository.experimentRepositoryId,
                codeRepositoryId: currentCodeRepository.codeRepositoryId,
                connectionId: currentConnection.connectionId,
                computeBudgetAccount: runData.unicoreComputeBudgetAccount,
                mlflowMlprojectFilePath:
                    runData.mlflowConfigPath + '/MLproject',
                entryPoint:
                    runData.entryPoints[currentEntryPointIdx].entryPoint,
                mlflowParameters: entryPointParams,
                backendConfig: yamlDataParsed,
                dataRepositoryId: currentDataRepository?.dataRepositoryId,
                dataBranch:
                    runData.dataBranchName === ''
                        ? undefined
                        : runData.dataBranchName,
                dataCommit:
                    runData.dataCommitHash === ''
                        ? undefined
                        : runData.dataCommitHash,
            }
            if (!runData.commitHash) {
                data.branch = runData.branchName
            } else {
                data.commit = runData.commitHash
            }

            postRunData(data)
        }
    }

    // on successful parsing of yaml file
    function parseAndSetEntryPoints(data: { [key: string]: unknown }): void {
        const entryPoints = getFromMlprojectConfig(data)

        if (entryPoints === undefined) return

        setRunData((prev) => ({
            ...prev,
            entryPoints,
        }))
        setRunDataError((prev) => ({
            ...prev,
            entryPoints: false,
        }))
    }

    /* #############################################
                    API CALLS
    ############################################# */

    const { mutate, isPending: isLoading } =
        useProjectsProjectIdRunsPostProjectsProjectIdRunsPost({
            mutation: {
                onSuccess: () => {
                    setStatus({
                        status: true,
                        type: 'success',
                        text: 'Run was submitted successfully!',
                    })
                    if (props.refetchData) {
                        props.refetchData() // Invoke the callback function if it's defined
                    }
                },
                onError: (error) => {
                    setStatus({
                        status: true,
                        type: 'error',
                        text: renderApiErrorMessage(error),
                    })
                },
            },
        })

    const getFileContent = useCallback(async (url: string) => {
        try {
            const response = await axios.post('/api/runs/get-file-content', {
                url,
            })
            if (response.data.data_response) {
                return response.data.data_response
            } else {
                console.error('file content error', response.data)
            }
        } catch (err: unknown) {
            console.error('catch file content error: ', err)
        }
    }, [])

    function postRunData(data: AddRun) {
        if (!projectId) return
        if (authentication === 'authenticated') {
            mutate({
                projectId,
                data,
            })
        } else {
            setStatus({
                status: true,
                type: 'error',
                text: 'Authentication error. Please check your inputs and try again.',
            })
        }
    }

    const setEntryPointsFromCodeRepository = useCallback(
        async (
            modelRepository: Run['modelRepository'],
            mlflowMlprojectFilePath: Run['mlflowMlprojectFilePath'],
            mlflowParameters: Run['mlflowParameters'],
            entryPoint: Run['entryPoint']
        ) => {
            if (!modelRepository.codeRepository) return

            const validRawUrlPath = convertToRawUrl(
                modelRepository.codeRepository.platform,
                modelRepository.codeRepository.uri,
                modelRepository.branch || modelRepository?.commit || '',
                mlflowMlprojectFilePath
            )
            if (!validRawUrlPath) return

            const data = await getFileContent(validRawUrlPath)
            const parsedYamlData = (await parseYaml(
                data
            )) as yamlDataState['data']
            if (!parsedYamlData) return

            const entryPointsFromFile = getFromMlprojectConfig(
                parsedYamlData.entry_points
            )
            if (!entryPointsFromFile) {
                setRunData((prev) => {
                    return {
                        ...prev,
                        entryPoints: null,
                    } as RunDialogProps
                })
                return
            }

            const entryPoints = entryPointsFromFile.map((entryPoint) => ({
                entryPoint: entryPoint.entryPoint,
                parameters: entryPoint.parameters.map((param) => {
                    return {
                        ...param,
                        value: mlflowParameters?.[param.name] ?? null,
                    }
                }),
            }))

            setRunData((prev) => {
                return {
                    ...prev,
                    entryPoints,
                } as RunDialogProps
            })

            const idx = getIndex(entryPoint, 'entryPoint', entryPoints)
            setCurrentEntryPointIdx(idx)
        },
        [getFileContent]
    )

    /**
     * if editData
     */

    useEffect(() => {
        if (!props.editData) return
        const editData = props.editData

        setRunData((prev) => ({
            ...prev,
            name: editData.name || '',
            branchName: editData.modelRepository?.branch || '',
            commitHash: editData.modelRepository?.commit || '',
            dataBranchName: editData.dataBranch || '',
            dataCommitHash: editData.dataCommit || '',
            mlflowConfigPath: editData.mlflowMlprojectFilePath
                ? editData?.mlflowMlprojectFilePath.replaceAll('/MLproject', '')
                : '',
            unicoreComputeBudgetAccount: editData.computeBudgetAccount
                ? editData?.computeBudgetAccount
                : '',
        }))

        if (props.editData.experimentRepository) {
            setCurrentExperimentRepository(props.editData.experimentRepository)
        }
        if (props.editData.modelRepository.codeRepository) {
            setCurrentCodeRepository(
                props.editData.modelRepository.codeRepository
            )
            // eslint-disable-next-line @typescript-eslint/no-floating-promises
            setEntryPointsFromCodeRepository(
                props.editData.modelRepository,
                props.editData.mlflowMlprojectFilePath,
                props.editData.mlflowParameters,
                props.editData.entryPoint
            )
        }
        if (props.editData.modelRepository.commit) {
            setSelectedRadioOption(BranchCommitOptions.COMMIT)
        }
        if (props.editData.dataRepository) {
            setCurrentDataRepository(props.editData.dataRepository)
        }
        if (props.editData.dataCommit) {
            setSelectedDataRadioOption(BranchCommitOptions.DATA_COMMIT)
        }

        setCurrentConnection(props.editData.connection ?? null)

        // get backend config with edit data backend config path
        const data = createYamlFromObject(props.editData.backendConfig)
        if (data) setUnparsedYamlData(data)
    }, [props.editData, setEntryPointsFromCodeRepository])

    const disableCodeOptions =
        currentCodeRepository === null || runDataError.codeRepo
    const disableDataOptions =
        currentDataRepository === null || runDataError.dataRepo

    return (
        <Dialog fullWidth maxWidth="sm" open={props.open} onClose={handleClose}>
            <DialogTitle>
                {props.editData ? 'Rerun' : 'Create New Run'}
            </DialogTitle>
            <DialogContent>
                <DialogContentText>
                    Select the parameters of your run
                </DialogContentText>
                <Spacing value={theme.spacing(2)} />
                <FormLayout handleClose={handleClose} status={status}>
                    <FormGroup ref={form}>
                        <FormControl>
                            <TextField
                                key="name"
                                type="text"
                                name="name"
                                id="name"
                                label="Name"
                                size="small"
                                value={runData.name}
                                error={runDataError.name}
                                helperText={
                                    runDataError.name ? 'Invalid Name' : ''
                                }
                                onChange={handleNameChange}
                                required
                            />
                        </FormControl>
                        <Spacing value={theme.spacing(2)} />
                        <ExperimentRepositorySelection
                            currentExperimentRepository={
                                currentExperimentRepository
                            }
                            setCurrentExperimentRepository={
                                setCurrentExperimentRepository
                            }
                        />
                        <Spacing value={theme.spacing(2)} />
                        <CodeRepositorySelection
                            currentCodeRepository={currentCodeRepository}
                            handleRepoChange={handleCodeRepoChange}
                            error={runDataError.codeRepo}
                        />
                        <Spacing value={theme.spacing(2)} />
                        <FormLabel component="legend" id="branchOrCommit">
                            Select an option:
                        </FormLabel>
                        <div
                            data-testid="codeRepositoryInputs"
                            style={{
                                display: 'flex',
                                flexDirection: 'row',
                            }}
                        >
                            <FormControl
                                component="fieldset"
                                disabled={disableCodeOptions}
                            >
                                <RadioGroup
                                    aria-labelledby="branchOrCommit"
                                    name="branchOrCommit"
                                    value={selectedRadioOption}
                                    onChange={handleCodeOptionChange}
                                >
                                    <div
                                        style={{
                                            display: 'flex',
                                            flexDirection: 'row',
                                        }}
                                    >
                                        <FormControlLabel
                                            value={BranchCommitOptions.BRANCH}
                                            control={<Radio />}
                                            label="Branch"
                                        />
                                        <FormControlLabel
                                            value={BranchCommitOptions.COMMIT}
                                            control={<Radio />}
                                            label="Commit Hash"
                                        />
                                    </div>
                                </RadioGroup>
                                <Spacing value={theme.spacing(2)} />
                            </FormControl>
                            {selectedRadioOption ===
                            BranchCommitOptions.BRANCH ? (
                                <FormControl sx={{ flexGrow: 1 }}>
                                    <TextField
                                        key="branchName"
                                        type="text"
                                        name="branchName"
                                        id="branchName"
                                        label="Branch Name *"
                                        size="small"
                                        value={runData.branchName}
                                        error={runDataError.branchName}
                                        helperText={
                                            runDataError.branchName
                                                ? 'Invalid URL'
                                                : ''
                                        }
                                        onChange={handleBranchChange}
                                        disabled={disableCodeOptions}
                                    />
                                </FormControl>
                            ) : (
                                <FormControl sx={{ flexGrow: 1 }}>
                                    <TextField
                                        key="commitHash"
                                        type="text"
                                        name="commitHash"
                                        id="commitHash"
                                        label="Commit Hash *"
                                        size="small"
                                        value={runData.commitHash}
                                        error={runDataError.commitHash}
                                        placeholder={
                                            'c26cf8af130955c5c67cfea96f9532680b963628'
                                        }
                                        helperText={
                                            runDataError.commitHash
                                                ? 'Invalid Commit Hash'
                                                : ''
                                        }
                                        onChange={handleCommitHashChange}
                                        disabled={disableCodeOptions}
                                    />
                                </FormControl>
                            )}
                        </div>
                        <Spacing value={theme.spacing(2)} />
                        <FormControl
                            style={{
                                flexGrow: 1,
                            }}
                        >
                            <TextField
                                key="mlflowConfigPath"
                                type="text"
                                name="mlflowConfigPath"
                                id="mlflowConfigPath"
                                label="Relative Path to MLflow Project Directory"
                                size="small"
                                value={runData.mlflowConfigPath}
                                error={runDataError.mlflowConfigPath}
                                helperText={
                                    runDataError.mlflowConfigPath ? (
                                        <>
                                            Cannot find MLproject file in given
                                            directory of the code repository:{' '}
                                            {
                                                currentCodeRepository?.codeRepositoryName
                                            }
                                            .<br />
                                            Cannot fetch raw file content at{' '}
                                            {currentCodeRepository?.uri}/
                                            {runData.mlflowConfigPath}
                                            /MLproject.
                                        </>
                                    ) : (
                                        ''
                                    )
                                }
                                onChange={handlePathChange}
                                disabled={
                                    !currentCodeRepository ||
                                    runDataError.branchName ||
                                    runDataError.commitHash ||
                                    (selectedRadioOption ===
                                    BranchCommitOptions.BRANCH
                                        ? runDataError.branchName
                                        : runDataError.commitHash)
                                }
                                required
                                InputProps={{
                                    endAdornment: (
                                        <Tooltip
                                            title={
                                                <Typography sx={{ pb: 1 }}>
                                                    The given path is assumed to
                                                    be relative to the root
                                                    directory of the selected
                                                    code repository.
                                                    <br />
                                                    The MLflow project directory{' '}
                                                    <strong>must</strong>{' '}
                                                    contain the MLproject file
                                                    named &quot;MLproject&quot;.
                                                    <br />
                                                    Only files contained in this
                                                    folder will be uploaded to
                                                    the run directory on the
                                                    remote system.
                                                    <br />
                                                    <br />
                                                    For details see{' '}
                                                    <a
                                                        href="https://mantik-ai.gitlab.io/mantik/remote-execution/preparing-your-application.html"
                                                        target="_blank"
                                                        rel="noreferrer"
                                                        style={{
                                                            color: 'white',
                                                            textDecoration:
                                                                'underline',
                                                        }}
                                                    >
                                                        our documentation
                                                    </a>
                                                    .
                                                </Typography>
                                            }
                                        >
                                            <Help color="info" />
                                        </Tooltip>
                                    ),
                                }}
                            />
                        </FormControl>
                        <Spacing value={theme.spacing(2)} />
                        {runDataError.entryPoints && (
                            <Typography variant="inherit" color="error">
                                {`Cannot parse ${runData.mlflowConfigPath} as MLflow project file (expected MLproject YAML format)`}
                            </Typography>
                        )}
                        {runData.mlflowConfigPath &&
                            runData.entryPoints &&
                            !runData.entryPoints.length && (
                                <>
                                    <Typography variant="inherit" color="error">
                                        No entry points found in MLproject file
                                    </Typography>
                                    <Spacing value={theme.spacing(2)} />
                                </>
                            )}
                        {runData.entryPoints &&
                            !!runData.entryPoints.length && (
                                <>
                                    <FormControl>
                                        <InputLabel id="select-entrypoint">
                                            Entry Point
                                        </InputLabel>
                                        <Select
                                            labelId="select-entrypoint"
                                            id="entrypoint"
                                            value={
                                                runData.entryPoints[
                                                    currentEntryPointIdx
                                                ]?.entryPoint
                                            }
                                            label="Entry Point"
                                            onChange={handleEntrypointChange}
                                        >
                                            {runData.entryPoints?.map(
                                                (item, index) => (
                                                    <MenuItem
                                                        key={index}
                                                        value={item.entryPoint}
                                                    >
                                                        {item.entryPoint}
                                                    </MenuItem>
                                                )
                                            )}
                                        </Select>
                                    </FormControl>
                                    <Spacing value={theme.spacing(2)} />

                                    {runData.entryPoints[
                                        currentEntryPointIdx
                                    ]?.parameters?.map((item, idx) => (
                                        <React.Fragment key={idx}>
                                            <FormControl>
                                                <TextField
                                                    key={idx}
                                                    type="text"
                                                    name={item.name}
                                                    id={item.name}
                                                    label={item.label}
                                                    size="small"
                                                    value={item.value || ''}
                                                    onChange={(e) =>
                                                        handleParameterChange(
                                                            e,
                                                            idx
                                                        )
                                                    }
                                                    required={item.required}
                                                />
                                            </FormControl>
                                            <Spacing value={theme.spacing(2)} />
                                        </React.Fragment>
                                    ))}
                                    {!runData.entryPoints[currentEntryPointIdx]
                                        ?.parameters?.length &&
                                        'No parameters defined for selected entry point'}
                                </>
                            )}
                        <FormControl>
                            <TextField
                                key="backendConfig"
                                type="text"
                                name="backendConfig"
                                id="backendConfig"
                                label="Relative path to Compute Backend Config File"
                                size="small"
                                value={runData.backendConfigPath}
                                error={runDataError.backendConfigPath}
                                helperText={
                                    runDataError.backendConfigPath ? (
                                        <>
                                            Cannot fetch raw file content at:{' '}
                                            {currentCodeRepository?.uri}/
                                            {runData.backendConfigPath}.
                                        </>
                                    ) : (
                                        ''
                                    )
                                }
                                onChange={(e) =>
                                    handlePathChange(e, 'backendConfigPath')
                                }
                                disabled={
                                    !currentCodeRepository ||
                                    runDataError.branchName
                                }
                                required={props.editData ? false : true}
                                InputProps={{
                                    endAdornment: (
                                        <Tooltip
                                            title={
                                                <Typography sx={{ pb: 1 }}>
                                                    The given path is assumed to
                                                    be relative to the root
                                                    directory of the selected
                                                    code repository.
                                                    <br />
                                                    <br />
                                                    For details on the Compute
                                                    Backend Config see{' '}
                                                    <a
                                                        href="https://mantik-ai.gitlab.io/mantik/remote-execution/compute-backend-config.html"
                                                        target="_blank"
                                                        rel="noreferrer"
                                                        style={{
                                                            color: 'white',
                                                            textDecoration:
                                                                'underline',
                                                        }}
                                                    >
                                                        our documentation
                                                    </a>
                                                    .
                                                </Typography>
                                            }
                                        >
                                            <Help color="info" />
                                        </Tooltip>
                                    ),
                                }}
                            />
                        </FormControl>
                        <Spacing value={theme.spacing(2)} />
                        {unparsedYamlData &&
                            !runDataError.backendConfigPath && (
                                <>
                                    <TextField
                                        name="backend-config"
                                        id="backend-config"
                                        label="Backend Config"
                                        multiline
                                        value={unparsedYamlData}
                                        onChange={handleBackendConfigChange}
                                        required
                                    />
                                    <Spacing value={theme.spacing(2)} />
                                </>
                            )}
                        {runData.backendConfigPath && !unparsedYamlData && (
                            <>
                                <Typography variant="inherit" color="error">
                                    {`Cannot fetch raw file content at ${runData.backendConfigPath}`}
                                </Typography>
                                <Spacing value={theme.spacing(2)} />
                            </>
                        )}
                        <Spacing value={theme.spacing(2)} />
                        <DataRepositorySelection
                            currentDataRepository={currentDataRepository}
                            handleRepoChange={handleDataRepoChange}
                            error={runDataError.dataRepo}
                        />
                        <Spacing value={theme.spacing(2)} />
                        <FormLabel component="legend" id="branchOrCommitData">
                            Select an option:
                        </FormLabel>
                        <div
                            data-testid="dataRepositoryInputs"
                            style={{
                                display: 'flex',
                                flexDirection: 'row',
                            }}
                        >
                            <FormControl
                                component="fieldset"
                                disabled={disableDataOptions}
                            >
                                <RadioGroup
                                    aria-labelledby="branchOrCommitData"
                                    name="branchOrCommitData"
                                    value={selectedDataRadioOption}
                                    onChange={handleDataOptionChange}
                                >
                                    <div
                                        style={{
                                            display: 'flex',
                                            flexDirection: 'row',
                                        }}
                                    >
                                        <FormControlLabel
                                            value={
                                                BranchCommitOptions.DATA_BRANCH
                                            }
                                            control={<Radio />}
                                            label="Branch"
                                        />
                                        <FormControlLabel
                                            value={
                                                BranchCommitOptions.DATA_COMMIT
                                            }
                                            control={<Radio />}
                                            label="Commit Hash"
                                        />
                                    </div>
                                </RadioGroup>
                                <Spacing value={theme.spacing(2)} />
                            </FormControl>
                            {selectedDataRadioOption ===
                            BranchCommitOptions.DATA_BRANCH ? (
                                <FormControl sx={{ flexGrow: 1 }}>
                                    <TextField
                                        key="dataBranchName"
                                        type="text"
                                        name="dataBranchName"
                                        id="dataBranchName"
                                        label="Branch Name *"
                                        size="small"
                                        value={runData.dataBranchName}
                                        error={runDataError.dataBranchName}
                                        helperText={
                                            runDataError.dataBranchName
                                                ? 'Invalid URL'
                                                : ''
                                        }
                                        onChange={handleDataBranchChange}
                                        disabled={disableDataOptions}
                                    />
                                </FormControl>
                            ) : (
                                <FormControl sx={{ flexGrow: 1 }}>
                                    <TextField
                                        key="dataCommitHash"
                                        type="text"
                                        name="dataCommitHash"
                                        id="dataCommitHash"
                                        label="Commit Hash *"
                                        size="small"
                                        value={runData.dataCommitHash}
                                        error={runDataError.dataCommitHash}
                                        placeholder={
                                            'c26cf8af130955c5c67cfea96f9532680b963628'
                                        }
                                        helperText={
                                            runDataError.dataCommitHash
                                                ? 'Invalid Commit Hash'
                                                : ''
                                        }
                                        onChange={handleDataCommitHashChange}
                                        disabled={disableDataOptions}
                                    />
                                </FormControl>
                            )}
                        </div>
                        <Spacing value={theme.spacing(2)} />
                        <ConnectionSelection
                            currentConnection={currentConnection}
                            setCurrentConnection={setCurrentConnection}
                            isRequired={true}
                            isEdit={!!props.editData}
                        />
                        <Spacing value={theme.spacing(1)} />
                        <FormControl required>
                            <TextField
                                size="small"
                                name="unicore-compute-budget-account"
                                id="unicore-compute-budget-account"
                                label="UNICORE Compute Budget Account"
                                value={runData.unicoreComputeBudgetAccount}
                                onChange={handleUnicoreChange}
                                required
                            />
                        </FormControl>
                        <Spacing value={theme.spacing(2)} />
                        {(!allRequiredFieldValuesGiven ||
                            !currentConnection) && (
                            <>
                                <Typography variant="inherit" color="error">
                                    {`Please ensure that all fields with * are filled out.`}
                                </Typography>
                                <Spacing value={theme.spacing(2)} />
                            </>
                        )}
                    </FormGroup>
                </FormLayout>
            </DialogContent>

            {status.type !== 'success' && (
                <DialogActions>
                    <Button onClick={handleClose}>Cancel</Button>
                    <ButtonWithLoadingStatus loading={isLoading}>
                        <Button
                            onClick={handleSubmitRun}
                            disabled={
                                !runData.entryPoints?.length ||
                                !allRequiredFieldValuesGiven ||
                                formError ||
                                !currentConnection
                            }
                        >
                            Run!
                        </Button>
                    </ButtonWithLoadingStatus>
                </DialogActions>
            )}
        </Dialog>
    )
}
