import * as React from 'react'
import Dialog from '@mui/material/Dialog'
import Slide from '@mui/material/Slide'
import { TransitionProps } from '@mui/material/transitions'
import { useEffect, useRef } from 'react'
import FullscreenIcon from '@mui/icons-material/Fullscreen'
import FullscreenExitIcon from '@mui/icons-material/FullscreenExit'
const Transition = React.forwardRef(function Transition(
    props: TransitionProps & {
        children: React.ReactElement
    },
    ref: React.Ref<unknown>
) {
    return <Slide direction="up" ref={ref} {...props} />
})

export default function RunDetailsLogsFullScreen({
    logs,
}: {
    logs: string[] | undefined
}) {
    const [open, setOpen] = React.useState(false)

    const handleClickOpen = () => {
        setOpen(true)
    }

    const handleClose = () => {
        setOpen(false)
    }
    const logsContainerRef = useRef<HTMLDivElement | null>(null)
    const scrollToBottom = () => {
        if (logsContainerRef.current) {
            logsContainerRef.current.scrollTop =
                logsContainerRef.current.scrollHeight
        }
    }

    useEffect(() => {
        if (open) {
            scrollToBottom()
        }
    }, [open])

    return (
        <div
            style={{
                position: 'sticky',
                top: 0,
                display: 'flex',
                justifyContent: 'flex-end',
            }}
        >
            <FullscreenIcon onClick={handleClickOpen} />
            <Dialog
                fullScreen
                open={open}
                onClose={handleClose}
                TransitionComponent={Transition}
            >
                <div
                    ref={(element) => {
                        if (element) {
                            logsContainerRef.current = element
                            if (open) {
                                scrollToBottom()
                            }
                        }
                    }}
                    style={{
                        border: '1px solid #ccc',
                        backgroundColor: 'black',
                        color: 'white',
                        overflowY: 'scroll',
                        padding: '0.6em',
                        whiteSpace: 'pre-wrap',
                        minHeight: '100vh',
                    }}
                >
                    <div
                        style={{
                            position: 'sticky',
                            top: 0,
                            display: 'flex',
                            justifyContent: 'flex-end',
                            padding: '1em',
                        }}
                    >
                        <FullscreenExitIcon onClick={handleClose} />
                    </div>
                    {logs &&
                        logs.map((log, index) => <pre key={index}>{log}</pre>)}
                </div>
            </Dialog>
        </div>
    )
}
