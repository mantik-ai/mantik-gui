import { IconButton } from '@mui/material'
import { GPUInfo, RunInfrastructure } from '../../../queries'

import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown'
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp'
import Table from '@mui/material/Table'
import TableBody from '@mui/material/TableBody'
import TableCell from '@mui/material/TableCell'
import TableContainer from '@mui/material/TableContainer'
import TableRow from '@mui/material/TableRow'
import { useState } from 'react'
import { NULL_VALUE_REPRESENTATION } from '../../../constants'

function GpuInfoCell({ gpuInfo }: { gpuInfo: GPUInfo }) {
    return (
        <Table size="small">
            <TableBody>
                <TableRow>
                    <TableCell>name</TableCell>
                    <TableCell>{gpuInfo.name}</TableCell>
                </TableRow>
                <TableRow>
                    <TableCell>driver</TableCell>
                    <TableCell>{gpuInfo.driver}</TableCell>
                </TableRow>
                <TableRow>
                    <TableCell>id</TableCell>
                    <TableCell>{gpuInfo.id}</TableCell>
                </TableRow>
                <TableRow
                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                >
                    <TableCell>totalMemory</TableCell>
                    <TableCell>{gpuInfo.totalMemory}</TableCell>
                </TableRow>
            </TableBody>
        </Table>
    )
}

function GpuInfoRow({ index, gpuInfo }: { index: number; gpuInfo: GPUInfo }) {
    const [open, setOpen] = useState(false)
    return (
        <TableRow>
            <TableCell style={{ verticalAlign: 'top' }}>
                gpuInfo {index}
                <IconButton size="small" onClick={() => setOpen(!open)}>
                    {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
                </IconButton>
            </TableCell>
            <TableCell>
                {open ? <GpuInfoCell gpuInfo={gpuInfo} /> : gpuInfo.name}
            </TableCell>
        </TableRow>
    )
}

function AttributeRow({
    name,
    value,
}: {
    name: string
    value: string | number
}) {
    return (
        <TableRow>
            <TableCell>{name}</TableCell>
            <TableCell>{value}</TableCell>
        </TableRow>
    )
}

export const RunInfrastructureCell = ({
    infrastructure,
}: {
    infrastructure?: RunInfrastructure
}) => {
    if (!infrastructure) {
        return <>{NULL_VALUE_REPRESENTATION}</>
    }

    return (
        <TableContainer>
            <Table size="small">
                <colgroup>
                    <col style={{ width: '20%' }} />
                </colgroup>
                <TableBody>
                    {Object.keys(infrastructure).map((attributeName) => {
                        const infrastructureAttribute =
                            attributeName as keyof typeof infrastructure

                        return infrastructureAttribute === 'gpuInfo' ? (
                            infrastructure[infrastructureAttribute].map(
                                (gpuInfo, index) => {
                                    return (
                                        <GpuInfoRow
                                            key={`gpuInfoRow${index}`}
                                            index={index}
                                            gpuInfo={gpuInfo}
                                        />
                                    )
                                }
                            )
                        ) : (
                            <AttributeRow
                                key={`attributeRow${infrastructureAttribute}`}
                                name={infrastructureAttribute}
                                value={infrastructure[infrastructureAttribute]}
                            />
                        )
                    })}
                </TableBody>
            </Table>
        </TableContainer>
    )
}
