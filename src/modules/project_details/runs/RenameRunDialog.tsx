import React, { useEffect, useState } from 'react'
import { useRunNamePutProjectsProjectIdRunsRunIdNamePut } from '../../../queries'
import { renderApiErrorMessage } from '../../../errors'
import { FormDialogLayout } from '../../../components/FormDialogLayout'
import { FormControl, FormGroup, TextField } from '@mui/material'

interface SelectedRunData {
    name: string
    projectId: string
    runId: string
}
interface RenameRunProps {
    open: boolean
    setOpen: React.Dispatch<React.SetStateAction<boolean>>
    projectId: string
    selectedRunData: SelectedRunData
    refetch: () => void
}

export const RenameRunDialog = (props: RenameRunProps) => {
    const { open, setOpen, projectId, selectedRunData, refetch } = props
    const [runName, setRunName] = useState('')
    const [status, setStatus] = useState({ status: false, type: '', text: '' })

    const reset = () => {
        setRunName('')
    }
    const { mutate, isPending: isLoading } =
        useRunNamePutProjectsProjectIdRunsRunIdNamePut({
            mutation: {
                onSuccess: () => {
                    setStatus({
                        status: true,
                        type: 'success',
                        text: `Run has been updated successfully!`,
                    })
                    reset()
                    refetch()
                },
                onError: (err) => {
                    console.log('Error: ', err)
                    setStatus({
                        status: true,
                        type: 'error',
                        text: renderApiErrorMessage(err),
                    })
                    reset()
                },
            },
        })
    function handleRenameRunSubmit() {
        mutate({
            data: runName,
            runId: selectedRunData.runId,
            projectId,
        })
    }

    function handleClose(): void {
        setOpen(false)
        setStatus({ status: false, type: '', text: '' })
    }
    useEffect(() => {
        if (selectedRunData) {
            setRunName(selectedRunData?.name || '')
        }
    }, [selectedRunData])
    //TODO:check the uniqueness of the name before submitting, refer models
    return (
        <FormDialogLayout
            open={open}
            handleClose={handleClose}
            content=""
            handleSubmit={handleRenameRunSubmit}
            isEdit={true}
            title="Edit Run"
            status={status}
            formDataState={{
                runName,
                error: false,
            }}
            loading={isLoading}
        >
            <FormGroup>
                <FormControl component="fieldset">
                    <TextField
                        key="name"
                        name="name"
                        label="Run Name"
                        size="small"
                        value={runName}
                        onChange={(e) => {
                            setRunName(e.target.value)
                        }}
                        required
                    />
                </FormControl>
            </FormGroup>
        </FormDialogLayout>
    )
}
