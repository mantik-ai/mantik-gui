import {
    Box,
    FormControl,
    FormHelperText,
    InputLabel,
    ListItemText,
    MenuItem,
    Select,
    SelectChangeEvent,
    Skeleton,
    Typography,
    useTheme,
} from '@mui/material'
import { Spacing } from '../../../components/Spacing'
import {
    CodeRepository,
    useProjectsProjectIdCodeGetProjectsProjectIdCodeGet,
} from '../../../queries'
import { useRouter } from 'next/router'
import { getIndex } from '../../../helpers'
import { PlatformIcon } from '../../../components/PlatformIcon'

export const CodeRepositorySelection = ({
    currentCodeRepository = null,
    handleRepoChange,
    error,
}: {
    currentCodeRepository?: CodeRepository | null
    handleRepoChange: (repo: CodeRepository) => void
    error?: boolean
}) => {
    const theme = useTheme()
    const router = useRouter()
    const { id } = router.query
    const projectId: string =
        typeof id === 'string' ? id : id?.length ? id[0] : ''
    const {
        data,
        isSuccess,
        isPending: isLoading,
    } = useProjectsProjectIdCodeGetProjectsProjectIdCodeGet(projectId)

    // set index of current select value
    function handleCodeRepositoryChange(e: SelectChangeEvent): void {
        if (!data?.codeRepositories) return
        const { value } = e.target
        const idx = getIndex(value, 'codeRepositoryId', data.codeRepositories)
        handleRepoChange(data.codeRepositories[idx])
    }

    const platformIconProps = {
        sx: {
            fontSize: '1em',
            mr: 1,
            color: 'grey.500',
        },
    }
    const formatValue = (item: CodeRepository | null): string =>
        item ? `${item.codeRepositoryName} (${item.uri})` : ''

    return (
        <FormControl required error={error}>
            <InputLabel id="codeRepositories">Code Repository</InputLabel>
            {isLoading && (
                <Skeleton variant="rounded" sx={{ height: '3.5em' }}>
                    <input
                        type="hidden"
                        id="codeRepositories"
                        value={currentCodeRepository?.codeRepositoryId || ''}
                        required
                    />
                </Skeleton>
            )}
            {isSuccess && (
                <Select
                    labelId="codeRepositories"
                    id="codeRepositories"
                    label="Code Repository"
                    value={currentCodeRepository?.codeRepositoryId || ''}
                    onChange={handleCodeRepositoryChange}
                    data-testid="codeRepositorySelector"
                    required
                >
                    {data?.codeRepositories?.length ? (
                        data.codeRepositories.map((item, index) => (
                            <MenuItem key={index} value={item.codeRepositoryId}>
                                <Box
                                    sx={{
                                        fontSize: '1.25em',
                                        display: 'flex',
                                        alignItems: 'center',
                                        margin: '-0.125em 0',
                                    }}
                                >
                                    <PlatformIcon
                                        platform={item?.platform}
                                        iconProps={platformIconProps}
                                    />
                                    <ListItemText primary={formatValue(item)} />
                                </Box>
                            </MenuItem>
                        ))
                    ) : (
                        <MenuItem value="No code repositories found">
                            No code repositories found
                        </MenuItem>
                    )}
                </Select>
            )}
            {error && (
                <FormHelperText>
                    Code repository could not be loaded
                </FormHelperText>
            )}
            <Spacing value={theme.spacing(1)} />
            {!data?.codeRepositories?.length && (
                <Typography variant="inherit" color="error">
                    {`Cannot find a code repository. Please go to Projects > Code > Add`}
                </Typography>
            )}
        </FormControl>
    )
}
