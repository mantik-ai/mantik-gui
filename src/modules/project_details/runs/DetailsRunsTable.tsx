import { Add, CancelOutlined, FileDownloadOutlined } from '@mui/icons-material'
import BookmarkAddIcon from '@mui/icons-material/BookmarkAdd'
import BookmarkRemoveIcon from '@mui/icons-material/BookmarkRemove'
import { Button, useTheme } from '@mui/material'
import Link from 'next/link'
import { useEffect, useState } from 'react'
import { CancelDialogLayout } from '../../../components/CancelDialogLayout'
import CopyToClipboard from '../../../components/CopyToClipboard'
import { DataStateIndicator } from '../../../components/DataStateIndicator'
import { DeleteDialogLayout } from '../../../components/DeleteDialogLayout'
import { DetailsDialogLayout } from '../../../components/DetailsDialogLayout'
import { RegisterModelDialog } from '../../../components/RegisterModelDialog'
import { Spacing } from '../../../components/Spacing'
import { StatusType } from '../../../components/TablesLayout/statusType'
import {
    TableColumn,
    TablesLayout,
    TablesLayoutRow,
} from '../../../components/TablesLayout/TablesLayout'
import { PAGE_LENGTH_OPTIONS } from '../../../constants'
import { useProject } from '../../../context/ProjectContext'
import { renderApiErrorMessage } from '../../../errors'
import { downloadFileInBackground, removeHyphens } from '../../../helpers'
import {
    useProjectsProjectIdRunsRunIdArtifactsGetProjectsProjectIdRunsRunIdArtifactsGet as useDownloadArtifacts,
    PresignedUrl,
    Run,
    RunStatus,
    useDeleteTrainedModelProjectsProjectIdModelsTrainedModelIdDelete,
    useProjectsProjectIdRunsGetProjectsProjectIdRunsGet,
    useProjectsProjectIdRunsRunIdCancelPutProjectsProjectIdRunsRunIdCancelPut,
    useProjectsProjectIdRunsRunIdDeleteProjectsProjectIdRunsRunIdDelete,
} from '../../../queries'
import ReRun from '../../../themes/icon/ReRun'
import { MenuItemProps } from '../../../types/MenuItemProps'
import { StatusProps } from '../../../types/statusProps'
import { UserRole } from '../../../types/UserRole'
import { RenderElement } from '../../../utils/renderResourceActions'
import { parseUserRole } from '../../../utils/userRole'
import { DetailsToolbar } from '../overview/ProjectDetailsToolbar'
import { isConnectionLocalForRun } from './connection'
import { DownloadRunDialog } from './DownloadRunDialog'
import { RenameRunDialog } from './RenameRunDialog'
import RunDetailViewTab from './RunDetailViewTab'
import { RunDialog } from './RunDialog'
import { RunInfrastructureCell } from './RunInfrastructureCell'
import RunStatusCustomCell from './RunStatusCustomCell'
import { RunCodeCell } from './RunCodeCell'
import { RunDataCell } from './RunDataCell'

type ExtraRowData = { isConnectionLocal: boolean; isANotebookRun?: boolean }

export const DetailsRunsTable = () => {
    const theme = useTheme()
    const { projectId } = useProject()
    const [editItem, setEditItem] = useState<Run | null>(null)
    const [rowsPerPage, setRowsPerPage] = useState<number>(
        PAGE_LENGTH_OPTIONS[0]
    )
    const [page, setPage] = useState(0)
    const handleRowsChange = (
        type: 'rowsPerPage' | 'page',
        num: number
    ): void => {
        if (type === 'rowsPerPage') {
            setRowsPerPage(num)
        } else if (type === 'page') {
            setPage(num)
        }
    }

    const [dialogOpen, setDialogOpen] = useState<RenderElement | null>(null)
    const [resetErrorRunId, setResetErrorRunId] = useState({
        previouslyRefetched: '',
        recentlyRefetched: '',
    })
    const [currentRunStatus, setCurrentRunStatus] = useState<{
        [id: string]: StatusType
    }>({})

    const {
        data: runData,
        status,
        refetch,
        error,
    } = useProjectsProjectIdRunsGetProjectsProjectIdRunsGet(projectId, {
        pagelength: rowsPerPage,
        startindex: page * rowsPerPage,
    })
    // generates the table data directly from the API response
    const tableData =
        runData?.runs?.map((run) => {
            const isConnectionLocal = isConnectionLocalForRun(run)
            const data = {
                'Created At': { data: run.createdAt },
                ID: {
                    data: run.runId,
                    hiddenTableColumn: true,
                    component: (
                        <CopyToClipboard title={run.runId} data={run.runId} />
                    ),
                },
                Name: {
                    data: run.name,
                    component: (
                        // Note: Currently cannot link directly to experiment
                        // This needs to be done in another ticket
                        <Link
                            href={`/mlflow/project/${projectId}/experiments/${
                                run.experimentRepository.mlflowExperimentId
                            }/runs/${removeHyphens(run.runId)}`}
                            key={'mlflow'}
                            legacyBehavior
                        >
                            {run.name}
                        </Link>
                    ),
                },
                'Experiment Repository': {
                    data: String(run.experimentRepository.name),
                },
                Code: {
                    data:
                        run.modelRepository.codeRepository
                            ?.codeRepositoryName || '',
                    hiddenTableColumn: true,
                    component: <RunCodeCell run={run} />,
                },
                Data: {
                    data: run.dataRepository,
                    hiddenTableColumn: true,
                    component: <RunDataCell run={run} />,
                },
                Infrastructure: {
                    data: run.infrastructure,
                    hiddenTableColumn: true,
                    component: (
                        <RunInfrastructureCell
                            infrastructure={run.infrastructure}
                        />
                    ),
                },
                'Start Time': { data: run.startTime },
                Connection: { data: run.connection?.connectionName },
                Status: {
                    data:
                        currentRunStatus[run.runId] ||
                        (run.status as StatusType) ||
                        StatusType.UNKNOWN,
                    type: 'status',
                    component: (
                        <RunStatusCustomCell
                            userRole={parseUserRole(runData.userRole)}
                            runId={run.runId}
                            runStatus={
                                currentRunStatus[run.runId] ||
                                (run.status as StatusType) ||
                                StatusType.UNKNOWN
                            }
                            resetError={
                                resetErrorRunId.previouslyRefetched ===
                                run.runId
                            }
                            setCurrentRunStatus={setCurrentRunStatus}
                            setResetErrorRunId={setResetErrorRunId}
                        />
                    ),
                },
                SavedModel: {
                    hiddenTableColumn: true,
                    data: run.savedModel?.name,
                },
            }
            const extra = {
                isConnectionLocal,
                isANotebookRun: Boolean(run.notebookSource),
            }
            return { data, extra }
        }) ?? []
    const [runStatus, setRunStatus] = useState<StatusProps>({
        status: false,
        type: '',
        text: '',
    })
    const [selectedData, setSelectedData] = useState({
        name: '',
        projectId,
        runId: '',
    })
    const [selectedModel, setSelectedModel] = useState({
        projectId,
        modelId: '',
        modelName: '',
        runName: '',
    })
    const [generateURLDownload, setGenerateURLDownload] = useState(false)
    const handleDataRefetch = () => {
        // eslint-disable-next-line @typescript-eslint/no-floating-promises
        refetch() // Refresh the data
    }

    const userRole = parseUserRole(runData?.userRole)

    function handleSelectedData(idx: number, type: RenderElement) {
        const row = runData && runData.runs ? runData.runs[idx] : null
        if (!!row) {
            setSelectedData((prev) => ({
                ...prev,
                name: String(row.name),
                runId: String(row.runId),
            }))
        }
        if (type === RenderElement.DOWNLOAD_ARTIFACTS) {
            setGenerateURLDownload(true)
        } else setDialogOpen(type)
    }

    function handleDeleteModel(idx: number, type: RenderElement) {
        const row = runData && runData.runs ? runData.runs[idx] : null
        if (!!row) {
            setSelectedModel((prev) => ({
                ...prev,
                modelId: String(row.savedModel?.modelId),
                modelName: String(row.savedModel?.name),
                runName: String(row.name),
            }))
        }
        setDialogOpen(type)
    }
    function handleDeleteSubmit() {
        const runId = selectedData.runId
        mutate(
            { projectId, runId },
            {
                // Optional: You can provide extra configuration here if needed
                // e.g., onSuccess, onError, onSettled, etc.
            }
        )
    }
    function handleDeleteModelSubmit() {
        const modelId = selectedModel?.modelId
        removeModel({ projectId, modelId })
    }
    function handleOpenDialog(idx = 0, type: RenderElement) {
        const row = runData && runData.runs ? runData.runs[idx] : null
        if (!!row) {
            setEditItem(row)
        }
        setDialogOpen(type)
    }
    function handleCloseDialog() {
        setDialogOpen(null)
        setEditItem(null)
        setRunStatus({ status: false, type: '', text: '' })
    }

    function handleCancelSubmit() {
        const runId = selectedData.runId
        cancelRunMutation.mutate({
            projectId,
            runId,
        })
    }
    const handleDownloadArtifacts = async ({ url }: PresignedUrl) => {
        setGenerateURLDownload(false)
        try {
            downloadFileInBackground(url)
        } catch (error) {
            console.error('Error downloading file:', error)
        }
    }
    const {
        data,
        isSuccess,
        isError,
        error: downloadArtifactsError,
    } = useDownloadArtifacts(projectId, selectedData.runId, {
        query: {
            enabled: generateURLDownload,
            queryKey: ['runId', selectedData.runId],
        },
    })

    useEffect(() => {
        if (isSuccess) {
            handleDownloadArtifacts(data).catch((error) => console.error(error))
        }
        if (isError) {
            setGenerateURLDownload(false)
            console.error({ downloadArtifactsError, id: selectedData.runId })
        }
    }, [data, downloadArtifactsError, isError, isSuccess, selectedData.runId])

    const { mutate, isPending: isLoading } =
        useProjectsProjectIdRunsRunIdDeleteProjectsProjectIdRunsRunIdDelete({
            mutation: {
                onSuccess: () => {
                    setRunStatus({
                        status: true,
                        type: 'success',
                        text: 'Run was deleted successfully!',
                    })
                    handleDataRefetch()
                },
                onError: (error) => {
                    setRunStatus({
                        status: true,
                        type: 'error',
                        text: renderApiErrorMessage(error),
                    })
                },
            },
        })
    const { mutate: removeModel, isPending: removeModalIsLoading } =
        useDeleteTrainedModelProjectsProjectIdModelsTrainedModelIdDelete({
            mutation: {
                onSuccess: () => {
                    setRunStatus({
                        status: true,
                        type: 'success',
                        text: 'Model was unregistered successfully!',
                    })
                    handleDataRefetch()
                },
                onError: (error) => {
                    setRunStatus({
                        status: true,
                        type: 'error',
                        text: renderApiErrorMessage(error),
                    })
                },
            },
        })
    const cancelRunMutation =
        useProjectsProjectIdRunsRunIdCancelPutProjectsProjectIdRunsRunIdCancelPut(
            {
                mutation: {
                    onSuccess: () => {
                        setRunStatus({
                            status: true,
                            type: 'success',
                            text: 'Run was cancelled successfully!',
                        })
                        setCurrentRunStatus((prev) => ({
                            ...prev,
                            [selectedData.runId]: StatusType.KILLED,
                        }))
                    },
                    onError: (error) => {
                        setRunStatus({
                            status: true,
                            type: 'error',
                            text: renderApiErrorMessage(error),
                        })
                    },
                },
            }
        )

    const menuItemsForRow = (row: TablesLayoutRow<ExtraRowData>) => {
        const runStatusData = row.data[TableColumn.STATUS]?.data
        const savedModelType = row.data[TableColumn.SAVEDMODEl]?.data
            ? 'SAVED'
            : 'UNSAVED'
        const menuItems: MenuItemProps[] = []
        if (
            [RunStatus.RUNNING, RunStatus.FINISHED, RunStatus.KILLED].includes(
                runStatusData
            )
        ) {
            menuItems.push({
                type: RenderElement.DOWNLOAD_ARTIFACTS,
                action:
                    userRole >= UserRole.Researcher
                        ? handleSelectedData
                        : undefined,
                icon: <FileDownloadOutlined fontSize="small" />,
                tooltipText: 'Download artifacts',
            })
        }
        if (
            [RunStatus.RUNNING, RunStatus.SCHEDULED].includes(runStatusData) &&
            !row.extra.isConnectionLocal
        )
            menuItems.push({
                type: RenderElement.CANCEL,
                action:
                    userRole >= UserRole.Owner ? handleSelectedData : undefined,
                icon: <CancelOutlined fontSize="small" />,
                tooltipText: 'Cancel run',
            })
        if (!row.extra.isANotebookRun)
            menuItems.push({
                type: RenderElement.RERUN,
                action:
                    userRole >= UserRole.Researcher
                        ? handleOpenDialog
                        : undefined,
                icon: <ReRun />,
                tooltipText: 'Re-submit run',
            })
        if (!row.extra.isConnectionLocal)
            menuItems.push({
                type: RenderElement.DOWNLOAD,
                action:
                    userRole >= UserRole.Owner ? handleOpenDialog : undefined,
                icon: <FileDownloadOutlined fontSize="small" />,
                tooltipText: 'Download run directory',
            })
        if (
            runStatusData === RunStatus.FINISHED &&
            savedModelType === 'UNSAVED'
        )
            menuItems.push({
                type: RenderElement.REGISTER_MODEL,
                action:
                    userRole >= UserRole.Researcher
                        ? handleOpenDialog
                        : undefined,
                icon: <BookmarkAddIcon fontSize="small" />,
                tooltipText: 'Register Model',
            })
        if (runStatusData === RunStatus.FINISHED && savedModelType === 'SAVED')
            menuItems.push({
                type: RenderElement.DELETE_MODEL,
                action:
                    userRole >= UserRole.Researcher
                        ? handleDeleteModel
                        : undefined,
                icon: <BookmarkRemoveIcon fontSize="small" />,
                tooltipText: 'Unregister Model',
            })
        return menuItems
    }

    return (
        <DataStateIndicator
            status={status}
            text="Loading Runs..."
            usePaper
            errorMessage={
                error?.response?.status === 401
                    ? "The project you're trying to access is private and you don't have the required permissions!"
                    : 'Error'
            }
        >
            <DetailsToolbar
                title="Runs"
                tool={
                    userRole >= UserRole.Researcher ? (
                        <Button
                            variant="text"
                            onClick={() => setDialogOpen(RenderElement.ADD)}
                        >
                            <Add></Add>Add
                        </Button>
                    ) : (
                        <></>
                    )
                }
            />
            <Spacing value={theme.spacing(1)}></Spacing>
            <TablesLayout<ExtraRowData>
                data={tableData}
                action={{
                    edit:
                        userRole >= UserRole.Owner
                            ? handleSelectedData
                            : undefined,
                    delete:
                        userRole >= UserRole.Researcher
                            ? handleSelectedData
                            : undefined,
                }}
                menuItemsForRow={menuItemsForRow}
                page={{
                    rowsPerPage: rowsPerPage,
                    page: page,
                    totalRecords: runData?.totalRecords || 0,
                    handleRowsChange,
                }}
                detailsComponent={(
                    currentRow,
                    detailsDialogOpen,
                    handleDetailsDialog
                ) => (
                    <DetailsDialogLayout
                        open={detailsDialogOpen}
                        setOpen={handleDetailsDialog}
                        title={'Details'}
                        toggleWidth={true}
                    >
                        <RunDetailViewTab
                            data={
                                currentRow !== null
                                    ? tableData[currentRow].data
                                    : {}
                            }
                            currentRow={currentRow}
                            runData={
                                runData?.runs?.length && currentRow !== null
                                    ? runData.runs[currentRow]
                                    : null
                            }
                        />
                    </DetailsDialogLayout>
                )}
            />
            {(dialogOpen === RenderElement.ADD ||
                dialogOpen === RenderElement.RERUN) && (
                <RunDialog
                    open={true}
                    setOpen={setDialogOpen}
                    refetchData={handleDataRefetch}
                    editData={editItem}
                    setEditItem={setEditItem}
                />
            )}
            <DeleteDialogLayout
                open={dialogOpen === RenderElement.DELETE}
                title="Delete Run"
                text={
                    <>
                        Are you sure you want to delete run{' '}
                        <strong>{selectedData?.name}</strong>?
                    </>
                }
                warning="All tracked parameters, metrics, artifacts, models, etc., will be deleted!"
                handleSubmit={handleDeleteSubmit}
                handleClose={handleCloseDialog}
                isLoading={isLoading}
                status={runStatus}
            />
            <CancelDialogLayout
                open={dialogOpen === RenderElement.CANCEL}
                title="Cancel Run Execution"
                text={
                    <>
                        Are you sure you want to cancel run{' '}
                        <strong>{selectedData?.name}</strong>?
                    </>
                }
                warning="This will kill all running processes launched in this run!"
                handleSubmit={handleCancelSubmit}
                handleClose={handleCloseDialog}
                data={selectedData}
                isLoading={cancelRunMutation.isPending}
                status={runStatus}
            />

            <DownloadRunDialog
                open={dialogOpen === RenderElement.DOWNLOAD}
                setOpen={handleCloseDialog}
                runData={editItem}
            />
            {editItem && (
                <RegisterModelDialog
                    open={dialogOpen === RenderElement.REGISTER_MODEL}
                    setOpen={handleCloseDialog}
                    projectId={projectId}
                    runData={editItem}
                    refetch={refetch}
                />
            )}
            <DeleteDialogLayout
                open={dialogOpen === RenderElement.DELETE_MODEL}
                title="Unregister Model"
                text={
                    <>
                        Are you sure you want to unregister model{' '}
                        <strong>{selectedModel?.modelName}</strong> of run{' '}
                        <strong>{selectedModel?.runName}</strong>?
                    </>
                }
                handleSubmit={handleDeleteModelSubmit}
                handleClose={handleCloseDialog}
                isLoading={removeModalIsLoading}
                status={runStatus}
                deleteButtonLabel="Unregister"
            />
            {selectedData && (
                <RenameRunDialog
                    open={dialogOpen === RenderElement.EDIT}
                    setOpen={handleCloseDialog}
                    projectId={projectId}
                    selectedRunData={selectedData}
                    refetch={refetch}
                />
            )}
        </DataStateIndicator>
    )
}
