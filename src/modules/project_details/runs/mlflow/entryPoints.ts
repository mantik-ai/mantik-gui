import { EntryPoints } from '../types/runDialogProps'

// on successfull parsing of yaml file
export function getFromMlprojectConfig(data: {
    [key: string]: any
}): EntryPoints | null | undefined {
    if (!data || typeof data !== 'object') return

    const entryPoints: EntryPoints | null = []

    for (const [key, value] of Object.entries(data)) {
        const params = (value as { parameters?: unknown }).parameters
        const flattenedParams = []
        if (params && typeof params === 'object') {
            for (const [paramKey, paramValue] of Object.entries(params)) {
                let paramString = `(type: ${paramValue})`
                let hasDefault = false

                if (typeof paramValue === 'object') {
                    paramString = Object.entries(paramValue)
                        .map((entry) => `${entry[0]}: ${entry[1]}`)
                        .join(', ')
                    paramString = `(${paramString})`
                    hasDefault = 'default' in paramValue
                }

                flattenedParams.push({
                    name: paramKey,
                    label: `${paramKey} ${paramString}`,
                    value: null,
                    required: !hasDefault,
                })
            }
        }

        entryPoints.push({
            entryPoint: key,
            parameters: flattenedParams,
        })
    }

    return entryPoints
}
export function prepareParamsForRequest(
    entryPoints: EntryPoints,
    index: number
) {
    const parameters: {
        [key: string]: string
    } = {}

    if (!entryPoints.length) {
        return parameters
    } else if (!!entryPoints) {
        entryPoints[index].parameters
            .filter((parameter) => parameter.value !== null)
            .forEach(
                (parameter) =>
                    (parameters[parameter.name] = parameter.value as string)
            )
    }

    return parameters
}
