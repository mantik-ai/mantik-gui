import Table from '@mui/material/Table'
import TableBody from '@mui/material/TableBody'
import TableCell from '@mui/material/TableCell'
import TableContainer from '@mui/material/TableContainer'
import TableRow from '@mui/material/TableRow'
import CopyToClipboard from '../../../components/CopyToClipboard'

export function RunRepositoryCell({
    name,
    id,
    uri,
    commitHash,
    branch,
}: {
    name?: string
    id?: string
    uri?: string
    commitHash?: string
    branch?: string
}) {
    return (
        <TableContainer>
            Repository
            <Table size="small">
                <TableBody>
                    {name && (
                        <TableRow>
                            <TableCell>Name</TableCell>
                            <TableCell>{name}</TableCell>
                        </TableRow>
                    )}
                    {id && (
                        <TableRow>
                            <TableCell>ID</TableCell>
                            <TableCell>
                                <CopyToClipboard title={id} data={id} />
                            </TableCell>
                        </TableRow>
                    )}
                    {uri && (
                        <TableRow>
                            <TableCell>URI</TableCell>
                            <TableCell>{uri}</TableCell>
                        </TableRow>
                    )}
                    {branch && (
                        <TableRow>
                            <TableCell>Branch</TableCell>
                            <TableCell>{branch}</TableCell>
                        </TableRow>
                    )}
                    {commitHash && (
                        <TableRow>
                            <TableCell>Commit Hash</TableCell>
                            <TableCell>{commitHash}</TableCell>
                        </TableRow>
                    )}
                </TableBody>
            </Table>
        </TableContainer>
    )
}
