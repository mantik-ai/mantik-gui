import {
    FormControl,
    FormGroup,
    TextField,
    Tooltip,
    Typography,
} from '@mui/material'
import { FormDialogLayout } from '../../../components/FormDialogLayout'
import { useRef, useState } from 'react'
import { StatusProps } from '../../../types/statusProps'
import { Run } from '../../../queries'
import { useRouter } from 'next/router'
import { renderApiErrorMessage } from '../../../errors'
import { mantikApi } from '../../auth/axios'
import { useSession } from 'next-auth/react'
import { Help } from '@mui/icons-material'

interface DownloadRunDialogProps {
    open: boolean
    setOpen: () => void
    runData: Run | null
}
export const DownloadRunDialog = ({
    runData,
    open,
    setOpen,
}: DownloadRunDialogProps) => {
    const [runStatus, setRunStatus] = useState<StatusProps>({
        status: false,
        type: '',
        text: '',
    })

    const router = useRouter()
    const { id } = router.query
    const projectId = String(id)
    const filePath = useRef<HTMLInputElement>(null)
    const session = useSession()
    const [loading, setLoading] = useState(false)

    function handleKeyDown(event: any) {
        if (event.key === 'Enter') {
            handleDownloadSubmit()
        }
    }

    async function handleDownloadSubmit() {
        setLoading(true)
        const response = await fetchFileOrFolder()
        setLoading(!!response?.status)
    }

    function processBlob(data: any) {
        if (!runData) return
        const blob = new Blob([data])
        const pathArray = filePath?.current?.value
            ? filePath.current.value.split('/')
            : null
        const isFile = data.type === 'application/octet-stream'
        const fileName = pathArray
            ? pathArray[pathArray.length - 1]
            : runData?.runId
        const url = window.URL.createObjectURL(blob)
        const a = document.createElement('a')
        a.style.display = 'none'
        a.href = url
        a.download = isFile ? fileName : `${fileName}.zip`
        document.body.appendChild(a)
        a.click()
        window.URL.revokeObjectURL(url)
    }

    async function fetchFileOrFolder() {
        const params = filePath?.current?.value
            ? {
                  path: filePath.current.value,
              }
            : null
        try {
            const response = await mantikApi.get(
                `/projects/${projectId}/runs/${runData?.runId}/download`,
                {
                    params,
                    responseType: 'blob',
                    headers: {
                        Authorization: `Bearer ${session.data?.user.accessToken}`,
                        Accept: 'application/json',
                    },
                }
            )
            if (response.status === 200) {
                processBlob(response.data)
                setRunStatus({
                    status: true,
                    type: 'success',
                    text: 'Run files were downloaded successfully!',
                })
            }
            return response
        } catch (err: any) {
            setRunStatus({
                status: true,
                type: 'error',
                text: renderApiErrorMessage(err),
            })
        }
    }

    function handleClose(): void {
        setOpen()
        setRunStatus({
            status: false,
            type: '',
            text: '',
        })
        setLoading(false)
    }

    function handlePathChange() {
        if (!runStatus.status) return
        setRunStatus({
            status: false,
            type: '',
            text: '',
        })
    }

    return (
        <FormDialogLayout
            open={open}
            handleClose={handleClose}
            content=""
            handleSubmit={handleDownloadSubmit}
            handleSubmitText="Download"
            isEdit={false}
            title="Download file or folder from run directory"
            status={runStatus}
            formDataState={{
                runStatus,
                error: false,
            }}
            loading={loading}
        >
            <FormGroup>
                <FormControl className="textfield-with-tooltip">
                    <TextField
                        key="path"
                        name="path"
                        id="path"
                        label="Path to file or folder"
                        size="small"
                        defaultValue={''}
                        error={false}
                        inputRef={filePath}
                        onKeyDown={handleKeyDown}
                        onChange={handlePathChange}
                    />
                    <Tooltip
                        title={
                            <Typography sx={{ pb: 1 }}>
                                File or folder path is assumed to be relative to
                                the run directory.
                                <br />
                                For folders, the entire folder is downloaded.
                                <br />
                                <strong>
                                    If empty, the entire run directory will be
                                    downloaded.
                                </strong>
                            </Typography>
                        }
                    >
                        <Help color="info" />
                    </Tooltip>
                </FormControl>
            </FormGroup>
        </FormDialogLayout>
    )
}
