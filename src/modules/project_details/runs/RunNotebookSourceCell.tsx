import Table from '@mui/material/Table'
import TableBody from '@mui/material/TableBody'
import TableCell from '@mui/material/TableCell'
import TableContainer from '@mui/material/TableContainer'
import TableRow from '@mui/material/TableRow'
import CopyToClipboard from '../../../components/CopyToClipboard'
import { NoteBookSource } from '../../../queries/models'

export function RunNotebookSourceCell({
    notebookSource,
}: {
    notebookSource: NoteBookSource
}) {
    return (
        <TableContainer>
            Notebook Source
            <Table size="small">
                <colgroup>
                    <col style={{ width: '15%' }} />
                </colgroup>
                <TableBody>
                    {notebookSource.location && (
                        <>
                            {notebookSource.provider === 'Jupyter' && (
                                <TableRow>
                                    <TableCell>Path</TableCell>
                                    <TableCell>
                                        <CopyToClipboard
                                            title={notebookSource.location}
                                            data={notebookSource.location}
                                        />
                                    </TableCell>
                                </TableRow>
                            )}
                            {notebookSource.provider === 'Colab' && (
                                <TableRow>
                                    <TableCell>Url</TableCell>
                                    <TableCell>
                                        <a
                                            href={notebookSource.location}
                                            target="_blank"
                                            rel="noopener noreferrer"
                                        >
                                            {notebookSource.location}
                                        </a>
                                    </TableCell>
                                </TableRow>
                            )}
                        </>
                    )}
                    {notebookSource.version && (
                        <TableRow>
                            <TableCell>Version</TableCell>
                            <TableCell>
                                <CopyToClipboard
                                    title={notebookSource.version}
                                    data={notebookSource.version}
                                />
                            </TableCell>
                        </TableRow>
                    )}
                </TableBody>
            </Table>
        </TableContainer>
    )
}
