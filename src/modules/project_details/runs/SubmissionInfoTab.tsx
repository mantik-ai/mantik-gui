import React from 'react'
import { List, ListItem, ListItemText, useTheme } from '@mui/material'
import {
    Run,
    useProjectsProjectIdRunsRunIdInfoGetProjectsProjectIdRunsRunIdInfoGet,
} from '../../../queries'
import { useRouter } from 'next/router'
import { DataStateIndicator } from '../../../components/DataStateIndicator'
import JSONPretty from 'react-json-pretty'
import { renderApiErrorMessage } from '../../../errors'
import { customJSONPrettyThemeWithBreaks as JSONWithLineBreaks } from '../../../themes/customJSONPrettyTheme'
import { RefreshButton } from '../../../components/RefreshButton'

export interface DetailsDialogListProps {
    runData: Run | undefined | null
}
export const SubmissionInfoTab = ({ runData }: DetailsDialogListProps) => {
    const theme = useTheme()
    const router = useRouter()
    const { id } = router.query
    const { data, status, refetch, error, isRefetching, isSuccess } =
        useProjectsProjectIdRunsRunIdInfoGetProjectsProjectIdRunsRunIdInfoGet(
            id as string,
            runData?.runId as string
        )

    // Convert the run logs array to a string for JSON pretty printing
    const transform = ([key, value]: [string, any]) => {
        switch (key) {
            case 'logs':
                return [key, value.join('\n')]
            default:
                return [key, value]
        }
    }

    return (
        <div>
            <RefreshButton loading={isRefetching} onClick={refetch} />
            <DataStateIndicator
                status={status}
                error={error ? renderApiErrorMessage(error) : undefined}
                text="Loading Data..."
            >
                {isSuccess && (
                    <List>
                        {Object.entries(data)
                            .map(transform)
                            .map(([label, value], idx) => (
                                <ListItem
                                    key={idx}
                                    sx={{
                                        display: 'block',
                                        backgroundColor:
                                            idx % 2 === 0
                                                ? theme.palette.grey[200]
                                                : 'transparent',
                                        padding: '0.5em 0.75em',
                                    }}
                                >
                                    <ListItemText primary={label} />
                                    <JSONPretty
                                        theme={JSONWithLineBreaks}
                                        id="json-pretty"
                                        data={value}
                                    ></JSONPretty>
                                </ListItem>
                            ))}
                    </List>
                )}
            </DataStateIndicator>
        </div>
    )
}
