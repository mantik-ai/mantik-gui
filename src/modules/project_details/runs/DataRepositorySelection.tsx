import {
    Box,
    FormControl,
    FormHelperText,
    InputLabel,
    ListItemText,
    MenuItem,
    Select,
    SelectChangeEvent,
    Skeleton,
    Typography,
    useTheme,
} from '@mui/material'
import { useRouter } from 'next/router'
import { PlatformIcon } from '../../../components/PlatformIcon'
import { Spacing } from '../../../components/Spacing'
import { getIndex } from '../../../helpers'
import {
    DataRepository,
    useProjectsProjectIdDataGetProjectsProjectIdDataGet,
} from '../../../queries'

interface DataRepositorySelectionProps {
    currentDataRepository?: DataRepository | null
    handleRepoChange: (repo: DataRepository) => void
    error?: boolean
}

export const DataRepositorySelection = ({
    currentDataRepository = null,
    handleRepoChange,
    error,
}: DataRepositorySelectionProps) => {
    const theme = useTheme()
    const router = useRouter()
    const { id } = router.query
    const projectId: string =
        typeof id === 'string' ? id : id?.length ? id[0] : ''

    const {
        data,
        isPending: isLoading,
        isSuccess,
    } = useProjectsProjectIdDataGetProjectsProjectIdDataGet(projectId)

    function handleDataRepositoryChange(e: SelectChangeEvent): void {
        if (!data?.dataRepositories) return
        const { value } = e.target
        const idx = getIndex(value, 'dataRepositoryId', data.dataRepositories)
        handleRepoChange(data.dataRepositories[idx])
    }

    const platformIconProps = {
        sx: {
            fontSize: '1em',
            mr: 1,
            color: 'grey.500',
        },
    }
    const formatValue = (item: DataRepository | null): string =>
        item ? `${item.dataRepositoryName} (${item.uri})` : ''

    return (
        <FormControl error={error}>
            <InputLabel id="dataRepositories">Data Repository</InputLabel>
            {isLoading && (
                <Skeleton variant="rounded" sx={{ height: '3.5em' }}>
                    <input
                        type="hidden"
                        id="dataRepositories"
                        value={currentDataRepository?.dataRepositoryId || ''}
                        required
                    />
                </Skeleton>
            )}
            {isSuccess && (
                <Select
                    labelId="dataRepositories"
                    id="dataRepositories"
                    label="Data Repository"
                    value={currentDataRepository?.dataRepositoryId || ''}
                    onChange={handleDataRepositoryChange}
                    data-testid="dataRepositorySelector"
                >
                    {data?.dataRepositories?.length ? (
                        data.dataRepositories.map((item, index) => (
                            <MenuItem key={index} value={item.dataRepositoryId}>
                                <Box
                                    sx={{
                                        fontSize: '1.25em',
                                        display: 'flex',
                                        alignItems: 'center',
                                        margin: '-0.125em 0',
                                    }}
                                >
                                    <PlatformIcon
                                        platform={item?.platform}
                                        iconProps={platformIconProps}
                                    />
                                    <ListItemText primary={formatValue(item)} />
                                </Box>
                            </MenuItem>
                        ))
                    ) : (
                        <MenuItem value="No data repositories found">
                            No data repositories found
                        </MenuItem>
                    )}
                </Select>
            )}
            {error && (
                <FormHelperText>
                    Data repository could not be loaded
                </FormHelperText>
            )}
            <Spacing value={theme.spacing(1)} />
            {!data?.dataRepositories?.length && (
                <Typography variant="inherit" color="error">
                    {`Cannot find a data repository. Please go to Projects > Data > Add`}
                </Typography>
            )}
        </FormControl>
    )
}
