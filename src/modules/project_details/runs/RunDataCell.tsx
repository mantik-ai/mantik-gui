import { NULL_VALUE_REPRESENTATION } from '../../../constants'
import { Run } from '../../../queries'
import { RunRepositoryCell } from './RunRepositoryCell'

export function RunDataCell({ run }: { run: Run }) {
    if (!run.dataRepository) {
        return <>{NULL_VALUE_REPRESENTATION}</>
    }

    return (
        <RunRepositoryCell
            name={run.dataRepository?.dataRepositoryName}
            id={run.dataRepository?.dataRepositoryId}
            uri={run.dataRepository?.uri}
            branch={run.dataBranch}
            commitHash={run.dataCommit}
        />
    )
}
