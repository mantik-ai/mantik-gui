import type { Run } from '../../../queries'
import { NullableWhereOptional } from '../../../types/utilityTypes'

export function isConnectionLocalForRun(
    run: NullableWhereOptional<Run>
): boolean {
    return !run.connection
}
