import {
    Cancel,
    CheckCircle,
    Error,
    Help,
    Refresh as RefreshIcon,
    Schedule,
    Timelapse,
} from '@mui/icons-material'
import InfoIcon from '@mui/icons-material/Info'
import { Box, Chip, ChipTypeMap, Tooltip } from '@mui/material'
import { useSession } from 'next-auth/react'
import { useRouter } from 'next/router'
import React, {
    Dispatch,
    memo,
    SetStateAction,
    useEffect,
    useState,
} from 'react'
import { ButtonWithLoadingStatus } from '../../../components/ButtonWithLoadingStatus'
import { StatusType } from '../../../components/TablesLayout/statusType'
import { renderApiErrorMessage } from '../../../errors'
import { StatusProps } from '../../../types/statusProps'
import { UserRole } from '../../../types/UserRole'
import { mantikApi } from '../../auth/axios'

export interface RunStatusCustomCellProps {
    userRole: UserRole
    runId: string
    runStatus: StatusType
    setCurrentRunStatus: Dispatch<SetStateAction<{ [id: string]: StatusType }>>
    resetError?: boolean
    setResetErrorRunId: Dispatch<
        SetStateAction<{
            recentlyRefetched: string
            previouslyRefetched: string
        }>
    >
}

// gets the chip color and text depending on the status
const getChip = (data: StatusType) => {
    const { color, iconComponent } = getStatusChipAttributes(data)

    const chip = (
        <Chip
            component="div"
            icon={iconComponent}
            label={data}
            color={color}
            style={{
                display: 'flex',
                width: '10em',
                justifyContent: 'start',
            }}
        />
    )
    function getStatusChipAttributes(data: StatusType): {
        color: ChipTypeMap['props']['color']
        iconComponent: React.ReactElement
    } {
        switch (data) {
            case StatusType.SCHEDULED:
                return { color: 'schedule', iconComponent: <Schedule /> }
            case StatusType.RUNNING:
                return { color: 'due', iconComponent: <Timelapse /> }
            case StatusType.FINISHED:
                return { color: 'success', iconComponent: <CheckCircle /> }
            case StatusType.FAILED:
                return { color: 'error', iconComponent: <Error /> }
            case StatusType.KILLED:
                return { color: 'warning', iconComponent: <Cancel /> }
            default:
                return { color: 'default', iconComponent: <Help /> }
        }
    }
    return chip
}

// renders the refetch icon and the chip
export const RunStatusComponent = ({
    data,
    handleRefreshClick,
    userRole,
    isRefetching,
}: {
    data: StatusType
    handleRefreshClick: Function
    userRole: UserRole
    isRefetching: boolean
}) => {
    const chip = getChip(data)

    return (
        <Box
            component="div"
            style={{
                width: '12.5em',
                position: 'relative',
                display: 'flex',
                alignItems: 'center',
                paddingRight: '2em',
            }}
        >
            <ButtonWithLoadingStatus margin={0} loading={isRefetching}>
                {chip}
            </ButtonWithLoadingStatus>
            {userRole >= UserRole.Researcher && (
                <Tooltip title={'Refresh run status'}>
                    <RefreshIcon
                        onClick={() => handleRefreshClick()}
                        style={{
                            width: '1.5em',
                            position: 'absolute', // prevents jumping colummn width when icon appears or disappears
                            right: 0,
                            cursor: isRefetching ? 'default' : 'pointer',
                            opacity: isRefetching ? 0.5 : 1,
                            transform: `rotate(${isRefetching ? 0 : -360}deg)`,
                            margin: '0 0.4em 0 0.25em',
                            transition: isRefetching
                                ? 'transform 0.5s ease-in-out'
                                : 'none',
                        }}
                    />
                </Tooltip>
            )}
        </Box>
    )
}

// does the api call, sets loading state and error state, renders error message if error
// memoized to prevent re-rendering when refetch icon is clicked
const MemorizedRunStatusDynamic = memo(function RunStatusDynamic({
    userRole,
    runId,
    runStatus,
    setCurrentRunStatus,
    resetError,
    setResetErrorRunId,
}: RunStatusCustomCellProps) {
    const router = useRouter()
    const { id } = router.query
    const session = useSession()

    const [errorStatus, setErrorStatus] = useState<StatusProps>({
        status: false,
        type: '',
        text: '',
    })
    const [loading, setLoading] = useState(false)

    function handleRefreshClick(runId: string) {
        fetchStatus(runId)
        setResetErrorRunId((prev) => ({
            previouslyRefetched: prev.recentlyRefetched,
            recentlyRefetched: runId,
        }))
    }

    useEffect(() => {
        if (resetError) {
            setErrorStatus({
                status: false,
                type: '',
                text: '',
            })
        }
    }, [resetError])

    async function fetchStatus(runId: string) {
        setLoading(true)
        try {
            const response = await mantikApi.get(
                `/projects/${id}/runs/${runId}/status`,
                {
                    headers: {
                        Authorization: `Bearer ${session.data?.user.accessToken}`,
                        Accept: 'application/json',
                    },
                }
            )
            if (response.status === 200) {
                setLoading(false)
                setCurrentRunStatus((prevRunStatus) => ({
                    ...prevRunStatus,
                    [runId]: response.data as StatusType,
                }))
            }
            return response
        } catch (err: any) {
            console.log('Error: ', err)
            setErrorStatus({
                status: true,
                type: 'error',
                text: renderApiErrorMessage(err),
            })
        }
        setLoading(false)
    }

    return (
        <>
            <RunStatusComponent
                data={
                    errorStatus.type === 'error'
                        ? StatusType.UNKNOWN
                        : runStatus
                }
                handleRefreshClick={() => handleRefreshClick(runId)}
                userRole={userRole}
                isRefetching={loading}
            />
            {errorStatus.type === 'error' && (
                <div
                    style={{
                        display: 'flex',
                        gap: 3,
                        width: '13.75em',
                        margin: '0.5em',
                    }}
                >
                    <InfoIcon />
                    {errorStatus.text}
                </div>
            )}
        </>
    )
})

// renders the status as a plain chip component or as a dynamic component with a refresh icon depending on initial state
function RunStatusCustomCell(props: RunStatusCustomCellProps) {
    const chip = getChip(props.runStatus)

    const dynamicStatusTypes = [
        StatusType.UNKNOWN,
        StatusType.SCHEDULED,
        StatusType.RUNNING,
    ]
    return dynamicStatusTypes.includes(props.runStatus) ? (
        <MemorizedRunStatusDynamic {...props} />
    ) : (
        chip
    )
}

export default RunStatusCustomCell
