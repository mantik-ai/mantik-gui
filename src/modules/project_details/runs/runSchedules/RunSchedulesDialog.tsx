import {
    Autocomplete,
    FormControl,
    FormGroup,
    FormLabel,
    Link,
    List,
    ListItem,
    TextField,
    Typography,
    useTheme,
} from '@mui/material'
import { Spacing } from '../../../../components/Spacing'
import {
    ChangeEvent,
    SyntheticEvent,
    useEffect,
    useMemo,
    useState,
} from 'react'
import { useRouter } from 'next/router'
import { ConnectionSelection } from '../../../../components/forms/ConnectionSelection'
import { FormDialogLayout } from '../../../../components/FormDialogLayout'
import { StatusProps } from '../../../../types/statusProps'
import { useCronExpressionToReadableString } from '../../../../hooks/useCronExpressionToReadableString'
import { timezones } from '../../../../components/data/timezones'
import { DatePicker, LocalizationProvider } from '@mui/x-date-pickers'
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs'
import dayjs from 'dayjs'
import utc from 'dayjs/plugin/utc'
import timezone from 'dayjs/plugin/timezone'
import { RunSelection } from '../RunSelection'
import {
    Connection,
    RunSchedule,
    useProjectsProjectIdRunsSchedulesPostProjectsProjectIdRunSchedulesPost,
    useProjectsProjectIdRunsSchedulesRunScheduleIdPutProjectsProjectIdRunSchedulesRunScheduleIdPut,
} from '../../../../queries'
import { useSession } from 'next-auth/react'
import { checkCronValues } from './checkCronValues'
import { renderApiErrorMessage } from '../../../../errors'
import { generateTitleFromCamelCaseKey } from '../../../../helpers'
import { useFilterErrors } from '../../../../hooks/useFilterErrors'
import {
    createCronExpressionObj,
    cronValuesKeyEnum,
} from './createCronExpressionObj'

dayjs.extend(utc)
dayjs.extend(timezone)
dayjs.tz.setDefault('Europe/Berlin')

export interface RunSchedulesDialogProps {
    open: boolean
    setOpen: (isOpen: boolean) => void
    editItem?: RunSchedule
    setEditItem: (item?: RunSchedule) => void
    refetchData?: () => void
}

export interface cronExpressionObjectProps {
    minute: string
    hour: string
    dayOfMonth: string
    month: string
    dayOfWeek: string
}

interface runSchedulesDataProps {
    name: string
    ownerId: string
    runId: string
    connectionId: string
    computeBudgetAccount: string
    timeZone: string
    endDate: number
}

interface setRunSchedulesDataErrorProps {
    name: boolean
    ownerId: boolean
    runId: boolean
    connectionId: boolean
    computeBudgetAccount: boolean
    cronExpression: { [key: string]: boolean } //cronExpressionErrorProps
    timeZone: boolean
}

const defaultCronExpression = '0 0 1 * ?'

export const RunSchedulesDialog = (props: RunSchedulesDialogProps) => {
    const theme = useTheme()
    const router = useRouter()
    const { id } = router.query
    const projectId: string | null =
        typeof id === 'string' ? id : id?.length ? id[0] : null

    const session = useSession()
    const [status, setStatus] = useState<StatusProps>({
        status: false,
        type: '',
        text: '',
    })

    const [currentRun, setCurrentRun] = useState<{ [key: string]: any } | null>(
        null
    )
    const [currentConnection, setCurrentConnection] =
        useState<Connection | null>(props.editItem?.connection || null)

    const [cronValues, setCronValues] = useState<cronExpressionObjectProps>(
        createCronExpressionObj(defaultCronExpression)
    )

    const [cronValuesHint, setCronValuesHint] = useState({
        [cronValuesKeyEnum.dayOfMonth]: '',
        [cronValuesKeyEnum.dayOfWeek]: '',
    })

    const [runSchedulesData, setRunSchedulesData] =
        useState<runSchedulesDataProps>(
            createDefaultRunScheduleData(currentRun, currentConnection)
        )
    const memoizedEditData = useMemo(() => props.editItem, [props.editItem])

    useEffect(() => {
        if (memoizedEditData) {
            setRunSchedulesData(
                createDefaultRunScheduleData(
                    memoizedEditData,
                    memoizedEditData?.connection
                )
            )
            setCurrentRun(memoizedEditData?.run)
            setCronValues(
                createCronExpressionObj(memoizedEditData.cronExpression)
            )
        }
    }, [memoizedEditData])

    const [runSchedulesDataError, setRunSchedulesDataError] =
        useState<setRunSchedulesDataErrorProps>({
            name: false,
            ownerId: true,
            runId: false,
            connectionId: true,
            computeBudgetAccount: false,
            cronExpression: {
                minute: false,
                hour: false,
                dayOfMonth: false,
                month: false,
                dayOfWeek: false,
            },
            timeZone: false,
        })

    const formError = useFilterErrors(runSchedulesDataError)

    const readableStrings = useCronExpressionToReadableString({
        cronExpression: Object.values(cronValues).join(' '),
        nextOccurrances: 5,
        timeZone: runSchedulesData.timeZone,
    })

    function handleSubmit() {
        if (!projectId) return

        const data = {
            name: runSchedulesData.name,
            ownerId: runSchedulesData.ownerId,
            runId: runSchedulesData.runId,
            connectionId: runSchedulesData.connectionId,
            computeBudgetAccount: runSchedulesData.computeBudgetAccount,
            cronExpression: Object.values(cronValues).join(' '),
            timeZone: runSchedulesData.timeZone,
            endDate: runSchedulesData.endDate,
        }

        if (session.status === 'authenticated') {
            if (props.editItem) {
                updateRunScheduleMutation.mutate({
                    projectId,
                    runScheduleId: props.editItem.runScheduleId,
                    data,
                })
            } else {
                mutate({
                    projectId,
                    data,
                })
            }
        } else {
            setStatus({
                status: true,
                type: 'error',
                text: 'Authentication error. Please check your inputs and try again.',
            })
        }
    }

    function handleNameChange(e: ChangeEvent<HTMLInputElement>): void {
        const { value } = e.target

        setRunSchedulesData((prev) => ({
            ...prev,
            name: value,
        }))
        setRunSchedulesDataError((prev) => ({
            ...prev,
            name: !value,
        }))
    }

    function handleUnicoreChange(e: ChangeEvent<HTMLInputElement>): void {
        const { value } = e.target
        setRunSchedulesData((prev) => ({
            ...prev,
            computeBudgetAccount: value,
        }))
        setRunSchedulesDataError((prev) => ({
            ...prev,
            computeBudgetAccount: !value,
        }))
    }

    function clearData(): void {
        setRunSchedulesData(
            createDefaultRunScheduleData(currentRun, currentConnection)
        )
        setRunSchedulesDataError({
            name: false,
            ownerId: true,
            runId: false,
            connectionId: true,
            computeBudgetAccount: false,
            cronExpression: {
                minute: false,
                hour: false,
                dayOfMonth: false,
                month: false,
                dayOfWeek: false,
            },
            timeZone: false,
        })
        setCronValues(createCronExpressionObj(defaultCronExpression))
        setCurrentConnection(null)
        setCurrentRun(null)
        setStatus({ status: false, type: '', text: '' })
    }

    function handleClose(): void {
        clearData()
        setStatus({ status: false, type: '', text: '' })
        props.setOpen(false)
        props.setEditItem()
    }

    function handleCronChange(
        e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
        key: string
    ): void {
        const { value } = e.target

        const isValid = checkCronValues(value, key)

        setRunSchedulesDataError((prev) => ({
            ...prev,
            cronExpression: {
                ...prev.cronExpression,
                [key]: !isValid,
            },
        }))

        setCronValues((prev) => ({
            ...prev,
            [key]: value,
        }))

        setCronValuesHint({
            [cronValuesKeyEnum.dayOfMonth]: '',
            [cronValuesKeyEnum.dayOfWeek]: '',
        })

        if (
            key === cronValuesKeyEnum.dayOfMonth ||
            key === cronValuesKeyEnum.dayOfWeek
        ) {
            if (!!value && !isNaN(Number(value))) {
                const altValue =
                    key === cronValuesKeyEnum.dayOfMonth
                        ? cronValuesKeyEnum.dayOfWeek
                        : cronValuesKeyEnum.dayOfMonth
                if (cronValues[altValue] !== '?') {
                    setCronValues((prev) => ({
                        ...prev,
                        [key]: value,
                        [altValue]: '?',
                    }))
                    setCronValuesHint((prev) => ({
                        ...prev,
                        [altValue]: 'updated value',
                    }))
                }
            }
        }
    }

    function handleTimezoneChange(value: string | null): void {
        if (!value) return
        dayjs.tz.setDefault(runSchedulesData.timeZone)
        setRunSchedulesData((prev) => ({ ...prev, timeZone: value }))
    }

    function handleEndDateChange(value: dayjs.Dayjs | null): void {
        if (value === null) throw new Error('date value can not be null')
        setRunSchedulesData((prev) => ({ ...prev, endDate: value.unix() }))
    }

    useEffect(() => {
        if (memoizedEditData) {
            setRunSchedulesData({
                name: props.editItem?.name || '',
                ownerId: props.editItem?.owner?.userId || '',
                runId: props.editItem?.run?.runId || '',
                connectionId: props.editItem?.connection?.connectionId || '',
                computeBudgetAccount:
                    props.editItem?.computeBudgetAccount || '',
                timeZone: props.editItem?.timeZone || 'Europe/Berlin',
                endDate: props.editItem?.endDate || 0,
            })
        }
        setRunSchedulesData((prev) => ({
            ...prev,
            ownerId: currentRun?.user?.userId || '',
            connectionId: currentConnection?.connectionId || '',
            runId: currentRun?.runId || '',
        }))
        setRunSchedulesDataError((prev) => ({
            ...prev,
            ownerId: !currentRun?.user?.userId,
            connectionId: !currentConnection?.connectionId,
            runId: !currentRun?.runId,
        }))
        setCronValues(
            createCronExpressionObj(
                props.editItem?.cronExpression || defaultCronExpression
            )
        )
    }, [currentConnection, currentRun, memoizedEditData])

    /* #############################################
                    API CALLS
    ############################################# */

    const { mutate, isPending: isLoading } =
        useProjectsProjectIdRunsSchedulesPostProjectsProjectIdRunSchedulesPost({
            mutation: {
                onSuccess: () => {
                    setStatus({
                        status: true,
                        type: 'success',
                        text: 'Run schedule was created successfully!',
                    })
                    if (props.refetchData) {
                        props.refetchData() // Invoke the callback function if it's defined
                    }
                },
                onError: (error) => {
                    setStatus({
                        status: true,
                        type: 'error',
                        text: renderApiErrorMessage(error),
                    })
                },
            },
        })
    const updateRunScheduleMutation =
        useProjectsProjectIdRunsSchedulesRunScheduleIdPutProjectsProjectIdRunSchedulesRunScheduleIdPut(
            {
                mutation: {
                    onSuccess: () => {
                        setStatus({
                            status: true,
                            type: 'success',
                            text: 'Run schedule was updated successfully!',
                        })
                        if (props.refetchData) {
                            props.refetchData() // Invoke the callback function if it's defined
                        }
                    },
                    onError: (error) => {
                        setStatus({
                            status: true,
                            type: 'error',
                            text: renderApiErrorMessage(error),
                        })
                    },
                },
            }
        )

    // reset status on input change
    useEffect(() => {
        setStatus({ status: false, type: '', text: '' })
    }, [currentRun, currentConnection, cronValues, runSchedulesData])

    return (
        <FormDialogLayout
            formDataState={{ ...runSchedulesData, error: formError }}
            status={status}
            open={props.open}
            title={
                !props.editItem
                    ? `Create New Run Schedule`
                    : `Edit Run Schedule`
            }
            content="Select the parameters of your run schedule"
            handleSubmit={handleSubmit}
            handleClose={handleClose}
            isEdit={!!props.editItem}
            loading={
                !!props.editItem
                    ? updateRunScheduleMutation.isPending
                    : isLoading
            }
        >
            <FormControl>
                <TextField
                    key="name"
                    type="text"
                    name="name"
                    id="name"
                    label="Name"
                    size="small"
                    value={runSchedulesData.name}
                    error={runSchedulesDataError.name}
                    helperText={
                        runSchedulesDataError.name ? 'Invalid Name' : ''
                    }
                    onChange={handleNameChange}
                    required
                />
            </FormControl>
            <Spacing value={theme.spacing(2)} />
            <RunSelection
                currentRun={currentRun}
                setCurrentRun={setCurrentRun}
            />
            <Spacing value={theme.spacing(2)} />
            <ConnectionSelection
                currentConnection={currentConnection}
                setCurrentConnection={setCurrentConnection}
                isRequired={true}
            />
            <Spacing value={theme.spacing(2)} />
            <FormControl>
                <TextField
                    size="small"
                    id="unicore-compute-budget-account"
                    name="unicore-compute-budget-account"
                    label="UNICORE Compute Budget Account"
                    value={runSchedulesData.computeBudgetAccount}
                    error={runSchedulesDataError.computeBudgetAccount}
                    onChange={handleUnicoreChange}
                    required
                />
            </FormControl>
            <Spacing value={theme.spacing(2)} />

            <FormControl>
                <FormLabel id="cron-expression">Cron Expression</FormLabel>
                <Spacing value={theme.spacing(2)} />
                <FormGroup row>
                    {Object.entries(cronValues).map(([key, value], idx) => (
                        <TextField
                            key={idx}
                            size="small"
                            id={`cron-${key}`}
                            name={`cron-${key}`}
                            label={generateTitleFromCamelCaseKey(key)}
                            value={value}
                            onChange={(e) => handleCronChange(e, key)}
                            error={runSchedulesDataError.cronExpression[key]}
                            helperText={
                                cronValuesHint[
                                    key as keyof typeof cronValuesHint
                                ]
                            }
                            required
                            sx={{
                                width: `${
                                    90 / Object.keys(cronValues).length
                                }%`,
                                mr: 1,
                            }}
                        />
                    ))}
                </FormGroup>
                <Spacing value={theme.spacing(1)} />
                <Typography variant="body1">
                    Only{' '}
                    <Link
                        href="https://en.wikipedia.org/wiki/Cron"
                        variant="body2"
                    >
                        UNIX Cron format
                    </Link>{' '}
                    allowed
                </Typography>
                <Spacing value={theme.spacing(2)} />
                <FormControl required>
                    <Autocomplete
                        disablePortal
                        id="timezone"
                        value={runSchedulesData.timeZone}
                        options={timezones}
                        sx={{ width: 300 }}
                        onChange={(
                            _event: SyntheticEvent<Element>,
                            newValue: string | null
                        ) => {
                            handleTimezoneChange(newValue)
                        }}
                        renderInput={(params) => (
                            <TextField {...params} label="Timezone" required />
                        )}
                    />
                </FormControl>
                <Spacing value={theme.spacing(2)} />
                <Typography variant="h5">Next runs:</Typography>
                <List>
                    {readableStrings.map((str, idx) => (
                        <ListItem key={idx}>- {str}</ListItem>
                    ))}
                </List>
            </FormControl>
            <Spacing value={theme.spacing(2)} />
            <FormControl>
                <LocalizationProvider dateAdapter={AdapterDayjs}>
                    <DatePicker
                        label="End Date"
                        value={dayjs.unix(runSchedulesData.endDate)}
                        format="MM-DD-YYYY"
                        onChange={handleEndDateChange}
                        timezone={runSchedulesData.timeZone}
                    />
                </LocalizationProvider>
            </FormControl>
        </FormDialogLayout>
    )
}

function createDefaultRunScheduleData(
    run: any,
    connection: Connection | null
): runSchedulesDataProps {
    return {
        name: '',
        ownerId: run?.user?.userId || '',
        runId: run?.run?.runId || '',
        connectionId: connection?.connectionId || '',
        computeBudgetAccount: run?.computeBudgetAccount || '',
        timeZone: run?.timeZone || 'Europe/Berlin',
        endDate: dayjs
            .tz(
                dayjs().year(dayjs().year() + 1),
                run?.timeZone || 'Europe/Berlin'
            )
            .unix(),
    }
}
