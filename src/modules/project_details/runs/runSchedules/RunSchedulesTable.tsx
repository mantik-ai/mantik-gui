import { Add } from '@mui/icons-material'
import { Button, useTheme } from '@mui/material'
import { useRouter } from 'next/router'
import React, { useState } from 'react'
import { DataStateIndicator } from '../../../../components/DataStateIndicator'
import { DeleteDialogLayout } from '../../../../components/DeleteDialogLayout'
import { Spacing } from '../../../../components/Spacing'
import {
    ActionProps,
    TablesLayout,
} from '../../../../components/TablesLayout/TablesLayout'
import { renderApiErrorMessage } from '../../../../errors'
import { useGenerateTableData } from '../../../../hooks/useGenerateTableData'
import {
    RunSchedule,
    useProjectsProjectIdRunSchedulesGetProjectsProjectIdRunSchedulesGet,
    useProjectsProjectIdRunsSchedulesRunScheduleIdDeleteProjectsProjectIdRunSchedulesRunScheduleIdDelete,
} from '../../../../queries'
import { StatusProps } from '../../../../types/statusProps'
import {
    RenderElement,
    renderResourceActions,
    ResourceType,
} from '../../../../utils/renderResourceActions'
import { parseUserRole } from '../../../../utils/userRole'
import { DetailsToolbar } from '../../overview/ProjectDetailsToolbar'
import { RunSchedulesDialog } from './RunSchedulesDialog'

export const RunSchedulesTable = () => {
    const theme = useTheme()
    const router = useRouter()
    const { id } = router.query
    const [deleteData, setDeleteData] = useState({
        name: '',
        runScheduleId: '',
        projectId: id,
    })

    const [editItem, setEditItem] = useState<RunSchedule | undefined>()
    const [deleteStatus, setDeleteStatus] = useState<StatusProps>({
        status: false,
        type: '',
        text: '',
    })

    const [runSchedulesDialogOpen, setRunSchedulesDialogOpen] =
        useState<boolean>(false)
    const [deleteDialogOpen, setDeleteDialogOpen] = useState<boolean>(false)
    const { data, status, refetch, error } =
        useProjectsProjectIdRunSchedulesGetProjectsProjectIdRunSchedulesGet(
            id as string,
            {},
            {
                // async fix: triggers data refetch when authorization changes
                // fixes issue where incorrect user role is returned
                /* request: {
                    headers: {
                        Authorization: `Bearer ${user?.accessToken}`,
                    },
                },*/
            }
        )

    const tableData = useGenerateTableData(data, 'runSchedules')

    const userRole = parseUserRole(data?.userRole)
    const userActions = renderResourceActions(
        ResourceType.RUN_SCHEDULES,
        userRole
    )

    const { mutate, isPending: isLoading } =
        useProjectsProjectIdRunsSchedulesRunScheduleIdDeleteProjectsProjectIdRunSchedulesRunScheduleIdDelete(
            {
                mutation: {
                    onSuccess: () => {
                        setDeleteStatus({
                            status: true,
                            type: 'success',
                            text: 'Run schedule was deleted successfully!',
                        })
                        handleDataRefetch()
                    },
                    onError: (error) => {
                        setDeleteStatus({
                            status: true,
                            type: 'error',
                            text: renderApiErrorMessage(error),
                        })
                    },
                },
            }
        )

    function handleDeleteSubmit() {
        const projectId = String(id)
        const runScheduleId = deleteData?.runScheduleId
        mutate(
            { projectId, runScheduleId },
            {
                // Optional: You can provide extra configuration here if needed
                // e.g., onSuccess, onError, onSettled, etc.
            }
        )
    }
    function handleDataRefetch() {
        // eslint-disable-next-line @typescript-eslint/no-floating-promises
        refetch() // Refresh the data
    }

    function handleDeleteOpen() {
        setDeleteDialogOpen(true)
    }

    function handleDeleteClose() {
        setDeleteDialogOpen(false)
        setDeleteStatus({ status: false, type: '', text: '' })
    }
    function handleEditData(idx = 0) {
        setRunSchedulesDialogOpen(true)
        const row = data && data.runSchedules ? data.runSchedules[idx] : null
        if (!!row) {
            setEditItem(row)
        }
    }

    function handleDelete(idx: number) {
        handleDeleteOpen()
        const row = data && data.runSchedules ? data.runSchedules[idx] : null
        if (!!row) {
            setDeleteData((prev) => ({
                ...prev,
                name: String(row.name),
                runScheduleId: String(row.runScheduleId),
            }))
        }
    }

    const action: ActionProps = {}

    if (userActions.includes(RenderElement.EDIT)) {
        action.edit = handleEditData
    }

    if (userActions.includes(RenderElement.DELETE)) {
        action.delete = handleDelete
    }
    return (
        <>
            <DataStateIndicator
                status={status}
                text="Loading Run Schedules..."
                errorMessage={
                    error?.response?.status === 401
                        ? "The project you're trying to access is private and you don't have the required permissions!"
                        : 'Error'
                }
            >
                <DetailsToolbar
                    title="Run Schedules"
                    tool={
                        userActions.includes(RenderElement.ADD) ? (
                            <Button
                                variant="text"
                                onClick={() => setRunSchedulesDialogOpen(true)}
                            >
                                <Add></Add>Add
                            </Button>
                        ) : (
                            <></>
                        )
                    }
                />
                <Spacing value={theme.spacing(1)}></Spacing>
                <TablesLayout data={tableData} action={action} />
            </DataStateIndicator>
            {runSchedulesDialogOpen && (
                <RunSchedulesDialog
                    open={true}
                    setOpen={setRunSchedulesDialogOpen}
                    editItem={editItem ?? undefined}
                    setEditItem={setEditItem}
                    refetchData={handleDataRefetch}
                />
            )}
            <DeleteDialogLayout
                open={deleteDialogOpen}
                title="Delete Run Schedule"
                text={
                    <>
                        Are you sure you want to delete Run Schedule{' '}
                        <strong>{deleteData?.name}</strong>?
                    </>
                }
                handleSubmit={handleDeleteSubmit}
                handleClose={handleDeleteClose}
                isLoading={isLoading}
                status={deleteStatus}
            ></DeleteDialogLayout>
        </>
    )
}
