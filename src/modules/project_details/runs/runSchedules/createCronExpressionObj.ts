import { cronExpressionObjectProps } from './RunSchedulesDialog'

export enum cronValuesKeyEnum {
    minute = 'minute',
    hour = 'hour',
    dayOfMonth = 'dayOfMonth',
    month = 'month',
    dayOfWeek = 'dayOfWeek',
}
export const cronValuesKeyProps = [
    cronValuesKeyEnum.minute,
    cronValuesKeyEnum.hour,
    cronValuesKeyEnum.dayOfMonth,
    cronValuesKeyEnum.month,
    cronValuesKeyEnum.dayOfWeek,
]
export function createCronExpressionObj(
    cronExpression: string
): cronExpressionObjectProps {
    const obj: cronExpressionObjectProps = {
        minute: '',
        hour: '',
        dayOfMonth: '',
        month: '',
        dayOfWeek: '',
    }
    cronExpression.split(' ').forEach((val, idx) => {
        obj[cronValuesKeyProps[idx]] = val
    })
    return obj
}
