import {
    CRON_DAYOFMONTH_REGEX,
    CRON_DAYOFWEEK_REGEX,
    CRON_HOURS_REGEX,
    CRON_MINUTES_REGEX,
    CRON_MONTH_REGEX,
} from '../../../../constants'

export const getNumbers = (str: string) =>
    str
        .split(/[^0-9]/)
        .filter((item: any) => !!item)
        .map((item: any) => Number(item))

export const numbersSmallerThan = (str: string, num: number) =>
    getNumbers(str).every((item: number) => item < num)

export const checkCronValues = (value: string, key: string): boolean => {
    let isValid = false

    switch (key) {
        case 'minute':
            isValid =
                CRON_MINUTES_REGEX.test(value) &&
                numbersSmallerThan(value, 60) &&
                /^-\d+/.test(value) !== true
            break
        case 'hour':
            isValid =
                CRON_HOURS_REGEX.test(value) &&
                numbersSmallerThan(value, 24) &&
                /^-\d+/.test(value) !== true
            break
        case 'dayOfMonth':
            isValid =
                CRON_DAYOFMONTH_REGEX.test(value) &&
                numbersSmallerThan(value, 32)
            break
        case 'month':
            isValid =
                CRON_MONTH_REGEX.test(value) &&
                numbersSmallerThan(value, 12) &&
                /^-\d+/.test(value) !== true
            break
        case 'dayOfWeek':
            isValid =
                CRON_DAYOFWEEK_REGEX.test(value) && numbersSmallerThan(value, 8)
            break
        default:
            isValid = false
    }
    return isValid
}
