import {
    CircularProgress,
    FormControl,
    FormHelperText,
    InputLabel,
    MenuItem,
    Select,
    SelectChangeEvent,
    Skeleton,
    Stack,
    useTheme,
} from '@mui/material'
import { Spacing } from '../../../components/Spacing'
import {
    ExperimentRepository,
    useProjectsProjectIdExperimentsGetProjectsProjectIdExperimentsGet,
} from '../../../queries'
import { useRouter } from 'next/router'
import { getIndex } from '../../../helpers'

interface ExperimentRepositorySelectionProps {
    currentExperimentRepository?: ExperimentRepository | null
    setCurrentExperimentRepository: (
        experimentRepository: ExperimentRepository
    ) => void
}

export const ExperimentRepositorySelection = ({
    currentExperimentRepository = null,
    setCurrentExperimentRepository,
}: ExperimentRepositorySelectionProps) => {
    const theme = useTheme()
    const router = useRouter()
    const { id } = router.query
    const projectId: string =
        typeof id === 'string' ? id : id?.length ? id[0] : ''
    const { data, isFetching, isLoading, isSuccess } =
        useProjectsProjectIdExperimentsGetProjectsProjectIdExperimentsGet(
            projectId
        )

    // set index of current select value
    function handleCodeRepositoryChange(e: SelectChangeEvent<string>): void {
        if (!data?.experimentRepositories) return
        const { value } = e.target
        const idx = getIndex(
            Number(value),
            'mlflowExperimentId',
            data.experimentRepositories
        )
        setCurrentExperimentRepository(data.experimentRepositories[idx])
    }

    // prevent error if currentExperimentRepository is not in data set
    function getCurrentExperimentIdIdx(value = -1): string {
        if (!data?.experimentRepositories) return ''
        const idx = getIndex(
            Number(value),
            'mlflowExperimentId',
            data.experimentRepositories
        )
        const current = data.experimentRepositories[idx]?.mlflowExperimentId
        return current ? String(current) : ''
    }

    return (
        <FormControl required sx={{ width: '100%' }}>
            <Stack
                style={{
                    display: 'flex',
                    flexDirection: 'row',
                    alignItems: 'center',
                    gap: '0.5em',
                }}
            >
                <InputLabel id="experimentRepositories">
                    Experiment Repository
                </InputLabel>
                {isLoading && (
                    <Skeleton variant="rounded" sx={{ height: '3.5em' }}>
                        <input
                            type="hidden"
                            id="experimentRepositories"
                            value={
                                currentExperimentRepository?.mlflowExperimentId ||
                                ''
                            }
                            required
                        />
                    </Skeleton>
                )}
                {isSuccess && (
                    <Select
                        labelId="experimentRepositories"
                        id="experimentRepositories"
                        label="Experiment Repository *"
                        value={getCurrentExperimentIdIdx(
                            currentExperimentRepository?.mlflowExperimentId
                        )}
                        onChange={handleCodeRepositoryChange}
                        required
                        sx={{ flexGrow: 1 }}
                    >
                        {data?.experimentRepositories?.length ? (
                            data.experimentRepositories.map((item, index) => (
                                <MenuItem
                                    key={index}
                                    value={String(item.mlflowExperimentId)}
                                >
                                    {`${item.name} (MLflow Experiment ID: ${item.mlflowExperimentId})`}
                                </MenuItem>
                            ))
                        ) : (
                            <MenuItem value="">
                                No experiment repositories found
                            </MenuItem>
                        )}
                    </Select>
                )}
                {isFetching && <CircularProgress size={20} />}
            </Stack>
            <Spacing value={theme.spacing(1)} />
            {!data?.experimentRepositories?.length && (
                <FormHelperText error>
                    {`Cannot find an experiment repository. Please go to Projects > Experiments > Add`}
                </FormHelperText>
            )}
        </FormControl>
    )
}
