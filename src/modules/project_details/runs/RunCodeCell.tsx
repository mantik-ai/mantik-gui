import { NULL_VALUE_REPRESENTATION } from '../../../constants'
import { Run } from '../../../queries'
import { RunNotebookSourceCell } from './RunNotebookSourceCell'
import { RunRepositoryCell } from './RunRepositoryCell'

export function RunCodeCell({ run }: { run: Run }) {
    if (!run.notebookSource && !run.modelRepository.codeRepository) {
        return <>{NULL_VALUE_REPRESENTATION}</>
    }

    return (
        <>
            {run.notebookSource && (
                <RunNotebookSourceCell notebookSource={run.notebookSource} />
            )}
            {run.modelRepository.codeRepository && (
                <RunRepositoryCell
                    name={run.modelRepository.codeRepository.codeRepositoryName}
                    id={run.modelRepository.codeRepository.codeRepositoryId}
                    uri={run.modelRepository.codeRepository.uri}
                    branch={run.modelRepository.branch}
                    commitHash={run.modelRepository.commit}
                />
            )}
        </>
    )
}
