export type Parameters = {
    name: string
    label: string
    value: string | null
    required: boolean
}

export type EntryPoints = {
    entryPoint: string
    parameters: Parameters[]
}[]

export interface RunDialogProps {
    name: string
    branchName: string
    commitHash: string
    mlflowConfigPath: string
    entryPoints: EntryPoints | null
    backendConfigPath: string
    unicoreComputeBudgetAccount: string
    dataBranchName?: string
    dataCommitHash?: string
}

export interface RunDialogErrorProps {
    name: boolean
    codeRepo: boolean
    branchName: boolean
    commitHash: boolean
    mlflowConfigPath: boolean
    entryPoints: boolean
    backendConfigPath: boolean
    unicoreComputeBudgetAccount: boolean
    dataRepo: boolean
    dataBranchName?: boolean
    dataCommitHash?: boolean
}
