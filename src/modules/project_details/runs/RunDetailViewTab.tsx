import { Tooltip } from '@mui/material'
import Box from '@mui/material/Box'
import Tab from '@mui/material/Tab'
import Tabs from '@mui/material/Tabs'
import { useEffect, useRef, useState } from 'react'
import { DetailsDialogList } from '../../../components/DetailsDialogList'
import { TablesLayoutRowData } from '../../../components/TablesLayout/TablesLayout'
import { debounce } from '../../../helpers'
import { Run } from '../../../queries'
import RunDetailsLogsTab from './RunDetailsLogsTab'
import { SubmissionInfoTab } from './SubmissionInfoTab'
import { usePermission } from '../../../context/ProjectContext'
import React from 'react'
import { SemanticUserRole } from '../../../utils/userRole'

interface TabPanelProps {
    children?: React.ReactNode
    index: number
    value: number
}

function TabPanel(props: TabPanelProps) {
    const { children, value, index, ...other } = props
    return (
        <div
            className="tab-panel"
            role="tabpanel"
            id={`tabPanel-${index}`}
            aria-labelledby={`tab-${index}`}
            {...other}
            style={{
                display: value === index ? 'block' : 'none',
                width: '100%',
                top: 0,
            }}
        >
            {children}
        </div>
    )
}

function tabProps(index: number) {
    return {
        id: `tab-${index}`,
        'aria-controls': `tabPanel-${index}`,
    }
}

export default function RunDetailViewTab({
    data,
    runData,
}: {
    data: TablesLayoutRowData
    currentRow: number | null
    runData?: Run | null | undefined
}) {
    const minHeight = '55vh'
    const [value, setValue] = useState(0)
    const [parentHeight, setParentHeight] = useState<number | null>(null)
    const [childHeight, setChildHeight] = useState<number | null>(null)
    const dialogContentRootRef = useRef<HTMLDivElement>(null)
    const tabsBarRef = useRef<HTMLDivElement>(null)
    const rundetailsViewTabRef = useRef<HTMLDivElement>(null)
    const role = usePermission()
    // TODO: temporary fix, will use @casl/react
    const readerRoles: SemanticUserRole[] = [
        'OWNER',
        'MAINTAINER',
        'RESEARCHER',
    ]
    const canReadRunDetails = readerRoles.includes(role)

    useEffect(() => {
        const resize = () => {
            setParentHeight(null)
            const parentElement = rundetailsViewTabRef.current?.parentElement
            if (parentElement) {
                const parentPaddingTop = parseInt(
                    getComputedStyle(parentElement).paddingTop
                )
                const parentPaddingBottom = parseInt(
                    getComputedStyle(parentElement).paddingBottom
                )
                const parentHeight =
                    parentElement.offsetHeight -
                    parentPaddingTop -
                    parentPaddingBottom
                setParentHeight(parentHeight)
                if (tabsBarRef.current) {
                    setChildHeight(
                        parentHeight - tabsBarRef.current.offsetHeight
                    )
                }
            }
        }
        resize()
        window.addEventListener('resize', debounce(resize))
        return () => window.removeEventListener('resize', resize)
    }, [])

    const handleChange = (_: React.SyntheticEvent, newValue: number) => {
        setValue(newValue)
        if (dialogContentRootRef.current)
            dialogContentRootRef.current.scrollTop = 0
    }
    const infoLogsTabs = ['Submission Info', 'Logs']

    return (
        <Box
            ref={rundetailsViewTabRef}
            sx={{
                display: 'flex',
                flexDirection: 'column',
                height: parentHeight
                    ? `max(${parentHeight}px, ${minHeight})`
                    : '100%',
            }}
        >
            <Box
                ref={tabsBarRef}
                sx={{ borderBottom: 1, borderColor: 'divider' }}
            >
                <Tabs value={value} onChange={handleChange} aria-label="tabs">
                    <Tab label="Details" {...tabProps(0)} />
                    {infoLogsTabs.map((tab, index) => (
                        <Tooltip
                            key={index}
                            title={`${
                                canReadRunDetails
                                    ? ''
                                    : 'Only the run owner can access the '
                            }${tab}`}
                        >
                            {canReadRunDetails ? (
                                <Tab
                                    key={index}
                                    label={tab}
                                    {...tabProps(index)}
                                />
                            ) : (
                                <span>
                                    <Tab key={index} disabled label={tab} />
                                </span>
                            )}
                        </Tooltip>
                    ))}
                </Tabs>
            </Box>
            <Box
                ref={dialogContentRootRef}
                sx={{
                    overflow: 'auto',
                    position: 'relative',
                    minWidth: 'min(536px, 80vw)',
                }}
            >
                <TabPanel value={value} index={0}>
                    <DetailsDialogList data={data} />
                </TabPanel>
                <TabPanel value={value} index={1}>
                    <SubmissionInfoTab runData={runData} />
                </TabPanel>
                <TabPanel value={value} index={2}>
                    <RunDetailsLogsTab
                        data={data}
                        height={childHeight}
                        minHeight={minHeight}
                        resetScrollPosition={value === 2}
                    />
                </TabPanel>
            </Box>
        </Box>
    )
}
