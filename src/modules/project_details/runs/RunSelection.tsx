import {
    FormControl,
    FormHelperText,
    InputLabel,
    MenuItem,
    Select,
    useTheme,
} from '@mui/material'
import { useEffect, useState } from 'react'
import { Spacing } from '../../../components/Spacing'
import { useRouter } from 'next/router'
import { getIndex } from '../../../helpers'
import { useProjectsProjectIdRunsGetProjectsProjectIdRunsGet } from '../../../queries'

interface RunSelectionProps {
    currentRun: { [key: string]: any } | null
    setCurrentRun: Function
}

export const RunSelection = ({
    currentRun,
    setCurrentRun,
}: RunSelectionProps) => {
    const theme = useTheme()
    const router = useRouter()
    const { id } = router.query
    const projectId: string =
        typeof id === 'string' ? id : id?.length ? id[0] : ''
    const { data } =
        useProjectsProjectIdRunsGetProjectsProjectIdRunsGet(projectId)
    const [currentRunIdx, setCurrentRunIdx] = useState<number>(0)

    // set index of current select value
    function handleRunsChange(e: any): void {
        if (!data?.runs) return
        const { value } = e.target
        const idx = getIndex(value, 'runId', data.runs)
        setCurrentRunIdx(idx)
        setCurrentRun(data.runs[idx])
    }

    useEffect(() => {
        if (!data?.runs || currentRun) return
        if (data.runs.length) {
            setCurrentRunIdx(0)
            setCurrentRun(data.runs[0])
        }
    }, [data?.runs, currentRun])

    useEffect(() => {
        if (!data?.runs || !currentRun) return
        const idx = getIndex(currentRun?.runId, 'runId', data.runs)
        setCurrentRunIdx(idx)
    }, [data?.runs, currentRun])

    return (
        <FormControl>
            <InputLabel id="runs">Run</InputLabel>
            {data?.runs && (
                <Select
                    labelId="runs"
                    id="runs"
                    label="Runs"
                    value={data.runs[currentRunIdx]?.runId}
                    onChange={handleRunsChange}
                >
                    {data.runs?.length ? (
                        data.runs.map((item, index) => (
                            <MenuItem key={index} value={item.runId}>
                                {`${item.name} (${item.runId})`}
                            </MenuItem>
                        ))
                    ) : (
                        <MenuItem value="No runs found">No runs found</MenuItem>
                    )}
                </Select>
            )}
            <Spacing value={theme.spacing(1)} />
            {!data?.runs?.length && (
                <FormHelperText error>
                    {`Cannot find any runs for this project. Please create a run first.`}
                </FormHelperText>
            )}
        </FormControl>
    )
}
