import {
    Add,
    DeleteOutline,
    EditOutlined,
    LaunchOutlined,
    VisibilityOutlined,
} from '@mui/icons-material'
import { Button, CircularProgress, Grid, Typography } from '@mui/material'
import { useRouter } from 'next/router'
import { useCallback, useEffect, useState } from 'react'
import { DataStateIndicator } from '../../../components/DataStateIndicator'
import { DeleteDialogLayout } from '../../../components/DeleteDialogLayout'
import { DetailsDialogLayout } from '../../../components/DetailsDialogLayout'
import { DetailsDialogList } from '../../../components/DetailsDialogList'
import MoreActionMenu from '../../../components/MoreActionMenu'
import { PlatformIcon } from '../../../components/PlatformIcon'
import { usePermission } from '../../../context/ProjectContext'
import { renderApiErrorMessage } from '../../../errors'
import { formatTimestamp } from '../../../helpers'
import { TileLayout } from '../../../layouts/TileLayout'
import {
    CodeRepository,
    Platform,
    useProjectsProjectIdCodeCodeRepositoryIdDeleteProjectsProjectIdCodeCodeRepositoryIdDelete,
    useProjectsProjectIdCodeGetProjectsProjectIdCodeGet,
} from '../../../queries'
import { StatusProps } from '../../../types/statusProps'
import {
    ActionButton,
    generateCustomActionButtonsArray,
} from '../../../utils/generateCustomActionButtonsArray'
import { hasPermission } from '../data/actions'
import { DetailsToolbar } from '../overview/ProjectDetailsToolbar'
import { CodeRepositorySettingsDialog } from './CodeRepositorySettingsDialog'

export const CodeRepositoriesOverview = () => {
    const router = useRouter()
    const { id } = router.query
    const [openCodeRepositoryDialog, setOpenCodeRepositoryDialog] =
        useState(false)
    const [detailsDialogOpen, setDetailsDialogOpen] = useState<boolean>(false)
    const [currentRow, setCurrentRow] = useState<number | null>(null)
    const [editData, setEditData] = useState<CodeRepository>()
    const initialEditData: CodeRepository = {
        codeRepositoryId: '',
        uri: '',
        platform: Platform.GitHub,
        labels: [],
        createdAt: '',
    }
    const [isEdit, setIsEdit] = useState<boolean>(false)
    const openDialog = () => {
        setOpenCodeRepositoryDialog(true)
    }
    const [deleteDialogOpen, setDeleteDialogOpen] = useState<boolean>(false)
    const [deleteData, setDeleteData] = useState({
        name: '',
        codeRepositoryId: '',
        projectId: id,
    })

    const [deleteStatus, setDeleteStatus] = useState<StatusProps>({
        status: false,
        type: '',
        text: '',
    })
    const { data, status, refetch, error, isFetching } =
        useProjectsProjectIdCodeGetProjectsProjectIdCodeGet(
            id as string,
            {},
            {
                // async fix: triggers data refetch when authorization changes
                // fixes issue where incorrect user role is returned
                /*  request: {
                    headers: {
                        Authorization: `Bearer ${user?.accessToken}`,
                    },
                },*/
            }
        )

    const role = usePermission()
    const canCreate = hasPermission(role, 'CREATE')
    const canRead = hasPermission(role, 'READ')
    const canEdit = hasPermission(role, 'UPDATE')
    const canDelete = hasPermission(role, 'DELETE')

    const handleDetailsDialog = (idx?: number) => {
        if (idx === undefined) {
            setDetailsDialogOpen(false)
            setCurrentRow(null)
        } else {
            setDetailsDialogOpen(true)
            setCurrentRow(idx)
        }
    }

    function handleIsEdit() {
        setEditData(initialEditData)
        setIsEdit(false)
    }
    const handleEditData = useCallback(
        (idx = 0) => {
            setOpenCodeRepositoryDialog(true)

            const row =
                data && data.codeRepositories
                    ? data.codeRepositories[idx]
                    : null
            if (!!row) {
                setEditData(row)
            }
            setIsEdit(true)
        },
        [data]
    )
    /**
     * Delete experiment functionality
     */

    function handleDeleteOpen() {
        setDeleteDialogOpen(true)
    }

    function handleDeleteClose() {
        setDeleteDialogOpen(false)
        setDeleteStatus({ status: false, type: '', text: '' })
    }

    function handleDelete(idx: number) {
        handleDeleteOpen()
        const row =
            data && data.codeRepositories ? data.codeRepositories[idx] : null
        if (!!row) {
            setDeleteData((prev) => ({
                ...prev,
                name: String(row.codeRepositoryName),
                codeRepositoryId: String(row.codeRepositoryId),
            }))
        }
    }

    function handleDeleteSubmit() {
        const projectId = String(id)
        const codeRepositoryId = deleteData?.codeRepositoryId || ''
        deleteMutation.mutate(
            { projectId, codeRepositoryId },
            {
                // Optional: You can provide extra configuration here if needed
                // e.g., onSuccess, onError, onSettled, etc.
            }
        )
    }

    interface CustomCellProps {
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        data: any
        type?: string
    }
    type TableProps = {
        [key: string]: CustomCellProps
    }[]
    const [tableData, setTableData] = useState<TableProps>([
        {
            ID: { data: '' },
            Name: { data: '' },
            URL: { data: '' },
            Description: { data: '' },
        },
    ])
    const openUrlInNewTab = (url: string) => {
        const newWindow = window.open(url, '_blank')
        if (newWindow) {
            newWindow.opener = null
        }
    }
    const deleteMutation =
        useProjectsProjectIdCodeCodeRepositoryIdDeleteProjectsProjectIdCodeCodeRepositoryIdDelete(
            {
                mutation: {
                    onSuccess: async () => {
                        setDeleteStatus({
                            status: true,
                            type: 'success',
                            text: 'Code Repository was deleted successfully!',
                        })
                        await refetch()
                    },
                    onError: (error) => {
                        setDeleteStatus({
                            status: true,
                            type: 'error',
                            text: renderApiErrorMessage(error),
                        })
                    },
                },
            }
        )

    useEffect(() => {
        if (!!data && !!data?.codeRepositories?.length) {
            const tData = data?.codeRepositories?.map((data) => ({
                ID: { data: String(data.codeRepositoryId), type: 'id' },
                Name: { data: String(data.codeRepositoryName) },
                URL: { data: String(data.uri) },
                Description: { data: String(data.description) },
                ...(data.labels && {
                    Label: { data: data.labels, type: 'labels' },
                }),
            }))
            setTableData(tData)
        }
    }, [data, status])

    return (
        <DataStateIndicator
            status={status}
            text="Loading Code Repositories..."
            errorMessage={
                error?.response?.status === 401
                    ? "The project you're trying to access is private and you don't have the required permissions!"
                    : 'Error'
            }
        >
            <DetailsToolbar
                title={'Code'}
                tool={
                    canCreate ? (
                        <Button variant="text" onClick={() => openDialog()}>
                            <Add></Add>Add
                        </Button>
                    ) : (
                        <></>
                    )
                }
            ></DetailsToolbar>
            <Grid container spacing={4} mt={0}>
                {isFetching ? (
                    <Grid
                        item
                        xs={12}
                        sx={{ display: 'flex', justifyContent: 'center' }}
                    >
                        <CircularProgress />
                    </Grid>
                ) : (
                    data?.codeRepositories?.map((repo, idx) => {
                        const actionButtons: ActionButton[] = [
                            {
                                title: 'Details',
                                actionFunction: canRead
                                    ? () => handleDetailsDialog(idx)
                                    : undefined,
                                iconComponent: (
                                    <VisibilityOutlined fontSize="small" />
                                ),
                            },
                            {
                                title: 'Edit',
                                actionFunction: canEdit
                                    ? () => handleEditData(idx)
                                    : undefined,
                                iconComponent: (
                                    <EditOutlined fontSize="small" />
                                ),
                            },
                            {
                                title: 'Delete',
                                actionFunction: canDelete
                                    ? () => handleDelete(idx)
                                    : undefined,
                                iconComponent: (
                                    <DeleteOutline
                                        color={canDelete ? 'error' : 'disabled'}
                                        fontSize="small"
                                    />
                                ),
                            },
                        ]
                        const codeTileActionButtons: ActionButton[] = [
                            {
                                title: 'Visit',
                                actionFunction: () => openUrlInNewTab(repo.uri),
                                iconComponent: (
                                    <LaunchOutlined fontSize="small" />
                                ),
                            },
                        ]
                        const actions = [
                            ...generateCustomActionButtonsArray(actionButtons),
                            <MoreActionMenu
                                key={'codeTile_MoreActionMenu'}
                                menuItems={[
                                    ...actionButtons,
                                    ...codeTileActionButtons,
                                ]}
                            />,
                        ]
                        return (
                            <Grid key={idx} item xs={12} md={6} lg={4} xl={3}>
                                <TileLayout
                                    key={repo.codeRepositoryId}
                                    logo={
                                        <PlatformIcon
                                            platform={repo.platform}
                                            iconProps={{
                                                sx: { color: 'grey.600' },
                                            }}
                                        />
                                    }
                                    title={repo.codeRepositoryName}
                                    bodyText={repo.uri}
                                    footer={
                                        <div data-testid="timestamp-caption">
                                            Created At:{' '}
                                            {formatTimestamp(repo.createdAt)}
                                        </div>
                                    }
                                    actions={actions}
                                    bodyContent={
                                        <Typography
                                            variant="body1"
                                            style={{ textAlign: 'left' }}
                                        >
                                            {repo.description ??
                                                '<No Description>'}
                                        </Typography>
                                    }
                                    alignTextCenter={false}
                                    labels={repo.labels}
                                />
                            </Grid>
                        )
                    })
                )}
            </Grid>
            {openCodeRepositoryDialog && (
                <CodeRepositorySettingsDialog
                    projectId={String(id)}
                    open={true}
                    setOpen={setOpenCodeRepositoryDialog}
                    setIsEdit={handleIsEdit}
                    isEdit={isEdit}
                    editData={editData}
                />
            )}

            <DetailsDialogLayout
                open={detailsDialogOpen}
                setOpen={handleDetailsDialog}
                title={'Details'}
            >
                <DetailsDialogList
                    data={currentRow !== null ? tableData[currentRow] : {}}
                />
            </DetailsDialogLayout>
            <DeleteDialogLayout
                open={deleteDialogOpen}
                title="Delete Code Repository"
                text={`Are you sure you want to delete Code Repository ${deleteData?.name} from the project?`}
                handleSubmit={handleDeleteSubmit}
                handleClose={handleDeleteClose}
                isLoading={deleteMutation.isPending}
                status={deleteStatus}
            ></DeleteDialogLayout>
        </DataStateIndicator>
    )
}
