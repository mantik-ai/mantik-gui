import { FormControl, FormGroup, TextField, useTheme } from '@mui/material'
import {
    ChangeEvent,
    useCallback,
    useContext,
    useEffect,
    useMemo,
    useState,
} from 'react'
import { Spacing } from '../../../components/Spacing'
import {
    AddCodeRepository,
    CodeRepository,
    Connection,
    ConnectionId,
    getProjectsProjectIdCodeGetProjectsProjectIdCodeGetQueryKey,
    Label,
    Platform,
    projectsProjectIdCodeCodeRepositoryIdConnectionIdDeleteProjectsProjectIdCodeCodeRepositoryIdConnectionConnectionIdDelete,
    projectsProjectIdCodeCodeRepositoryIdConnectionProjectsProjectIdCodeCodeRepositoryIdConnectionPost,
    useProjectsProjectIdCodeCodeRepositoryIdPutProjectsProjectIdCodeCodeRepositoryIdPut,
    useProjectsProjectIdCodePostProjectsProjectIdCodePost,
    useUsersUserIdSettingsConnectionsConnectionIdGetUsersUserIdSettingsConnectionsConnectionIdGet,
} from '../../../queries'
import { renderApiErrorMessage } from '../../../errors'
import { FormDialogLayout } from '../../../components/FormDialogLayout'
import { useFilterErrors } from '../../../hooks/useFilterErrors'
import { useAuthentication } from '../../../hooks/useAuthentication'
import { LabelSelector } from '../../projects_overview/components/LabelSelector'
import { PlatformSelect } from './platform/PlatformSelect'
import { RepositoryUri } from './repository/RepositoryUri'
import { ConnectionSelection } from '../../../components/forms/ConnectionSelection'
import AuthContext from '../../../context/AuthContext'
import { useQueryClient } from '@tanstack/react-query'

interface CodeRepositorySettingsDialogProps {
    open: boolean
    projectId: string
    setOpen: React.Dispatch<React.SetStateAction<boolean>>
    editData?: CodeRepository
    isEdit?: boolean
    setIsEdit: (isEdit?: boolean) => void
}
interface FormErrors {
    name: boolean
    uri: boolean
}
const initialAddCodeRepository: AddCodeRepository = {
    codeRepositoryName: '',
    platform: 'GitHub',
    uri: '',
    description: '',
}
const initialFormErrors: FormErrors = {
    name: false,
    uri: false,
}

export const CodeRepositorySettingsDialog = (
    props: CodeRepositorySettingsDialogProps
) => {
    const theme = useTheme()
    const authentication = useAuthentication()
    const userContext = useContext(AuthContext)
    const userId = String(userContext.usersData?.userId)

    // State for form errors
    const [formErrors, setFormErrors] = useState<FormErrors>(initialFormErrors)
    const formError = useFilterErrors(formErrors)

    const [addCodeRepository, setAddCodeRepository] =
        useState<AddCodeRepository>(initialAddCodeRepository)
    const [currentConnection, setCurrentConnection] =
        useState<Connection | null>(null)
    const [status, setStatus] = useState({ status: false, type: '', text: '' })

    const handleClose = () => {
        props.setOpen(false)
        props.setIsEdit()
        setAddCodeRepository(initialAddCodeRepository)
        setCurrentConnection(initialConnectionData ?? null)
        resetErrors()
    }

    const client = useQueryClient()

    function resetErrors() {
        setFormErrors(initialFormErrors)
        setStatus({ status: false, type: '', text: '' })
    }

    function handleChange(
        e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
        key: string
    ) {
        setAddCodeRepository((prev) => ({
            ...prev,
            [key]: e.target.value,
        }))
        if (key === 'codeRepositoryName') {
            setFormErrors((prev) => ({
                ...prev,
                name: !e.target.value.length,
            }))
        }
        resetErrors()
    }

    function setPlatform(platform: Platform) {
        setAddCodeRepository((prev) => ({
            ...prev,
            platform,
        }))
        setCurrentConnection(null)
        resetErrors()
    }

    const setConnection = useCallback(function setConnection(
        connection: Connection | null
    ) {
        setCurrentConnection(connection)
        resetErrors()
    }, [])

    const { data: initialConnectionData } =
        useUsersUserIdSettingsConnectionsConnectionIdGetUsersUserIdSettingsConnectionsConnectionIdGet(
            userId,
            props.editData?.connectionId ?? ''
        )

    function setRepositoryUri(uri: string, error: boolean) {
        setAddCodeRepository((prev) => ({
            ...prev,
            uri,
        }))
        setFormErrors((prev) => ({
            ...prev,
            uri: error,
        }))
        resetErrors()
    }

    const { mutate, isPending: isLoading } =
        useProjectsProjectIdCodePostProjectsProjectIdCodePost({
            mutation: {
                onSuccess: async () => {
                    setStatus({
                        status: true,
                        type: 'success',
                        text: 'Code Repository saved successfully!',
                    })
                    await client.invalidateQueries({
                        queryKey:
                            getProjectsProjectIdCodeGetProjectsProjectIdCodeGetQueryKey(
                                props.projectId
                            ),
                    })
                },
                onError: (error) => {
                    setStatus({
                        status: true,
                        type: 'error',
                        text: renderApiErrorMessage(error),
                    })
                },
            },
        })

    const updateMutation =
        useProjectsProjectIdCodeCodeRepositoryIdPutProjectsProjectIdCodeCodeRepositoryIdPut(
            {
                mutation: {
                    onSuccess: async () => {
                        try {
                            await handleUpdateConnection()

                            setStatus({
                                status: true,
                                type: 'success',
                                text: 'Code Repository was updated successfully!',
                            })
                        } catch (e) {
                            setStatus({
                                status: true,
                                type: 'error',
                                text: renderApiErrorMessage(e),
                            })
                        }
                        await client.invalidateQueries({
                            queryKey:
                                getProjectsProjectIdCodeGetProjectsProjectIdCodeGetQueryKey(
                                    props.projectId
                                ),
                        })
                    },
                    onError: (error) => {
                        setStatus({
                            status: true,
                            type: 'error',
                            text: renderApiErrorMessage(error),
                        })
                    },
                },
            }
        )

    const handleUpdateConnection = async () => {
        if (props.editData?.connectionId === currentConnection?.connectionId) {
            return
        }
        if (props.editData?.connectionId) {
            await projectsProjectIdCodeCodeRepositoryIdConnectionIdDeleteProjectsProjectIdCodeCodeRepositoryIdConnectionConnectionIdDelete(
                props.projectId,
                props.editData?.codeRepositoryId ?? '',
                props.editData?.connectionId ?? ''
            )
        }

        if (currentConnection) {
            const connectionId: ConnectionId = {
                connectionId: currentConnection?.connectionId ?? '',
            }
            await projectsProjectIdCodeCodeRepositoryIdConnectionProjectsProjectIdCodeCodeRepositoryIdConnectionPost(
                props.projectId,
                props.editData?.codeRepositoryId ?? '',
                connectionId
            )
        }
    }

    const handleSubmit = async () => {
        setFormErrors(initialFormErrors)
        // call the mutate function with the current project value
        if (authentication === 'authenticated') {
            if (props.isEdit) {
                updateMutation.mutate({
                    projectId: props.projectId,
                    codeRepositoryId: props.editData?.codeRepositoryId || '',
                    data: addCodeRepository,
                })
            } else {
                mutate({
                    projectId: props.projectId,
                    data: {
                        ...addCodeRepository,
                        connectionId: currentConnection?.connectionId,
                    },
                })
            }
        } else {
            setStatus({
                status: true,
                type: 'error',
                text: 'Authentication error. Please check your inputs and try again.',
            })
        }
    }

    function handleLabelSelect(value: Label[]) {
        const newLabels = value.map((val) => val.labelId)
        setAddCodeRepository((prev) => ({
            ...prev,
            labels: newLabels,
        }))
        resetErrors()
    }

    const memoizedEditData = useMemo(() => props.editData, [props.editData])

    useEffect(() => {
        if (props.isEdit) {
            setAddCodeRepository({
                codeRepositoryName: props.editData?.codeRepositoryName || '',
                platform: props.editData?.platform || Platform.GitHub,
                uri: props.editData?.uri || '',
                description: props.editData?.description || '',
            })

            setCurrentConnection(initialConnectionData ?? null)
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [initialConnectionData, memoizedEditData, props.isEdit])

    return (
        <FormDialogLayout
            formDataState={{ ...addCodeRepository, error: formError }}
            status={status}
            open={props.open}
            title={
                !props.isEdit
                    ? `Create Code Repository`
                    : `Edit Code Repository`
            }
            content="Configure the Code Repository details"
            handleSubmit={handleSubmit}
            handleClose={handleClose}
            isEdit={props.isEdit}
            loading={updateMutation.isPending || isLoading}
        >
            <FormGroup>
                <FormControl>
                    <TextField
                        key="name"
                        name="name"
                        id="name"
                        label="Name"
                        size="small"
                        value={addCodeRepository.codeRepositoryName}
                        error={formErrors.name}
                        helperText={formErrors.name ? 'Invalid Name' : ''}
                        onChange={(e) => handleChange(e, 'codeRepositoryName')}
                        required
                    />
                </FormControl>
                <Spacing value={theme.spacing(2)} />

                <PlatformSelect
                    platform={addCodeRepository.platform || ''}
                    setPlatform={setPlatform}
                />

                <Spacing value={theme.spacing(2)} />

                <RepositoryUri
                    repositoryUri={addCodeRepository.uri}
                    repositoryUriError={formErrors.uri}
                    handleRepoChange={setRepositoryUri}
                />

                <Spacing value={theme.spacing(2)} />

                <ConnectionSelection
                    currentConnection={currentConnection}
                    setCurrentConnection={setConnection}
                    isRequired={false}
                    platform={addCodeRepository.platform}
                />

                <Spacing value={theme.spacing(2)} />
                <FormControl>
                    <TextField
                        key="description"
                        name="description"
                        id="description"
                        label="Description"
                        size="small"
                        value={addCodeRepository.description}
                        onChange={(e) => handleChange(e, 'description')}
                    />
                </FormControl>
                <Spacing value={theme.spacing(2)} />
                <LabelSelector
                    currentLabels={memoizedEditData?.labels}
                    queryString="code"
                    handleLabelSelect={handleLabelSelect}
                />
                <Spacing value={theme.spacing(2)} />
            </FormGroup>
        </FormDialogLayout>
    )
}
