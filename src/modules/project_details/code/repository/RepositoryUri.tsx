import { Help } from '@mui/icons-material'
import { FormControl, TextField, Tooltip, Typography } from '@mui/material'
import { debounce, sanitzeRepoUrl } from '../../../../helpers'
import { useEffect, useRef, useState } from 'react'
import { fetchRepo } from '../../../../utils/fetchRepo'
import { VALID_REPOSITORY_PATH } from '../../../../constants'

interface RepositoryUriProps {
    repositoryUri: string
    repositoryUriError: boolean
    handleRepoChange: (uri: string, error: boolean) => void
}

const DEBOUNCE_DELAY = 500 // amount of ms API calls are debounced

export const RepositoryUri = ({
    repositoryUri,
    handleRepoChange,
    repositoryUriError,
}: RepositoryUriProps) => {
    const initial = useRef(true) // set editData only on initial render
    const [repoUrl, setRepoUrl] = useState<string>('')

    useEffect(() => {
        if (!repositoryUri || !initial.current) return
        setRepoUrl(repositoryUri)
        initial.current = false
    }, [repositoryUri])

    const debouncedRepoFetch = debounce(
        (val: string) =>
            fetchRepo(
                val,
                () => handleRepoChange(val, false), // for success
                () => handleRepoChange(val, true) // for failure
            ),
        DEBOUNCE_DELAY
    )

    const handleChange = (event: any) => {
        const value = event.target.value
        handleRepoChange(value, true)
        setRepoUrl(value) // prevent sluggish behaviour on typing by setting the value immediately
        const sanitizedUrl = sanitzeRepoUrl(value)
        const validUrl = sanitizedUrl?.match(VALID_REPOSITORY_PATH)
        if (validUrl?.length) {
            debouncedRepoFetch(validUrl[0])
        }
    }

    return (
        <FormControl style={{ display: 'flex', flexDirection: 'row' }}>
            <TextField
                key="url"
                name="url"
                id="url"
                label="URL"
                size="small"
                sx={{ width: '95%' }}
                value={repoUrl}
                error={repositoryUriError}
                helperText={repositoryUriError ? 'Invalid URL' : ''}
                onChange={handleChange}
                required
            />
            <Tooltip
                sx={{ mt: 1, ml: 1 }}
                title={
                    <>
                        <Typography sx={{ pb: 1 }}>
                            Only <strong>public</strong> repositories are
                            supported.
                        </Typography>
                        <Typography variant="h6">Valid URL formats:</Typography>
                        <ul style={{ fontSize: '0.85em' }}>
                            <li>https://gitlab.com/mantik-ai/mantik</li>
                            <li>https://gitlab.com/mantik-ai/mantik.git</li>
                            <li>git@gitlab.com:mantik-ai/mantik.git</li>
                        </ul>
                        <Typography>
                            A given URL will automatically be converted into the
                            required https://{'<'}platform{'>'}
                            .com/{'<'}path{'>'} format (e.g.
                            https://gitlab.com/mantik-ai/mantik)
                        </Typography>
                    </>
                }
            >
                <Help color="info" />
            </Tooltip>
        </FormControl>
    )
}
