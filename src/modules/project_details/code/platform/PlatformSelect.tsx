import {
    Box,
    FormControl,
    InputLabel,
    ListItemText,
    MenuItem,
    Select,
} from '@mui/material'
import { Platform } from '../../../../queries'

import * as React from 'react'
import { PlatformIcon } from '../../../../components/PlatformIcon'

interface PlatformSelectProps {
    platform?: Platform | 'GitHub'
    setPlatform: (platform: Platform) => void
}

export const PlatformSelect = ({
    platform,
    setPlatform,
}: PlatformSelectProps) => {
    const platformIconProps = {
        sx: {
            fontSize: '1.25em',
            mr: 1,
            color: 'primary.main',
        },
    }

    return (
        <FormControl size="small" required>
            <InputLabel id="platform">Platform</InputLabel>
            <Select
                displayEmpty
                id="platform"
                label="Platform"
                value={platform}
                renderValue={(selected: Platform) =>
                    selected ? (
                        <Box
                            sx={{
                                fontSize: '1.25em',
                                display: 'flex',
                                alignItems: 'center',
                            }}
                        >
                            <span style={{ marginBottom: '-0.2em' }}>
                                <PlatformIcon
                                    platform={selected}
                                    iconProps={platformIconProps}
                                />
                            </span>
                            <ListItemText primary={selected} />
                        </Box>
                    ) : (
                        <></>
                    )
                }
                onChange={(e) => setPlatform(e.target.value as Platform)}
            >
                {Object.values(Platform).map((item, index) => (
                    <MenuItem key={index} value={item}>
                        <PlatformIcon
                            platform={item}
                            iconProps={platformIconProps}
                        />
                        <ListItemText primary={item} />
                    </MenuItem>
                ))}
            </Select>
        </FormControl>
    )
}
