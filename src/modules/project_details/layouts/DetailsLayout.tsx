import {
    AssignmentOutlined,
    Code,
    DatasetOutlined,
    DirectionsRun,
    ExpandLess,
    ExpandMore,
    OnlinePredictionOutlined,
    ScienceOutlined,
    SettingsOutlined,
} from '@mui/icons-material'
import {
    Collapse,
    List,
    ListItemButton,
    ListItemIcon,
    ListItemText,
    Tooltip,
} from '@mui/material'
import { useRouter } from 'next/router'
import React, { useContext, useState } from 'react'
import DataContext, { DataProvider } from '../../../context/DataContext'
import { LayoutWithSidebar } from '../../../layouts/LayoutWithSidebar'
import { Route } from '../../../types/route'
import {
    RenderElement,
    renderResourceActions,
    ResourceType,
} from '../../../utils/renderResourceActions'
import { parseUserRole } from '../../../utils/userRole'
import SideBarItem from '../overview/components/SideBarItem'
import { ProjectProvider } from '../../../context/ProjectContext'

interface DetailsLayoutProps {
    children: React.ReactNode
}

const SettingsList = () => {
    const router = useRouter()
    const { id } = router.query
    const projectDetailContext = useContext(DataContext)
    const [open, setOpen] = useState(false)
    const handleClick = () => {
        setOpen((prev) => !prev)
    }
    const settingRoutes: Route[] = [
        {
            name: 'General',
            path: `${id}/settings/general`,
        },
        {
            name: 'Collaboration',
            path: `${id}/settings/collaboration`,
        },
    ]

    const userRole = parseUserRole(
        projectDetailContext.projectDetailsData?.userRole
    )
    const userActions = renderResourceActions(ResourceType.PROJECT, userRole)

    const renderSettingsList = () => {
        const hasSettingsLink = userActions?.includes(RenderElement.SETTINGS)
        return !hasSettingsLink ? (
            <Tooltip
                title={
                    "You don't have the required permissions to access the project settings"
                }
            >
                <div>
                    <ListItemButton
                        onClick={handleClick}
                        disabled={!hasSettingsLink}
                    >
                        <ListItemIcon>
                            <SettingsOutlined />
                        </ListItemIcon>
                        <ListItemText primary="Settings" />
                        {open ? <ExpandLess /> : <ExpandMore />}
                    </ListItemButton>
                </div>
            </Tooltip>
        ) : (
            <ListItemButton onClick={handleClick} disabled={!hasSettingsLink}>
                <ListItemIcon>
                    <SettingsOutlined />
                </ListItemIcon>
                <ListItemText primary="Settings" />
                {open ? <ExpandLess /> : <ExpandMore />}
            </ListItemButton>
        )
    }

    return (
        <>
            {renderSettingsList()}
            <Collapse in={open} timeout="auto" unmountOnExit>
                <List component="div" disablePadding>
                    {settingRoutes.map((route) => (
                        <SideBarItem
                            key={route.path}
                            name={route.name}
                            icon={<></>}
                            path={`/projects/details/${route.path}`}
                        />
                    ))}
                </List>
            </Collapse>
        </>
    )
}
export const DetailsLayout = (props: DetailsLayoutProps) => {
    const router = useRouter()
    const { id } = router.query

    const routes: Route[] = [
        {
            name: 'Overview',
            path: `/projects/details/${id}`,
            icon: <AssignmentOutlined />,
        },
        {
            name: 'Code',
            path: `/projects/details/${id}/code`,
            icon: <Code />,
        },
        {
            name: 'Data',
            path: `/projects/details/${id}/data`,
            icon: <DatasetOutlined />,
        },
        {
            name: 'Experiments',
            path: `/projects/details/${id}/experiments`,
            icon: <ScienceOutlined />,
        },
        {
            name: 'Runs',
            icon: <DirectionsRun />,
            path: '',
            subRoutes: [
                {
                    name: 'Submissions',
                    path: `/projects/details/${id}/runs/submissions`,
                    icon: <></>,
                },
                {
                    name: 'Schedules',
                    path: `/projects/details/${id}/runs/schedules`,
                    icon: <></>,
                },
            ],
        },
        {
            name: 'Models',
            path: `/projects/details/${id}/models`,
            icon: <OnlinePredictionOutlined />,
        },
    ]

    return (
        <DataProvider>
            <ProjectProvider>
                <LayoutWithSidebar
                    routes={routes}
                    additional={<SettingsList />}
                    {...props}
                />
            </ProjectProvider>
        </DataProvider>
    )
}
