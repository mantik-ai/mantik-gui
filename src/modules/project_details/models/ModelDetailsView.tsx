import {
    Add,
    DeleteOutline,
    EditOutlined,
    VisibilityOutlined,
} from '@mui/icons-material'
import { Button, Grid } from '@mui/material'
import { useRouter } from 'next/router'
import React, { useState } from 'react'

import CopyToClipboard from '../../../components/CopyToClipboard'
import { DataStateIndicator } from '../../../components/DataStateIndicator'
import { DeleteDialogLayout } from '../../../components/DeleteDialogLayout'
import { DetailsDialogLayout } from '../../../components/DetailsDialogLayout'
import { DetailsDialogList } from '../../../components/DetailsDialogList'
import { renderApiErrorMessage } from '../../../errors'
import { formatTimestamp } from '../../../helpers'
import { TileLayout } from '../../../layouts/TileLayout'
import {
    TrainedModel,
    useDeleteTrainedModelV02ProjectsProjectIdModelsTrainedModelIdDelete as unregisterTrainedModel,
    useGetAllTrainedModelsOfProjectProjectsProjectIdModelsTrainedGet,
} from '../../../queries'
import {
    ActionButton,
    generateCustomActionButtonsArray,
} from '../../../utils/generateCustomActionButtonsArray'
import {
    renderResourceActions,
    ResourceType,
} from '../../../utils/renderResourceActions'
import { parseUserRole } from '../../../utils/userRole'
import { DetailsToolbar } from '../overview/ProjectDetailsToolbar'
import { AddModelDialog } from './AddModelDialog'
import ModelActionMenu from './ModelActionMenu'
import { usePermission } from '../../../context/ProjectContext'
import { hasPermission } from '../data/actions'

const PageLengthOptions = [50, 25, 10, 5]

const blankStatus = {
    status: false,
    type: '',
    text: '',
}

const defaultSuccessStatus = {
    status: true,
    type: 'success',
    text: 'Success!',
}

const defaultFailureStatus = {
    status: true,
    type: 'error',
    text: 'Failed!',
}

export const ModelDetailsView = () => {
    const router = useRouter()
    const { id } = router.query
    const [openModelDetailsDialog, setOpenModelDetailsDialog] = useState(false)
    const [detailsDialogOpen, setDetailsDialogOpen] = React.useState(false)
    const [currentModel, setCurrentModel] = React.useState<
        TrainedModel | undefined
    >(undefined)
    const [isEdit, setIsEdit] = useState<boolean>(false)
    const [deleteDialogOpen, setdeleteDialogOpen] = React.useState(false)
    const [deleteStatus, setDeleteStatus] = React.useState(blankStatus)

    const page = 0
    const rowsPerPage = PageLengthOptions[0]

    const role = usePermission()
    const canCreate = hasPermission(role, 'CREATE')
    const canRead = hasPermission(role, 'READ')
    const canEdit = hasPermission(role, 'UPDATE')
    const canDelete = hasPermission(role, 'DELETE')
    /**
     * Get all Models functionality
     */
    const {
        data: trainedModels,
        status,
        refetch,
        error,
    } = useGetAllTrainedModelsOfProjectProjectsProjectIdModelsTrainedGet(
        id as string,
        {
            pagelength: rowsPerPage,
            startindex: page * rowsPerPage,
        }
    )
    const modelNames = trainedModels?.models.map((model) => model.name)
    /**
     * Unregister a Model(Delete) functionality
     */
    const mutatedUnregisterTrainedModel = unregisterTrainedModel({
        mutation: {
            onSuccess: () => {
                setDeleteStatus({
                    ...defaultSuccessStatus,
                    text: 'Model Deleted Successfully',
                })
                refetch()
            },
            onError: (error) => {
                setDeleteStatus({
                    ...defaultFailureStatus,
                    text: renderApiErrorMessage(error),
                })
            },
        },
    })
    const handleSubmitUnregister = () => {
        mutatedUnregisterTrainedModel.mutate({
            projectId: id as string,
            modelId: currentModel?.modelId ?? 'No_id_found',
        })
    }
    const handleDelete = (trainedModel: TrainedModel) => {
        setCurrentModel(trainedModel)
        setdeleteDialogOpen(true)
    }

    const userRole = parseUserRole(trainedModels?.userRole)
    const userActions = renderResourceActions(ResourceType.MODEL, userRole)

    /**
     * Details Model functionality
     */
    const handleDetails = (trainedModel: TrainedModel) => {
        setCurrentModel(trainedModel)
        setDetailsDialogOpen(!detailsDialogOpen)
    }
    /**
     * Edit Model functionality
     */
    function handleEditData(trainedModel: TrainedModel) {
        setOpenModelDetailsDialog(true)
        setCurrentModel(trainedModel)
        setIsEdit(true)
    }

    return (
        <DataStateIndicator
            status={status}
            text="Loading Models..."
            usePaper
            errorMessage={
                error?.response?.status === 403
                    ? "The project you're trying to access is private and you don't have the required permissions!"
                    : 'Error'
            }
        >
            <DetailsToolbar
                title={'Models'}
                tool={
                    canCreate ? (
                        <Button
                            variant="text"
                            onClick={() => setOpenModelDetailsDialog(true)}
                        >
                            <Add></Add>Add
                        </Button>
                    ) : (
                        <></>
                    )
                }
            ></DetailsToolbar>
            <Grid container spacing={4} mt={0}>
                {trainedModels?.models?.map((trainedModel) => {
                    const actionButtons: ActionButton[] = [
                        {
                            title: 'Details',
                            actionFunction: canRead
                                ? () => handleDetails(trainedModel)
                                : undefined,
                            iconComponent: (
                                <VisibilityOutlined fontSize="small" />
                            ),
                        },
                        {
                            title: 'Edit',
                            actionFunction: canEdit
                                ? () => handleEditData(trainedModel)
                                : undefined,
                            iconComponent: <EditOutlined fontSize="small" />,
                        },
                        {
                            title: 'unregister',
                            actionFunction: canDelete
                                ? () => handleDelete(trainedModel)
                                : undefined,
                            iconComponent: (
                                <DeleteOutline
                                    color={canDelete ? 'error' : 'disabled'}
                                    fontSize="small"
                                />
                            ),
                        },
                    ]
                    const actions = [
                        ...generateCustomActionButtonsArray(actionButtons),
                        <ModelActionMenu
                            key={'modelTile_MoreActionMenu'}
                            actionButtons={actionButtons}
                            modelId={trainedModel.modelId}
                            runId={trainedModel.runId ?? ''}
                            projectId={id as string}
                            userActions={userActions}
                        />,
                    ]
                    return (
                        <Grid
                            key={trainedModel.modelId}
                            item
                            xs={12}
                            md={6}
                            lg={4}
                            xl={3}
                        >
                            <TileLayout
                                key={trainedModel.modelId}
                                title={trainedModel.name}
                                subHeading={
                                    trainedModel.runName ? (
                                        <div>{`Registered from run: ${trainedModel.runName}`}</div>
                                    ) : (
                                        <></>
                                    )
                                }
                                footer={
                                    <div>
                                        Created At:{' '}
                                        {formatTimestamp(
                                            trainedModel.createdAt
                                        )}
                                    </div>
                                }
                                actions={actions}
                            />
                        </Grid>
                    )
                })}
            </Grid>

            <AddModelDialog
                open={openModelDetailsDialog}
                projectId={id as string}
                setOpen={setOpenModelDetailsDialog}
                refetchData={refetch}
                isEdit={isEdit}
                setIsEdit={setIsEdit}
                editData={currentModel}
                key={id as string}
                modelNames={modelNames ?? []}
            />
            <DetailsDialogLayout
                open={detailsDialogOpen}
                setOpen={() => setDetailsDialogOpen(!detailsDialogOpen)}
                title={'Details'}
            >
                <DetailsDialogList
                    data={
                        currentModel
                            ? {
                                  'Model ID': {
                                      data: currentModel.modelId,
                                      component: (
                                          <CopyToClipboard
                                              title={currentModel.modelId}
                                              data={currentModel.modelId}
                                          />
                                      ),
                                  },
                                  Name: { data: currentModel.name },
                                  'Run ID': {
                                      data: currentModel.runId ?? '',
                                      component: (
                                          <CopyToClipboard
                                              title={currentModel?.runId}
                                              data={currentModel?.runId ?? ''}
                                          />
                                      ),
                                  },
                                  'Created At': {
                                      data: currentModel?.createdAt
                                          ? formatTimestamp(
                                                currentModel?.createdAt
                                            )
                                          : '',
                                  },
                              }
                            : {}
                    }
                ></DetailsDialogList>
            </DetailsDialogLayout>
            <DeleteDialogLayout
                open={deleteDialogOpen}
                title="Delete Trained Model"
                text={`Are you sure you want to delete Trained Model ${currentModel?.name} from the project?`}
                handleSubmit={handleSubmitUnregister}
                handleClose={() => {
                    setdeleteDialogOpen(false)
                    setDeleteStatus(blankStatus)
                }}
                isLoading={mutatedUnregisterTrainedModel.isPending}
                status={deleteStatus}
            ></DeleteDialogLayout>
        </DataStateIndicator>
    )
}
