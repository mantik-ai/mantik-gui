import {
    Autocomplete,
    FormControl,
    FormGroup,
    TextField,
    useTheme,
} from '@mui/material'
import React, { useEffect, useState } from 'react'
import { Spacing } from '../../../components/Spacing'
import {
    AddTrainedModel,
    Run,
    TrainedModel,
    useCreateNewTrainedModelEntryV02ProjectsProjectIdModelsTrainedPost as addTrainedModel,
    useProjectsProjectIdRunsGetProjectsProjectIdRunsGet as getAllRuns,
    useUpdateTrainedModelV02ProjectsProjectIdModelsTrainedModelIdPut as updateModelName,
} from '../../../queries'
import { renderApiErrorMessage } from '../../../errors'
import { FormDialogLayout } from '../../../components/FormDialogLayout'
import { PAGE_LENGTH_OPTIONS } from '../../../constants'
import { StatusType } from '../../../components/TablesLayout/statusType'

interface AddDialogProps {
    open: boolean
    projectId: string
    setOpen: React.Dispatch<React.SetStateAction<boolean>>
    editData?: TrainedModel | undefined
    isEdit?: boolean
    setIsEdit: Function
    refetchData: () => void
    modelNames: string[]
}

export const AddModelDialog = (props: AddDialogProps) => {
    const theme = useTheme()
    const [name, setName] = useState('')
    const [status, setStatus] = useState({ status: false, type: '', text: '' })
    const [rowsPerPage, setRowsPerPage] = useState<number>(
        PAGE_LENGTH_OPTIONS[0]
    )
    const [finishedStatusRuns, setFinishedStatusRuns] = useState<Run[]>([])
    const [totalRecords, setTotalRecords] = useState<number>(0)
    const page = 0
    const [selectedRun, setSelectedRun] = useState<Run | undefined | null>(null)
    const { data, refetch: refetchAllRuns } = getAllRuns(
        props.projectId,
        {
            pagelength: rowsPerPage,
            startindex: page * rowsPerPage,
        },
        {
            query: {
                retry: 1,
            },
        }
    )
    const { mutate, isPending: isLoading } = addTrainedModel({
        mutation: {
            onSuccess: () => {
                setStatus({
                    status: true,
                    type: 'success',
                    text: 'Model details were saved successfully!',
                })
                props.refetchData()
                resetData()
            },
            onError: (error) => {
                setStatus({
                    status: true,
                    type: 'error',
                    text: renderApiErrorMessage(error),
                })
            },
        },
    })
    const renameModelName = updateModelName({
        mutation: {
            onSuccess: () => {
                setStatus({
                    status: true,
                    type: 'success',
                    text: 'Model name was updated successfully!',
                })
                props.refetchData()
            },
            onError: (error) => {
                setStatus({
                    status: true,
                    type: 'error',
                    text: renderApiErrorMessage(error),
                })
            },
        },
    })
    function handleSubmit() {
        if (!props.projectId) return
        if (props.isEdit && props.editData) {
            const data: AddTrainedModel = {
                name,
                uri: props.editData?.uri,
                location: props.editData?.location,
            }
            renameModelName.mutate({
                projectId: props.projectId,
                modelId: props.editData?.modelId,
                data,
            })
        } else {
            const data: AddTrainedModel = { name, runId: selectedRun?.runId }
            mutate({ projectId: props.projectId, data })
        }
    }

    function resetData() {
        setName('')
        props.setIsEdit(false)
        setSelectedRun(null)
        refetchAllRuns()
    }

    function handleClose(): void {
        props.setOpen(false)
        setStatus({ status: false, type: '', text: '' })
        resetData()
    }

    function handleNameChange(value: string) {
        setName(value)
        if (props.modelNames.includes(value)) {
            setStatus({
                status: true,
                type: 'error',
                text: 'Model name already exists',
            })
        } else if (!value) {
            setStatus({
                status: true,
                type: 'error',
                text: 'Please enter a name',
            })
        } else {
            setStatus({ status: false, type: '', text: '' })
        }
    }
    useEffect(() => {
        if (data && data.runs) {
            setTotalRecords(data.totalRecords)
            const runsListForDropdown = data.runs?.filter((run) => {
                return (
                    run.status === StatusType.FINISHED &&
                    run.savedModel === null
                )
            })
            setFinishedStatusRuns(runsListForDropdown || [])
        }
    }, [data])
    useEffect(() => {
        if (totalRecords > PAGE_LENGTH_OPTIONS[0]) {
            setRowsPerPage(totalRecords)
        }
    }, [totalRecords])
    useEffect(() => {
        if (props.isEdit) {
            setName(props.editData?.name || '')
        }
    }, [props.editData])

    return (
        <FormDialogLayout
            formDataState={{ name, selectedRun, error: status.status }}
            status={status}
            open={props.open}
            title={!props.isEdit ? `Create a Model` : `Edit Model`}
            content="Configure the Model details"
            handleSubmit={handleSubmit}
            handleClose={handleClose}
            isEdit={props.isEdit}
            loading={props.isEdit ? renameModelName.isPending : isLoading}
        >
            <FormGroup>
                <FormControl>
                    <TextField
                        key="name"
                        name="name"
                        id="name"
                        label="Name"
                        placeholder={'please give a unique name'}
                        size="small"
                        value={name}
                        onChange={(e) => {
                            handleNameChange(e.target.value)
                        }}
                        required
                    />
                </FormControl>
                <Spacing value={theme.spacing(2)} />
                {!props.isEdit && (
                    <FormControl>
                        <Autocomplete
                            key={props.projectId}
                            id="run"
                            options={finishedStatusRuns || []}
                            getOptionLabel={(option) => option.name}
                            value={selectedRun}
                            onChange={(event, newValue) => {
                                setSelectedRun(newValue)
                            }}
                            renderInput={(params) => (
                                <TextField
                                    {...params}
                                    label="Run"
                                    size="small"
                                    required
                                />
                            )}
                        />
                    </FormControl>
                )}
            </FormGroup>
        </FormDialogLayout>
    )
}
