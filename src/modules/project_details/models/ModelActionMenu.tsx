import React, { useEffect } from 'react'
import { useSession } from 'next-auth/react'
import DownloadIcon from '@mui/icons-material/Download'
import InventoryIcon from '@mui/icons-material/Inventory'
import HourglassTopIcon from '@mui/icons-material/HourglassTop'
import ErrorOutlinedIcon from '@mui/icons-material/ErrorOutlined'
import { ActionButton } from '../../../utils/generateCustomActionButtonsArray'
import MoreActionMenu from '../../../components/MoreActionMenu'
import {
    ContainerBuildStatus,
    PresignedUrl,
    useBuildContainerFromTrainedModelProjectsProjectIdModelsTrainedModelIdDockerBuildPost,
    useCreateAndReturnModelContainerPresignedUrlV02ProjectsProjectIdModelsTrainedModelIdDockerGet as useGenerateContainerizedModelURL,
    useGetTrainedModelProjectsProjectIdModelsTrainedModelIdGet,
} from '../../../queries'
import { RenderElement } from '../../../utils/renderResourceActions'
import { mantikApi } from '../../auth/axios'
import { downloadFileInBackground } from '../../../helpers'

type ExtendedContainerBuildStatus = ContainerBuildStatus | null
interface ModelActionMenuProps {
    projectId: string
    modelId: string
    runId: string
    actionButtons: ActionButton[]
    userActions: RenderElement[]
}

export default function ModelActionMenu(props: ModelActionMenuProps) {
    const session = useSession()
    const [generateURLContainerDownload, setGenerateURLContainerDownload] =
        React.useState(false)
    const triggerGenerateURL = () => {
        setGenerateURLContainerDownload(true)
    }

    const handleDownloadContainer = async ({ url }: PresignedUrl) => {
        setGenerateURLContainerDownload(false)
        try {
            downloadFileInBackground(url)
        } catch (error) {
            console.error('Error downloading file:', error)
        }
    }

    const { data, isSuccess, isError, error } =
        useGenerateContainerizedModelURL(props.projectId, props.modelId, {
            query: {
                enabled: generateURLContainerDownload,
                queryKey: ['modelId', props.modelId],
            },
        })

    useEffect(() => {
        if (isSuccess) {
            handleDownloadContainer(data).catch((error) => console.error(error))
        }
        if (isError) {
            setGenerateURLContainerDownload(false)
            console.error({ error, id: props.modelId })
        }
    }, [data, error, isError, isSuccess, props.modelId])

    const { data: trainedModel, refetch } =
        useGetTrainedModelProjectsProjectIdModelsTrainedModelIdGet(
            props.projectId,
            props.modelId
        )
    const { mutate } =
        useBuildContainerFromTrainedModelProjectsProjectIdModelsTrainedModelIdDockerBuildPost(
            {
                mutation: {
                    onSuccess: () => {
                        refetch()
                    },
                },
            }
        )
    const dockerizeModel = (projectId: string, modelId: string) => {
        const data = { projectId, modelId }
        mutate(data)
    }

    async function fetchModelArtifacts() {
        try {
            const response = await mantikApi.get(
                `/projects/${props.projectId}/runs/${props.runId}/artifacts`,
                {
                    headers: {
                        Authorization: `Bearer ${session.data?.user.accessToken}`,
                        Accept: 'application/json',
                    },
                }
            )
            if (response.status === 200) {
                downloadFileInBackground(response.data.url)
            }
            return response
        } catch (err: any) {
            alert(
                JSON.stringify(err?.response?.data?.detail ?? 'Unknown Error')
            )
        }
    }

    const modelTileActionButtons: ActionButton[] = [
        {
            title: 'Download Artifacts',
            actionFunction: props.userActions.includes(
                RenderElement.DOWNLOAD_ARTIFACTS
            )
                ? () => fetchModelArtifacts()
                : undefined,
            iconComponent: <DownloadIcon fontSize="small" />,
        },
    ]
    switch (trainedModel?.status as ExtendedContainerBuildStatus) {
        case null:
            modelTileActionButtons.push({
                title: 'Containerize Model',
                actionFunction: props.userActions.includes(
                    RenderElement.CONTAINERIZE_MODEL
                )
                    ? () => dockerizeModel(props.projectId, props.modelId)
                    : undefined,
                iconComponent: (
                    <InventoryIcon color="primary" fontSize="small" />
                ),
            })
            break
        case ContainerBuildStatus.SUCCESSFUL:
            modelTileActionButtons.push({
                title: 'Download Container',
                actionFunction: props.userActions.includes(
                    RenderElement.DOWNLOAD_CONTAINERIZE_MODEL
                )
                    ? triggerGenerateURL
                    : undefined,
                iconComponent: (
                    <DownloadIcon color="primary" fontSize="small" />
                ),
            })
            break

        case ContainerBuildStatus.PENDING:
            modelTileActionButtons.push({
                title: 'Build Pending',
                actionFunction: props.userActions.includes(
                    RenderElement.BUILDING_PENDING
                )
                    ? () => console.log('Build pending')
                    : undefined,
                iconComponent: (
                    <HourglassTopIcon color="primary" fontSize="small" />
                ),
            })
            break
        case ContainerBuildStatus.FAILED:
            modelTileActionButtons.push({
                title: 'Build Failed',
                actionFunction: props.userActions.includes(
                    RenderElement.BUILDING_FAILED
                )
                    ? () => console.log('Build failed')
                    : undefined,
                iconComponent: (
                    <ErrorOutlinedIcon color="error" fontSize="small" />
                ),
            })
            break
        case ContainerBuildStatus.BUILDING:
            modelTileActionButtons.push({
                title: 'Building Container',
                actionFunction: props.userActions.includes(
                    RenderElement.BUILDING_CONTAINER
                )
                    ? () => console.log('Build')
                    : undefined,
                iconComponent: (
                    <HourglassTopIcon color="primary" fontSize="small" />
                ),
            })
            break
        default:
            break
    }

    return (
        <MoreActionMenu
            key={'modelTile_MoreActionMenu'}
            menuItems={[...modelTileActionButtons, ...props.actionButtons]}
        />
    )
}
