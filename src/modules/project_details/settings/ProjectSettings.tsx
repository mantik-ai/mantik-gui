import React, { useCallback, useContext, useEffect, useState } from 'react'
import Box from '@mui/material/Box'
import {
    Alert,
    Button,
    Checkbox,
    FormControl,
    FormControlLabel,
    FormGroup,
    Grid,
    IconButton,
    Paper,
    TextField,
    Typography,
    useTheme,
} from '@mui/material'
import { useRouter } from 'next/router'
import { Spacing } from '../../../components/Spacing'
import {
    Label,
    useProjectsDeleteProjectsProjectIdDelete,
    useProjectsProjectIdGetProjectsProjectIdGet,
    useProjectsProjectIdPutProjectsProjectIdPut,
} from '../../../queries'
import ProjectDialogContext, {
    ProjectDialogParameters,
} from '../../projects_overview/contexts/ProjectDialogContext'
import { renderApiErrorMessage } from '../../../errors'
import { DeleteDialogLayout } from '../../../components/DeleteDialogLayout'
import { Delete, FileCopyOutlined } from '@mui/icons-material'
import { LabelSelector } from '../../projects_overview/components/LabelSelector'
import MarkdownEditor from '../../../components/MarkdownEditor'
import { DetailsToolbar } from '../overview/ProjectDetailsToolbar'
import { ButtonWithLoadingStatus } from '../../../components/ButtonWithLoadingStatus'
import { useSession } from 'next-auth/react'

const alertDelay = 5000

const ProjectSettings = () => {
    const theme = useTheme()
    const router = useRouter()
    const { id } = router.query
    const session = useSession()
    const projectContext =
        useContext<Partial<ProjectDialogParameters>>(ProjectDialogContext)

    const [status, setStatus] = useState({ status: false, type: '', text: '' })
    const [deleteDialogOpen, setDeleteDialogOpen] = useState(false)
    const [toggleDataTrigger, setToggleDataTrigger] = useState(false)
    const [projectChangedAlertActive, setProjectChangedAlertActive] =
        useState(false)
    const [projectDeleted, setProjectDeleted] = useState(false)

    const {
        data,
        isPending: isLoading,
        refetch,
    } = useProjectsProjectIdGetProjectsProjectIdGet(id as string)

    const changeProject = useProjectsProjectIdPutProjectsProjectIdPut({
        mutation: {
            onSuccess: async () => {
                // eslint-disable-next-line @typescript-eslint/no-unused-expressions
                setStatus({
                    status: true,
                    type: 'success',
                    text: 'Project details were saved successfully!',
                }),
                    projectContext.clearProject!()
                await refetch()
                setToggleDataTrigger((prev) => !prev)
            },
            onError: (error) => {
                setStatus({
                    status: true,
                    type: 'error',
                    text: renderApiErrorMessage(error),
                })
            },
        },
    })

    const deleteProject = useProjectsDeleteProjectsProjectIdDelete({
        mutation: {
            onSuccess: async () => {
                setStatus({
                    status: true,
                    type: 'success',
                    text: 'Project was deleted successfully!',
                })
                projectContext.clearProject!()
                setProjectDeleted(true)
            },
            onError: (error) => {
                setStatus({
                    status: true,
                    type: 'error',
                    text: renderApiErrorMessage(error),
                })
            },
        },
    })

    function handlePutSubmit() {
        if (!data?.projectId || !projectContext.project) return
        const id = data.projectId

        if (session.status === 'authenticated') {
            changeProject.mutate({
                projectId: id,
                data: projectContext.project,
            })
        } else {
            setStatus({
                status: true,
                type: 'error',
                text: 'Authentication error. Please check your inputs and try again.',
            })
        }
        setProjectChangedAlertActive(true)
    }

    function handleDeleteSubmit() {
        if (!data?.projectId) return
        const id = data.projectId

        if (session.status === 'authenticated') {
            deleteProject.mutate({
                projectId: id,
            })
        } else {
            setStatus({
                status: true,
                type: 'error',
                text: 'Authentication error. Please check your inputs and try again.',
            })
        }
    }

    function resetStatus() {
        setStatus({ status: false, type: '', text: '' })
    }

    async function handleDialogClose() {
        setDeleteDialogOpen(false)
        setProjectDeleted(false)
        if (status.type === 'success') {
            projectContext.clearProject!()
            resetStatus()
            await router.push(`/projects`)
        }
    }

    function handleClickDelete() {
        setDeleteDialogOpen(true)
        setProjectChangedAlertActive(false)
        resetStatus()
    }

    function handleLabelSelect(value: Label[]) {
        if (!projectContext.project) return
        const newLabels = value.map((val: Label) => val.labelId)
        projectContext.setLabels!(newLabels)
    }

    const projectContextCallback = useCallback(() => {
        const labels = data?.labels
            ? data?.labels.map((label) => label.labelId)
            : []
        projectContext.setProject!({
            name: data?.name || '',
            executiveSummary: data?.executiveSummary || '',
            detailedDescription: data?.detailedDescription || '',
            public: data?.public || false,
            labels: labels,
        })
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [data, toggleDataTrigger])
    const [copied, setCopied] = useState(false)

    const handleCopy = async (data: string) => {
        try {
            await navigator.clipboard.writeText(data)
            setCopied(true)
        } catch (err) {
            console.error('Failed to copy to clipboard', err)
        }
    }
    // initially load data into context
    useEffect(() => {
        if (session.status === 'authenticated') {
            projectContextCallback()
        }
    }, [projectContextCallback, session.status])

    // hide alert after project change
    useEffect(() => {
        if (!projectChangedAlertActive) return

        const timeoutId = setTimeout(() => {
            resetStatus()
            setProjectChangedAlertActive(false)
        }, alertDelay)

        return () => {
            clearTimeout(timeoutId)
        }
    }, [projectChangedAlertActive])

    return (
        <>
            <DetailsToolbar title="Settings : General" />
            <Paper className="details-content">
                <Box
                    sx={{
                        display: 'flex',
                        flexDirection: 'column',
                        gap: 3,
                    }}
                >
                    <Box>
                        {!projectDeleted && (
                            <Grid container spacing={2}>
                                <Grid item xs={12} lg={10} xl={8}>
                                    <FormGroup>
                                        <FormControl>
                                            <TextField
                                                key="id"
                                                type="text"
                                                label="Project ID"
                                                value={String(id)}
                                                disabled
                                                InputProps={{
                                                    endAdornment: (
                                                        <>
                                                            <IconButton
                                                                onClick={() =>
                                                                    handleCopy(
                                                                        String(
                                                                            id
                                                                        )
                                                                    )
                                                                }
                                                                color="primary"
                                                                aria-label="Copy to Clipboard"
                                                            >
                                                                <FileCopyOutlined fontSize="small" />
                                                            </IconButton>
                                                            {copied && (
                                                                <span>
                                                                    Copied!
                                                                </span>
                                                            )}
                                                        </>
                                                    ),
                                                }}
                                            />
                                        </FormControl>
                                        <Spacing value={theme.spacing(2)} />
                                        <FormControl>
                                            <TextField
                                                key="projectname"
                                                type="text"
                                                name="projectname"
                                                label="Project Name"
                                                value={
                                                    projectContext.project?.name
                                                }
                                                onChange={(e) => {
                                                    projectContext.setName!(
                                                        e.target.value
                                                    )
                                                }}
                                                error={
                                                    projectContext.project
                                                        ?.name === ''
                                                }
                                                helperText={
                                                    !projectContext.project
                                                        ?.name
                                                        ? 'Please enter a project name.'
                                                        : ''
                                                }
                                                required
                                            />
                                        </FormControl>
                                        <Spacing value={theme.spacing(2)} />
                                        <FormControl>
                                            <TextField
                                                key="shortdescription"
                                                type="text"
                                                name="shortdescription"
                                                label="Short Description"
                                                value={
                                                    projectContext.project
                                                        ?.executiveSummary
                                                }
                                                onChange={(e) => {
                                                    projectContext.setExecutiveSummary!(
                                                        e.target.value
                                                    )
                                                }}
                                                error={
                                                    projectContext.project
                                                        ?.executiveSummary ===
                                                    ''
                                                }
                                                helperText={
                                                    !projectContext.project
                                                        ?.executiveSummary
                                                        ? 'Please enter a short description of the project.'
                                                        : ''
                                                }
                                                required
                                            />
                                        </FormControl>
                                        <Spacing value={theme.spacing(2)} />
                                        <FormControl>
                                            <Typography variant="overline">
                                                Long Description
                                            </Typography>
                                            <MarkdownEditor
                                                value={
                                                    projectContext.project
                                                        ?.detailedDescription ??
                                                    ''
                                                }
                                                setValue={(e) => {
                                                    projectContext.setDetailedDescription!(
                                                        e
                                                    )
                                                }}
                                            />
                                        </FormControl>
                                        <Spacing value={theme.spacing(2)} />
                                        <FormControl>
                                            <Typography variant="overline">
                                                Project visibility
                                            </Typography>
                                            <FormControlLabel
                                                control={
                                                    <Checkbox
                                                        checked={
                                                            projectContext
                                                                .project?.public
                                                        }
                                                        onChange={(e) => {
                                                            projectContext.setIsPublic!(
                                                                e.target.checked
                                                            )
                                                        }}
                                                    />
                                                }
                                                label="Public"
                                            />
                                        </FormControl>
                                        <Spacing value={theme.spacing(2)} />
                                        <FormControl>
                                            <TextField
                                                key="projectowner"
                                                type="text"
                                                name="projectowner"
                                                label="Project Owner"
                                                value={data?.owner?.name || ''}
                                                disabled
                                            />
                                        </FormControl>
                                        <Spacing value={theme.spacing(2)} />
                                        <LabelSelector
                                            currentLabels={data?.labels}
                                            queryString="project"
                                            handleLabelSelect={
                                                handleLabelSelect
                                            }
                                        />
                                        <Box component="div" sx={{ mt: 3 }}>
                                            {projectChangedAlertActive && (
                                                <Alert
                                                    variant="outlined"
                                                    severity={
                                                        status.type === 'error'
                                                            ? 'error'
                                                            : 'success'
                                                    }
                                                >
                                                    {status.text}
                                                </Alert>
                                            )}
                                        </Box>
                                        <Box
                                            sx={{ mt: 3, mb: 1 }}
                                            display="flex"
                                        >
                                            <Button
                                                variant="outlined"
                                                color="error"
                                                startIcon={<Delete />}
                                                onClick={handleClickDelete}
                                            >
                                                Delete
                                            </Button>
                                            <ButtonWithLoadingStatus
                                                loading={
                                                    changeProject.isPending
                                                }
                                                margin={0}
                                            >
                                                <Button
                                                    variant="outlined"
                                                    onClick={handlePutSubmit}
                                                    disabled={
                                                        isLoading ||
                                                        !projectContext.project
                                                            ?.name ||
                                                        !projectContext.project
                                                            ?.executiveSummary
                                                        // || !dataChanged --> if state setter isn't triggered by quill editor without user input
                                                    }
                                                >
                                                    Save
                                                </Button>
                                            </ButtonWithLoadingStatus>
                                        </Box>
                                    </FormGroup>
                                </Grid>
                            </Grid>
                        )}
                    </Box>
                </Box>
            </Paper>

            <DeleteDialogLayout
                open={deleteDialogOpen}
                title="Delete Project"
                text={
                    <>
                        Are you sure you want to delete the{' '}
                        <strong>{projectContext.project?.name}</strong> poject?
                    </>
                }
                warning={
                    <>
                        <Typography variant="h6" component="p">
                            All related project data will be deleted!
                        </Typography>
                        <Typography component="p">This includes:</Typography>
                        <ul>
                            <li>
                                Experiments and Runs (tracked parameters,
                                metrics, artifacts, models, etc.)
                            </li>
                            <li>Code repositories</li>
                            <li>Data repositories</li>
                        </ul>
                    </>
                }
                handleSubmit={handleDeleteSubmit}
                handleClose={handleDialogClose}
                isLoading={deleteProject.isPending}
                status={status}
            ></DeleteDialogLayout>
        </>
    )
}

export default ProjectSettings
