import { Add } from '@mui/icons-material'
import { Button, Paper, useTheme } from '@mui/material'
import { useRouter } from 'next/router'
import React, { useContext, useState } from 'react'
import { DataStateIndicator } from '../../../../components/DataStateIndicator'
import { DeleteDialogLayout } from '../../../../components/DeleteDialogLayout'
import { Spacing } from '../../../../components/Spacing'
import {
    ActionProps,
    TablesLayout,
} from '../../../../components/TablesLayout/TablesLayout'
import { PAGE_LENGTH_OPTIONS } from '../../../../constants'
import AuthContext from '../../../../context/AuthContext'
import { renderApiErrorMessage } from '../../../../errors'
import { handlePagination } from '../../../../helpers'
import {
    useProjectsProjectIdMembersGetProjectsProjectIdMembersGet,
    useProjectsProjectIdMembersUserIdDeleteProjectsProjectIdMembersUserIdDelete,
    useUsersGetUsersGet,
} from '../../../../queries'
import { StatusProps } from '../../../../types/statusProps'
import { UserRole } from '../../../../types/UserRole'
import {
    generateCollaborationData,
    TableEntry,
} from '../../../../utils/generateCollaborationData'
import {
    RenderElement,
    renderResourceActions,
    ResourceType,
} from '../../../../utils/renderResourceActions'
import { parseUserRole } from '../../../../utils/userRole'
import {
    AddCollaboratorDialog,
    InvitedToType,
    InvitedType,
} from '../../../collaborations/AddCollaboratorDialog'
import { DetailsToolbar } from '../../overview/ProjectDetailsToolbar'

function MembersSettings() {
    const theme = useTheme()
    const router = useRouter()
    const { id } = router.query
    const [rowsPerPage, setRowsPerPage] = useState<number>(
        PAGE_LENGTH_OPTIONS[0]
    )
    const [addDialogOpen, setAddDialogOpen] = useState<boolean>(false)
    const [editItem, setEditItem] = useState<TableEntry | null>(null)
    const [page, setPage] = useState<number>(0)
    const user = useContext(AuthContext)

    const { data, status, refetch } =
        useProjectsProjectIdMembersGetProjectsProjectIdMembersGet(
            id as string,
            {
                pagelength: rowsPerPage,
                startindex: page * rowsPerPage,
            },
            {
                // async fix: triggers data refetch when authorization changes
                // fixes issue where incorrect user role is returned
                request: {
                    headers: {
                        Authorization: `Bearer ${user?.accessToken}`,
                    },
                },
            }
        )
    const { data: users } = useUsersGetUsersGet()
    const { transformedData, tableData } = generateCollaborationData(
        data?.members,
        'User'
    )
    const [deleteDialogOpen, setDeleteDialogOpen] = useState<boolean>(false)
    const [deleteStatus, setDeleteStatus] = useState<StatusProps>({
        status: false,
        type: '',
        text: '',
    })
    const [deleteData, setDeleteData] = useState({
        projectId: id,
        userId: '',
        name: '',
    })
    /**
     * Delete member functionality
     */
    const { mutate, isPending: isLoading } =
        useProjectsProjectIdMembersUserIdDeleteProjectsProjectIdMembersUserIdDelete(
            {
                mutation: {
                    onSuccess: () => {
                        setDeleteStatus({
                            status: true,
                            type: 'success',
                            text: 'Collaborator was deleted successfully!',
                        })
                        refetch()
                    },
                    onError: (error) => {
                        setDeleteStatus({
                            status: true,
                            type: 'error',
                            text: renderApiErrorMessage(error),
                        })
                    },
                },
            }
        )

    const userRole = parseUserRole(data?.userRole)
    const userActions = renderResourceActions(ResourceType.SETTINGS, userRole)

    function handleDeleteClose() {
        setDeleteDialogOpen(false)
        setDeleteStatus({ status: false, type: '', text: '' })
    }
    const handleMemberDelete = (index: number) => {
        setDeleteDialogOpen(true)
        const collaborator = transformedData ? transformedData[index] : null
        if (!!collaborator) {
            setDeleteData({
                projectId: id,
                userId: collaborator.ID?.data || '',
                name: collaborator.Name.data,
            })
        }
    }
    function handleDeleteSubmit() {
        const projectId = String(id)
        const userId = deleteData.userId
        mutate({ projectId, userId })
    }
    /**
     * Edit member functionality
     */
    const handleMemberEdit = (index: number) => {
        setAddDialogOpen(true)
        const collaborator = transformedData ? transformedData[index] : null
        if (!!collaborator) {
            setEditItem(collaborator)
        }
    }

    const action: ActionProps = {}
    if (userActions.includes(RenderElement.DELETE)) {
        action.delete = handleMemberDelete
    }
    if (userActions.includes(RenderElement.EDIT)) {
        action.edit = handleMemberEdit
    }

    return (
        <Paper>
            <DataStateIndicator
                status={status}
                text="Loading Members..."
                usePaper
            >
                <DetailsToolbar
                    title="Members"
                    tool={
                        <Button
                            variant="text"
                            disabled={!userActions.includes(RenderElement.ADD)}
                            onClick={() => setAddDialogOpen(true)}
                        >
                            <Add></Add>Add
                        </Button>
                    }
                />
                <Spacing value={theme.spacing(1)}></Spacing>
                <TablesLayout
                    data={tableData}
                    action={action}
                    page={{
                        rowsPerPage: rowsPerPage,
                        page: page,
                        totalRecords: data?.totalRecords || 0,
                        handleRowsChange: (type, num) =>
                            handlePagination(
                                type,
                                num,
                                setRowsPerPage,
                                setPage
                            ),
                    }}
                />
            </DataStateIndicator>
            <AddCollaboratorDialog
                id={id as string}
                users={users?.users || []}
                invitedType={InvitedType.USER}
                invitedToType={InvitedToType.PROJECT}
                open={addDialogOpen}
                setOpen={() => setAddDialogOpen(false)}
                role={UserRole.Guest}
                editItem={editItem}
                setEditItem={setEditItem}
                refetch={refetch}
            />
            <DeleteDialogLayout
                warning="Removing a member from the project will remove their access rights to the project. This doesn't apply if the member has access via their group/organization membership."
                open={deleteDialogOpen}
                title={'Delete Collaborator'}
                text={`Are you sure you want to delete Collaborator - ${deleteData?.name} from the project?`}
                handleClose={handleDeleteClose}
                isLoading={isLoading}
                status={deleteStatus}
                handleSubmit={handleDeleteSubmit}
            />
        </Paper>
    )
}

export default MembersSettings
