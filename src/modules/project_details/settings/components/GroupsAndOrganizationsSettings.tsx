import { Add } from '@mui/icons-material'
import { Button, useTheme } from '@mui/material'
import { useRouter } from 'next/router'
import React, { useContext, useState } from 'react'
import { DataStateIndicator } from '../../../../components/DataStateIndicator'
import { DeleteDialogLayout } from '../../../../components/DeleteDialogLayout'
import { Spacing } from '../../../../components/Spacing'
import {
    ActionProps,
    TablesLayout,
} from '../../../../components/TablesLayout/TablesLayout'
import { PAGE_LENGTH_OPTIONS } from '../../../../constants'
import AuthContext from '../../../../context/AuthContext'
import { renderApiErrorMessage } from '../../../../errors'
import { handlePagination } from '../../../../helpers'
import {
    useGroupsGetGroupsGet,
    useOrganizationsGetOrganizationsGet,
    useProjectsProjectIdGroupsGetProjectsProjectIdGroupsGet,
    useProjectsProjectIdGroupsGroupIdDeleteProjectsProjectIdGroupsGroupIdDelete,
    useProjectsProjectIdOrganizationGetProjectsProjectIdOrganizationsGet,
    useProjectsProjectIdOrganizationOrganizationIdDeleteProjectsProjectIdOrganizationsOrganizationIdDelete,
} from '../../../../queries'
import { StatusProps } from '../../../../types/statusProps'
import { UserRole } from '../../../../types/UserRole'
import { addTotalRecords } from '../../../../utils/addTotalRecords'

import {
    generateCollaborationData,
    TableEntry,
} from '../../../../utils/generateCollaborationData'
import {
    RenderElement,
    renderResourceActions,
    ResourceType,
} from '../../../../utils/renderResourceActions'
import { parseUserRole } from '../../../../utils/userRole'
import {
    AddCollaboratorDialog,
    InvitedToType,
    InvitedType,
} from '../../../collaborations/AddCollaboratorDialog'
import { DetailsToolbar } from '../../overview/ProjectDetailsToolbar'

export enum RadioOptionInviteType {
    GROUP = 'GROUP',
    ORGANIZATION = 'ORGANIZATION',
}
export function GroupsAndOrganizationsSettings() {
    const theme = useTheme()
    const router = useRouter()
    const { id } = router.query
    const [rowsPerPage, setRowsPerPage] = useState<number>(
        PAGE_LENGTH_OPTIONS[0]
    )
    const [radioOptionInviteType, setRadioOptionInviteType] = useState(
        RadioOptionInviteType.GROUP
    )

    const [page, setPage] = useState<number>(0)
    const [addDialogOpen, setAddDialogOpen] = useState<boolean>(false)
    const user = useContext(AuthContext)
    const [editItem, setEditItem] = useState<TableEntry | null>(null)
    const [deleteDialogOpen, setDeleteDialogOpen] = useState<boolean>(false)
    const [deleteStatus, setDeleteStatus] = useState<StatusProps>({
        status: false,
        type: '',
        text: '',
    })
    const [deleteData, setDeleteData] = useState({
        projectId: id,
        collaboratorId: '',
        name: '',
        type: '',
    })
    const {
        data: organizationsData,
        status,
        refetch: refetchOrganizations,
    } = useProjectsProjectIdOrganizationGetProjectsProjectIdOrganizationsGet(
        id as string,
        {
            pagelength: rowsPerPage,
            startindex: page * rowsPerPage,
        },
        {
            // async fix: triggers data refetch when authorization changes
            // fixes issue where incorrect user role is returned
            request: {
                headers: {
                    Authorization: `Bearer ${user?.accessToken}`,
                },
            },
        }
    )
    const { data: groupsData, refetch: refetchGroups } =
        useProjectsProjectIdGroupsGetProjectsProjectIdGroupsGet(
            id as string,
            {
                pagelength: rowsPerPage,
                startindex: page * rowsPerPage,
            },
            {
                // async fix: triggers data refetch when authorization changes
                // fixes issue where incorrect user role is returned
                request: {
                    headers: {
                        Authorization: `Bearer ${user?.accessToken}`,
                    },
                },
            }
        )

    const { transformedData, tableData: organizationsTableData } =
        generateCollaborationData(
            organizationsData?.organizations,
            'Organization'
        )
    const {
        transformedData: groupsTransformedData,
        tableData: groupsTableData,
    } = generateCollaborationData(groupsData?.groups, 'Group')

    const groupsOrganizationsConcatenatedData = [
        ...transformedData,
        ...groupsTransformedData,
    ]
    const refetchFunctions = {
        refetchGroupCollaborations: refetchGroups,
        refetchOrganizationCollaborations: refetchOrganizations,
    }
    const { data: getAllGroups } = useGroupsGetGroupsGet()
    const { data: getAllOrganizations } = useOrganizationsGetOrganizationsGet()
    /**
     * Delete group/organization functionality
     */
    const {
        mutate: deleteOrganization,
        isPending: deleteOrganizationIsLoading,
    } =
        useProjectsProjectIdOrganizationOrganizationIdDeleteProjectsProjectIdOrganizationsOrganizationIdDelete(
            {
                mutation: {
                    onSuccess: () => {
                        setDeleteStatus({
                            status: true,
                            type: 'success',
                            text: 'Organization was deleted successfully!',
                        })
                        refetchOrganizations()
                    },
                    onError: (error) => {
                        setDeleteStatus({
                            status: true,
                            type: 'error',
                            text: renderApiErrorMessage(error),
                        })
                    },
                },
            }
        )
    const { mutate: deleteGroup, isPending: deleteGroupIsLoading } =
        useProjectsProjectIdGroupsGroupIdDeleteProjectsProjectIdGroupsGroupIdDelete(
            {
                mutation: {
                    onSuccess: () => {
                        setDeleteStatus({
                            status: true,
                            type: 'success',
                            text: 'Group was deleted successfully!',
                        })
                        refetchGroups()
                    },
                    onError: (error) => {
                        setDeleteStatus({
                            status: true,
                            type: 'error',
                            text: renderApiErrorMessage(error),
                        })
                    },
                },
            }
        )

    const userRole = parseUserRole(organizationsData?.userRole)
    const userActions = renderResourceActions(ResourceType.SETTINGS, userRole)

    function handleDeleteClose() {
        setDeleteDialogOpen(false)
        setDeleteStatus({ status: false, type: '', text: '' })
    }
    const handleDelete = (index: number) => {
        setDeleteDialogOpen(true)
        const collaborator =
            groupsOrganizationsConcatenatedData.length &&
            groupsOrganizationsConcatenatedData[index]
        if (!!collaborator && collaborator.Type?.data) {
            setDeleteData({
                projectId: id,
                collaboratorId: collaborator.ID?.data || '',
                name: collaborator.Name.data,
                type: collaborator.Type.data,
            })
        }
    }
    function handleDeleteSubmit() {
        const projectId = String(id)
        if (deleteData.type === 'Group') {
            const groupId = deleteData.collaboratorId
            deleteGroup({ projectId, groupId })
        } else {
            const organizationId = deleteData.collaboratorId
            deleteOrganization({ projectId, organizationId })
        }
    }
    const handleEdit = (index: number) => {
        setAddDialogOpen(true)
        const collaborator =
            groupsOrganizationsConcatenatedData.length &&
            groupsOrganizationsConcatenatedData[index]
        if (!!collaborator) {
            setEditItem(collaborator)
        }
    }

    const action: ActionProps = {}
    if (userActions.includes(RenderElement.DELETE)) {
        action.delete = handleDelete
    }
    if (userActions.includes(RenderElement.EDIT)) {
        action.edit = handleEdit
    }
    return (
        <DataStateIndicator
            status={status}
            text="Loading Groups/Organizations..."
            usePaper
        >
            <DetailsToolbar
                title="Groups/Organizations"
                tool={
                    <Button
                        variant="text"
                        disabled={!userActions.includes(RenderElement.ADD)}
                        onClick={() => setAddDialogOpen(true)}
                    >
                        <Add></Add>Add
                    </Button>
                }
            />
            <Spacing value={theme.spacing(1)}></Spacing>
            <TablesLayout
                data={[...organizationsTableData, ...groupsTableData]}
                action={action}
                page={{
                    rowsPerPage: rowsPerPage,
                    page: page,
                    totalRecords:
                        addTotalRecords(
                            organizationsData?.totalRecords,
                            groupsData?.totalRecords
                        ) || 0,
                    handleRowsChange: (type, num) =>
                        handlePagination(type, num, setRowsPerPage, setPage),
                }}
            />
            <AddCollaboratorDialog
                id={id as string}
                users={
                    radioOptionInviteType === RadioOptionInviteType.GROUP &&
                    getAllGroups?.userGroups
                        ? getAllGroups?.userGroups
                        : radioOptionInviteType ===
                                RadioOptionInviteType.ORGANIZATION &&
                            getAllOrganizations?.organizations
                          ? getAllOrganizations?.organizations
                          : undefined
                }
                invitedType={InvitedType.GROUP}
                invitedToType={InvitedToType.PROJECT}
                open={addDialogOpen}
                setOpen={() => setAddDialogOpen(false)}
                role={UserRole.Guest}
                editItem={editItem}
                setEditItem={setEditItem}
                refetch={refetchFunctions}
                radioOptionInviteType={radioOptionInviteType}
                setRadioOptionInviteType={setRadioOptionInviteType}
            />
            <DeleteDialogLayout
                warning="Removing a group/organization from the project will remove all its member's access rights to the project. This doesn't apply if a member from the group/organization has direct access to the project."
                open={deleteDialogOpen}
                title={'Delete Collaborator'}
                text={`Are you sure you want to delete Collaborator - ${deleteData?.name} from the project?`}
                handleClose={handleDeleteClose}
                isLoading={
                    deleteData.type === 'Group'
                        ? deleteGroupIsLoading
                        : deleteOrganizationIsLoading
                }
                status={deleteStatus}
                handleSubmit={handleDeleteSubmit}
            />
        </DataStateIndicator>
    )
}
