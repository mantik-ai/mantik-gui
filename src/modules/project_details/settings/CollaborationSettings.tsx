import * as React from 'react'
// import EditUserGroupsContainer from './components/EditUserGroupsContainer'
// import EditUserContainer from './components/EditUsersContainer'
import { Spacing } from '../../../components/Spacing'
import { useTheme } from '@mui/material'

export const CollaborationSettings = () => {
    const theme = useTheme()

    return (
        <>
            {/*<EditUserGroupsContainer /> TODO (ts) Can be added again once EditUserGroupsContainer is fixed*/}
            <Spacing value={theme.spacing(4)} />
            {/*<EditUserContainer /> TODO (ts) Can be added again once EditUserContainer is fixed*/}
        </>
    )
}
