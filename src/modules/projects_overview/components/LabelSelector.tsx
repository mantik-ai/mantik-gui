import { Help } from '@mui/icons-material'
import { Autocomplete, TextField, Tooltip } from '@mui/material'
import { useEffect, useRef, useState } from 'react'
import { Label, Scope, useLabelsGetLabelsGet } from '../../../queries'
import { autoCompleteLabelOption } from '../../../utils/autoCompleteLabelOption'

interface LabelOption extends Label {
    categoryWithSubCategory: string
}

interface LabelSelectorProps {
    currentLabels?: Label[]
    queryString?: string
    handleLabelSelect?: (value: LabelOption[]) => void
    helpText?: string
}
export const LabelSelector = ({
    currentLabels = [],
    queryString = '',
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    handleLabelSelect = () => {},
    helpText = '',
}: LabelSelectorProps) => {
    const scope: Scope = queryString as Scope

    const queryParam = queryString
        ? {
              scope: scope,
          }
        : {}
    const { data, status } = useLabelsGetLabelsGet(queryParam)
    const loading = status !== 'success'
    const options = autoCompleteLabelOption(data)
    if (options)
        currentLabels = currentLabels.filter((label) =>
            options.find((l) => l.labelId === label.labelId)
        )
    const [value, setValue] = useState<LabelOption[]>([])

    function handleChange(value: LabelOption[]) {
        handleLabelSelect(value)
        setValue(value)
    }

    const memorizedLabelIds = useRef<string[]>([]) // hack to avoid constant rerendering due to currentLabels in the dependency array being an object
    useEffect(() => {
        if (!currentLabels.length) return
        const hasChanged = currentLabels.some(
            ({ labelId }, idx) => memorizedLabelIds.current[idx] !== labelId
        )
        if (hasChanged) {
            memorizedLabelIds.current = currentLabels.map(
                ({ labelId }) => labelId
            )
            const newValue = autoCompleteLabelOption({
                totalRecords: currentLabels.length,
                pageRecords: currentLabels.length,
                labels: currentLabels,
            })
            setValue(newValue)
        }
    }, [currentLabels])

    return (
        <div
            style={{
                display: 'flex',
                alignItems: 'center',
            }}
        >
            <Autocomplete
                multiple
                id="labels"
                options={
                    options
                        ? options.sort(
                              (a, b) =>
                                  -b.categoryWithSubCategory.localeCompare(
                                      a.categoryWithSubCategory
                                  )
                          )
                        : []
                }
                groupBy={(option) => option.categoryWithSubCategory}
                getOptionLabel={(option) => option.name}
                loading={loading}
                isOptionEqualToValue={(option, value) =>
                    option.labelId === value.labelId
                } // fix for value error onChange -- fails if currentLabel is not in options --> console error is thrown
                onChange={(_, value) => handleChange(value)}
                value={value}
                renderInput={(params) => (
                    <TextField
                        {...params}
                        label="Labels"
                        size="small"
                        placeholder="add..."
                    />
                )}
                fullWidth
            />
            {helpText && (
                <Tooltip title={helpText} arrow>
                    <Help
                        style={{
                            marginLeft: '8px',
                            cursor: 'pointer',
                        }}
                    />
                </Tooltip>
            )}
        </div>
    )
}
