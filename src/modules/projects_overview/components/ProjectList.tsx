import { Stack, Typography, useTheme } from '@mui/material'
import React from 'react'
import { DataStateIndicator } from '../../../components/DataStateIndicator'
import { useSearchParameterContext } from '../contexts/SearchParameterContext'
import { ProjectCard } from './ProjectCard'
import { LcpContent } from '../../../components/LcpContent'

export const ProjectList = () => {
    const theme = useTheme()
    const searchParameterContext = useSearchParameterContext()
    const styling = {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        height: '100%',
    }
    return (
        <>
            <DataStateIndicator
                status={searchParameterContext.state.projectsQueryStatus}
                text="Loading Projects..."
            >
                <LcpContent />
                {searchParameterContext.state.projectsQuery?.projects
                    ?.length ? (
                    <Stack spacing={theme.spacing(2)}>
                        {searchParameterContext.state.projectsQuery?.projects?.map(
                            (project) => (
                                <ProjectCard
                                    key={project.projectId}
                                    project={project}
                                ></ProjectCard>
                            )
                        )}
                    </Stack>
                ) : (
                    <Stack sx={styling}>
                        <Typography variant="body1">
                            No matching projects found
                        </Typography>
                    </Stack>
                )}
            </DataStateIndicator>
        </>
    )
}
