import { Help, Person, SearchOutlined } from '@mui/icons-material'
import {
    Box,
    Divider,
    FormControl,
    IconButton,
    List,
    ListItemButton,
    Link as MUILink,
    Stack,
    TextField,
    Tooltip,
    Typography,
    useMediaQuery,
    useTheme,
} from '@mui/material'
import React, { useContext } from 'react'
import { Spacing } from '../../../components/Spacing'
import { useSearchParameterContext } from '../contexts/SearchParameterContext'
import { LabelSelector } from './LabelSelector'
import { useProjectsUserUserIdGetProjectsUserUserIdGet } from '../../../queries'
import AuthContext from '../../../context/AuthContext'
import { DataStateIndicator } from '../../../components/DataStateIndicator'
import { shortenString } from '../../../helpers'
import { useSession } from 'next-auth/react'

export const SearchSideBar = () => {
    const theme = useTheme()
    const searchParameterContext = useSearchParameterContext()
    const { status: sessionStatus } = useSession()
    const userContext = useContext(AuthContext)
    const userId = userContext.usersData?.userId

    const upToMediumSize = useMediaQuery(theme.breakpoints.up('md'))
    const { data, status } = useProjectsUserUserIdGetProjectsUserUserIdGet(
        userId ?? '',
        undefined,
        { query: { enabled: Boolean(userId) } }
    )

    const handleKeyPress = (e: { key: string }) => {
        if (e.key === 'Enter') {
            handleSearch()
        }
    }
    const handleSearch = () => {
        searchParameterContext.handleRefetch()
    }

    const styling = {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        height: '100%',
    }

    return (
        <Box sx={{ pl: theme.spacing(0.5) }}>
            <Spacing value={theme.spacing(4)}></Spacing>
            <FormControl fullWidth>
                <div
                    style={{
                        display: 'flex',
                        alignItems: 'center',
                    }}
                >
                    <TextField
                        id="search-string"
                        label="Search"
                        size="small"
                        variant="outlined"
                        value={searchParameterContext.state.searchString}
                        onChange={(e) => {
                            searchParameterContext.dispatch({
                                type: 'setSearchString',
                                payload: e.target.value,
                            })
                        }}
                        onKeyDown={handleKeyPress}
                        InputProps={{
                            endAdornment: (
                                <IconButton onClick={handleSearch}>
                                    <SearchOutlined />
                                </IconButton>
                            ),
                        }}
                        fullWidth
                    />
                    <Tooltip
                        title="The free text search looks for case-sensitive matches in project title, short, and long description."
                        arrow
                    >
                        <Help
                            style={{
                                marginLeft: '8px',
                                cursor: 'pointer',
                            }}
                        />
                    </Tooltip>
                </div>
                <Spacing value={theme.spacing(4)}></Spacing>
                <LabelSelector
                    currentLabels={searchParameterContext.state.searchLabels}
                    handleLabelSelect={(e) =>
                        searchParameterContext.dispatch({
                            type: 'setSearchLabels',
                            payload: e,
                        })
                    }
                    helpText="Projects are matched by labels assigned to the project itself or any of it's related code repositories, data, and experiments."
                />

                <Spacing value={theme.spacing(4)}></Spacing>
                <Divider />
                <Spacing value={theme.spacing(4)}></Spacing>
                {upToMediumSize ? (
                    <>
                        <Stack direction="row" gap={theme.spacing(0.5)}>
                            <Stack justifyContent="center">
                                <Person />
                            </Stack>
                            <Typography variant="h6">Your Projects</Typography>
                        </Stack>
                        <Spacing value={theme.spacing(1)}></Spacing>
                        <DataStateIndicator
                            status={
                                sessionStatus === 'unauthenticated'
                                    ? 'error'
                                    : status
                            }
                            text="Loading Your Projects..."
                            errorMessage="Please log in to create a project."
                        >
                            {data && data.projects?.length ? (
                                <List>
                                    {data?.projects?.map((project) => (
                                        <ListItemButton
                                            key={project.projectId}
                                            sx={{
                                                backgroundColor:
                                                    theme.palette.background
                                                        .paper,
                                                boxShadow: theme.shadows[2],
                                                '&:hover': {
                                                    backgroundColor:
                                                        theme.palette.grey[300],
                                                },
                                                marginBottom: theme.spacing(1),
                                                padding: 0,
                                            }}
                                        >
                                            <MUILink
                                                paragraph
                                                variant="body1"
                                                color="inherit"
                                                underline="none"
                                                href={`/projects/details/${project.projectId}`}
                                                sx={{
                                                    cursor: 'pointer',
                                                    marginBottom: 0,
                                                    padding: `${theme.spacing(
                                                        1
                                                    )} ${theme.spacing(3)}`,
                                                    width: '100%',
                                                }}
                                            >
                                                {shortenString(
                                                    project.name,
                                                    30,
                                                    50,
                                                    window.innerWidth
                                                )}
                                            </MUILink>
                                        </ListItemButton>
                                    ))}
                                </List>
                            ) : (
                                <Stack sx={styling}>
                                    <Typography variant="body1">
                                        No matching projects found
                                    </Typography>
                                </Stack>
                            )}
                        </DataStateIndicator>
                    </>
                ) : null}
            </FormControl>
        </Box>
    )
}
