import {
    Button,
    Checkbox,
    Dialog,
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
    FormControl,
    FormControlLabel,
    FormGroup,
    TextField,
    Typography,
    useTheme,
    Alert,
} from '@mui/material'
import React, { useContext, useState } from 'react'
import { Spacing } from '../../components/Spacing'
import ProjectDialogContext from './contexts/ProjectDialogContext'
import router from 'next/router'
import { Label, useProjectsPostProjectsPost } from '../../queries'
import { renderApiErrorMessage } from '../../errors'
import { LabelSelector } from './components/LabelSelector'
import { ButtonWithLoadingStatus } from '../../components/ButtonWithLoadingStatus'
import { useAuthentication } from '../../hooks/useAuthentication'
import MarkdownEditor from '../../components/MarkdownEditor'

interface ProjectRepeatDialogProps {
    open: boolean
    projectId: string
    setOpen: React.Dispatch<React.SetStateAction<boolean>>
}

export const ProjectDialog = (props: ProjectRepeatDialogProps) => {
    const theme = useTheme()
    const authentication = useAuthentication()
    const projectContext = useContext(ProjectDialogContext)
    const [loading, setLoading] = useState(false)
    const [status, setStatus] = useState({ status: false, type: '', text: '' })
    const handleClose = () => {
        props.setOpen(false)
        projectContext.clearProject!()
        setStatus({ status: false, type: '', text: '' })
    }

    const [nameErrorText, setNameErrorText] = useState('')
    const { mutate } = useProjectsPostProjectsPost({
        mutation: {
            onSuccess: async (response) => {
                await router.push(`/projects/details/${response.projectId}`)
                setStatus({
                    status: true,
                    type: 'success',
                    text: 'Project details were saved successfully!',
                })
                setLoading(false)
                handleClose()
            },
            onError: (error) => {
                setStatus({
                    status: true,
                    type: 'error',
                    text: renderApiErrorMessage(error),
                })
                setLoading(false)
            },
        },
    })
    const onSubmit = () => {
        setLoading(true)
        if (!projectContext.project?.name) {
            setNameErrorText('Please enter name')
            setLoading(false)
        } else {
            setNameErrorText('')
            if (authentication === 'authenticated') {
                mutate({ data: projectContext.project })
            } else {
                setStatus({
                    status: true,
                    type: 'error',
                    text: 'Authentication error. Please check your inputs and try again.',
                })
                setLoading(false)
            }
        }
    }

    function handleLabelSelect(value: any) {
        if (!projectContext.project) return
        const newLabels = value.map((val: Label) => val.labelId)
        projectContext.setLabels!(newLabels)
    }

    return (
        <Dialog fullWidth maxWidth="sm" open={props.open} onClose={handleClose}>
            <DialogTitle>Create a Project</DialogTitle>
            {status.status && (
                <DialogContent>
                    <Alert
                        variant="outlined"
                        severity={status.type === 'error' ? 'error' : 'success'}
                    >
                        {status.text}
                    </Alert>
                    {status.type === 'success' && (
                        <Button onClick={() => handleClose()}>Close</Button>
                    )}
                </DialogContent>
            )}
            {status.type != 'success' && (
                <>
                    <DialogContent>
                        <DialogContentText>
                            Configure the project details
                        </DialogContentText>
                        <Spacing value={theme.spacing(2)} />
                        <FormGroup>
                            <FormControl>
                                <TextField
                                    key="name"
                                    name="name"
                                    label="Project Name"
                                    size="small"
                                    value={projectContext.project?.name}
                                    error={!!nameErrorText}
                                    helperText={nameErrorText}
                                    onChange={(e) => {
                                        projectContext.setName!(e.target.value)
                                        setNameErrorText('')
                                    }}
                                    required
                                />
                            </FormControl>
                            <Spacing value={theme.spacing(2)} />
                            <FormControl>
                                <TextField
                                    key="executive_summary"
                                    name="executive_summary"
                                    label="Short Description"
                                    size="small"
                                    value={
                                        projectContext.project?.executiveSummary
                                    }
                                    onChange={(e) =>
                                        projectContext.setExecutiveSummary!(
                                            e.target.value
                                        )
                                    }
                                    required
                                />
                            </FormControl>
                            <Spacing value={theme.spacing(2)} />
                            <FormControl>
                                <Typography variant="overline">
                                    Long Description
                                </Typography>
                                <MarkdownEditor
                                    value={
                                        projectContext.project
                                            ?.detailedDescription ?? ''
                                    }
                                    setValue={(e) => {
                                        projectContext.setDetailedDescription!(
                                            e
                                        )
                                    }}
                                />
                            </FormControl>
                            <Spacing value={theme.spacing(2)} />
                            <FormControlLabel
                                control={
                                    <Checkbox
                                        checked={projectContext.project?.public}
                                        onChange={(e) => {
                                            projectContext.setIsPublic!(
                                                e.target.checked
                                            )
                                        }}
                                    />
                                }
                                label="Make this a Public Project"
                            />
                        </FormGroup>
                        <Spacing value={theme.spacing(2)} />
                        <LabelSelector
                            queryString="project"
                            handleLabelSelect={handleLabelSelect}
                        />
                    </DialogContent>

                    <DialogActions>
                        <Button onClick={handleClose}>Cancel</Button>
                        <ButtonWithLoadingStatus loading={loading}>
                            <Button disabled={loading} onClick={onSubmit}>
                                Create
                            </Button>
                        </ButtonWithLoadingStatus>
                    </DialogActions>
                </>
            )}
        </Dialog>
    )
}
