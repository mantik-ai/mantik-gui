import { createContext, Dispatch, SetStateAction, useState } from 'react'
import { AddProject } from '../../../queries'

export interface ProjectDialogParameters {
    project?: AddProject
    setName: (x: string) => void
    setExecutiveSummary: (x: string) => void
    setDetailedDescription: (x: string) => void
    setIsPublic: (x: boolean) => void
    setProject: Dispatch<SetStateAction<AddProject | undefined>>
    setLabels: (x: string[]) => void
    removeLabel: (x: string) => void
    clearProject: () => void
}
const ProjectDialogContext = createContext<Partial<ProjectDialogParameters>>({})

interface ProjectDialogProviderProps {
    children: React.ReactNode
}
export const ProjectDialogProvider: React.FC<ProjectDialogProviderProps> = (
    props
) => {
    // initialization for Project object data
    const projectObject = {
        name: '',
        executiveSummary: '',
        detailedDescription: '',
        public: false,
        labels: [],
    } as AddProject
    const [project, setProject] = useState<AddProject | undefined>(
        projectObject
    )
    const clearProject = () => {
        setProject(projectObject)
    }
    const setName = (x: string) => {
        if (project) {
            setProject((project) => ({
                ...project,
                name: x,
            }))
        }
    }
    const setExecutiveSummary = (x: string) => {
        if (project) {
            setProject((project) => ({
                ...project,
                executiveSummary: x,
            }))
        }
    }
    const setDetailedDescription = (x: string) => {
        if (project) {
            setProject((project) => ({
                ...project,
                detailedDescription: x,
            }))
        }
    }
    const setIsPublic = (x: boolean) => {
        if (project) {
            setProject((project) => ({
                ...project,
                public: x,
            }))
        }
    }

    const setLabels = (x: string[]) => {
        if (project && project.labels) {
            setProject((project) => ({
                ...project,
                labels: [...x],
            }))
        }
    }

    const removeLabel = (x: string) => {
        if (project && project.labels) {
            const filteredLabels = project.labels.filter((id) => id !== x)
            setProject((project) => ({
                ...project,
                labels: filteredLabels,
            }))
        }
    }

    return (
        <ProjectDialogContext.Provider
            value={{
                project,
                setName,
                setExecutiveSummary,
                setDetailedDescription,
                setIsPublic,
                setProject,
                setLabels,
                removeLabel,
                clearProject,
            }}
        >
            {props.children}
        </ProjectDialogContext.Provider>
    )
}

export default ProjectDialogContext
