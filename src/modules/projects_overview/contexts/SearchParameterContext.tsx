import { createContext, useContext, useEffect, useReducer } from 'react'
import { QueryStatus } from '@tanstack/react-query'
import useDebounce from '../../../hooks/useDebounce'
import {
    Label,
    ProjectsGet200Response,
    useProjectsGetProjectsGet,
} from '../../../queries'
import { SEARCH_DEBOUNCE } from '../../../constants'

interface ProblemType {
    name: string
    active: boolean
}

type Action =
    | { type: 'setSearchString'; payload: string }
    | { type: 'setProblemType'; payload: { idx: number; value: boolean } }
    | { type: 'setAllProblemTypes'; payload: Label[] }
    | {
          type: 'setSearchLabels'
          payload: Label[]
      }
    | {
          type: 'setProjectsQueryResult'
          payload: ProjectsGet200Response
      }
    | {
          type: 'setProjectsQueryStatus'
          payload: QueryStatus
      }
type Dispatch = (action: Action) => void

export interface SearchParameters {
    searchString: string
    searchLabels: Label[]
    problemTypes: ProblemType[]
    projectsQueryStatus?: QueryStatus
    projectsQuery?: ProjectsGet200Response
}

const SearchParameterContext = createContext<
    | {
          state: SearchParameters
          dispatch: Dispatch
          handleRefetch: () => void
      }
    | undefined
>(undefined)

const reducer = (state: SearchParameters, action: Action): SearchParameters => {
    switch (action.type) {
        case 'setSearchString':
            return { ...state, searchString: action.payload }
        case 'setAllProblemTypes':
            return {
                ...state,
                problemTypes: action.payload.map(
                    (l) => ({ name: l.name, active: false }) as ProblemType
                ),
            }
        case 'setProblemType':
            state.problemTypes[action.payload.idx].active = action.payload.value
            return { ...state }
        case 'setSearchLabels':
            return { ...state, searchLabels: action.payload }
        case 'setProjectsQueryStatus':
            return { ...state, projectsQueryStatus: action.payload }
        case 'setProjectsQueryResult':
            return { ...state, projectsQuery: action.payload }
    }
}
interface SearchParameterProviderProps {
    children: React.ReactNode
}
const SearchParameterProvider: React.FC<SearchParameterProviderProps> = (
    props
) => {
    const [state, dispatch] = useReducer(reducer, {
        searchString: '',
        problemTypes: [],
        searchLabels: [],
    })

    const debouncedSearchString = useDebounce(
        state.searchString,
        SEARCH_DEBOUNCE
    )
    const labelIds: string[] = state.searchLabels.map((label) => label.labelId)

    const {
        data: projectsResult,
        status: projectsResultStatus,
        refetch,
    } = useProjectsGetProjectsGet({
        labels: labelIds,
        words: debouncedSearchString.length
            ? debouncedSearchString.split(' ')
            : undefined,
    })
    const handleRefetch = async () => {
        await refetch()
    }
    useEffect(() => {
        if (projectsResult) {
            dispatch({
                type: 'setProjectsQueryResult',
                payload: projectsResult,
            })
        }
    }, [projectsResult])

    useEffect(() => {
        dispatch({
            type: 'setProjectsQueryStatus',
            payload: projectsResultStatus,
        })
    }, [projectsResultStatus])

    const value = { state, dispatch, handleRefetch }

    return (
        <SearchParameterContext.Provider value={value}>
            {props.children}
        </SearchParameterContext.Provider>
    )
}

function useSearchParameterContext() {
    const context = useContext(SearchParameterContext)
    if (context === undefined) {
        throw new Error('useCount must be used within a CountProvider')
    }
    return context
}

export { SearchParameterProvider, useSearchParameterContext }
