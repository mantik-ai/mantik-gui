import {
    Autocomplete,
    FormControl,
    FormControlLabel,
    FormGroup,
    FormLabel,
    Radio,
    RadioGroup,
    TextField,
    useTheme,
} from '@mui/material'
import { useRouter } from 'next/router'
import React, { useEffect, useMemo, useState } from 'react'
import { FormDialogLayout } from '../../components/FormDialogLayout'
import { Spacing } from '../../components/Spacing'
import { renderApiErrorMessage } from '../../errors'
import {
    AddInvitation,
    MantikApiModelsProjectRoleProjectRole,
    Organization,
    useInvitationsPostInvitationsPost,
    useProjectsProjectIdGroupsGroupIdPutProjectsProjectIdGroupsGroupIdPut,
    useProjectsProjectIdMembersUserIdPutProjectsProjectIdMembersUserIdPut,
    useProjectsProjectIdOrganizationOrganizationIdPutProjectsProjectIdOrganizationsOrganizationIdPut,
    User,
    UserGroup,
} from '../../queries'
import { UserRole } from '../../types/UserRole'
import { TableEntry } from '../../utils/generateCollaborationData'
import { SemanticUserRole, semanticUserRoles } from '../../utils/userRole'
import { RadioOptionInviteType } from '../project_details/settings/components/GroupsAndOrganizationsSettings'

type RefetchFunctions = {
    refetchGroupCollaborations?: () => void
    refetchOrganizationCollaborations?: () => void
}
interface AddCollaboratorDialogProps {
    id: string
    users: readonly (User | UserGroup | Organization)[] | undefined
    open: boolean
    invitedType: InvitedType
    setOpen: React.Dispatch<React.SetStateAction<boolean>>
    role?: UserRole
    invitedToType: InvitedToType
    editItem?: TableEntry | null
    setEditItem?: Function
    refetch?: Function | RefetchFunctions
    radioOptionInviteType?: RadioOptionInviteType
    setRadioOptionInviteType?: Function
}
export enum InvitedToType {
    PROJECT = 'PROJECT',
    GROUP = 'GROUP',
    ORGANIZATION = 'ORGANIZATION',
}
export enum InvitedType {
    USER = 'USER',
    GROUP = 'GROUP',
    ORGANIZATION = 'ORGANIZATION',
}
export const AddCollaboratorDialog = (props: AddCollaboratorDialogProps) => {
    const {
        editItem,
        setEditItem,
        setOpen,
        users,
        invitedType,
        invitedToType,
        radioOptionInviteType,
        setRadioOptionInviteType,
        refetch,
    } = props

    const theme = useTheme()
    const [member, setMember] = useState<
        User | UserGroup | Organization | null
    >(null)
    const [role, setRole] = useState<SemanticUserRole | '' | null>(null)
    const [status, setStatus] = useState({ status: false, type: '', text: '' })
    const [loadingUpdate, setLoadingUpdate] = useState(false)
    const router = useRouter()
    const { id } = router.query

    const projectId: string | null =
        typeof id === 'string' ? id : id?.length ? id[0] : null
    const reset = () => {
        setMember(null)
        setRole(null)
        if (setEditItem) {
            setEditItem(null)
        }
    }
    const handleClose = () => {
        setOpen(false)
        setStatus({ status: false, type: '', text: '' })
        reset()
    }
    const { mutate, isPending: isLoading } = useInvitationsPostInvitationsPost({
        mutation: {
            onSuccess: () => {
                setStatus({
                    status: true,
                    type: 'success',
                    text: `Invitation has been sent successfully!`,
                })
                reset()
            },
            onError: (err) => {
                console.log('Error: ', err)
                setStatus({
                    status: true,
                    type: 'error',
                    text: renderApiErrorMessage(err),
                })
                reset()
            },
        },
    })
    const updateMemberProjectRole =
        useProjectsProjectIdMembersUserIdPutProjectsProjectIdMembersUserIdPut({
            mutation: {
                onSuccess: () => {
                    setStatus({
                        status: true,
                        type: 'success',
                        text: 'Project Role was updated successfully!',
                    })

                    if (refetch && typeof refetch === 'function') {
                        refetch()
                    }
                },
                onError: (error) => {
                    setStatus({
                        status: true,
                        type: 'error',
                        text: renderApiErrorMessage(error),
                    })
                },
            },
        })
    const updateOrganizationProjectRole =
        useProjectsProjectIdOrganizationOrganizationIdPutProjectsProjectIdOrganizationsOrganizationIdPut(
            {
                mutation: {
                    onSuccess: () => {
                        setStatus({
                            status: true,
                            type: 'success',
                            text: 'Project Role was updated successfully!',
                        })
                        if (
                            typeof refetch === 'object' &&
                            refetch.refetchOrganizationCollaborations
                        ) {
                            refetch.refetchOrganizationCollaborations()
                        }
                    },
                    onError: (error) => {
                        setStatus({
                            status: true,
                            type: 'error',
                            text: renderApiErrorMessage(error),
                        })
                    },
                },
            }
        )
    const updateGroupProjectRole =
        useProjectsProjectIdGroupsGroupIdPutProjectsProjectIdGroupsGroupIdPut({
            mutation: {
                onSuccess: () => {
                    setStatus({
                        status: true,
                        type: 'success',
                        text: 'Project Role was updated successfully!',
                    })

                    if (
                        typeof refetch === 'object' &&
                        refetch.refetchGroupCollaborations
                    ) {
                        refetch.refetchGroupCollaborations()
                    }
                },
                onError: (error) => {
                    setStatus({
                        status: true,
                        type: 'error',
                        text: renderApiErrorMessage(error),
                    })
                },
            },
        })

    const onSubmit = () => {
        if (editItem && role && projectId) {
            const updatedRole: MantikApiModelsProjectRoleProjectRole = {
                role,
            }
            const ID = editItem?.ID?.data
            const setLoadingUpdateAndMutate = (
                mutateFn: any,
                idKey: string
            ) => {
                setLoadingUpdate(mutateFn.isLoading)
                mutateFn.mutate({
                    projectId,
                    [idKey]: ID,
                    data: updatedRole,
                })
            }
            if (invitedType === InvitedType.USER && ID) {
                setLoadingUpdateAndMutate(updateMemberProjectRole, 'userId')
            } else if (editItem?.Type?.data === 'Organization' && ID) {
                setLoadingUpdateAndMutate(
                    updateOrganizationProjectRole,
                    'organizationId'
                )
            } else if (editItem?.Type?.data === 'Group' && ID) {
                setLoadingUpdateAndMutate(updateGroupProjectRole, 'groupId')
            }
        }
        if (!editItem && member) {
            let data: AddInvitation = {
                invitedId: 'userId' in member ? member.userId : '',
                invitedToId: props.id,
                invitedType,
                invitedToType,
            }

            if (invitedToType === InvitedToType.PROJECT && role) {
                data = {
                    ...data,
                    role,
                    invitedType:
                        invitedType !== InvitedType.USER &&
                        radioOptionInviteType
                            ? radioOptionInviteType
                            : data.invitedType,
                    invitedId:
                        'userGroupId' in member
                            ? member.userGroupId
                            : 'organizationId' in member
                              ? member.organizationId
                              : member.userId,
                }
            }

            mutate({ data })
        }
    }
    const requiredFields = !props?.editItem
        ? !member || (invitedToType === InvitedToType.PROJECT && !role)
        : !role

    function handleOptionChange(event: React.ChangeEvent<HTMLInputElement>) {
        const selectedValue = event.target.value
        setRadioOptionInviteType &&
            setRadioOptionInviteType(selectedValue as InvitedType)
        setRole(null)
        setMember(null)
    }
    const memoizedEditItem = useMemo(() => editItem, [editItem])
    useEffect(() => {
        if (memoizedEditItem) {
            setRole(
                (memoizedEditItem?.Role?.data ?? '') as SemanticUserRole | ''
            )
        }
    }, [memoizedEditItem])
    return (
        <FormDialogLayout
            open={props.open}
            handleClose={handleClose}
            handleSubmit={onSubmit}
            title={
                !editItem
                    ? 'Invite a collaborator'
                    : `Update your collaborator's role`
            }
            content={
                !editItem
                    ? ' Please Note: Only an Admin/Owner can invite/update a collaborator.'
                    : ' Please Note: A member is eligible for the highest role either directly assigned or through group/organization affiliation.'
            }
            isEdit={!!editItem}
            formDataState={{
                member,
                role,
                error: requiredFields,
            }}
            status={status}
            handleSubmitText={'Invite'}
            loading={!!editItem ? loadingUpdate : isLoading}
        >
            <FormGroup>
                {invitedType !== InvitedType.USER &&
                    invitedToType === InvitedToType.PROJECT &&
                    !editItem && (
                        <FormControl component="fieldset">
                            <FormLabel component="legend">
                                Select an option:
                            </FormLabel>
                            <RadioGroup
                                aria-label="inviteType"
                                name="inviteType"
                                value={radioOptionInviteType}
                                onChange={handleOptionChange}
                            >
                                <div
                                    style={{
                                        display: 'flex',
                                        flexDirection: 'row',
                                    }}
                                >
                                    <FormControlLabel
                                        value={'GROUP'}
                                        control={<Radio />}
                                        label="GROUP"
                                    />
                                    <FormControlLabel
                                        value={'ORGANIZATION'}
                                        control={<Radio />}
                                        label="ORGANIZATION"
                                    />
                                </div>
                            </RadioGroup>
                            <Spacing value={theme.spacing(2)} />
                        </FormControl>
                    )}
                {!editItem && (
                    <FormControl>
                        <Autocomplete
                            id="tags-standard"
                            options={users || []}
                            getOptionLabel={(option) => option.name}
                            defaultValue={member}
                            value={member}
                            onChange={(event, newValue) => {
                                setMember(newValue)
                                setStatus({
                                    status: false,
                                    type: '',
                                    text: '',
                                })
                            }}
                            renderInput={(params) => (
                                <TextField
                                    {...params}
                                    label="Select a Collaborator"
                                    size="small"
                                />
                            )}
                        />
                    </FormControl>
                )}
                <Spacing value={theme.spacing(2)}></Spacing>
                {props.role !== undefined ? (
                    <FormControl>
                        <Autocomplete
                            id="tags-standard"
                            options={semanticUserRoles.filter(
                                (role) => role !== 'VISITOR'
                            )}
                            defaultValue={role}
                            value={role}
                            onChange={(event, newValue) => {
                                setRole(newValue)
                            }}
                            renderInput={(params) => (
                                <TextField
                                    {...params}
                                    label="Role"
                                    size="small"
                                    required
                                />
                            )}
                        />
                    </FormControl>
                ) : null}
            </FormGroup>
        </FormDialogLayout>
    )
}
