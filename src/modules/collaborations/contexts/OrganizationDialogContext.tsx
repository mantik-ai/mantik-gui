import {
    createContext,
    Dispatch,
    SetStateAction,
    useEffect,
    useState,
} from 'react'
import {
    Organization,
    OrganizationsGet200Response,
    UsersGet200Response,
    useUsersGetUsersGet,
    useUsersUserIdOrganizationsGetUsersUserIdOrganizationsGet,
} from '../../../queries'
import { useSession } from 'next-auth/react'
import { QueryStatus } from '@tanstack/react-query'

export interface OrganizationDialogParameters {
    myOrganizations?: Organization[] | null
    setMyOrganizations: Dispatch<SetStateAction<any | undefined>>
    myOrganizationQueryStatus: QueryStatus
    users: UsersGet200Response | undefined
    myOrganizationsData: OrganizationsGet200Response | undefined
}
const OrganizationDialogContext = createContext<
    Partial<OrganizationDialogParameters>
>({})

interface OrganizationDialogProviderProps {
    children: React.ReactNode
}

export const OrganizationDialogProvider: React.FC<
    OrganizationDialogProviderProps
> = (props) => {
    const session = useSession()
    const [myOrganizations, setMyOrganizations] = useState<
        Organization[] | null
    >()
    const [myOrganizationQueryStatus, setOrganizationQueryStatus] =
        useState<QueryStatus>('pending')
    let userId: string | null = null
    if (typeof window !== 'undefined') {
        userId = sessionStorage.getItem('userId')
    }
    const { data: myOrganizationsData, status: myOrganizationsStatus } =
        useUsersUserIdOrganizationsGetUsersUserIdOrganizationsGet(userId || '')

    const { data: users } = useUsersGetUsersGet()
    useEffect(() => {
        if (session.status === 'authenticated') {
            setOrganizationQueryStatus('pending')
        }
    }, [session.status])
    useEffect(() => {
        if (myOrganizationsStatus === 'success') {
            setMyOrganizations(myOrganizationsData.organizations?.reverse())
            setOrganizationQueryStatus('success')
        }
    }, [myOrganizationsStatus])

    return (
        <OrganizationDialogContext.Provider
            value={{
                myOrganizations,
                setMyOrganizations,
                myOrganizationQueryStatus,
                myOrganizationsData,
                users,
            }}
        >
            {props.children}
        </OrganizationDialogContext.Provider>
    )
}

export default OrganizationDialogContext
