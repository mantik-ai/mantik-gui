import {
    createContext,
    Dispatch,
    SetStateAction,
    useEffect,
    useState,
} from 'react'
import { useSession } from 'next-auth/react'
import {
    GroupsGet200Response,
    UserGroup,
    UsersGet200Response,
    useUsersGetUsersGet,
    useUsersUserIdGroupsGetUsersUserIdGroupsGet,
} from '../../../queries'
import { QueryStatus } from '@tanstack/react-query'
import { AXIOS_INSTANCE } from '../../auth/mantikApi'

export interface GroupDialogParameters {
    myGroups?: UserGroup[] | null
    setMyGroups: Dispatch<SetStateAction<any | undefined>>
    groupsQueryStatus: QueryStatus
    users: UsersGet200Response | undefined
    myGroupsData: GroupsGet200Response | undefined
}
const GroupDialogContext = createContext<Partial<GroupDialogParameters>>({})
interface GroupDialogProviderProps {
    children: React.ReactNode
}
export const GroupDialogProvider: React.FC<GroupDialogProviderProps> = (
    props
) => {
    const session = useSession()
    const [myGroups, setMyGroups] = useState<UserGroup[] | null>()
    const [groupsQueryStatus, setGroupsQueryStatus] =
        useState<QueryStatus>('pending')
    let userId: string | null = null
    if (typeof window !== 'undefined') {
        userId = sessionStorage.getItem('userId')
    }
    const { data: myGroupsData, status: myGroupsStatus } =
        useUsersUserIdGroupsGetUsersUserIdGroupsGet(userId || '')
    const { data: users } = useUsersGetUsersGet()
    useEffect(() => {
        if (session.status === 'authenticated') {
            setGroupsQueryStatus('pending')
        }
    }, [session.status])
    useEffect(() => {
        if (myGroupsStatus === 'success') {
            setMyGroups(myGroupsData.userGroups?.reverse())
            setGroupsQueryStatus('success')
        }
    }, [myGroupsStatus])

    useEffect(() => {
        if (session?.status === 'authenticated') {
            AXIOS_INSTANCE.interceptors.request.use((config) => {
                if (config.headers) {
                    config.headers['Authorization'] =
                        `Bearer ${session?.data?.user.accessToken}`
                    config.headers['Content-Type'] = 'application/json'
                }
                return config
            })
        }
    }, [session])
    return (
        <GroupDialogContext.Provider
            value={{
                myGroups,
                setMyGroups,
                groupsQueryStatus,
                users,
                myGroupsData,
            }}
        >
            {props.children}
        </GroupDialogContext.Provider>
    )
}
export default GroupDialogContext
