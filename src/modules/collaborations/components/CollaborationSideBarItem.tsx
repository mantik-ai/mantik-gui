import { ListItemButton, ListItemIcon, ListItemText } from '@mui/material'
import Link from 'next/link'
import React, { ReactNode } from 'react'

export default function CollaborationSideBarItem({
    name,
    path,
    icon,
}: {
    name: string
    path: string
    icon: ReactNode
}) {
    return (
        <Link key={name} href={path} passHref legacyBehavior>
            <ListItemButton key={name}>
                <ListItemIcon>{icon}</ListItemIcon>
                <ListItemText>{name}</ListItemText>
            </ListItemButton>
        </Link>
    )
}
