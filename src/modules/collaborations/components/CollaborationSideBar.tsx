import { OnlinePredictionOutlined } from '@mui/icons-material'
import PeopleIcon from '@mui/icons-material/People'
import OrganizationIcon from '@mui/icons-material/CorporateFare'

import { Box, List, Paper, useTheme } from '@mui/material'
import React from 'react'
import SideBarItem from './CollaborationSideBarItem'
import { Route } from '../../../types/route'

export const CollaborationSideBar = () => {
    const theme = useTheme()
    const routes: Route[] = [
        {
            name: 'My Groups',
            path: `/collaborations`,
            icon: <PeopleIcon fontSize="small" />,
        },
        {
            name: 'My Organizations',
            path: `/collaborations/Organizations`,
            icon: <OrganizationIcon />,
        },
        {
            name: 'My Projects',
            path: `/collaborations/Projects`,
            icon: <OnlinePredictionOutlined />,
        },
    ]

    return (
        <Box
            sx={{
                flex: 1,
                p: theme.spacing(2),
                minWidth: 300,
            }}
        >
            <Paper sx={{ height: '100%' }}>
                <List sx={{ height: '100%' }}>
                    {routes.map((route) => (
                        <SideBarItem
                            key={route.path}
                            name={route.name}
                            path={route.path}
                            icon={route.icon}
                        />
                    ))}
                </List>
            </Paper>
        </Box>
    )
}
