import React from 'react'
import { Box, useTheme } from '@mui/material'
import { CollaborationSideBar } from './CollaborationSideBar'
import { OrganizationDialogProvider } from '../contexts/OrganizationDialogContext'
import { GroupDialogProvider } from '../contexts/GroupDialogContext'

interface CollaborationLayoutProps {
    children: React.ReactNode
}
export const CollaborationsLayout = (props: CollaborationLayoutProps) => {
    const theme = useTheme()
    return (
        <GroupDialogProvider>
            <OrganizationDialogProvider>
                <Box sx={{ display: 'flex', height: '100%' }}>
                    <CollaborationSideBar />
                    <Box
                        sx={{
                            flex: 4,
                            height: '100%',
                            p: theme.spacing(2),
                        }}
                    >
                        {props.children}
                    </Box>
                </Box>
            </OrganizationDialogProvider>
        </GroupDialogProvider>
    )
}
