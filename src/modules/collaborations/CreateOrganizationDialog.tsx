import {
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
    FormControl,
    FormGroup,
    TextField,
    useTheme,
    Alert,
} from '@mui/material'
import React, { useContext, useState } from 'react'
import { Spacing } from '../../components/Spacing'
import OrganisationDialogContext from './contexts/OrganizationDialogContext'
import { useSession } from 'next-auth/react'
import {
    AddUserGroup,
    useOrganizationsPostOrganizationsPost,
} from '../../queries'
import { renderApiErrorMessage } from '../../errors'

interface OrganisationDialogProps {
    open: boolean
    setOpen: React.Dispatch<React.SetStateAction<boolean>>
    refetchData?: () => void
}
export const OrganizationDialog = (props: OrganisationDialogProps) => {
    const organisationContext = useContext(OrganisationDialogContext)
    const theme = useTheme()
    const session = useSession()
    const [organizationName, setOrganizationName] = useState<AddUserGroup>({
        name: '',
    })
    const [createOrganizationStatus, setCreateOrganizationStatus] = useState({
        status: false,
        type: '',
        text: '',
    })
    const [organizationNameErrorText, setOrganizationNameErrorText] =
        React.useState('')

    const { mutate } = useOrganizationsPostOrganizationsPost({
        mutation: {
            onSuccess: () => {
                setCreateOrganizationStatus({
                    status: true,
                    type: 'success',
                    text: `${organizationName.name} Organization has been created successfully!`,
                })
                if (props.refetchData) {
                    props.refetchData() // Invoke the callback function if it's defined
                }
            },
            onError: (error) => {
                setCreateOrganizationStatus({
                    status: true,
                    type: 'error',
                    text: renderApiErrorMessage(error),
                })
            },
        },
    })
    const handleClose = () => {
        props.setOpen(false)
        setCreateOrganizationStatus({ status: false, type: '', text: '' })
        setOrganizationName({ name: '' })
        setOrganizationNameErrorText('')
    }
    const onSubmit = () => {
        if (!organizationName.name) {
            setOrganizationNameErrorText('Please enter name')
            return
        }
        const filteredOrganizationByName =
            organisationContext.myOrganizations?.filter(
                (organization) => organization.name === organizationName.name
            )
        if (filteredOrganizationByName?.length) {
            setOrganizationNameErrorText(
                `Organization name "${organizationName.name}" already exists!`
            )
            return
        }
        setOrganizationNameErrorText('')
        if (session.status === 'authenticated') {
            mutate({
                data: organizationName,
            })
        } else {
            setCreateOrganizationStatus({
                status: true,
                type: 'error',
                text: 'Authentication error. Please check your inputs and try again.',
            })
        }
    }
    return (
        <Dialog open={props.open} onClose={handleClose} fullWidth={true}>
            {!createOrganizationStatus.status && (
                <>
                    <DialogTitle>Create an Organization</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            Please note that when you create an organization you
                            will be the organization contact which could be
                            edited in the settings.
                        </DialogContentText>
                        <Spacing value={theme.spacing(2)} />
                        <FormGroup>
                            <FormControl>
                                <TextField
                                    key="name"
                                    name="name"
                                    label="Organization Name"
                                    size="small"
                                    value={organizationName.name}
                                    error={!!organizationNameErrorText}
                                    helperText={organizationNameErrorText}
                                    onChange={(e) => {
                                        setOrganizationName({
                                            name: e.target.value,
                                        })
                                        setOrganizationNameErrorText('')
                                    }}
                                    required
                                />
                            </FormControl>
                            <Spacing value={theme.spacing(2)} />
                        </FormGroup>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={handleClose}>Cancel</Button>
                        <Button onClick={onSubmit}>Create</Button>
                    </DialogActions>
                </>
            )}
            {createOrganizationStatus.status && (
                <DialogContent>
                    <Alert
                        variant="outlined"
                        severity={
                            createOrganizationStatus.type === 'error'
                                ? 'error'
                                : 'success'
                        }
                    >
                        {createOrganizationStatus.text}
                    </Alert>
                    {createOrganizationStatus.type === 'success' && (
                        <Button onClick={() => handleClose()}>Close</Button>
                    )}
                </DialogContent>
            )}
        </Dialog>
    )
}
