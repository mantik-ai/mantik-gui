import {
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
    FormControl,
    FormGroup,
    TextField,
    useTheme,
    Alert,
} from '@mui/material'
import React, { useContext, useState } from 'react'
import { Spacing } from '../../components/Spacing'
import GroupDialogContext from './contexts/GroupDialogContext'
import { useSession } from 'next-auth/react'
import { AddUserGroup, useGroupsPostGroupsPost } from '../../queries'
import { renderApiErrorMessage } from '../../errors'

interface GroupDialogProps {
    open: boolean
    setOpen: React.Dispatch<React.SetStateAction<boolean>>
    refetchData?: () => void
}

export const CreateGroupDialog = (props: GroupDialogProps) => {
    const groupContext = useContext(GroupDialogContext)
    const theme = useTheme()
    const session = useSession()
    const [groupNameErrorText, setGroupNameErrorText] = useState('')
    const [groupName, setGroupName] = useState<AddUserGroup>({ name: '' })
    const [createGroupStatus, setCreateGroupStatus] = useState({
        status: false,
        type: '',
        text: '',
    })

    const handleClose = () => {
        props.setOpen(false)
        setCreateGroupStatus({ status: false, type: '', text: '' })
        setGroupName({ name: '' })
        setGroupNameErrorText('')
    }

    const { mutate } = useGroupsPostGroupsPost({
        mutation: {
            onSuccess: () => {
                setCreateGroupStatus({
                    status: true,
                    type: 'success',
                    text: `${groupName.name} group has been created successfully!`,
                })
                if (props.refetchData) {
                    props.refetchData() // Invoke the callback function if it's defined
                }
            },
            onError: (error) => {
                setCreateGroupStatus({
                    status: true,
                    type: 'error',
                    text: renderApiErrorMessage(error),
                })
            },
        },
    })
    const onSubmit = () => {
        if (!groupName.name) {
            setGroupNameErrorText('Please enter name')
            return
        }
        const filteredArrayByGroupName = groupContext.myGroups?.filter(
            (group) => group.name === groupName.name
        )
        if (filteredArrayByGroupName?.length) {
            setGroupNameErrorText(
                `Group name "${groupName.name}" already exists!`
            )
            return
        }
        setGroupNameErrorText('')
        if (session.status === 'authenticated') {
            mutate({
                data: groupName,
            })
        } else {
            setCreateGroupStatus({
                status: true,
                type: 'error',
                text: 'Authentication error. Please check your inputs and try again.',
            })
        }
    }
    return (
        <Dialog open={props.open} onClose={handleClose}>
            {!createGroupStatus.status && (
                <>
                    <DialogTitle>Create a group</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            Please note that when you create the group you will
                            be the group admin which could be edited in the
                            settings.
                        </DialogContentText>
                        <Spacing value={theme.spacing(2)} />
                        <FormGroup>
                            <FormControl>
                                <TextField
                                    key="name"
                                    name="name"
                                    label="Group Name"
                                    size="small"
                                    value={groupName.name}
                                    error={!!groupNameErrorText}
                                    helperText={groupNameErrorText}
                                    onChange={(e) => {
                                        setGroupName({ name: e.target.value })
                                        setGroupNameErrorText('')
                                    }}
                                    required
                                />
                            </FormControl>
                            <Spacing value={theme.spacing(2)} />
                        </FormGroup>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={handleClose}>Cancel</Button>
                        <Button onClick={onSubmit}>Create</Button>
                    </DialogActions>
                </>
            )}
            {createGroupStatus.status && (
                <DialogContent>
                    <Alert
                        variant="outlined"
                        severity={
                            createGroupStatus.type === 'error'
                                ? 'error'
                                : 'success'
                        }
                    >
                        {createGroupStatus.text}
                    </Alert>
                    {createGroupStatus.type === 'success' && (
                        <Button onClick={() => handleClose()}>Close</Button>
                    )}
                </DialogContent>
            )}
        </Dialog>
    )
}
