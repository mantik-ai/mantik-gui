import {
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
    FormControl,
    FormGroup,
    TextField,
    useTheme,
    Alert,
    Autocomplete,
} from '@mui/material'
import React, { useContext, useState } from 'react'
import { Spacing } from '../../components/Spacing'
import GroupDialogContext from './contexts/GroupDialogContext'
import { useSession } from 'next-auth/react'
import {
    UpdateUserGroup,
    useGroupsGroupIdPutGroupsGroupIdPut,
    User,
} from '../../queries'
import { renderApiErrorMessage } from '../../errors'

interface EditGroupDialogProps {
    groupId: string
    groupName: string
    groupAdmin: User | null
    groupMembers: User[] | []
    open: boolean
    setOpen: React.Dispatch<React.SetStateAction<boolean>>
}

export const EditGroupDialog = (props: EditGroupDialogProps) => {
    const groupContext = useContext(GroupDialogContext)
    const theme = useTheme()
    const session = useSession()
    const [groupNameErrorText, setGroupNameErrorText] = useState('')
    const [groupAdminErrorText, setGroupAdminErrorText] = useState('')

    const [name, setName] = useState(props.groupName)
    const [admin, setAdmin] = useState<User | null>(props.groupAdmin)
    const [editGroupStatus, setEditGroupStatus] = useState({
        status: false,
        type: '',
        text: '',
    })

    const handleClose = () => {
        props.setOpen(false)
        setEditGroupStatus({ status: false, type: '', text: '' })
        setGroupNameErrorText('')
    }

    const { mutate } = useGroupsGroupIdPutGroupsGroupIdPut({
        mutation: {
            onSuccess: () => {
                setEditGroupStatus({
                    status: true,
                    type: 'success',
                    text: 'Group has been edited successfully!!',
                })
            },
            onError: (error) => {
                setEditGroupStatus({
                    status: true,
                    type: 'error',
                    text: renderApiErrorMessage(error),
                })
            },
        },
    })

    const onSubmit = () => {
        if (!name) {
            setGroupNameErrorText('Please enter name')
            return
        }
        if (!admin) {
            setGroupAdminErrorText('Please assign an Admin')
            return
        }
        const filteredArrayByGroupName = groupContext.myGroups?.filter(
            (group) =>
                group.name === name && group.userGroupId !== props.groupId
        )
        if (filteredArrayByGroupName?.length) {
            setGroupNameErrorText(`Group name "${name}" already exists!`)
            return
        }
        setGroupNameErrorText('')
        setGroupAdminErrorText('')
        if (session.status === 'authenticated') {
            const memberIds = props.groupMembers.map((member) => member.userId)
            const data: UpdateUserGroup = {
                name: name,
                adminId: admin?.userId,
                memberIds: memberIds,
            }
            mutate({
                groupId: props.groupId,
                data: data,
            })
        } else {
            setEditGroupStatus({
                status: true,
                type: 'error',
                text: 'Authentication error. Please check your inputs and try again.',
            })
        }
    }
    return (
        <Dialog
            open={props.open}
            onClose={handleClose}
            sx={{ backgroundColor: 'transparent' }}
        >
            {!editGroupStatus.status && (
                <>
                    <DialogTitle>Edit group</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            Please Note: Only group Admin can edit a group.
                        </DialogContentText>
                        <Spacing value={theme.spacing(2)} />
                        <FormGroup>
                            <FormControl>
                                <TextField
                                    key="name"
                                    name="name"
                                    label="Group Name"
                                    size="small"
                                    value={name}
                                    error={!!groupNameErrorText}
                                    helperText={groupNameErrorText}
                                    onChange={(e) => {
                                        setName(e.target.value)
                                        setGroupNameErrorText('')
                                    }}
                                    required
                                />
                            </FormControl>
                            <Spacing value={theme.spacing(2)} />
                            <FormControl>
                                <Autocomplete
                                    id="tags-standard"
                                    options={groupContext.users?.users ?? []}
                                    getOptionLabel={(option: User) =>
                                        option.name
                                    }
                                    defaultValue={admin}
                                    value={admin}
                                    onChange={(event, newValue) =>
                                        setAdmin(newValue)
                                    }
                                    renderInput={(params) => (
                                        <TextField
                                            {...params}
                                            label="Admin"
                                            size="small"
                                            required
                                            error={!!groupAdminErrorText}
                                            helperText={groupAdminErrorText}
                                        />
                                    )}
                                />
                            </FormControl>
                        </FormGroup>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={handleClose}>Cancel</Button>
                        <Button onClick={onSubmit}>Update</Button>
                    </DialogActions>
                </>
            )}
            {editGroupStatus.status && (
                <DialogContent>
                    <Alert
                        variant="outlined"
                        severity={
                            editGroupStatus.type === 'error'
                                ? 'error'
                                : 'success'
                        }
                    >
                        {editGroupStatus.text}
                    </Alert>
                    {editGroupStatus.type === 'success' && (
                        <Button onClick={() => handleClose()}>Close</Button>
                    )}
                </DialogContent>
            )}
        </Dialog>
    )
}
