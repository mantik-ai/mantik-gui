import {
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
    FormControl,
    FormGroup,
    TextField,
    useTheme,
    Alert,
    Autocomplete,
} from '@mui/material'
import React, { useContext, useState } from 'react'
import { Spacing } from '../../components/Spacing'
import { useSession } from 'next-auth/react'
import {
    UpdateOrganization,
    useOrganizationsOrganizationIdPutOrganizationsOrganizationIdPut,
    User,
    UserGroup,
} from '../../queries'
import OrganizationDialogContext from './contexts/OrganizationDialogContext'
import { renderApiErrorMessage } from '../../errors'

interface EditOrganizationDialogProps {
    id: string
    name: string
    contact: User | null
    members: User[] | undefined
    groups: UserGroup[] | undefined
    open: boolean
    setOpen: React.Dispatch<React.SetStateAction<boolean>>
}

export const EditOrganizationDialog = (props: EditOrganizationDialogProps) => {
    const organizationContext = useContext(OrganizationDialogContext)
    const theme = useTheme()
    const session = useSession()
    const [organizationNameErrorText, setOrganizationNameErrorText] =
        React.useState('')
    const [organizationAdminErrorText, setOrganizationAdminErrorText] =
        useState('')
    const [name, setName] = useState(props.name)
    const [admin, setAdmin] = useState<User | null>(props.contact)
    const [editOrganizationStatus, setEditOrganizationStatus] = useState({
        status: false,
        type: '',
        text: '',
    })
    const { mutate } =
        useOrganizationsOrganizationIdPutOrganizationsOrganizationIdPut({
            mutation: {
                onSuccess: () => {
                    setEditOrganizationStatus({
                        status: true,
                        type: 'success',
                        text: 'Group has been edited successfully!!',
                    })
                },
                onError: (error) => {
                    setEditOrganizationStatus({
                        status: true,
                        type: 'error',
                        text: renderApiErrorMessage(error),
                    })
                },
            },
        })
    const handleClose = () => {
        props.setOpen(false)
        setEditOrganizationStatus({ status: false, type: '', text: '' })
        setOrganizationAdminErrorText('')
    }

    const onSubmit = () => {
        if (!name) {
            setOrganizationNameErrorText('Please enter name')
            return
        }
        if (!admin) {
            setOrganizationAdminErrorText('Please assign an Admin')
            return
        }
        const filteredOrganizationByName =
            organizationContext.myOrganizations?.filter(
                (organization) => organization.name === name
            )
        if (filteredOrganizationByName?.length) {
            setOrganizationNameErrorText(
                `Organization name "${name}" already exists!`
            )
            return
        }
        setOrganizationNameErrorText('')
        setOrganizationAdminErrorText('')
        if (session.status === 'authenticated') {
            const memberIds = props.members!.map((member) => member.userId)
            const groupIds = props.groups!.map((group) => group.userGroupId)
            const data: UpdateOrganization = {
                name: name,
                contactId: admin?.userId,
                memberIds,
                groupIds,
            }
            mutate({ organizationId: props.id, data })
        } else {
            setEditOrganizationStatus({
                status: true,
                type: 'error',
                text: 'Authentication error. Please check your inputs and try again.',
            })
        }
    }
    return (
        <Dialog
            open={props.open}
            onClose={handleClose}
            sx={{ backgroundColor: 'transparent' }}
        >
            {!editOrganizationStatus.status && (
                <>
                    <DialogTitle>Edit group</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            Please Note: Only group Admin can edit a group.
                        </DialogContentText>
                        <Spacing value={theme.spacing(2)} />
                        <FormGroup>
                            <FormControl>
                                <TextField
                                    key="name"
                                    name="name"
                                    label="Group Name"
                                    size="small"
                                    value={name}
                                    error={!!organizationNameErrorText}
                                    helperText={organizationNameErrorText}
                                    onChange={(e) => {
                                        setName(e.target.value)
                                        setOrganizationNameErrorText('')
                                    }}
                                    required
                                />
                            </FormControl>
                            <Spacing value={theme.spacing(2)} />
                            <FormControl>
                                <Autocomplete
                                    id="tags-standard"
                                    options={
                                        organizationContext.users?.users ?? []
                                    }
                                    getOptionLabel={(option: User) =>
                                        option.name
                                    }
                                    defaultValue={admin}
                                    value={admin}
                                    onChange={(event, newValue) =>
                                        setAdmin(newValue)
                                    }
                                    renderInput={(params) => (
                                        <TextField
                                            {...params}
                                            label="Admin"
                                            size="small"
                                            required
                                            error={!!organizationAdminErrorText}
                                            helperText={
                                                organizationAdminErrorText
                                            }
                                        />
                                    )}
                                />
                            </FormControl>
                        </FormGroup>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={handleClose}>Cancel</Button>
                        <Button onClick={onSubmit}>Update</Button>
                    </DialogActions>
                </>
            )}
            {editOrganizationStatus.status && (
                <DialogContent>
                    <Alert
                        variant="outlined"
                        severity={
                            editOrganizationStatus.type === 'error'
                                ? 'error'
                                : 'success'
                        }
                    >
                        {editOrganizationStatus.text}
                    </Alert>
                    {editOrganizationStatus.type === 'success' && (
                        <Button onClick={() => handleClose()}>Close</Button>
                    )}
                </DialogContent>
            )}
        </Dialog>
    )
}
