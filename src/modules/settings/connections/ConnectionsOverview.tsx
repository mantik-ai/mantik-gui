import { Button, Grid } from '@mui/material'
import {
    Add,
    DeleteOutline,
    EditOutlined,
    VisibilityOutlined,
} from '@mui/icons-material'
import React, { useContext, useEffect, useState } from 'react'
import AuthContext from '../../../context/AuthContext'
import { useAuthentication } from '../../../hooks/useAuthentication'
import {
    Connection,
    useUsersUserIdSettingsConnectionsConnectionIdDeleteUsersUserIdSettingsConnectionsConnectionIdDelete,
    useUsersUserIdSettingsConnectionsGetUsersUserIdSettingsConnectionsGet,
} from '../../../queries'
import { StatusProps } from '../../../types/statusProps'
import { renderApiErrorMessage } from '../../../errors'
import { DataStateIndicator } from '../../../components/DataStateIndicator'
import { TileLayout } from '../../../layouts/TileLayout'
import { ConnectionDialog } from './ConnectionDialog'
import { DetailsDialogLayout } from '../../../components/DetailsDialogLayout'
import { DetailsDialogList } from '../../../components/DetailsDialogList'
import { DeleteDialogLayout } from '../../../components/DeleteDialogLayout'
import { DetailsToolbar } from '../../project_details/overview/ProjectDetailsToolbar'
import { formatTimestamp } from '../../../helpers'
import {
    generateCustomActionButtonsArray,
    ActionButton,
} from '../../../utils/generateCustomActionButtonsArray'

interface CustomCellProps {
    data: any
}

type TableProps = {
    [key: string]: CustomCellProps
}[]
export const ConnectionsOverview = () => {
    const userContext = useContext(AuthContext)
    const userId = String(userContext.usersData?.userId)
    const authentication = useAuthentication()
    const [openConnectionsDialog, setOpenConnectionsDialog] =
        useState<boolean>(false)
    const [editData, setEditData] = useState<Connection>()
    const [isEdit, setIsEdit] = useState<boolean>(false)
    const [detailsDialogOpen, setDetailsDialogOpen] = useState<boolean>(false)
    const [currentRow, setCurrentRow] = useState<number | null>(null)
    const [deleteDialogOpen, setDeleteDialogOpen] = useState<boolean>(false)
    const [deleteData, setDeleteData] = useState({
        name: '',
        connectionId: '',
    })

    const [deleteStatus, setDeleteStatus] = useState<StatusProps>({
        status: false,
        type: '',
        text: '',
    })
    const [tableData, setTableData] = useState<TableProps>([
        {
            ID: { data: '' },
            Name: { data: '' },
            Provider: { data: '' },
            'Authentication Method': { data: '' },
        },
    ])
    const handleDetailsDialog = (idx?: number) => {
        if (idx === undefined) {
            setDetailsDialogOpen(false)
            setCurrentRow(null)
        } else {
            setDetailsDialogOpen(true)
            setCurrentRow(idx)
        }
    }

    const { data, status, refetch } =
        useUsersUserIdSettingsConnectionsGetUsersUserIdSettingsConnectionsGet(
            userId as string,
            {
                // Optional: You can provide extra configuration here for pagination, currently not implemented in tiles layout
            }
        )
    const handleOpenConnectionsDialog = () => {
        setOpenConnectionsDialog((prev) => !prev)
    }

    const handleDataRefetch = () => {
        refetch() // Refresh the data
    }
    function handleIsEdit() {
        setIsEdit(false)
    }
    function handleEditData(idx = 0) {
        setOpenConnectionsDialog(true)
        const row = data && data.connections ? data.connections[idx] : null
        if (!!row) {
            setEditData(row)
        }
        setIsEdit(true)
    }
    function handleDeleteOpen() {
        setDeleteDialogOpen(true)
    }

    function handleDeleteClose() {
        setDeleteDialogOpen(false)
        setDeleteStatus({ status: false, type: '', text: '' })
    }

    function handleDelete(idx: number) {
        handleDeleteOpen()
        const row = data && data.connections ? data.connections[idx] : null
        if (!!row) {
            setDeleteData((prev) => ({
                ...prev,
                name: String(row.connectionName),
                connectionId: String(row.connectionId),
            }))
        }
    }
    function handleDeleteSubmit() {
        const connectionId = deleteData?.connectionId || ''
        if (authentication === 'authenticated') {
            deleteMutation.mutate(
                { userId, connectionId },
                {
                    // Optional: You can provide extra configuration here if needed
                    // e.g., onSuccess, onError, onSettled, etc.
                }
            )
        }
    }
    const deleteMutation =
        useUsersUserIdSettingsConnectionsConnectionIdDeleteUsersUserIdSettingsConnectionsConnectionIdDelete(
            {
                mutation: {
                    onSuccess: () => {
                        setDeleteStatus({
                            status: true,
                            type: 'success',
                            text: 'Connection was deleted successfully!',
                        })
                        handleDataRefetch()
                    },
                    onError: (error) => {
                        setDeleteStatus({
                            status: true,
                            type: 'error',
                            text: renderApiErrorMessage(error),
                        })
                    },
                },
            }
        )

    useEffect(() => {
        if (!!data && !!data?.connections?.length) {
            const tData = data?.connections?.map((data) => ({
                ID: { data: String(data.connectionId), type: 'id' },
                Name: { data: String(data.connectionName) },
                Provider: { data: String(data.connectionProvider) },
                'Authentication Method': { data: String(data.authMethod) },
            }))
            setTableData(tData)
        }
    }, [data, status])
    return (
        <>
            <DetailsToolbar
                title={'Connections'}
                tool={
                    <Button
                        variant="text"
                        onClick={handleOpenConnectionsDialog}
                    >
                        <Add></Add>Add
                    </Button>
                }
            ></DetailsToolbar>

            <DataStateIndicator status={status} text="Loading Connections...">
                <Grid container spacing={4} mt={0}>
                    {data?.connections?.map((value, idx) => {
                        const actionButtons: ActionButton[] = [
                            {
                                title: 'Details',
                                actionFunction: () => handleDetailsDialog(idx),
                                iconComponent: (
                                    <VisibilityOutlined fontSize="small" />
                                ),
                            },
                            {
                                title: 'Edit',
                                actionFunction: () => handleEditData(idx),
                                iconComponent: (
                                    <EditOutlined fontSize="small" />
                                ),
                            },
                            {
                                title: 'Delete',
                                actionFunction: () => handleDelete(idx),
                                iconComponent: (
                                    <DeleteOutline
                                        color="error"
                                        fontSize="small"
                                    />
                                ),
                            },
                        ]

                        return (
                            <Grid key={idx} item xs={12} md={6} lg={4} xl={3}>
                                <TileLayout
                                    key={value.connectionId}
                                    title={value.connectionName}
                                    bodyText={value.connectionProvider}
                                    footer={
                                        <div>
                                            Created At:{' '}
                                            {formatTimestamp(value.createdAt)}
                                        </div>
                                    }
                                    actions={generateCustomActionButtonsArray(
                                        actionButtons
                                    )}
                                />
                            </Grid>
                        )
                    })}
                </Grid>
            </DataStateIndicator>

            <ConnectionDialog
                open={openConnectionsDialog}
                handleCloseDialog={handleOpenConnectionsDialog}
                setIsEdit={handleIsEdit}
                isEdit={isEdit}
                editData={editData}
                refetchData={handleDataRefetch}
            />
            <DetailsDialogLayout
                open={detailsDialogOpen}
                setOpen={handleDetailsDialog}
                title={'Details'}
            >
                <DetailsDialogList
                    data={currentRow !== null ? tableData[currentRow] : {}}
                />
            </DetailsDialogLayout>
            <DeleteDialogLayout
                open={deleteDialogOpen}
                title="Delete Connection"
                text={
                    <>
                        Are you sure you want to remove Connection{' '}
                        <strong>{deleteData?.name}</strong>?
                    </>
                }
                handleSubmit={handleDeleteSubmit}
                handleClose={handleDeleteClose}
                isLoading={deleteMutation.isPending}
                status={deleteStatus}
            ></DeleteDialogLayout>
        </>
    )
}
