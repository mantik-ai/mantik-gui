import {
    FormControl,
    FormGroup,
    TextField,
    useTheme,
    Select,
    InputLabel,
    MenuItem,
    OutlinedInput,
    IconButton,
    InputAdornment,
    SelectChangeEvent,
} from '@mui/material'
import { Spacing } from '../../../components/Spacing'
import { useContext, useEffect, useMemo, useRef, useState } from 'react'
import { useSession } from 'next-auth/react'
import {
    AddConnection,
    AuthMethod,
    Connection,
    ConnectionProvider,
    useUsersUserIdSettingsConnectionsConnectionIdPutUsersUserIdSettingsConnectionsConnectionIdPut,
    useUsersUserIdSettingsConnectionsPostUsersUserIdSettingsConnectionsPost,
} from '../../../queries'
import { renderApiErrorMessage } from '../../../errors'
import { FormDialogLayout } from '../../../components/FormDialogLayout'
import { VisibilityOff, Visibility } from '@mui/icons-material'
import AuthContext from '../../../context/AuthContext'

interface ConnectionDialogProps {
    open: boolean
    handleCloseDialog: Function
    editData?: Connection
    isEdit?: boolean
    setIsEdit: Function
    refetchData?: () => void // Callback function to be invoked when code repository is added
}

type AuthenticationProps = {
    loginName: string
    password: string
    token: string
    privateKey: string
}

interface ConnectionProviderList {
    [key: string]: {
        readonly authMethods: readonly string[]
    }
}

const connectionProviderList: ConnectionProviderList = {
    GitHub: { authMethods: ['Token'] },
    GitLab: { authMethods: ['Token'] },
    'Jülich Supercomputing Centre (JSC)': {
        authMethods: ['Username-Password', 'Token'],
    },
    'E4 Computer Engineering (E4)': {
        authMethods: ['Username-Password', 'Token'],
    },
    'Swiss National Supercomputing Centre (CSCS)': {
        authMethods: ['Username-Password', 'Token'],
    },
    'Remote Compute System': {
        authMethods: ['SSH-Key', 'Username-Password'],
    },
    S3: { authMethods: ['Username-Password', 'Token'] },
} as const

export const ConnectionDialog = ({
    open,
    handleCloseDialog,
    editData,
    isEdit,
    setIsEdit,
    refetchData,
}: ConnectionDialogProps) => {
    const theme = useTheme()
    const session = useSession()
    const userContext = useContext(AuthContext)
    const userId = String(userContext.usersData?.userId)

    const form: React.RefObject<HTMLElement | null> = useRef(null)

    const [status, setStatus] = useState({ status: false, type: '', text: '' })

    const initialFormData: AddConnection = {
        connectionName: editData?.connectionName || '',
        connectionProvider: editData?.connectionProvider || 'GitHub',
        authMethod: editData?.authMethod || 'Token',
    }
    const [connectionDialogData, setConnectionDialogData] =
        useState<AddConnection>(initialFormData)
    const [authentication, setAuthentication] = useState<AuthenticationProps>({
        loginName: '',
        password: '',
        token: '',
        privateKey: '',
    })
    const [showPassword, setShowPassword] = useState(false)

    const handleClickShowPassword = () => setShowPassword((show) => !show)
    const handleMouseDownPassword = (
        event: React.MouseEvent<HTMLButtonElement>
    ) => {
        event.preventDefault()
    }
    const clearForm = () => {
        setConnectionDialogData({
            connectionName: '',
            connectionProvider: 'GitHub',
            authMethod: 'Token',
        })
        setAuthentication({
            loginName: '',
            password: '',
            token: '',
            privateKey: '',
        })
    }

    const handleClose = () => {
        setStatus({ status: false, type: '', text: '' })
        clearForm()
        handleCloseDialog()
        setIsEdit(false)
        setShowPassword(false)
    }

    const handleConnectionnameChange = (e: any) => {
        const { value } = e.target
        setConnectionDialogData((prev) => ({
            ...prev,
            connectionName: value,
        }))
    }

    const handleSelectProvider = (
        e: SelectChangeEvent<ConnectionProvider>
    ): void => {
        const { value } = e.target
        // Update which form inputs are shown as soon as the provider is selected by the user
        const authMethods = Array.from(
            connectionProviderList[value].authMethods
        )
        setConnectionDialogData((prev) => ({
            ...prev,
            connectionProvider: value as ConnectionProvider,
            authMethod: authMethods[0] as AuthMethod,
        }))
        setAuthentication({
            loginName: '',
            password: '',
            token: '',
            privateKey: '',
        })
    }

    const handleSelectAuthMethod = (e: any): void => {
        const { value } = e.target
        // Update which form inputs are shown
        setConnectionDialogData((prev) => ({
            ...prev,
            authMethod: value,
        }))
        setAuthentication({
            loginName: '',
            password: '',
            token: '',
            privateKey: '',
        })
    }

    const handleAuthenticationDataChange = (e: any) => {
        const { target } = e
        const { value } = target
        setAuthentication((prev) => ({
            ...prev,
            [target.name]: value,
        }))
    }

    // Update the dropdown list of available authMethods for that provider
    const renderAuthMethodList = () => {
        if (connectionDialogData.connectionProvider) {
            return connectionProviderList[
                connectionDialogData.connectionProvider
            ].authMethods.map((authMethod, index) => (
                <MenuItem key={index} value={authMethod}>
                    {authMethod}
                </MenuItem>
            ))
        }
    }

    function handleSubmit() {
        const data: AddConnection = {
            ...connectionDialogData,
            ...authentication,
        }
        if (session.status === 'authenticated' && !!userId) {
            if (isEdit) {
                updateMutation.mutate({
                    userId,
                    connectionId: editData?.connectionId || '',
                    data,
                })
            } else {
                mutate({
                    userId,
                    data,
                })
            }
        } else {
            setStatus({
                status: true,
                type: 'error',
                text: 'Authentication error. Please check your inputs and try again.',
            })
        }
    }
    const { mutate } =
        useUsersUserIdSettingsConnectionsPostUsersUserIdSettingsConnectionsPost(
            {
                mutation: {
                    onSuccess: () => {
                        setStatus({
                            status: true,
                            type: 'success',
                            text: 'Connection saved successfully!',
                        })
                        if (refetchData) {
                            refetchData() // Invoke the callback function if it's defined
                        }
                    },
                    onError: (error) => {
                        setStatus({
                            status: true,
                            type: 'error',
                            text: renderApiErrorMessage(error),
                        })
                    },
                },
            }
        )
    const updateMutation =
        useUsersUserIdSettingsConnectionsConnectionIdPutUsersUserIdSettingsConnectionsConnectionIdPut(
            {
                mutation: {
                    onSuccess: () => {
                        setStatus({
                            status: true,
                            type: 'success',
                            text: 'Connection updated successfully!',
                        })
                        if (refetchData) {
                            refetchData() // Invoke the callback function if it's defined
                        }
                    },
                    onError: (error) => {
                        setStatus({
                            status: true,
                            type: 'error',
                            text: renderApiErrorMessage(error),
                        })
                    },
                },
            }
        )

    const memoizedEditData = useMemo(() => editData, [editData])

    useEffect(() => {
        if (isEdit) {
            setConnectionDialogData({
                connectionName: editData?.connectionName || '',
                connectionProvider: editData?.connectionProvider || 'GitHub',
                authMethod: editData?.authMethod || 'Token',
            })
            setAuthentication({
                loginName: editData?.loginName || '',
                password: editData?.password || '',
                token: editData?.token || '',
                privateKey: editData?.privateKey || '',
            })
        }
    }, [isEdit, memoizedEditData])

    return (
        <FormDialogLayout
            formDataState={{ ...connectionDialogData, error: false }}
            status={status}
            open={open}
            title={!isEdit ? `Create Connection` : `Edit Connection`}
            content="Configure the Connection details"
            handleSubmit={handleSubmit}
            handleClose={handleClose}
            isEdit={isEdit}
        >
            <FormGroup ref={form}>
                <FormControl required>
                    <TextField
                        key="connectionName"
                        name="connectionName"
                        id="connectionName"
                        label="Name"
                        size="small"
                        type="text"
                        value={connectionDialogData.connectionName}
                        onChange={handleConnectionnameChange}
                        required
                    />
                </FormControl>
                <Spacing value={theme.spacing(2)} />
                <FormControl required>
                    <InputLabel id="select-connectionProvider">
                        Provider
                    </InputLabel>
                    <Select
                        labelId="select-connectionProvider"
                        id="connectionProvider"
                        label="Provider"
                        value={connectionDialogData.connectionProvider}
                        onChange={(e) => handleSelectProvider(e)}
                        required
                    >
                        {Object.keys(connectionProviderList).map(
                            (connectionProvider, index) => (
                                <MenuItem
                                    key={index}
                                    value={connectionProvider}
                                >
                                    {connectionProvider}
                                </MenuItem>
                            )
                        )}
                    </Select>
                </FormControl>
                <Spacing value={theme.spacing(2)} />
                <FormControl required>
                    <InputLabel id="select-authentication-method">
                        Authentication Method
                    </InputLabel>
                    <Select
                        labelId="select-authentication-method"
                        id="authentication-method"
                        label="Authentication Method"
                        value={connectionDialogData.authMethod}
                        onChange={(e) => handleSelectAuthMethod(e)}
                        required
                    >
                        {renderAuthMethodList()}
                    </Select>
                </FormControl>
                <Spacing value={theme.spacing(2)} />
                {connectionDialogData.authMethod === 'Username-Password' && (
                    <>
                        <FormControl required>
                            <TextField
                                key="loginName"
                                name="loginName"
                                id="loginName"
                                label="Username"
                                type="text"
                                value={authentication.loginName}
                                onChange={handleAuthenticationDataChange}
                                required
                            />
                        </FormControl>
                        <Spacing value={theme.spacing(2)} />
                        <FormControl
                            required={
                                connectionDialogData.connectionProvider !==
                                'Remote Compute System'
                            }
                            variant="outlined"
                        >
                            <InputLabel htmlFor="outlined-adornment-password">
                                Password
                            </InputLabel>
                            <OutlinedInput
                                id="outlined-adornment-password"
                                key="password"
                                name="password"
                                label="Password"
                                value={authentication.password}
                                onChange={handleAuthenticationDataChange}
                                required
                                type={showPassword ? 'text' : 'password'}
                                endAdornment={
                                    <InputAdornment position="end">
                                        <IconButton
                                            aria-label="toggle password visibility"
                                            onClick={handleClickShowPassword}
                                            onMouseDown={
                                                handleMouseDownPassword
                                            }
                                            edge="end"
                                        >
                                            {showPassword ? (
                                                <VisibilityOff />
                                            ) : (
                                                <Visibility />
                                            )}
                                        </IconButton>
                                    </InputAdornment>
                                }
                            />
                        </FormControl>
                        <Spacing value={theme.spacing(2)} />
                    </>
                )}
                {connectionDialogData.authMethod === 'Token' && (
                    <>
                        <FormControl required variant="outlined">
                            <InputLabel htmlFor="outlined-adornment-token">
                                Token
                            </InputLabel>
                            <OutlinedInput
                                id="outlined-adornment-token"
                                key="token"
                                name="token"
                                label="Token"
                                value={authentication.token}
                                onChange={handleAuthenticationDataChange}
                                required
                                type={showPassword ? 'text' : 'password'}
                                endAdornment={
                                    <InputAdornment position="end">
                                        <IconButton
                                            aria-label="toggle token visibility"
                                            onClick={handleClickShowPassword}
                                            onMouseDown={
                                                handleMouseDownPassword
                                            }
                                            edge="end"
                                        >
                                            {showPassword ? (
                                                <VisibilityOff />
                                            ) : (
                                                <Visibility />
                                            )}
                                        </IconButton>
                                    </InputAdornment>
                                }
                            />
                        </FormControl>
                        <Spacing value={theme.spacing(2)} />
                    </>
                )}
                {connectionDialogData.authMethod === 'SSH-Key' && (
                    <>
                        <FormControl required>
                            <TextField
                                key="loginName"
                                name="loginName"
                                id="loginName"
                                label="Username"
                                type="text"
                                value={authentication.loginName}
                                onChange={handleAuthenticationDataChange}
                                required
                            />
                        </FormControl>
                        <Spacing value={theme.spacing(2)} />
                        <FormControl variant="outlined">
                            <InputLabel htmlFor="outlined-adornment-ssh-key">
                                Private Key
                            </InputLabel>
                            <OutlinedInput
                                id="outlined-adornment-ssh-key"
                                key="privateKey"
                                name="privateKey"
                                label="Private Key"
                                value={authentication.privateKey}
                                onChange={handleAuthenticationDataChange}
                                required
                                type={showPassword ? 'text' : 'password'}
                                endAdornment={
                                    <InputAdornment position="end">
                                        <IconButton
                                            aria-label="toggle ssh key visibility"
                                            onClick={handleClickShowPassword}
                                            onMouseDown={
                                                handleMouseDownPassword
                                            }
                                            edge="end"
                                        >
                                            {showPassword ? (
                                                <VisibilityOff />
                                            ) : (
                                                <Visibility />
                                            )}
                                        </IconButton>
                                    </InputAdornment>
                                }
                            />
                        </FormControl>
                        <Spacing value={theme.spacing(2)} />
                    </>
                )}
            </FormGroup>
        </FormDialogLayout>
    )
}
