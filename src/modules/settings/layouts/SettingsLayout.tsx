import React from 'react'
import AccountCircleIcon from '@mui/icons-material/AccountCircle'
import HubIcon from '@mui/icons-material/Hub'
import { Route } from '../../../types/route'
import { LayoutWithSidebar } from '../../../layouts/LayoutWithSidebar'

const routes: Route[] = [
    {
        name: 'Account',
        path: `/settings/account`,
        icon: <AccountCircleIcon />,
    },
    {
        name: 'Connections',
        path: `/settings/connections`,
        icon: <HubIcon />,
    },
]

export const SettingsLayout = (props: { children: React.ReactNode }) => {
    return <LayoutWithSidebar routes={routes} {...props} />
}
