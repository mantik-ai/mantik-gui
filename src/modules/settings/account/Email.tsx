import { PageContent } from '../../../components/PageContent'
import { ButtonWithLoadingStatus } from '../../../components/ButtonWithLoadingStatus'
import {
    Button,
    FormControl,
    FormGroup,
    Paper,
    TextField,
    Tooltip,
    Typography,
    useTheme,
} from '@mui/material'
import { useContext, useEffect, useState } from 'react'
import { Spacing } from '../../../components/Spacing'
import AccountContext from '../../../context/AccountContext'
import {
    useUsersUserIdEmailPutUsersUserIdEmailPut,
    useUsersUserIdEmailVerifyUpdatePostUsersUserIdEmailVerifyUpdatePost,
} from '../../../queries'
import { renderApiErrorMessage } from '../../../errors'
import { EMAIL_REGEX } from '../../../constants'
import { Help } from '@mui/icons-material'
import { FormDialogLayout } from '../../../components/FormDialogLayout'

export const Email = () => {
    const theme = useTheme()
    const account = useContext(AccountContext)
    const { accountDetailsData, refetch } = account
    const userId = accountDetailsData?.userId || ''
    const [confirmationCode, setConfirmationCode] = useState('')
    const [openDialog, setOpenDialog] = useState(false)
    const [email, setEmail] = useState(accountDetailsData?.email || '')
    const [emailError, setEmailError] = useState(false)
    const defaultStatus = {
        status: false,
        type: '',
        text: '',
    }
    const [status, setStatus] = useState(defaultStatus)

    useEffect(() => {
        setEmail(accountDetailsData?.email || '')
    }, [accountDetailsData?.email])

    const { mutate, isPending: isLoading } =
        useUsersUserIdEmailPutUsersUserIdEmailPut({
            mutation: {
                onSuccess: () => {
                    refetch && refetch()
                },
                onError: (err) => {
                    setStatus({
                        status: true,
                        type: 'error',
                        text: renderApiErrorMessage(err),
                    })
                },
            },
        })
    const {
        mutate: mutateConfirmationCode,
        isPending: isLoadingConfirmationCode,
    } = useUsersUserIdEmailVerifyUpdatePostUsersUserIdEmailVerifyUpdatePost({
        mutation: {
            onSuccess: () => {
                refetch && refetch()
                handleCloseDialog()
            },
            onError: (err) => {
                setStatus({
                    status: true,
                    type: 'error',
                    text: renderApiErrorMessage(err),
                })
            },
        },
    })

    function handleSubmitEmail() {
        if (!userId || !account) return

        mutate({
            userId,
            data: { newEmail: email },
        })
        setOpenDialog(true)
    }

    function handleCloseDialog() {
        setStatus(defaultStatus)
        setEmailError(false)
        setEmail(accountDetailsData?.email || '')
        setConfirmationCode('')
        setOpenDialog(false)
    }

    function handleEmailChange(event: React.ChangeEvent<HTMLInputElement>) {
        const validEmail = EMAIL_REGEX.test(event.target.value)
        setEmailError(!validEmail)
        setEmail(event.target.value)
    }

    function handleConfirmationCodeSubmit() {
        mutateConfirmationCode({
            userId,
            data: { confirmationCode },
        })
    }

    return (
        <Paper className="details-content">
            <PageContent title="Email">
                <FormGroup>
                    <TextField
                        type="text"
                        name="email"
                        id="form-email"
                        label="Email"
                        size="medium"
                        value={email}
                        onChange={handleEmailChange}
                        error={emailError}
                        helperText={emailError && 'Invalid email'}
                        required
                        InputProps={{
                            endAdornment: (
                                <Tooltip
                                    title={
                                        <Typography sx={{ pb: 1 }}>
                                            {
                                                <>
                                                    If you change your email,
                                                    the old email is used until
                                                    you've verified your new
                                                    email.
                                                </>
                                            }
                                        </Typography>
                                    }
                                >
                                    <Help color="info" />
                                </Tooltip>
                            ),
                        }}
                    />
                    <Spacing value={theme.spacing(2)} />
                </FormGroup>

                <ButtonWithLoadingStatus loading={isLoading} margin={0}>
                    <Button
                        variant="outlined"
                        disabled={emailError}
                        onClick={handleSubmitEmail}
                    >
                        Save
                    </Button>
                </ButtonWithLoadingStatus>
                <Button
                    onClick={() => {
                        setOpenDialog(true)
                    }}
                >
                    Enter confirmation code
                </Button>
            </PageContent>
            <FormDialogLayout
                formDataState={{ confirmationCode, error: !confirmationCode }}
                status={status}
                open={openDialog}
                title="Confirmation Code"
                content="An email with a confirmation code has been sent to your new email. Please enter the confirmation code below to finish updating your email."
                handleSubmit={handleConfirmationCodeSubmit}
                handleClose={handleCloseDialog}
                loading={isLoadingConfirmationCode}
                handleSubmitText="Submit"
            >
                <FormGroup>
                    <FormControl required>
                        <TextField
                            type="text"
                            name="enter-confirmation-code"
                            id="form-confirmation-code"
                            label="Confirmation code"
                            size="medium"
                            value={confirmationCode}
                            onChange={(e) =>
                                setConfirmationCode(e.target.value)
                            }
                            required
                        />
                        <Spacing value={theme.spacing(2)} />
                    </FormControl>
                </FormGroup>
            </FormDialogLayout>
        </Paper>
    )
}
