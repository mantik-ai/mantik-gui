import {
    Button,
    FormControl,
    FormHelperText,
    IconButton,
    InputAdornment,
    InputLabel,
    OutlinedInput,
    Paper,
    useTheme,
} from '@mui/material'
import { Dispatch, SetStateAction, useContext, useState } from 'react'
import { Visibility, VisibilityOff } from '@mui/icons-material'
import { RequirementsChecklist } from '../../../components/forms/RequirementsChecklist'
import AccountContext from '../../../context/AccountContext'
import { usePasswordValidator } from '../../../hooks/usePasswordValidator'
import { useUsersUserIdPasswordPostUsersUserIdPasswordPost } from '../../../queries'
import { renderApiErrorMessage } from '../../../errors'
import { PageContent } from '../../../components/PageContent'
import { FormLayout } from '../../../components/FormLayout'
import { Spacing } from '../../../components/Spacing'

interface PasswordInputfieldProps {
    id: string
    title: string
    data: string
    setPassword: Dispatch<SetStateAction<string>>
    error?: boolean
    handleChange: () => void
}
const PasswordInputfield = ({
    id,
    title,
    data,
    setPassword,
    error,
    handleChange,
}: PasswordInputfieldProps) => {
    const [showPassword, setShowPassword] = useState(false)
    const handleMouseDownPassword = (
        event: React.MouseEvent<HTMLButtonElement>
    ) => {
        event.preventDefault()
    }
    return (
        <FormControl fullWidth variant="outlined" required>
            <InputLabel color={error ? 'error' : 'primary'} htmlFor={id}>
                {title}
            </InputLabel>
            <OutlinedInput
                type={showPassword ? 'text' : 'password'}
                name={id}
                id={id}
                size="medium"
                value={data}
                error={error}
                label={title}
                onChange={(e) => {
                    setPassword(e.target.value)
                    handleChange()
                }}
                required
                endAdornment={
                    <InputAdornment position="end">
                        <IconButton
                            size="small"
                            aria-label="toggle password visibility"
                            onClick={() => setShowPassword((prev) => !prev)}
                            onMouseDown={handleMouseDownPassword}
                        >
                            {showPassword ? <VisibilityOff /> : <Visibility />}
                        </IconButton>
                    </InputAdornment>
                }
            />
        </FormControl>
    )
}

const initialStatus = {
    status: false,
    type: '',
    text: '',
}
export const PasswordReset = () => {
    const theme = useTheme()
    const account = useContext(AccountContext)
    const { accountDetailsData: data } = account
    const userId = data?.userId || ''

    const [oldPassword, setOldPassword] = useState('')
    const [newPassword, setNewPassword] = useState('')
    const [reEnterPassword, setReEnterPassword] = useState('')
    const [status, setStatus] = useState(initialStatus)
    const { passwordRequirements, error } = usePasswordValidator(newPassword)
    const allFieldsFilled = oldPassword && newPassword && reEnterPassword

    const { mutate, isPending: isLoading } =
        useUsersUserIdPasswordPostUsersUserIdPasswordPost({
            mutation: {
                onSuccess: () => {
                    setStatus({
                        status: true,
                        type: 'success',
                        text: `Password was updated successfully!`,
                    })
                },
                onError: (err) => {
                    setStatus({
                        status: true,
                        type: 'error',
                        text: renderApiErrorMessage(err),
                    })
                },
            },
        })

    function handleDialogClose() {
        setStatus(initialStatus)
        setOldPassword('')
        setNewPassword('')
        setReEnterPassword('')
    }

    function handleSubmitPassword() {
        mutate({
            userId,
            data: {
                oldPassword,
                newPassword,
            },
        })
    }

    function handlePasswordChange() {
        setStatus(initialStatus)
    }

    return (
        <Paper className="details-content">
            <PageContent title="Password">
                <FormLayout
                    handleClose={handleDialogClose}
                    status={status}
                    buttonVariant="outlined"
                >
                    <PasswordInputfield
                        title="Old password"
                        id="oldPassword"
                        data={oldPassword}
                        setPassword={setOldPassword}
                        handleChange={handlePasswordChange}
                    />
                    <Spacing value={theme.spacing(2)} />
                    <PasswordInputfield
                        title="New password"
                        id="newPassword"
                        data={newPassword}
                        setPassword={setNewPassword}
                        handleChange={handlePasswordChange}
                        error={!!newPassword.length && error}
                    />
                    {!!newPassword.length && (
                        <FormHelperText>
                            <RequirementsChecklist
                                requirements={passwordRequirements}
                            />
                        </FormHelperText>
                    )}
                    <Spacing value={theme.spacing(2)} />
                    <PasswordInputfield
                        title="Re-enter password"
                        id="reEnterPassword"
                        data={reEnterPassword}
                        setPassword={setReEnterPassword}
                        error={
                            !!reEnterPassword.length &&
                            reEnterPassword !== newPassword
                        }
                        handleChange={handlePasswordChange}
                    />
                    {!!reEnterPassword.length &&
                        reEnterPassword !== newPassword && (
                            <FormHelperText color="error">
                                Passwords do not match
                            </FormHelperText>
                        )}
                    <Spacing value={theme.spacing(2)} />
                </FormLayout>
                {!status.status && (
                    <Button
                        loading={isLoading}
                        variant="outlined"
                        onClick={handleSubmitPassword}
                        disabled={
                            error ||
                            !allFieldsFilled ||
                            reEnterPassword !== newPassword
                        }
                    >
                        Save
                    </Button>
                )}
            </PageContent>
        </Paper>
    )
}
