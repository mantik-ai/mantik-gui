import {
    Button,
    FormControl,
    FormGroup,
    InputAdornment,
    InputLabel,
    OutlinedInput,
    Paper,
    TextField,
    useTheme,
} from '@mui/material'
import { PageContent } from '../../../components/PageContent'
import { FormLayout } from '../../../components/FormLayout'
import {
    Dispatch,
    Fragment,
    SetStateAction,
    useContext,
    useEffect,
    useRef,
    useState,
} from 'react'
import { renderApiErrorMessage } from '../../../errors'
import { useUsersUserIdPutUsersUserIdPut } from '../../../queries'
import { generateTitleFromCamelCaseKey } from '../../../helpers'
import { Spacing } from '../../../components/Spacing'
import AccountContext from '../../../context/AccountContext'
import CopyToClipboard from '../../../components/CopyToClipboard'

// split functions for testing purposes
// probably not enough use cases to make more generic (would require all form props from MUI as props) and reusable
interface FormInputProps<T> {
    data: { [K in keyof T]: T[K] }
    setData: Dispatch<SetStateAction<T>>
}
export function FormTextfieldGroup<T>({ data, setData }: FormInputProps<T>) {
    const formRef = useRef<HTMLDivElement>(null)
    const theme = useTheme()

    return (
        <FormGroup ref={formRef}>
            {Object.keys(data).map((field) => (
                <Fragment key={field}>
                    <FormControl>
                        <TextField
                            type="text"
                            name={field}
                            id={field}
                            label={generateTitleFromCamelCaseKey(field)}
                            size="medium"
                            value={data[field as keyof typeof data]}
                            error={false}
                            onChange={(e) => {
                                setData((prev) => ({
                                    ...prev,
                                    [field]: e.target.value,
                                }))
                            }}
                            disabled={field === 'name'}
                        />
                    </FormControl>
                    <Spacing value={theme.spacing(2)} />
                </Fragment>
            ))}
        </FormGroup>
    )
}

export const Profile = () => {
    const account = useContext(AccountContext)
    const { accountDetailsData: data, refetch } = account
    const userId = data?.userId || ''
    const theme = useTheme()

    const [status, setStatus] = useState({
        status: false,
        type: '',
        text: '',
    })

    const [profile, setProfile] = useState({
        name: '',
        fullName: '',
        info: '',
        company: '',
        jobTitle: '',
        websiteUrl: '',
    })

    const { mutate, isPending: isLoading } = useUsersUserIdPutUsersUserIdPut({
        mutation: {
            onSuccess: () => {
                setStatus({
                    status: true,
                    type: 'success',
                    text: `Profile was updated successfully!`,
                })
                if (refetch) refetch()
            },
            onError: (err) => {
                setStatus({
                    status: true,
                    type: 'error',
                    text: renderApiErrorMessage(err),
                })
            },
        },
    })

    useEffect(() => {
        if (!data) return
        setProfile({
            name: data.name,
            fullName: data.fullName || '',
            info: data.info || '',
            company: data.company || '',
            jobTitle: data.jobTitle || '',
            websiteUrl: data.websiteUrl || '',
        })
    }, [data])

    function handleSubmitProfile() {
        if (!userId || !account) return

        mutate({
            userId,
            data: { ...profile },
        })
    }

    function handleDialogClose() {
        setStatus({
            status: false,
            type: '',
            text: '',
        })
    }

    return (
        <Paper className="details-content">
            <PageContent title="Profile">
                <FormLayout
                    handleClose={handleDialogClose}
                    status={status}
                    buttonVariant="outlined"
                >
                    <FormControl fullWidth>
                        <InputLabel htmlFor="userId">User ID</InputLabel>
                        <OutlinedInput
                            type="text"
                            name="userId"
                            id="userId"
                            size="medium"
                            value={userId}
                            disabled
                            label="User ID"
                            endAdornment={
                                <InputAdornment position="end">
                                    <CopyToClipboard data={userId} />
                                </InputAdornment>
                            }
                        />
                        <Spacing value={theme.spacing(2)} />
                    </FormControl>
                    <FormTextfieldGroup data={profile} setData={setProfile} />
                </FormLayout>

                {!status.status && (
                    <Button
                        loading={isLoading}
                        variant="outlined"
                        onClick={handleSubmitProfile}
                    >
                        Submit
                    </Button>
                )}
            </PageContent>
        </Paper>
    )
}
