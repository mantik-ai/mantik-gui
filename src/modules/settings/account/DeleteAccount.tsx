import { Button, Paper, Typography, useTheme } from '@mui/material'
import { PageContent } from '../../../components/PageContent'
import { Spacing } from '../../../components/Spacing'
import { useUsersUserIdDeleteUsersUserIdDelete } from '../../../queries'
import { useContext, useState } from 'react'
import AccountContext from '../../../context/AccountContext'
import { ButtonWithLoadingStatus } from '../../../components/ButtonWithLoadingStatus'
import { DeleteDialogLayout } from '../../../components/DeleteDialogLayout'
import { renderApiErrorMessage } from '../../../errors'
import { signOut } from 'next-auth/react'
import { useRouter } from 'next/router'

export const DeleteAccount = () => {
    const theme = useTheme()
    const router = useRouter()
    const account = useContext(AccountContext)
    const userId = account.accountDetailsData?.userId || ''
    const [openDialog, setOpenDialog] = useState(false)
    const defaultStatus = {
        status: false,
        type: '',
        text: '',
    }
    const [status, setStatus] = useState(defaultStatus)

    const { mutate, isPending: isLoading } =
        useUsersUserIdDeleteUsersUserIdDelete({
            mutation: {
                onSuccess: () => {
                    setStatus({
                        status: true,
                        type: 'success',
                        text: 'Account was deleted successfully.',
                    })
                },
                onError: (err) => {
                    setStatus({
                        status: true,
                        type: 'error',
                        text: renderApiErrorMessage(err),
                    })
                },
            },
        })

    async function handleCloseDialog() {
        setOpenDialog(false)
        if (status.status && status.type === 'success') {
            await router.push('/login')
            signOut()
        }
    }

    function handleSubmitDelete() {
        if (!userId) return
        mutate({ userId })
    }

    return (
        <Paper className="details-content">
            <PageContent title="Delete account">
                <Typography>
                    You must transfer all your projects to other project members
                    before being able to delete your account.
                </Typography>
                <Spacing value={theme.spacing(2)} />
                <ButtonWithLoadingStatus loading={isLoading} margin={0}>
                    <Button
                        variant="outlined"
                        color="error"
                        onClick={() => {
                            setOpenDialog(true)
                        }}
                    >
                        Delete account
                    </Button>
                </ButtonWithLoadingStatus>
            </PageContent>
            <DeleteDialogLayout
                open={openDialog}
                title="Delete Account"
                warning={
                    <>
                        Are you sure that you want to delete your account{' '}
                        <strong>{account.accountDetailsData?.name}</strong>?
                    </>
                }
                handleClose={handleCloseDialog}
                handleSubmit={handleSubmitDelete}
                isLoading={isLoading}
                status={status}
                deleteButtonLabel="Delete"
            />
        </Paper>
    )
}
