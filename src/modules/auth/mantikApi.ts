import { AxiosError, AxiosRequestConfig } from 'axios'
import axios from 'axios'
// eslint-disable-next-line @typescript-eslint/no-require-imports
const qs = require('qs')
export const AXIOS_INSTANCE = axios.create({
    baseURL: process.env.NEXT_PUBLIC_API_URL,
    paramsSerializer: (params) => {
        return qs.stringify(params, { arrayFormat: 'repeat' })
    },
})

export const mantikApiInstance = <T>(
    config: AxiosRequestConfig,
    options?: AxiosRequestConfig
): Promise<T> => {
    const source = axios.CancelToken.source()
    const promise = AXIOS_INSTANCE({
        ...config,
        ...options,
        cancelToken: source.token,
    }).then(({ data }) => data)

    //@ts-expect-error todo:fix this type
    promise.cancel = () => {
        source.cancel('Query was cancelled')
    }

    return promise
}

// In some case with react-query and swr you want to be able to override the return error type so you can also do it here like this
export type ErrorType<Error> = AxiosError<Error>
