import {
    Button,
    Card,
    CardContent,
    CircularProgress,
    Divider,
    FormHelperText,
    Link,
    Stack,
    Typography,
} from '@mui/material'
import React, { ReactNode } from 'react'
import { SvgIconComponent } from '@mui/icons-material'

export const enum AuthCardTypes {
    LOGIN = 'login',
    REGISTER = 'register',
    CONFIRM = 'confirm',
    FORGOT_PWD = 'send',
    RESEND_VERIFICATION_CODE = 'resend',
    PASSWORD_RESET = 'reset',
}

interface AuthCardProps {
    icon: SvgIconComponent
    fields: ReactNode[]
    type: AuthCardTypes
    onClick?: React.MouseEventHandler<HTMLButtonElement>
    disabled?: boolean
    globalError?: string
    globalSuccess?: ReactNode
    loading?: boolean
    unverifiedEmail?: boolean
}

export const AuthCard = (props: AuthCardProps) => {
    let heading = ''
    let linkText = ''
    let link = ''

    switch (props.type) {
        case AuthCardTypes.LOGIN:
            heading = AuthCardTypes.LOGIN
            linkText = 'Not yet registered?'
            link = '/register'
            break
        case AuthCardTypes.REGISTER:
            heading = AuthCardTypes.REGISTER
            linkText = 'Already registered?'
            link = '/login'
            break
        case AuthCardTypes.CONFIRM:
            heading = 'Enter confirmation code'
            linkText = ''
            link = ''
            break
        case AuthCardTypes.FORGOT_PWD:
            heading = 'Enter username or email'
            linkText = ''
            link = '/forgot-password'
            break
        case AuthCardTypes.RESEND_VERIFICATION_CODE:
            heading = 'Enter username or email'
            linkText = ''
            link = ''
            break
        case AuthCardTypes.PASSWORD_RESET:
            heading = 'Enter new password'
            linkText = ''
            link = ''
            break
        default:
            break
    }

    return (
        <Stack alignItems={'center'} justifyContent={'center'} flexGrow={1}>
            <Card
                sx={{
                    p: 3,
                    pt: 4,
                    pb: 1,
                    width: '100%',
                    maxWidth: '400px',
                    boxShadow: { xs: 0, sm: 3 },
                }}
            >
                <Divider textAlign="left">
                    <props.icon
                        sx={{
                            fontSize: '3rem',
                            backgroundColor: 'primary.main',
                            color: 'white',
                            borderRadius: '50%',
                            padding: '0.5rem',
                        }}
                    />
                </Divider>
                <CardContent>
                    {props.loading ? (
                        <Stack direction="row" justifyContent="center">
                            <CircularProgress></CircularProgress>
                        </Stack>
                    ) : (
                        <Stack direction={'column'} spacing={2}>
                            <Stack
                                mb={1}
                                direction="row"
                                justifyContent="space-between"
                                alignItems="center"
                            >
                                <Typography
                                    variant="h4"
                                    textTransform="capitalize"
                                >
                                    {heading}
                                </Typography>
                                <Link
                                    href={link}
                                    variant="body2"
                                    color="textSecondary"
                                    align="right"
                                >
                                    {linkText}
                                </Link>
                            </Stack>
                            {props.fields}

                            {props.type === AuthCardTypes.LOGIN && (
                                <Link
                                    variant="body2"
                                    color="textSecondary"
                                    align="left"
                                    href="/forgot-password"
                                >
                                    Forgot password?
                                </Link>
                            )}
                            {props.type ===
                                AuthCardTypes.RESEND_VERIFICATION_CODE && (
                                <Typography
                                    variant="body2"
                                    color="red"
                                    align="left"
                                >
                                    Please enter your username or email to
                                    receive a new verification code
                                </Typography>
                            )}
                            {!props.globalSuccess && (
                                <Button
                                    loading={props.loading}
                                    disabled={props.disabled}
                                    variant="contained"
                                    onClick={props.onClick}
                                    type="submit"
                                >
                                    {props.type}
                                </Button>
                            )}
                            {props.globalError ? (
                                <FormHelperText error>
                                    {props.globalError}
                                    {props.unverifiedEmail && (
                                        <>
                                            <br />
                                            <Link href="/resend-code">
                                                Resend verification code
                                            </Link>
                                        </>
                                    )}
                                </FormHelperText>
                            ) : null}

                            {props.globalSuccess && props.globalSuccess}
                        </Stack>
                    )}
                </CardContent>
            </Card>
        </Stack>
    )
}
