import NotificationsIcon from '@mui/icons-material/Notifications'
import {
    Badge,
    Popover,
    List,
    ListItem,
    Button,
    Typography,
    Divider,
    ListItemText,
} from '@mui/material'
import * as React from 'react'
import {
    Invitation,
    UpdateInvitation,
    useInvitationsGetInvitationsGet,
    useInvitationsInvitationIdPutInvitationsInvitationIdPut,
} from '../../queries'
import { useEffect, useState } from 'react'
import { useAuthentication } from '../../hooks/useAuthentication'
import { ButtonWithLoadingStatus } from '../../components/ButtonWithLoadingStatus'
import { capitalizeFirstLetter } from '../../helpers'

const Notification = () => {
    const [anchorEl, setAnchorEl] = useState<null | HTMLSpanElement>(null)
    const [notifications, setNotifications] = useState<
        Invitation[] | undefined
    >()
    const authentication = useAuthentication()

    const {
        data: getNotifications,
        status: notificationStatus,
        isPending: isLoading,
    } = useInvitationsGetInvitationsGet(
        {},
        {
            query: {
                retry: 1,
                enabled: authentication === 'authenticated',
            },
        }
    )
    const { mutate } = useInvitationsInvitationIdPutInvitationsInvitationIdPut()

    useEffect(() => {
        if (notificationStatus === 'success') {
            setNotifications(getNotifications?.invitations)
        }
    }, [notificationStatus, getNotifications?.invitations])
    const handleNotificationClick = (
        event: React.MouseEvent<HTMLSpanElement>
    ) => {
        setAnchorEl(event.currentTarget)
    }

    const handleNotificationClose = () => {
        setAnchorEl(null)
    }

    const handleInvitationResponse = (
        notification: Invitation,
        accepted: boolean
    ) => {
        const data: UpdateInvitation = {
            accepted,
        }

        mutate({
            invitationId: notification.invitationId,
            data,
        })

        removeNotification(notification)
    }

    const removeNotification = (notification: Invitation) => {
        setNotifications((prevNotifications) =>
            prevNotifications?.filter(
                (invitation) =>
                    invitation.invitationId !== notification.invitationId
            )
        )
    }
    const open = Boolean(anchorEl)
    const id = open ? 'notification-popover' : undefined
    function generateMessage(notification: Invitation) {
        if (notification) {
            const message = `${
                notification.inviterName
                    ? capitalizeFirstLetter(notification.inviterName)
                    : ''
            } has invited ${
                notification.invitedName
                    ? capitalizeFirstLetter(notification.invitedName)
                    : ''
            } to the ${
                notification.invitedToType
                    ? notification.invitedToType.toLowerCase()
                    : ''
            } - ${notification.invitedToName ? notification.invitedToName : ''}`
            return notification.role
                ? `${message} with a role ${notification.role} .`
                : `${message}.`
        } else {
            return ''
        }
    }

    return (
        <>
            <ButtonWithLoadingStatus loading={isLoading}>
                <Badge
                    badgeContent={notifications?.length}
                    max={99}
                    color="info"
                    sx={{
                        cursor: 'pointer',
                        '& .MuiBadge-badge': {
                            color: 'white',
                            backgroundColor: '#bdbdbd',
                        },
                    }}
                    onClick={handleNotificationClick}
                >
                    <NotificationsIcon color="secondary" />
                </Badge>
            </ButtonWithLoadingStatus>
            <Popover
                id={id}
                open={open}
                anchorEl={anchorEl}
                onClose={handleNotificationClose}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'right',
                }}
                transformOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                }}
            >
                <List>
                    <Typography
                        variant="h6"
                        component="div"
                        sx={{ margin: 1, padding: 1 }}
                    >
                        {notifications?.length
                            ? 'Invitations'
                            : 'Great! You have caught up!'}
                    </Typography>
                    <Divider variant="middle" />
                    {notifications &&
                        notifications.map((notification) => {
                            return (
                                <React.Fragment key={notification.invitationId}>
                                    <ButtonWithLoadingStatus
                                        loading={isLoading}
                                    >
                                        <ListItem>
                                            <ListItemText
                                                primary={generateMessage(
                                                    notification
                                                )}
                                            />
                                            <Button
                                                onClick={() =>
                                                    handleInvitationResponse(
                                                        notification,
                                                        true
                                                    )
                                                }
                                            >
                                                Accept
                                            </Button>
                                            <Button
                                                onClick={() =>
                                                    handleInvitationResponse(
                                                        notification,
                                                        false
                                                    )
                                                }
                                            >
                                                Reject
                                            </Button>
                                        </ListItem>
                                    </ButtonWithLoadingStatus>
                                    <Divider variant="middle" />
                                </React.Fragment>
                            )
                        })}
                </List>
            </Popover>
        </>
    )
}

export default Notification
