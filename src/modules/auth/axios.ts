import axios from 'axios'
import getConfig from 'next/config'

const { publicRuntimeConfig } = getConfig()

export const mantikApi = axios.create({
    baseURL: process.env.NEXT_PUBLIC_API_URL,
})

const instance = axios.create({
    baseURL: publicRuntimeConfig?.nextAuthUrl ?? process.env.NEXTAUTH_URL,
})
export default instance
