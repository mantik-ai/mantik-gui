import { IconButton, IconButtonProps, Tooltip } from '@mui/material'
import React from 'react'

export interface ActionButton {
    title: string
    actionFunction: (() => void) | undefined
    iconComponent: React.ReactNode
    color?: IconButtonProps['color']
}
export function generateCustomActionButtonsArray(
    actionButtons: ActionButton[]
) {
    return actionButtons.map((button) => {
        return (
            <Tooltip
                key={button.title}
                title={button.title}
                color={'primary'}
                placement="top"
                onClick={button.actionFunction}
            >
                <IconButton
                    size="small"
                    color={button.color ? button.color : 'primary'}
                    disabled={!button.actionFunction}
                >
                    {button.iconComponent}
                </IconButton>
            </Tooltip>
        )
    })
}
