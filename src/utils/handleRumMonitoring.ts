import { AwsRum, AwsRumConfig } from 'aws-rum-web'
import { useCallback, useEffect, useRef } from 'react'

function initRUM() {
    try {
        const APPLICATION_ID = process.env.NEXT_PUBLIC_RUM_APPLICATION_ID
        const APPLICATION_VERSION = '1.0.0'
        const APPLICATION_REGION =
            process.env.NEXT_PUBLIC_RUM_APPLICATION_REGION
        const RUM_IDENTITY_POOL_ID =
            process.env.NEXT_PUBLIC_RUM_IDENTITY_POOL_ID

        if (!APPLICATION_ID || !APPLICATION_REGION || !RUM_IDENTITY_POOL_ID) {
            throw new Error(
                'AWS RUM application ID, region or identity pool id is not set'
            )
        }

        const config: AwsRumConfig = {
            sessionSampleRate: 1,
            identityPoolId: RUM_IDENTITY_POOL_ID,
            endpoint: `https://dataplane.rum.${APPLICATION_REGION}.amazonaws.com`,
            telemetries: ['performance', 'errors', 'http'],
            allowCookies: true,
            enableXRay: false,
        }

        return new AwsRum(
            APPLICATION_ID,
            APPLICATION_VERSION,
            APPLICATION_REGION,
            config
        )
    } catch (error) {
        console.error(
            'AWS RUM initialization failed. Please check your AWS RUM configuration and environment variables.',
            error
        )
    }
}

export function useRUMMonitoring(initialConsentValue: boolean) {
    const rumRef = useRef<AwsRum | undefined>(undefined)

    useEffect(() => {
        if (initialConsentValue && !rumRef.current) {
            rumRef.current = initRUM()
        }
    }, [initialConsentValue])

    const enable = useCallback(() => {
        if (rumRef.current) {
            rumRef.current.enable()
        } else {
            rumRef.current = initRUM()
        }
    }, [])

    const disable = useCallback(() => {
        rumRef.current?.disable()
    }, [])

    return {
        enable,
        disable,
    }
}
