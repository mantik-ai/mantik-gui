export type FormState = Record<string, any>

// Purpose of FormAction<FormState> is to reduce boilerplate:
// Can generate type definition of Action from type definition of State

// Given some FormState (i.e. typeof a normal JS object extending FormState)
// such as
// interface AddFormState extends FormState {
//     name: string
//     isDVC: boolean
// }
// Generate a FormAction type such as
// type AddFormAction = {
//     name: { target: 'name', payload: string }
//     isDVC: { target: 'isDVC', payload: boolean }
// }
// This way, we can use the FormAction type to generate the type of the action object in the reducer
// and avoid having to write the action object manually.
export type FormAction<S extends FormState> = {
    [key in keyof S]: { target: key; payload: S[key] }
}[keyof S]

export type DisplayName<S extends FormState> = { [key in keyof S]: string }

// In reducer patter, we define a functin called the reducer
// which takes the current state and an action, and returns the new state
// we pass this function along with initial state to useReducer hook,
// which returns the current state and a dispatch function
// dispatch is like a "fancier" setState, for modifying a complex state object
export const formReducer = <S extends FormState>(
    state: S,
    action: FormAction<S>
) =>
    action.target === '@reset'
        ? action.payload
        : {
              ...state,
              [action.target]: action.payload,
          }

const validateAllRequired = <S extends FormState>(
    formState: S,
    requiredFields: (keyof S)[]
) => {
    const isValid = requiredFields.every(
        (key) => formState[key] !== undefined && formState[key] !== ''
    )
    return isValid
}

// export type CustomValidator = <S extends FormState>(state: S) => boolean
// export type CustomValidatorMap<S extends FormState> =  Record<[K keyS, CustomValidator<S>>

export type CustomValidator<S extends FormState> = Record<
    string,
    (state: S) => boolean
>

const validateCustomValidators = <S extends FormState>(
    state: S,
    customValidators: CustomValidator<S>
) => {
    const isValid = Object.keys(customValidators).every(
        (key) => customValidators[key]?.(state) ?? true
    )
    return isValid
}

export type DefaultValidatorProps<S extends FormState> = {
    state: S
    requiredFields: (keyof S)[]
    customValidators?: CustomValidator<S>
}
export const defaultValidator = <S extends FormState>({
    state,
    requiredFields = [],
    customValidators = {},
}: DefaultValidatorProps<S>) => {
    const allRequired = validateAllRequired(state, requiredFields)
    const allCustomValidationsPass = validateCustomValidators(
        state,
        customValidators
    )
    return allRequired && allCustomValidationsPass
}
