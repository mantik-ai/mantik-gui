import {
    OrganizationProjectRole,
    ProjectMember,
    UserGroupProjectRole,
} from '../queries'
import { parseUserRole, toSemanticUserRole } from './userRole'

export type TableEntry = {
    Name: { data: string }
    Role: { data: string }
    Type?: { data: string | undefined }
    ID?: { data: string }
}

export type TransformApiDataResult = {
    transformedData: TableEntry[]
    tableData: Array<{ data: TableEntry }>
}

export type ApiItemType =
    | OrganizationProjectRole
    | UserGroupProjectRole
    | ProjectMember

function extractIdAndName(type: string | undefined, item: ApiItemType) {
    let idValue = ''
    let nameValue = ''

    if (type === 'Organization' && 'organization' in item) {
        idValue = item.organization.organizationId
        nameValue = item.organization.name
    } else if (type === 'Group' && 'userGroup' in item) {
        idValue = item.userGroup.userGroupId
        nameValue = item.userGroup.name
    } else if (type === 'User' && 'user' in item) {
        idValue = item.user.userId
        nameValue = item.user.name
    }

    return { idValue, nameValue }
}

export function generateCollaborationData(
    apiData: ApiItemType[] | undefined,
    type?: string
): TransformApiDataResult {
    if (Array.isArray(apiData) && apiData.length > 0) {
        const transformedData = apiData.map((item) => {
            const { idValue, nameValue } = extractIdAndName(type, item)

            const entry: TableEntry = {
                Name: { data: nameValue },
                Role: {
                    data: toSemanticUserRole(
                        parseUserRole(item.role.toString())
                    ),
                },
                ID: { data: idValue },
            }

            if (type !== 'User') {
                entry.Type = { data: type }
            }

            return entry
        })

        const tableData = transformedData.map(({ ID: _ID, ...rest }) => ({
            data: rest,
        }))

        return { transformedData, tableData }
    }

    return { transformedData: [], tableData: [] }
}
