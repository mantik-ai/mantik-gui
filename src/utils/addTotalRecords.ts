export const addTotalRecords = (
    groupsTotalRecords: number | undefined,
    organizationTotalRecords: number | undefined
): number | undefined => {
    if (groupsTotalRecords && organizationTotalRecords) {
        return groupsTotalRecords + organizationTotalRecords
    } else {
        return groupsTotalRecords || organizationTotalRecords
    }
}
