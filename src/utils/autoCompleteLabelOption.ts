import { Label, LabelsGet200Response } from '../queries'

export const autoCompleteLabelOption = (
    data: LabelsGet200Response | undefined
) => {
    return (
        data?.labels?.map((option: Label) => ({
            ...option,
            categoryWithSubCategory: option.category
                ? option.subCategory
                    ? `${option.category}: ${option.subCategory}`
                    : option.category
                : '',
        })) || []
    )
}
