import { UserRole } from '../types/UserRole'

export enum ResourceType {
    PROJECT = 'Project',
    EXPERIMENTS = 'Experiments',
    RUN_SCHEDULES = 'Run Schedules',
    CODE = 'Code',
    SETTINGS = 'Settings',
    MODEL = 'Model',
}
export enum RenderElement {
    SETTINGS = 'Settings',
    ML_FLOW_EXPERIMENTS_LINK = 'MlFlowExperimentsLink',
    ADD = 'Add',
    EDIT = 'Edit',
    DELETE = 'Delete',
    RERUN = 'Rerun',
    CANCEL = 'Cancel',
    DOWNLOAD = 'Download',
    SHOW_DETAILS = 'ShowDetails',
    REGISTER_MODEL = 'RegisterModel',
    DELETE_MODEL = 'DeleteModel',
    CONTAINERIZE_MODEL = 'ContainerizeModel',
    DOWNLOAD_CONTAINERIZE_MODEL = 'DownloadContainerizeModel',
    BUILDING_PENDING = 'Building Pending',
    BUILDING_CONTAINER = 'Building Container',
    BUILDING_FAILED = 'Building Failed',
    DOWNLOAD_ARTIFACTS = 'Download Artifacts',
}

/**
 * @deprecated The responsibilities of this function are too high.
 *   Prefer directly checking the role where the item is rendered or use at
 *   least separate functions per resource type.
 */
export function renderResourceActions(
    resourceType: ResourceType,
    userRole: UserRole
): RenderElement[] {
    const actions: RenderElement[] = []
    if (userRole >= UserRole.Owner) {
        if (resourceType === ResourceType.SETTINGS) {
            actions.push(RenderElement.ADD)
        }
    }
    if (userRole >= UserRole.Maintainer) {
        actions.push(RenderElement.SETTINGS)
    }

    if (userRole >= UserRole.Reporter) {
        if (resourceType === ResourceType.EXPERIMENTS) {
            actions.push(RenderElement.ML_FLOW_EXPERIMENTS_LINK)
        }
        if (resourceType === ResourceType.MODEL) {
            actions.push(
                RenderElement.DOWNLOAD_ARTIFACTS,
                RenderElement.DOWNLOAD_CONTAINERIZE_MODEL
            )
        }
    }

    if (userRole >= UserRole.Researcher) {
        if (resourceType !== ResourceType.SETTINGS) {
            actions.push(RenderElement.ADD)
        }
        actions.push(RenderElement.DELETE)
        actions.push(RenderElement.EDIT)
        if (resourceType === ResourceType.MODEL) {
            actions.push(
                RenderElement.CONTAINERIZE_MODEL,
                RenderElement.BUILDING_PENDING,
                RenderElement.BUILDING_CONTAINER,
                RenderElement.BUILDING_FAILED
            )
        }
    }
    return actions
}
