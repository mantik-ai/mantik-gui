import { MantikApiDatabaseRoleDetailsProjectRole } from '../queries'
import { UserRole } from '../types/UserRole'

export const semanticUserRoles = [
    'VISITOR',
    'GUEST',
    'REPORTER',
    'RESEARCHER',
    'MAINTAINER',
    'OWNER',
] as const
export type SemanticUserRole = (typeof semanticUserRoles)[number]

export function parseUserRole(
    userRole:
        | MantikApiDatabaseRoleDetailsProjectRole
        | number
        | string
        | null
        | undefined
): UserRole {
    if (!userRole) return UserRole.Visitor
    const number = parseInt(userRole.toString())
    if (Object.values(UserRole).includes(number)) return number as UserRole
    else return UserRole.Visitor
}

export function toSemanticUserRole(userRole: UserRole): SemanticUserRole {
    switch (userRole) {
        case UserRole.Visitor:
            return 'VISITOR'
        case UserRole.Guest:
            return 'GUEST'
        case UserRole.Reporter:
            return 'REPORTER'
        case UserRole.Researcher:
            return 'RESEARCHER'
        case UserRole.Maintainer:
            return 'MAINTAINER'
        case UserRole.Owner:
            return 'OWNER'
        default:
            return '' as never
    }
}
