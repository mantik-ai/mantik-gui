import { Platform } from '../queries'

export function convertToRawUrl(
    platform: Platform,
    repoUri: string,
    commitOrBranch: string,
    path = ''
): string | null {
    path = path[0] === '/' ? path.substring(1, path.length) : path
    repoUri = repoUri.replace('https://', '')

    const rawUrl = {
        [Platform.GitLab]: `https://${repoUri}/raw/${commitOrBranch}/${path}`,
        [Platform.Bitbucket]: `https://${repoUri}/raw/${commitOrBranch}/${path}`,
        [Platform.GitHub]: `https://raw.${repoUri}/${commitOrBranch}/${path}`,
        //[selfhosted bitbucket]: `https://${repoUri}/raw/${path}?at=refs%2Fheads%2F${commitOrBranch}`,
    }
    return rawUrl[platform] || null
}
