import axios from '../modules/auth/axios'

export async function fetchRepo(
    url: string,
    fnSuccess: (url?: string) => object | void,
    fnError: () => object | void
) {
    try {
        const response = await axios.post('/api/runs/get-repository', {
            url,
        })
        if (response.data.data_response) {
            return { success: true, ...fnSuccess(url) }
        } else {
            return { success: false, ...fnError() }
        }
    } catch (err: unknown) {
        console.log('get url error: ', err)
        return { success: false }
    }
}
