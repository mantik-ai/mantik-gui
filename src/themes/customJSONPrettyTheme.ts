const linebreaks = `
    word-break: break-all; 
    overflow-wrap: break-word; 
    white-space: initial;`

const linebreaksBreakAndWrap = `
    word-break: break-all; 
    overflow-wrap: break-word;
    white-space: pre-wrap;
`

const muiListItemTextStyles = `
    font-size: 0.875rem;
    line-height: 1.43;
    margin: 0 0 .25em;
    color: rgba(0, 0, 0, 0.6);`

const jsonPrettyStyles = muiListItemTextStyles + linebreaks
const jsonPrettyStylesWithLineBreaks =
    muiListItemTextStyles + linebreaksBreakAndWrap

export const customJSONPrettyTheme = {
    main: jsonPrettyStyles,
    error: jsonPrettyStyles,
    key: jsonPrettyStyles,
    string: jsonPrettyStyles,
    value: jsonPrettyStyles,
    boolean: jsonPrettyStyles,
}

export const customJSONPrettyThemeWithBreaks = {
    main: jsonPrettyStylesWithLineBreaks,
    error: jsonPrettyStylesWithLineBreaks,
    key: jsonPrettyStylesWithLineBreaks,
    string: jsonPrettyStylesWithLineBreaks,
    value: jsonPrettyStylesWithLineBreaks,
    boolean: jsonPrettyStylesWithLineBreaks,
}
