import { SvgIcon } from '@mui/material'
import { useEffect, useRef } from 'react'

const animationDuration = '600ms'
export default function ToggleWidth({
    fullWidth = false,
}: {
    fullWidth: boolean
}) {
    const shrinkSize = {
        pointsLeft: '15,11.9 10,7 10,10 5,10 5,11.9 5,14 10,14 10,17 ',
        pointsRight: '27,11.9 27,10 22,10 22,7 17,11.9 22,17 22,14 27,14 ',
    }
    const increaseSize = {
        pointsLeft: '10,11.9 10,10 5,10 5,7 0,11.9 5,17 5,14 10,14 ',
        pointsRight: '32,11.9 27,7 27,10 22,10 22,11.9 22,14 27,14 27,17 ',
    }
    const morphSize = {
        pointsLeft: '6,12 6,10 5,10 4,10 4,12 4,14 5,14 6,14 ',
        pointsRight: '28,12 28,10 27,10 26,10 26,12 26,14 27,14 28,14 ',
    }
    const from = fullWidth ? increaseSize : shrinkSize
    const to = fullWidth ? shrinkSize : increaseSize

    const strokeLeftAnimation = useRef<SVGAnimateElement>(null)
    const strokeRightAnimation = useRef<SVGAnimateElement>(null)
    const arrowLeftAnimation = useRef<SVGAnimateElement>(null)
    const arrowRightAnimation = useRef<SVGAnimateElement>(null)
    useEffect(() => {
        if (strokeLeftAnimation.current)
            strokeLeftAnimation.current.beginElement()
        if (strokeRightAnimation.current)
            strokeRightAnimation.current.beginElement()
        if (arrowLeftAnimation.current)
            arrowLeftAnimation.current.beginElement()
        if (arrowRightAnimation.current)
            arrowRightAnimation.current.beginElement()
    }, [fullWidth])

    return (
        <SvgIcon>
            <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="currentColor"
                viewBox="0 0 32 24"
            >
                <rect x="0" y="4" width="3" height="16">
                    <animate
                        ref={strokeLeftAnimation}
                        attributeName="x"
                        from={fullWidth ? '12' : '0'}
                        to={fullWidth ? '0' : '12'}
                        dur={animationDuration}
                        fill="freeze"
                    />
                </rect>
                <rect x="29" y="4" width="3" height="16">
                    <animate
                        ref={strokeRightAnimation}
                        attributeName="x"
                        from={fullWidth ? '17' : '29'}
                        to={fullWidth ? '29' : '17'}
                        dur={animationDuration}
                        fill="freeze"
                    />
                </rect>
                <polygon points={to.pointsLeft}>
                    <animate
                        ref={arrowLeftAnimation}
                        attributeName="points"
                        values={`${from.pointsLeft};${morphSize.pointsLeft};${to.pointsLeft}`}
                        dur={animationDuration}
                        fill="freeze"
                    />
                </polygon>
                <polygon points={to.pointsRight}>
                    <animate
                        ref={arrowRightAnimation}
                        attributeName="points"
                        values={`${from.pointsRight};${morphSize.pointsRight};${to.pointsRight}`}
                        dur={animationDuration}
                        fill="freeze"
                    />
                </polygon>
            </svg>
        </SvgIcon>
    )
}
