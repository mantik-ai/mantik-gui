import { SvgIcon, SvgIconProps } from '@mui/material'

export default function BitBucket(props: SvgIconProps) {
    return (
        <SvgIcon {...props}>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                <path
                    fillOpacity={0.9}
                    d="M22.1,3.8c0.1-0.3-0.2-0.7-0.5-0.7h-0.1H2.6c-0.4,0-0.7,0.3-0.7,0.6v0.1l2.8,16.9C4.8,21,5,21.2,5.2,21.2h13.4
	c0.3,0,0.6-0.2,0.6-0.5L22.1,3.8z M14.1,15.1H9.9L8.8,9.3h6.4L14.1,15.1z"
                />
                <linearGradient
                    id="gradient"
                    x2="1"
                    gradientTransform="rotate(141 22.239 22.239) scale(31.4)"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop offset="0" stopOpacity={0.7} />
                    <stop offset="1" stopOpacity={0} />
                </linearGradient>
                <path
                    fill="url('#gradient')"
                    d="M15.2,9.3l-1.1,5.8H10v6.1h8.6c0.3,0,0.6-0.2,0.6-0.5l2-11.4H15.2z"
                />
            </svg>
        </SvgIcon>
    )
}
