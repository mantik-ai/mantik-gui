import {
    PaletteColor,
    PaletteColorOptions,
    createTheme,
} from '@mui/material/styles'

declare module '@mui/material/styles' {
    interface Palette {
        due: PaletteColor
        schedule: PaletteColor
    }

    interface PaletteOptions {
        due: PaletteColorOptions
        schedule: PaletteColorOptions
    }
}

declare module '@mui/material/Chip' {
    interface ChipPropsColorOverrides {
        due: true
        schedule: true
    }
}

export const defaultTheme = createTheme({
    palette: {
        primary: {
            main: '#4F98F5',
            dark: '#2e598f',
        },
        secondary: {
            main: '#ffffff',
        },
        due: {
            main: '#4F98F5',
            light: '#4F98F5',
            dark: '#4F98F5',
            contrastText: '#ffffff',
        },
        schedule: {
            main: '#e8b910',
            light: '#e8b910',
            dark: '#e8b910',
            contrastText: '#ffffff',
        },
        info: {
            main: '#888888',
        },
        background: {
            default: '#f9fafb',
        },
    },
    shadows: [
        'none',
        '0px 2px 4px rgba(0, 0, 0, 0.1)',
        '0px 4px 8px rgba(0, 0, 0, 0.1)',
        '0px 8px 16px rgba(0, 0, 0, 0.1)',
        '0px 16px 24px rgba(0, 0, 0, 0.1)',
        '0px 24px 32px rgba(0, 0, 0, 0.1)',
        '0px 32px 40px rgba(0, 0, 0, 0.1)',
        '0px 40px 48px rgba(0, 0, 0, 0.1)',
        '0px 48px 56px rgba(0, 0, 0, 0.1)',
        '0px 56px 64px rgba(0, 0, 0, 0.1)',
        '0px 64px 72px rgba(0, 0, 0, 0.1)',
        '0px 72px 80px rgba(0, 0, 0, 0.1)',
        '0px 80px 88px rgba(0, 0, 0, 0.1)',
        '0px 88px 96px rgba(0, 0, 0, 0.1)',
        '0px 96px 104px rgba(0, 0, 0, 0.1)',
        '0px 104px 112px rgba(0, 0, 0, 0.1)',
        '0px 112px 120px rgba(0, 0, 0, 0.1)',
        '0px 120px 128px rgba(0, 0, 0, 0.1)',
        '0px 128px 136px rgba(0, 0, 0, 0.1)',
        '0px 136px 144px rgba(0, 0, 0, 0.1)',
        '0px 144px 152px rgba(0, 0, 0, 0.1)',
        '0px 152px 160px rgba(0, 0, 0, 0.1)',
        '0px 160px 168px rgba(0, 0, 0, 0.1)',
        '0px 168px 176px rgba(0, 0, 0, 0.1)',
        '0px 176px 184px rgba(0, 0, 0, 0.1)',
    ],
    typography: {
        fontFamily: ['Blinker', 'sans-serif'].join(','),
        fontWeightMedium: 600,
        h1: {
            fontWeight: 300,
            fontSize: 'calc(1.5em + 0.75vw)',
            paddingBottom: '0.25em',
        },
        h2: {
            fontWeight: 300,
            fontSize: 'calc(1.1em + 0.75vw)',
            paddingBottom: '0.125em',
        },
        h3: {
            fontWeight: 700,
            fontSize: '24px',
        },
        h4: {
            fontWeight: 300,
            fontSize: '24px',
            lineHeight: '26px',
        },
        h5: {
            fontWeight: 600,
            fontSize: '20px',
        },
        h6: {
            fontWeight: 400,
            fontSize: '20px',
        },
        button: {
            fontSize: '16px',
            fontWeight: 600,
            minWidth: '95px',
        },
    },
    shape: {
        borderRadius: 6,
    },
    components: {
        MuiCard: {
            styleOverrides: {
                root: {
                    display: 'flex',
                    flexDirection: 'column',
                    justifyContent: 'flex-start',
                },
            },
        },
        MuiCardHeader: {
            styleOverrides: {
                root: {
                    alignItems: 'flex-start',
                    '& .MuiCardHeader-title': {
                        fontSize: '1.15em',
                        fontWeight: 600,
                        lineHeight: 1.25,
                    },
                },
            },
        },
        MuiTypography: {
            styleOverrides: {
                root: {
                    '&.fontWeightBold': {
                        fontWeight: '700',
                    },
                },
            },
        },
        MuiAccordion: {
            styleOverrides: {
                root: {
                    '&.Mui-expanded': {
                        margin: 0,
                    },
                },
            },
        },
        MuiAccordionSummary: {
            styleOverrides: {
                root: {
                    margin: 0,
                    '&.Mui-expanded': {
                        minHeight: 0,
                        padding: 0,
                    },
                },
                content: {
                    margin: 0,
                    '&.Mui-expanded': {
                        margin: 0,
                    },
                },
            },
        },
        MuiAccordionDetails: {
            styleOverrides: {
                root: {
                    padding: 0,
                },
            },
        },
        MuiFormControl: {
            styleOverrides: {
                root: {
                    '&.password': {
                        '.MuiFormHelperText-root': {
                            '&.Mui-error': {
                                color: '#555',
                            },
                        },
                    },
                },
            },
        },
        MuiFormHelperText: {
            styleOverrides: {
                root: {
                    fontSize: '0.9em',
                },
            },
        },
        MuiButton: {
            styleOverrides: {
                root: {
                    marginRight: '0.75em',
                },
                contained: {
                    color: '#fff',
                },
                outlined: {
                    border: '2px solid',
                    '&:hover': {
                        border: '2px solid',
                    },
                    '&:disabled': {
                        border: '2px solid',
                    },
                },
            },
        },
        MuiTooltip: {
            styleOverrides: {
                tooltip: {
                    '& .MuiTypography-root': {
                        fontSize: '1.35em',
                        padding: '0.75em',
                        lineHeight: 1.35,
                    },
                    '& .MuiListSubheader-root': {
                        backgroundColor: 'transparent',
                        color: '#fff',
                        padding: 0,
                    },
                    '& .MuiListItem-root': {
                        padding: '0 0 0.5em 0',
                    },
                },
            },
        },
        MuiPaper: {
            styleOverrides: {
                root: {
                    '&.details-content': {
                        padding: '2em',
                        // flex: 1, use if content area should take full height
                        marginTop: '0.75em',
                    },
                },
            },
        },
    },
})
