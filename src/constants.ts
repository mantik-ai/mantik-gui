export const SEARCH_DEBOUNCE = 500
export const MANTIK_PROVIDER_ID = 'mantik'
export const PAGE_LENGTH_OPTIONS = [50, 25, 10, 5]
export const EMAIL_REGEX = new RegExp(
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
)
export const UPPERCASE_REGEX = new RegExp(/^(?=.*[A-Z]).*$/)
export const LOWERCASE_REGEX = new RegExp(/^(?=.*[a-z]).*$/)
export const NUMBER_REGEX = new RegExp(/^(?=.*[0-9]).*$/)
export const SYMBOL_REGEX = new RegExp(
    /^(?=.*[~`!@#$%^&*()--+={}\[\]|\\:;"'<>,.?/_₹]).*$/
)
export const NO_WHITESPACE_REGEX = new RegExp(/^\S*$/)

// GitHub and GitLab

export const VALID_REPOSITORY_PATH = new RegExp(
    /https:\/\/[a-zA-Z0-9]+\.[a-zA-Z0-9-.\/_]*/gm
)
export const VALID_GITHUB_GITLAB_PATH = new RegExp(
    /(gitlab|github)\.com\/[A-Za-z0-9_.-]+\/[A-Za-z0-9_.-]+/
)
export const VALID_GITLAB_PATH = new RegExp(
    /https:\/\/gitlab\.com\/[A-Za-z0-9_.-]+\/[A-Za-z0-9_.-]+/
)
export const VALID_GITHUB_PATH = new RegExp(
    /https:\/\/github\.com\/[A-Za-z0-9_.-]+\/[A-Za-z0-9_.-]+/
)

export const UUID_REGEX = new RegExp(
    /[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}/
)

//export const CRON_MINUTE_HOURS_REGEX = new RegExp(/(\*(\/\d{1,2})?)|(\d{1,2}(,\d{1,2})?(-\d{1,2})?(,\d{1,2})?)/);
export const CRON_MINUTES_REGEX = new RegExp(
    /(\*|(?:[0-9]|(?:[1-5][0-9]))(?:(?:\-[0-9]|\-(?:[1-5][0-9]))?|(?:\,(?:[0-9]|(?:[1-5][0-9])))*))/
)
export const CRON_HOURS_REGEX = new RegExp(
    /(\*|(?:[0-9]|1[0-9]|2[0-3])(?:(?:\-(?:[0-9]|1[0-9]|2[0-3]))?|(?:\,(?:[0-9]|1[0-9]|2[0-3]))*))/
)
export const CRON_MONTH_REGEX = new RegExp(
    /(\*|(?:[1-9]|1[012]|JAN|FEB|MAR|APR|MAY|JUN|JUL|AUG|SEP|OCT|NOV|DEC)(?:(?:\-(?:[1-9]|1[012]|JAN|FEB|MAR|APR|MAY|JUN|JUL|AUG|SEP|OCT|NOV|DEC))?|(?:\,(?:[1-9]|1[012]|JAN|FEB|MAR|APR|MAY|JUN|JUL|AUG|SEP|OCT|NOV|DEC))*))/
)
export const CRON_DAYOFMONTH_REGEX = new RegExp(
    /(\*|(?:[1-9]|(?:[12][0-9])|3[01])(?:(?:\-(?:[1-9]|(?:[12][0-9])|3[01]))?|(?:\,(?:[1-9]|(?:[12][0-9])|3[01]))*))|\?/
)
export const CRON_DAYOFWEEK_REGEX = new RegExp(
    /(\*|(?:[0-6]|SUN|MON|TUE|WED|THU|FRI|SAT)(?:(?:\-(?:[0-6]|SUN|MON|TUE|WED|THU|FRI|SAT))?|(?:\,(?:[0-6]|SUN|MON|TUE|WED|THU|FRI|SAT))*))|\?/
)

export const MLFLOW_UI_URL =
    process.env.NEXT_PUBLIC_MANTIK_MLFLOW_UI_URL || 'https://example.com'

export const MAIN_LAYOUT_MARGIN_TOP = '3.6em'
export const MAIN_LAYOUT_MARGIN_BOTTOM = '3.6em'

export const NULL_VALUE_REPRESENTATION = '-'
