import { createContext, Dispatch, SetStateAction, useEffect } from 'react'
import { useSession } from 'next-auth/react'
import { QueryStatus } from '@tanstack/react-query'
import {
    getUserDetailsGetUserDetailsGetQueryKey,
    User,
    useUserDetailsGetUserDetailsGet,
} from '../queries'
import { logout } from '../modules/auth/utils'

export interface AuthParameters {
    setUser: Dispatch<SetStateAction<unknown>>
    queryStatus: QueryStatus
    usersData: User
    accessToken: string
}
const AuthContext = createContext<Partial<AuthParameters>>({})
interface AuthProviderProps {
    children: React.ReactNode
}
declare global {
    interface Window {
        accessToken?: string
    }
}

const isPlaywright =
    typeof window !== 'undefined' && Boolean(window.navigator.webdriver)
export const AuthProvider: React.FC<AuthProviderProps> = (props) => {
    const { data: session, status } = useSession()

    const { data: usersData, status: usersDataStatus } =
        useUserDetailsGetUserDetailsGet({
            query: {
                queryKey: getUserDetailsGetUserDetailsGetQueryKey(),
                enabled: status === 'authenticated',
            },
        })

    useEffect(() => {
        if (session?.error === 'RefreshTokenError') {
            logout().catch((e) => console.error('Unable to log out', e))
        }
        if (!session?.user.accessToken) return
        if (isPlaywright) {
            window.accessToken = session.user.accessToken
        }
    }, [session?.error, session?.user.accessToken])

    return (
        <AuthContext.Provider
            value={{
                usersData,
                accessToken: session?.user.accessToken as string,
                queryStatus: usersDataStatus,
            }}
        >
            {props.children}
        </AuthContext.Provider>
    )
}
export default AuthContext
