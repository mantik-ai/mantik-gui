import { createContext, useContext, useEffect, useState } from 'react'
import { QueryStatus } from '@tanstack/react-query'
import { UserDetails, useUsersUserIdGetUsersUserIdGet } from '../queries'
import AuthContext from './AuthContext'

export interface AccountParameters {
    accountDetailsDataStatus: QueryStatus
    accountDetailsData: UserDetails
    refetch: () => void
}
const AccountContext = createContext<Partial<AccountParameters>>({})
interface DataProviderProps {
    children: React.ReactNode
}
export const AccountProvider: React.FC<DataProviderProps> = (props) => {
    const [accountQueryStatus, setAccountQueryStatus] =
        useState<QueryStatus>('pending')

    const user = useContext(AuthContext)
    const userId = user?.usersData?.userId || ''
    const {
        data: accountDetailsData,
        refetch,
        status: accountDetailsDataStatus,
    } = useUsersUserIdGetUsersUserIdGet(userId, {
        request: {
            headers: {
                Authorization: `Bearer ${user.accessToken}`,
                ['Content-Type']: 'application/json',
            },
        },
    })
    useEffect(() => {
        if (accountDetailsDataStatus === 'success' && user?.accessToken) {
            setAccountQueryStatus('success')
        }
    }, [accountDetailsDataStatus, user?.accessToken])

    return (
        <AccountContext.Provider
            value={{
                accountDetailsDataStatus: accountQueryStatus,
                accountDetailsData,
                refetch,
            }}
        >
            {props.children}
        </AccountContext.Provider>
    )
}
export default AccountContext
