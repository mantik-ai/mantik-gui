import { createContext, FC, useContext, useEffect, useState } from 'react'
import { NextRouter, useRouter } from 'next/router'
import { QueryStatus } from '@tanstack/react-query'
import {
    HTTPValidationError,
    Project,
    useProjectsProjectIdGetProjectsProjectIdGet as useGetProject,
} from '../queries'
import { ErrorType } from '../modules/auth/mantikApi'
import { DataStateIndicator } from '../components/DataStateIndicator'
import {
    parseUserRole,
    SemanticUserRole,
    toSemanticUserRole,
} from '../utils/userRole'
import AuthContext from '../context/AuthContext'

type Success = {
    project: Project
    success: true
    status: 'success'
}
type Initial = {
    status: 'idle'
    success: false
}

type Other = {
    project?: Project
    status: QueryStatus
    success: false
    error: ErrorType<HTTPValidationError>
}
type ContextState = Success | Initial | Other

export interface ProjectContextProps {
    state: ContextState
}

const getProjectId = (router: NextRouter): string => {
    if (router.query.id) {
        return router.query.id as string
    }
    const currentUrl = window.location.pathname
    const [_base, resource, _detail, resourceId] = currentUrl.split('/')
    if (resource === 'projects' && resourceId) {
        return resourceId
    }
    throw new Error(
        'Project ID not found, you should only be using this ContextProvider within a project page.'
    )
}

const ProjectContext = createContext<ProjectContextProps | undefined>(undefined)

interface MantikProviderProps {
    children: React.ReactNode
}
export const ProjectProvider: FC<MantikProviderProps> = ({ children }) => {
    const user = useContext(AuthContext)
    const [projectId, setProjectId] = useState<string | undefined>(undefined)
    const router = useRouter()
    useEffect(() => {
        const id = getProjectId(router)
        setProjectId(id)
    }, [router])

    const { data, status } = useGetProject(projectId as string, {
        query: {
            enabled: !!projectId && !!user?.accessToken,
        },
    })

    return status === 'success' ? (
        <ProjectContext.Provider
            value={{
                state: { status, success: true, project: data },
            }}
        >
            {children}
        </ProjectContext.Provider>
    ) : status === 'pending' ? (
        <DataStateIndicator text="Loading Project..." status={status}>
            <></>
        </DataStateIndicator>
    ) : null
}

// Useful on pages where we know project has definitely been fetched
// and want to get e.g. project.projectId, project.userRole
// (This may be a react-query anti-pattern)

// Project should always be defined if useProject is called within a ProjectProvider
// since {children} is wrapped in <ProjectContext.Provider value={value}>
// and only rendered if project query was successful
export const useProject = (): Project => {
    const context = useContext(ProjectContext)
    if (!context) {
        throw new Error('useProject must be used within a ProjectProvider.')
    }
    if (!context.state.success) {
        throw new Error('Error retrieving project data.')
    }
    return context.state.project
}

// Get semantic user role in context provider, means we can use it in any child component
// as far down the tree as we want without destructuring project object and passing props
// Use with hasPermission and define permission table for resource if it doesn't exist already
export const usePermission = (): SemanticUserRole => {
    const context = useContext(ProjectContext)
    if (!context) {
        throw new Error('usePermission must be used within a ProjectProvider.')
    }
    if (!context.state.success) {
        throw new Error('Error retrieving project data.')
    }
    const userRole = context.state.project.userRole
    const semanticUserRole = toSemanticUserRole(parseUserRole(userRole))
    return semanticUserRole
}
