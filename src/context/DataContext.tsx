import { createContext } from 'react'
import {
    Project,
    useProjectsProjectIdGetProjectsProjectIdGet,
} from '../queries'
import { useRouter } from 'next/router'
import { QueryStatus } from '@tanstack/react-query'

export interface DataParameters {
    projectDetailsDataStatus: QueryStatus
    projectDetailsErrorResponseStatus: number | undefined
    projectDetailsData: Project | undefined
}
const DataContext = createContext<Partial<DataParameters>>({})
interface DataProviderProps {
    children: React.ReactNode
}
export const DataProvider: React.FC<DataProviderProps> = (props) => {
    const router = useRouter()
    const { id } = router.query
    const {
        data: projectDetailsData,
        status: projectDetailsDataStatus,
        error,
    } = useProjectsProjectIdGetProjectsProjectIdGet(id as string)

    const projectDetailsErrorResponseStatus = error?.response?.status
    return (
        <DataContext.Provider
            value={{
                projectDetailsDataStatus,
                projectDetailsErrorResponseStatus,
                projectDetailsData,
            }}
        >
            {props.children}
        </DataContext.Provider>
    )
}
export default DataContext
