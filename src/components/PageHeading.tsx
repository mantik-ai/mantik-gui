import { Box, Divider, Typography, useTheme } from '@mui/material'
import { Spacing } from './Spacing'

interface PageHeadingProps {
    children: React.ReactNode
    description: string
    right?: React.ReactNode
}

export const PageHeading = (props: PageHeadingProps) => {
    const theme = useTheme()
    return (
        <Box
            sx={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'stretch',
            }}
        >
            <Box
                sx={{
                    display: 'flex',
                    alignItems: 'baseline',
                    justifyContent: 'space-between',
                    flexDirection: { xs: 'column', md: 'row' },
                    mb: 1,
                }}
            >
                <Box>
                    <Typography variant="h1">{props.children}</Typography>
                    {props.description && (
                        <Typography>{props.description}</Typography>
                    )}
                </Box>
                <Spacing horizontal value={theme.spacing(2)}></Spacing>
                {props.right}
            </Box>
            <Divider />
        </Box>
    )
}
