# Reusable / layout components

- ### Table Layout

    _The TablesLayout component renders passed data in a mui table component. Table data can either be passed directly or be configured in `TablesLayout > tableProps.json`_

    _A documentation of how to use: `TablesLayout > TablesLayout_DOC.md`_

- ### Button with loading status

    (MUI) Button passed as child with loading animation overlay: _`ButtonWithLoadingStatus.tsx`_

- ### Refresh button

    (MUI) Refresh icon button passed as child to ButtonWithLoadingStatus: _`RefreshButton.tsx`_

- ### Delete Dialog

    Dialog with action buttons rendered according to status prop passed: _`DeleteDialogLayout.tsx`_

- ### Details Dialog

    Basic and generic dialog with close button that renders any children: _`DetailsDialogLayout.tsx`_

- ### Details Dialog List

    Simple list component with alternating row background color: _`DetailsDialogList.tsx`_

- ### Form Layout

    Form wrapper component with status alerts: _`FormLayout.tsx`_

- ### Form Dialog Layout

    Dialog wrapper component for form component with dialog actions: _`FormDialogLayout.tsx`_

- ### Details Sidebar

    Displays a menu side bar with routes to subpages: _`DetailsSideBar.tsx`_
