import React from 'react'
import {
    Button,
    DialogTitle,
    Dialog,
    DialogContent,
    DialogActions,
    Alert,
    DialogContentText,
} from '@mui/material'
import { Cancel } from '@mui/icons-material'
import { Spacing } from './Spacing'
import { StatusProps } from '../types/statusProps'
import { ButtonWithLoadingStatus } from './ButtonWithLoadingStatus'

interface CancelDialogLayoutProps {
    open: boolean
    title: string
    warning?: string | React.ReactNode
    text?: string | React.ReactNode
    handleClose: any
    handleSubmit?: () => void
    data: any
    isLoading: boolean
    status: StatusProps
}

export const CancelDialogLayout = (props: CancelDialogLayoutProps) => {
    const { open, title, handleClose, handleSubmit, isLoading } = props
    function handleCloseDialog(): void {
        handleClose()
    }

    return (
        <Dialog fullWidth maxWidth="sm" open={open} onClose={handleCloseDialog}>
            {props.warning && <Alert severity="warning">{props.warning}</Alert>}
            <DialogTitle>{title}</DialogTitle>
            {!props.status.status && (
                <>
                    <DialogContent>
                        {props.text && (
                            <DialogContentText>{props.text}</DialogContentText>
                        )}
                        <Spacing value="2em" />
                    </DialogContent>

                    <DialogActions>
                        <Button onClick={handleCloseDialog}>Return</Button>
                        <ButtonWithLoadingStatus loading={isLoading}>
                            <Button
                                onClick={handleSubmit}
                                color="error"
                                startIcon={<Cancel />}
                            >
                                Cancel Run
                            </Button>
                        </ButtonWithLoadingStatus>
                    </DialogActions>
                </>
            )}
            {props.status.status && (
                <>
                    <DialogContent>
                        <Alert
                            variant="outlined"
                            severity={
                                props.status.type === 'error'
                                    ? 'error'
                                    : 'success'
                            }
                        >
                            {props.status.text}
                        </Alert>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={() => handleClose()}>Close</Button>
                    </DialogActions>
                </>
            )}
        </Dialog>
    )
}
