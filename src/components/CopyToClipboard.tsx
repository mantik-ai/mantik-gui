import React, { useState } from 'react'
import IconButton from '@mui/material/IconButton'
import { FileCopyOutlined } from '@mui/icons-material'
import { Tooltip } from '@mui/material'

interface CopyToClipboardProps {
    data: string
    title?: string
}

const CopyToClipboard: React.FC<CopyToClipboardProps> = ({ data, title }) => {
    const [copied, setCopied] = useState(false)

    const handleCopy = async () => {
        try {
            await navigator.clipboard.writeText(data)
            setCopied(true)
        } catch (err) {
            console.error('Failed to copy to clipboard', err)
        }
    }

    return (
        <>
            {data && (
                <>
                    {title && title}
                    <Tooltip title="Copy to Clipboard">
                        <IconButton
                            onClick={handleCopy}
                            color="primary"
                            aria-label="Copy to Clipboard"
                        >
                            <FileCopyOutlined fontSize="small" />
                        </IconButton>
                    </Tooltip>
                    {copied && <span>Copied!</span>}
                </>
            )}
        </>
    )
}

export default CopyToClipboard
