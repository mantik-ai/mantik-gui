import React from 'react'
import IconButton from '@mui/material/IconButton'
import Menu from '@mui/material/Menu'
import MenuItem from '@mui/material/MenuItem'
import MoreVertIcon from '@mui/icons-material/MoreVert'
import { MenuItemProps } from '../types/MenuItemProps'

interface MoreVerticalIconMenuProps {
    menuItems: MenuItemProps[]
    runIndex: number
}

export const MoreVerticalIconMenu: React.FC<MoreVerticalIconMenuProps> = ({
    menuItems,
    runIndex,
}) => {
    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null)
    const open = Boolean(anchorEl)

    const handleClick = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorEl(event.currentTarget)
    }

    const handleClose = () => {
        setAnchorEl(null)
    }

    return (
        <div>
            <IconButton
                aria-label="more"
                id="long-button"
                aria-controls={open ? 'long-menu' : undefined}
                aria-expanded={open ? 'true' : undefined}
                aria-haspopup="true"
                onClick={handleClick}
                size="small"
            >
                <MoreVertIcon fontSize="small" />
            </IconButton>
            <Menu
                id="long-menu"
                MenuListProps={{
                    'aria-labelledby': 'long-button',
                }}
                anchorEl={anchorEl}
                open={open}
                onClose={handleClose}
            >
                {menuItems.map((option, index) => (
                    <MenuItem
                        key={index}
                        selected={false}
                        onClick={() => {
                            handleClose()
                            option.action?.(runIndex, option.type)
                        }}
                        disabled={!option.action}
                    >
                        <IconButton size="small" color={'primary'}>
                            {option.icon}
                        </IconButton>
                        <span style={{ paddingBottom: '.1em' }}>
                            {option.tooltipText}
                        </span>
                    </MenuItem>
                ))}
            </Menu>
        </div>
    )
}
