import { Link, LinkProps } from '@mui/material'

// Because so many of our links are external, so auto set rel="noreferrer" and target="_blank"
export const ExternalLink = ({
    href,
    children,
    text,
}: LinkProps & { text?: string }) => (
    <Link href={href} target="_blank" rel="noreferrer">
        {text ?? children}
    </Link>
)
