import { Tooltip } from '@mui/material'
import React from 'react'

export function MLflowLink({
    title,
    href,
    linkName,
}: {
    title: string
    href: string
    linkName: string
}) {
    return (
        <Tooltip title={title}>
            <a rel="noreferrer" href={href} target="_blank">
                {linkName}
            </a>
        </Tooltip>
    )
}
