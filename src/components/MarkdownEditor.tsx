import React from 'react'

import { MdEditor } from 'md-editor-rt'
import 'md-editor-rt/lib/style.css'

interface MarkdownEditorProps {
    value: string
    setValue?: (value: string) => void
}

const MarkdownEditor: React.FC<MarkdownEditorProps> = (props) => {
    return (
        <div>
            <MdEditor
                modelValue={props.value}
                onChange={(e) => {
                    if (props.setValue) {
                        props.setValue(e)
                    }
                }}
                previewTheme="github"
                language="en-US"
                preview={false}
                noUploadImg={true}
                style={{ width: '100%', maxHeight: '350px' }}
                toolbars={[
                    'bold',
                    'underline',
                    'italic',
                    '-',
                    'strikeThrough',
                    'title',
                    'quote',
                    'unorderedList',
                    'orderedList',
                    '-',
                    'codeRow',
                    'code',
                    'link',
                    'table',
                ]}
                footers={['markdownTotal', '=']}
            />
        </div>
    )
}

export default MarkdownEditor
