import { Box, Paper, Toolbar, Typography, useTheme } from '@mui/material'

type DataType = {
    [key: string]: any
    name: string
}

export interface DetailsToolbarProps {
    title: string
    center?: React.ReactNode
    tool?: React.ReactNode
    data?: DataType | undefined
}
const DetailsToolbar = ({ title, center, tool, data }: DetailsToolbarProps) => {
    const theme = useTheme()

    return (
        <Paper sx={{ p: theme.spacing(1) }}>
            <Toolbar>
                <Typography
                    sx={{ flex: '1 1 100%' }}
                    variant="h6"
                    id="tableTitle"
                    component="div"
                >
                    {data && `${data.name} - `}
                    <strong>{title}</strong>
                </Typography>
                <Box component="div" flex="1 1 100%">
                    {center}
                </Box>
                {tool}
            </Toolbar>
        </Paper>
    )
}

export default DetailsToolbar
