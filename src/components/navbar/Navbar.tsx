import * as React from 'react'
import { useEffect, useState } from 'react'
import AppBar from '@mui/material/AppBar'
import Box from '@mui/material/Box'
import Divider from '@mui/material/Divider'
import Drawer from '@mui/material/Drawer'
import IconButton from '@mui/material/IconButton'
import List from '@mui/material/List'
import Toolbar from '@mui/material/Toolbar'
import Typography from '@mui/material/Typography'
import MenuIcon from '@mui/icons-material/Menu'
import Link from 'next/link'
import {
    ListItem,
    ListItemButton,
    MenuItem,
    Stack,
    useTheme,
} from '@mui/material'
import { useRouter } from 'next/router'
import { useSession } from 'next-auth/react'
import { NavbarProps } from '../../types/navbarProps'
import Notification from '../../modules/auth/Notification'
import AccountMenu from '../../modules/auth/AccountMenu'
import LoginRegister from '../../modules/auth/LoginRegister'
import ListItemIcon from '@mui/material/ListItemIcon'
import Settings from '@mui/icons-material/Settings'
import PeopleIcon from '@mui/icons-material/People'
import { Logo } from './Logo'
import { isTokenExpired } from '../../helpers'

const drawerWidth = 240
const paddingLogo = 'calc(6px + 0.25vw)'

export default function Navbar(props: NavbarProps) {
    const [mobileOpen, setMobileOpen] = React.useState(false)
    const session = useSession()
    const router = useRouter()
    const theme = useTheme()
    const [isTokenExpire, setIsTokenExpire] = useState(false)
    const handleDrawerToggle = () => {
        setMobileOpen(!mobileOpen)
    }
    const memorizedIsTokenExpired = React.useCallback(
        () => isTokenExpired(`${session?.data?.user.accessToken}`),
        [session]
    )

    useEffect(() => {
        const intervalId = setInterval(() => {
            if (memorizedIsTokenExpired()) {
                setIsTokenExpire(true)
            }
        }, 10000)
        return () => {
            clearInterval(intervalId)
        }
    }, [memorizedIsTokenExpired])

    if (isTokenExpire) {
        router.reload()
    }

    const drawer = (
        <Box onClick={handleDrawerToggle} sx={{ textAlign: 'center' }}>
            <Typography
                variant={'h2'}
                color="primary"
                style={{ cursor: 'pointer' }}
            >
                Mantik
            </Typography>

            <Divider />
            <List>
                {props.routes
                    .filter((route) => route.positions?.includes('drawer'))
                    .map((route) => (
                        <ListItem key={route.name} disablePadding>
                            <ListItemButton
                                component={Link}
                                href={route.path}
                                target={route.target}
                            >
                                <Typography variant="h5" color="primary.dark">
                                    {route.name}
                                </Typography>
                            </ListItemButton>
                        </ListItem>
                    ))}
                {session.data && (
                    <>
                        <Divider sx={{ my: 1 }} />
                        <MenuItem component={Link} href={'/settings/account'}>
                            <ListItemIcon>
                                <Settings fontSize="small" />
                            </ListItemIcon>
                            Settings
                        </MenuItem>
                        <MenuItem component={Link} href={'/collaborations'}>
                            <ListItemIcon>
                                <PeopleIcon fontSize="small" />
                            </ListItemIcon>
                            My Collaborations
                        </MenuItem>
                    </>
                )}
            </List>
        </Box>
    )

    const activeRoute = router.pathname

    return (
        <>
            <AppBar component="nav">
                <Toolbar
                    sx={{
                        alignItems: 'flex-end',
                        pl: { md: 1.5 },
                        minHeight: '0!important',
                    }}
                >
                    <IconButton
                        color="secondary"
                        aria-label="open drawer"
                        edge="start"
                        onClick={handleDrawerToggle}
                        sx={{
                            display: { md: 'none' },
                            alignSelf: 'center',
                        }}
                    >
                        <MenuIcon />
                    </IconButton>
                    <Link href={'/'}>
                        <Box
                            sx={{
                                width: 'calc( 180px + 1vw)',
                                height: 'calc( 36px + 0.25vw)',
                                mr: 3,
                                my: paddingLogo,
                            }}
                        >
                            <Logo />
                        </Box>
                    </Link>
                    <Box sx={{ display: { xs: 'none', md: 'block' } }}>
                        <List sx={{ display: 'flex' }} disablePadding>
                            {props.routes
                                .filter((route) =>
                                    route.positions?.includes('navbar')
                                )
                                .map((route) => (
                                    <ListItem disablePadding key={route.name}>
                                        <ListItemButton
                                            component={Link}
                                            href={route.path}
                                            target={route.target}
                                            sx={{
                                                textDecoration:
                                                    activeRoute.includes(
                                                        route.name.toLowerCase()
                                                    )
                                                        ? 'underline'
                                                        : 'none',
                                                color: theme.palette.secondary
                                                    .main,
                                            }}
                                        >
                                            <Typography
                                                component="span"
                                                variant="h5"
                                                color="secondary"
                                                sx={{
                                                    lineHeight: 1,
                                                }}
                                            >
                                                {route.name}
                                            </Typography>
                                        </ListItemButton>
                                    </ListItem>
                                ))}
                        </List>
                    </Box>
                    <Stack
                        direction={'row'}
                        justifyContent={'end'}
                        alignItems={'center'}
                        alignSelf={'center'}
                        flex={1}
                        spacing={2}
                    >
                        {session.data ? (
                            <>
                                <Notification />
                                <AccountMenu
                                    username={session.data.user?.name ?? ''}
                                />
                            </>
                        ) : session.status === 'loading' ? (
                            <></>
                        ) : (
                            <LoginRegister />
                        )}
                    </Stack>
                </Toolbar>
            </AppBar>
            <Box component="nav">
                <Drawer
                    variant="temporary"
                    open={mobileOpen}
                    onClose={handleDrawerToggle}
                    ModalProps={{
                        keepMounted: true, // Better open performance on mobile.
                    }}
                    sx={{
                        display: { xs: 'block', md: 'none' },
                        '& .MuiDrawer-paper': {
                            boxSizing: 'border-box',
                            width: drawerWidth,
                        },
                    }}
                >
                    {drawer}
                </Drawer>
            </Box>
        </>
    )
}
