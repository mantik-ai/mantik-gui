import { FormControl, FormGroup, TextField } from '@mui/material'
import React, { useState } from 'react'

import { FormDialogLayout } from './FormDialogLayout'
import {
    Run,
    useCreateNewTrainedModelEntryProjectsProjectIdModelsTrainedPost,
} from '../queries'
import { renderApiErrorMessage } from '../errors'

interface RegisterModelProps {
    open: boolean
    setOpen: React.Dispatch<React.SetStateAction<boolean>>
    projectId: string
    runData: Run
    refetch: () => void
}

export const RegisterModelDialog = (props: RegisterModelProps) => {
    const [modelName, setModelName] = useState('')
    const [status, setStatus] = useState({ status: false, type: '', text: '' })
    const { open, setOpen, projectId, runData, refetch } = props

    const reset = () => {
        setModelName('')
    }
    const { mutate, isPending: isLoading } =
        useCreateNewTrainedModelEntryProjectsProjectIdModelsTrainedPost({
            mutation: {
                onSuccess: () => {
                    setStatus({
                        status: true,
                        type: 'success',
                        text: `Modal has been registered successfully!`,
                    })
                    reset()
                    refetch()
                },
                onError: (err) => {
                    console.error('Error: ', err)
                    setStatus({
                        status: true,
                        type: 'error',
                        text: renderApiErrorMessage(err),
                    })
                    reset()
                },
            },
        })
    function handleRegisterSubmit() {
        const data = {
            name: modelName,
            runId: runData.runId,
        }
        mutate({
            projectId,
            data,
        })
    }

    function handleClose(): void {
        setOpen(false)
        setStatus({ status: false, type: '', text: '' })
    }
    return (
        <FormDialogLayout
            open={open}
            handleClose={handleClose}
            content=""
            handleSubmit={handleRegisterSubmit}
            handleSubmitText="Register"
            title="Register Model"
            status={status}
            formDataState={{
                modelName,
                error: false,
            }}
            loading={isLoading}
        >
            <FormGroup>
                <FormControl component="fieldset">
                    <TextField
                        key="name"
                        name="name"
                        label="Model Name"
                        size="small"
                        value={modelName}
                        onChange={(e) => {
                            setModelName(e.target.value)
                        }}
                        required
                    />
                </FormControl>
            </FormGroup>
        </FormDialogLayout>
    )
}
