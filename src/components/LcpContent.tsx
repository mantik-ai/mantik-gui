const minimalOpacity = 0.0000000000000000001
const minimalWidth = '100px'
const fakeContent = 'A'.repeat(100)

// Hack to force browser to detect this element to be the LCP
export function LcpContent() {
    return (
        <div
            style={{
                opacity: minimalOpacity,
                position: 'absolute',
                wordBreak: 'break-all',
                width: minimalWidth,
            }}
        >
            {fakeContent}
        </div>
    )
}
