import {
    BottomNavigation,
    Box,
    Button,
    ButtonProps,
    Paper,
} from '@mui/material'
import { styled } from '@mui/material/styles'
import { LinkProps } from 'next/link'
import * as CookieConsent from 'vanilla-cookieconsent'

const StyledLink = styled(Button)<LinkProps | ButtonProps>(({ theme }) => ({
    color: theme.palette.text.primary,
    fontWeight: 500,
    '&:hover': {
        color: theme.palette.primary.main,
        backgroundColor: 'transparent',
    },
    textTransform: 'none',
    marginLeft: '0.75rem',
}))

const NavigationFooterLinks = () => {
    return (
        <Box>
            <StyledLink href="/imprint">Imprint</StyledLink>|
            <StyledLink href="/privacy">Privacy</StyledLink>|
            <StyledLink href="/terms">Terms</StyledLink>|
            <StyledLink onClick={CookieConsent.showPreferences}>
                Show Cookie Preferences
            </StyledLink>
        </Box>
    )
}

export const Footer = () => {
    return (
        <Paper elevation={3}>
            <BottomNavigation sx={{ alignItems: 'center' }}>
                <NavigationFooterLinks />
            </BottomNavigation>
        </Paper>
    )
}
