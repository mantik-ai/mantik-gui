import {
    Accordion,
    AccordionDetails,
    AccordionSummary,
    Box,
    Divider,
    ListItemButton,
    ListItemIcon,
    ListItemText,
    Paper,
    useTheme,
} from '@mui/material'
import ExpandMoreIcon from '@mui/icons-material/ExpandMore'
import React from 'react'
import { Route } from '../types/route'
import SideBarItem from '../modules/project_details/overview/components/SideBarItem'

export interface DetailsSidebarProps {
    routes: Route[]
    additional?: React.ReactElement
}
export const DetailsSideBar = ({ routes, additional }: DetailsSidebarProps) => {
    const theme = useTheme()
    const [expanded, setExpanded] = React.useState<string | false>(false)
    const handleChange =
        (panel: string) =>
        (event: React.SyntheticEvent, isExpanded: boolean) => {
            setExpanded(isExpanded ? panel : false)
        }

    return (
        <Box
            sx={{
                flex: 1,
                p: theme.spacing(2),
                minWidth: 300,
            }}
        >
            <Paper sx={{ height: '100%' }}>
                {routes.map((route, idx) => (
                    <Accordion
                        key={idx}
                        expanded={expanded === `panel${idx}`}
                        onChange={handleChange(`panel${idx}`)}
                        sx={{
                            boxShadow: 'none',
                        }}
                    >
                        <AccordionSummary
                            aria-controls={`panel${idx + 1}d-content`}
                            id={`panel${idx + 1}d-header`}
                            sx={{
                                padding: 0,
                                '& .MuiAccordionSummary-expandIconWrapper': {
                                    position: 'absolute',
                                    right: '1em',
                                },
                            }}
                            expandIcon={
                                !!route.subRoutes ? (
                                    <ExpandMoreIcon sx={{ color: 'black' }} />
                                ) : (
                                    <></>
                                )
                            }
                        >
                            {route.path ? (
                                <SideBarItem
                                    key={route.path}
                                    name={route.name}
                                    path={route.path ?? ''}
                                    icon={route.icon}
                                />
                            ) : (
                                <ListItemButton selected={false}>
                                    <ListItemIcon>{route.icon}</ListItemIcon>
                                    <ListItemText>{route.name}</ListItemText>
                                </ListItemButton>
                            )}
                        </AccordionSummary>
                        {!!route.subRoutes && (
                            <AccordionDetails>
                                {route.subRoutes.map((subRoute) => (
                                    <SideBarItem
                                        key={subRoute.path}
                                        name={subRoute.name}
                                        path={subRoute.path}
                                        icon={subRoute.icon}
                                        subItem
                                    />
                                ))}
                            </AccordionDetails>
                        )}
                    </Accordion>
                ))}
                <Divider />
                {additional && additional}
            </Paper>
        </Box>
    )
}
