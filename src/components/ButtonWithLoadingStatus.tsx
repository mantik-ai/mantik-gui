// Should be replaced by MUI LoadingButton

import { Box, CircularProgress } from '@mui/material'
import { green } from '@mui/material/colors'

interface ButtonWithLoadingStatusProps {
    loading: boolean
    margin?: number
    children: React.ReactNode
}
/**
 * @deprecated
 */
export const ButtonWithLoadingStatus = ({
    loading,
    children,
    margin = 1,
}: ButtonWithLoadingStatusProps) => {
    return (
        <Box sx={{ m: margin, position: 'relative', display: 'inline-block' }}>
            <div
                style={{
                    opacity: loading ? 0.5 : 1,
                    pointerEvents: loading ? 'none' : 'auto',
                }}
            >
                {children}
            </div>
            {loading && (
                <CircularProgress
                    size={24}
                    sx={{
                        color: green[500],
                        position: 'absolute',
                        top: '50%',
                        left: '50%',
                        marginTop: '-12px',
                        marginLeft: '-12px',
                    }}
                />
            )}
        </Box>
    )
}
