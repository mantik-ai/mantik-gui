import React from 'react'
import { Box, Chip, useTheme } from '@mui/material'
import { Label } from '../queries'

interface LabelArrayProps {
    labels?: Label[]
}
export const LabelArray = (props: LabelArrayProps) => {
    const theme = useTheme()
    if (!props.labels) return null

    return (
        <Box
            sx={{
                display: 'flex',
                flexDirection: 'row',
                flexWrap: 'wrap',
                p: theme.spacing(0.5),
            }}
            component="ul"
        >
            {props.labels.map((label, id) => {
                return (
                    <Box
                        p={theme.spacing(0.5)}
                        // key={`${label.scope}-${label.name}`} //to-do: fix if this is getting used, commanted as orval config changed
                        key={id}
                    >
                        <Chip label={label.name} />
                    </Box>
                )
            })}
        </Box>
    )
}
