import React from 'react'
import { Box, List, ListItem, ListItemText, useTheme } from '@mui/material'
import { CustomCell } from './TablesLayout/CustomCell'

interface CustomCellProps {
    data: string
    component?: React.ReactElement
}

interface DetailsDialogListProps {
    data: { [key: string]: CustomCellProps }
}
export const DetailsDialogList = ({
    data: dataObject,
}: DetailsDialogListProps) => {
    const theme = useTheme()

    return (
        <List>
            {Object.entries(dataObject).map(([label, cellProps], idx) => {
                if (cellProps.data == null && cellProps.component == null) {
                    return null
                }

                return (
                    <ListItem
                        key={idx}
                        sx={{
                            backgroundColor:
                                idx % 2 === 0
                                    ? theme.palette.grey[200]
                                    : 'transparent',
                            padding: '0.5em 0.75em',
                        }}
                    >
                        <ListItemText
                            primary={<Box sx={{ mb: 0.5 }}>{label}</Box>}
                            secondary={
                                cellProps.component || (
                                    <CustomCell data={cellProps.data} />
                                )
                            }
                        />
                    </ListItem>
                )
            })}
        </List>
    )
}
