import { Box, Typography, useTheme } from '@mui/material'

interface PageContent {
    children: React.ReactNode
    title: string
}
export function PageContent({ children, title }: PageContent) {
    const theme = useTheme()

    return (
        <Box maxWidth="lg">
            <Box pb={theme.spacing(3)}>
                <Typography variant="h2">{title}</Typography>
            </Box>
            {children}
        </Box>
    )
}
