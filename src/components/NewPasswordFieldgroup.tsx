import Visibility from '@mui/icons-material/Visibility'
import VisibilityOff from '@mui/icons-material/VisibilityOff'
import { IconButton, InputAdornment, TextField } from '@mui/material'
import { RequirementsChecklist } from './forms/RequirementsChecklist'

interface NewPasswordFieldgroupProps {
    password: string
    confirmPassword: string
    confirmPasswordErrorMessage: string
    showPassword: boolean
    showConfirmPassword: boolean
    handleShowPassword: (field: string) => void
    handleChange: (e: React.ChangeEvent<HTMLInputElement>) => void
    handleKeyDown: (e: React.KeyboardEvent<HTMLInputElement>) => void
    passwordRequirements: { requirement: string; error: boolean }[]
    passwordError: boolean
}
export const NewPasswordFieldgroup = ({
    password,
    confirmPassword,
    confirmPasswordErrorMessage,
    showPassword,
    showConfirmPassword,
    handleShowPassword,
    handleChange,
    handleKeyDown,
    passwordRequirements,
    passwordError,
}: NewPasswordFieldgroupProps) => {
    return (
        <>
            <TextField
                key="password"
                className="password"
                name="password"
                label="password"
                size="small"
                type={showPassword ? 'text' : 'password'}
                error={passwordError}
                helperText={
                    <RequirementsChecklist
                        requirements={passwordRequirements}
                    />
                }
                value={password}
                onChange={handleChange}
                onKeyDown={handleKeyDown}
                InputProps={{
                    endAdornment: (
                        <InputAdornment position="end">
                            <IconButton
                                aria-label="toggle password visibility"
                                onClick={() => handleShowPassword('password')}
                            >
                                {showPassword ? (
                                    <Visibility />
                                ) : (
                                    <VisibilityOff />
                                )}
                            </IconButton>
                        </InputAdornment>
                    ),
                }}
                required
            />
            <TextField
                key="password-confirm"
                name="confirmPassword"
                label="confirm password"
                size="small"
                type={showConfirmPassword ? 'text' : 'password'}
                error={!!confirmPasswordErrorMessage}
                helperText={confirmPasswordErrorMessage}
                value={confirmPassword}
                onChange={handleChange}
                onKeyDown={handleKeyDown}
                InputProps={{
                    endAdornment: (
                        <InputAdornment position="end">
                            <IconButton
                                aria-label="toggle password visibility"
                                onClick={() =>
                                    handleShowPassword('confirmPassword')
                                }
                            >
                                {showConfirmPassword ? (
                                    <Visibility />
                                ) : (
                                    <VisibilityOff />
                                )}
                            </IconButton>
                        </InputAdornment>
                    ),
                }}
                required
            />
        </>
    )
}
