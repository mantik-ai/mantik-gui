import { GitHub } from '@mui/icons-material'
import { Platform } from '../queries'
import BitBucket from '../themes/icon/BitBucket'
import GitLab from '../themes/icon/GitLab'

interface PlatformIconProps {
    platform: Platform
    iconProps?: { [key: string]: any }
}
export const PlatformIcon = ({ platform, iconProps }: PlatformIconProps) => {
    const platformIcon = {
        [Platform.GitHub]: <GitHub {...iconProps} />,
        [Platform.GitLab]: <GitLab {...iconProps} />,
        [Platform.Bitbucket]: <BitBucket {...iconProps} />,
    }
    return platformIcon[platform]
}
