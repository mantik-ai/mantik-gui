import {
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    IconButton,
    useTheme,
} from '@mui/material'
import { ReactNode, useState } from 'react'
import ToggleWidth from '../themes/icon/ToggleWidth'

export interface DetailsDialogLayoutProps {
    open: boolean
    setOpen: (idx?: number) => void
    title: string
    children: ReactNode
    toggleWidth?: boolean
}

export const DetailsDialogLayout = ({
    open,
    setOpen,
    title,
    children,
    toggleWidth = false,
}: DetailsDialogLayoutProps) => {
    const theme = useTheme()
    const [maxWidth, setMaxWidth] = useState<false | 'sm'>('sm')

    const handleCloseDialog = () => {
        setOpen()
        setMaxWidth('sm')
    }

    return (
        <Dialog
            sx={{
                '& > .MuiDialog-container > div': {
                    width: '100%',
                    transition: 'max-width 0.5s ease-in-out',
                },
            }}
            maxWidth={maxWidth}
            open={open}
            onClose={handleCloseDialog}
        >
            <DialogTitle>{title}</DialogTitle>
            {toggleWidth && (
                <IconButton
                    sx={{
                        display: 'inline',
                        width: 'fit-content',
                        position: 'absolute',
                        right: '1em',
                        top: '1em',
                        zIndex: 1,
                        color: theme.palette.primary.main,
                    }}
                    onClick={() =>
                        setMaxWidth((prev) => (prev === false ? 'sm' : false))
                    }
                    size="small"
                >
                    <ToggleWidth fullWidth={!maxWidth} />
                </IconButton>
            )}
            <DialogContent>{children}</DialogContent>
            <DialogActions>
                <Button onClick={handleCloseDialog}>Close</Button>
            </DialogActions>
        </Dialog>
    )
}
