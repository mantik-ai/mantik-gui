import { Typography, useTheme } from '@mui/material'
import { formatTimestamp } from '../helpers'
import { FC } from 'react'

interface TimestampCaptionProps {
    timestamp: string
    displayName: string
}
const TimestampCaption: FC<TimestampCaptionProps> = ({
    timestamp,
    displayName,
}) => {
    const theme = useTheme()
    return (
        <Typography
            data-testid="timestamp-caption"
            variant="caption"
            style={{
                marginRight: '1em',
                marginTop: 'auto',
                paddingBottom: '0.5em',
                textAlign: 'right',
                color: theme.palette.grey[700],
            }}
        >
            {displayName}: {formatTimestamp(timestamp)}
        </Typography>
    )
}

export { TimestampCaption }
