import React from 'react'
import { Button, Alert } from '@mui/material'
import { StatusProps } from '../types/statusProps'

interface FormLayoutProps {
    handleClose: any
    status: StatusProps
    children: React.ReactNode
    buttonVariant?: 'text' | 'outlined' | 'contained' | undefined
}
export const FormLayout = (props: FormLayoutProps) => {
    const { handleClose, status, children, buttonVariant } = props

    return (
        <>
            {status.status && (
                <>
                    <Alert
                        sx={{ mb: 2 }}
                        variant="outlined"
                        severity={status.type === 'error' ? 'error' : 'success'}
                    >
                        {status.text}
                    </Alert>

                    {status.type === 'success' && (
                        <Button
                            variant={buttonVariant}
                            sx={{
                                alignSelf: 'flex-end',
                            }}
                            onClick={handleClose}
                        >
                            Close
                        </Button>
                    )}
                </>
            )}
            {status.type !== 'success' && children}
        </>
    )
}
