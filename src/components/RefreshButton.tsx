import { IconButton } from '@mui/material'
import { ButtonWithLoadingStatus } from './ButtonWithLoadingStatus'
import { Refresh } from '@mui/icons-material'

interface RefreshButtonProps {
    loading: boolean
    onClick: () => void
}
export const RefreshButton = ({ loading, onClick }: RefreshButtonProps) => {
    return (
        <div
            style={{
                display: 'flex',
                justifyContent: 'flex-end',
                top: 0,
                position: 'sticky',
                zIndex: 1,
                padding: '0.25em 0.5em',
            }}
        >
            <div
                style={{
                    borderRadius: '50%',
                    backgroundColor: 'rgba(255,255,255,0.8)',
                }}
            >
                <ButtonWithLoadingStatus margin={0} loading={loading}>
                    <IconButton
                        disabled={loading}
                        color="primary"
                        onClick={onClick}
                    >
                        <Refresh /> {loading}
                    </IconButton>
                </ButtonWithLoadingStatus>
            </div>
        </div>
    )
}
