import React, { useRef } from 'react'
import {
    Button,
    DialogTitle,
    Dialog,
    DialogContent,
    DialogContentText,
    DialogActions,
    FormGroup,
    FormHelperText,
} from '@mui/material'
import { StatusProps } from '../types/statusProps'
import { Spacing } from './Spacing'
import { useRequiredFieldsCheck } from '../hooks/useRequiredFieldsCheck'
import { FormLayout } from './FormLayout'
import { ButtonWithLoadingStatus } from './ButtonWithLoadingStatus'

interface FormDialogLayoutProps {
    children: React.ReactNode
    open: boolean
    handleClose: any
    handleSubmit: any
    title: string
    content: string
    formDataState: {
        [key: string]: any
        error: boolean
    }
    status: StatusProps
    isEdit?: boolean
    loading?: boolean
    handleSubmitText?: string
}
export const FormDialogLayout = (props: FormDialogLayoutProps) => {
    const {
        children,
        open,
        handleClose,
        handleSubmit,
        title,
        content,
        formDataState,
        status,
        isEdit,
        loading = false,
        handleSubmitText = 'Create',
    } = props

    const form: React.RefObject<HTMLElement | null> = useRef(null)
    const allRequiredFieldValuesGiven = useRequiredFieldsCheck(form?.current, {
        formDataState,
    })

    function handleCloseDialog(): void {
        handleClose()
    }

    return (
        <Dialog fullWidth maxWidth="sm" open={open} onClose={handleCloseDialog}>
            <DialogTitle>{title}</DialogTitle>
            <DialogContent>
                <DialogContentText>{content}</DialogContentText>
                <Spacing value="2em" />
                <FormGroup ref={form}>
                    <FormLayout handleClose={handleCloseDialog} status={status}>
                        {children}
                    </FormLayout>
                    {!allRequiredFieldValuesGiven && (
                        <>
                            <Spacing value={'1em'} />
                            <FormHelperText error sx={{ pl: 2 }}>
                                {`Please ensure that all fields with * are filled out.`}
                            </FormHelperText>
                        </>
                    )}
                </FormGroup>
            </DialogContent>

            {status.type !== 'success' && (
                <DialogActions>
                    <div style={{ display: 'flex' }}>
                        <Button onClick={handleCloseDialog}>Cancel</Button>
                        <ButtonWithLoadingStatus loading={loading}>
                            <Button
                                onClick={handleSubmit}
                                disabled={
                                    !allRequiredFieldValuesGiven ||
                                    formDataState.error ||
                                    status.type === 'error' ||
                                    loading
                                }
                            >
                                {isEdit ? 'Update' : handleSubmitText}
                            </Button>
                        </ButtonWithLoadingStatus>
                    </div>
                </DialogActions>
            )}
        </Dialog>
    )
}
