# TablesLayout.tsx

The TablesLayout component renders passed data in a mui table component. The data structure needs to be an array of objects where each object represents a table row. A custom component can be rendered as table cell, but is optional. If not passed, the data will be rendered in a default mui table cell.

```typescript
<TablesLayout
    data={tableData}
    action={{
        edit: () => {},
        delete: () => {},
    }}
/>
```

## Required structure for tableData prop:

```typescript
[
    // Table row
    {
        [column name]: {
            data: 'string', // cell data
            type?: 'string', // e.g. date | link
            link?: 'string' // url
            component?: 'ReactElement' // optional: Custom component for table cell
        }
    },
]
```

The received data can either

- be refactored inside the parent component according to the required data structure
- be passed into the TablesLayout component as is – then basic refactoring from extractTableData function will take place
- or be configured inside `tableProps.json` file

## Configuring table data in JSON

When working with the JSON configuration, the data is fetched in the parent component and passed to the hook `useGenerateTableData.tsx`.

The data is passed as 1st argument and the name of the prop containing the data to be rendered as 2nd argument:

```typescript
const { data } = useProjectsProjectIdRunsGetProjectsProjectIdRunsGet(id)

const tableData = useGenerateTableData(data, 'runs')
```

Inside the JSON file you have a prop with the same name as passed to the hook as 2nd argument with a map prop.

The title is converted from camelCasing to an uppercased title unless an alternative title is passed as title prop:

> runId --> Run Id

or

```json
"runs": {
    "map": {
        "runId": {
            title: 'Id'
        }
    }
}
```

> runId --> Id

### Example for rendering a table:

#### rendered table:

| Name               | Model                                       |
| ------------------ | ------------------------------------------- |
| Simple Example Run | [Simple Mantik Project](https://www.url.de) |

#### Data structure:

```json
{
    "runs": [
        {
            "name": "Simple Example Run",
            "modelRepository": {
                "modelRepositoryId": "951d8805",
                "codeRepository": {
                    "codeRepositoryId": "5bd3e2f7",
                    "codeRepositoryName": "Simple Mantik Project",
                    "uri": "https://www.url.de"
                }
            }
        }
    ]
}
```

#### JSON config:

```json
"runs": {
    "map": {
        "name": "name",
        "modelRepository": {
            "title": "Model",
            "data": "codeRepository.codeRepositoryName",
            "link": "codeRepository.uri",
            "type": "link"
        },
    }
}
```

In the link and data prop you pass the path to the value:

#### Data

```json
modelRepository: {
    codeRepository: {
        uri: 'https://www.url.de'
    }
}
```

##### JSON

```json
"link": "codeRepository.uri"
```

#### Hiding table columns

Sometimes we want to show more detailed information for a single row. Then the data shown in the details view will be configured as described in the map property of the JSON file. Additionally, a `hiddenTableColumns` prop can be added below the map prop. It containes a list of properties from the map that should not be shown as table columns:

```json
"runSchedules": {
    "map": {
        "name": "name",
        "run": "run.name",
        "computeBudgetAccount": "computeBudgetAccount"
    },
    "hiddenTableColumns": ["computeBudgetAccount"]
}
```

This would render a table with two columns and a details view for a single table row with three rows.

## Actions

Actions buttons are rendered in the layout component. The details button is default. If an action prop object with edit and / or delete properties is passed to the TableLayout component, the buttons are rendered accordingly.

```typescript
export interface TableProps {
    data: TablesLayoutRowData[]
    action?: {
        delete?: (idx: number) => void
        edit?: (idx: number) => void
    }
}
```

```typescript
<TablesLayout
  data={tableData}
  action={{
      edit: () => {}, edit functionality
      delete: () => {} delete functionality
  }}
/>
```
