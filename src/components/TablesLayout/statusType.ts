export enum StatusType {
    SCHEDULED = 'SCHEDULED',
    RUNNING = 'RUNNING',
    FINISHED = 'FINISHED',
    FAILED = 'FAILED',
    KILLED = 'KILLED',
    UNKNOWN = 'UNKNOWN',
}
