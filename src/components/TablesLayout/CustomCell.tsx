import React from 'react'
import { Box, Chip, Link } from '@mui/material'
import { formatTimestamp } from '../../helpers'
import CopyToClipboard from '../CopyToClipboard'
import { RenderElement } from '../../utils/renderResourceActions'
import { Label } from '../../queries'

type UnknownData = {
    data: unknown
    type?: undefined
}

type StringData = {
    data: string
    type?: undefined
}

type IdData = {
    data: string
    type: 'id'
}

export type LabelData = {
    data: Label[]
    type: 'labels'
}

type LinkData = {
    data: string
    link: string
    type: 'link'
}

type DateData = {
    data: string
    type: 'date'
}

export type CustomCellData =
    | IdData
    | LabelData
    | LinkData
    | DateData
    | StringData
    | UnknownData
    | string

export interface CustomCellProps {
    data: CustomCellData | null
    component?: React.ReactElement
    detail?: boolean
    userActions?: RenderElement[]
    link?: string
}

export const CustomCell = ({ data: cellData }: CustomCellProps) => {
    if (typeof cellData === 'string') return <>{cellData}</>
    if (cellData?.type === 'link') {
        return <Link href={cellData.link}>{cellData.data}</Link>
    } else if (cellData?.type === 'date') {
        return <>{formatTimestamp(cellData.data)}</>
    } else if (cellData?.type === 'labels') {
        return (
            <Box component="span">
                {cellData.data.map((label, idx) => (
                    <Chip
                        component="span"
                        key={idx}
                        label={label.name}
                        style={{ margin: '0.13em' }}
                    />
                ))}
            </Box>
        )
    } else if (cellData?.type === 'id') {
        return (
            <CopyToClipboard
                title={cellData.data}
                data={cellData.data}
            ></CopyToClipboard>
        )
    } else if (typeof cellData?.data === 'string') {
        return <>{cellData.data}</>
    }

    return null
}
