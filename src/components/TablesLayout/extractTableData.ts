import { formatUnixTimestamp } from '../../helpers'
import { StatusType } from './statusType'
import tableDataFilter from './tableProps.json'

//import { testRuns } from './testData'

enum ValueTypes {
    Link = 'link',
    Date = 'date',
    Status = 'status',
    Labels = 'labels',
}

interface DataObjectProps {
    [key: string]: any
}

interface ValueProps {
    data: string | number
    title?: string
    link?: string
    type?: ValueTypes
}

type DataProps = DataObjectProps[]

// returns an array of keys with the first letter capitalized
export function refactorDataKeys(obj: DataObjectProps) {
    const refactoredObject: { [key: string]: any } = {}
    for (const key in obj) {
        if (Object.keys(obj[key]).includes('title')) {
            refactoredObject[obj[key].title] = obj[key]
            delete obj[key].title
            continue
        }
        let refactoredKey = key[0].toUpperCase() + key.slice(1)
        const firstLetters: string[] = refactoredKey.match(/[A-Z]/g) || []
        const refactoredKeyArray = refactoredKey.split(/[A-Z]/)
        refactoredKeyArray.shift()
        refactoredKey = refactoredKeyArray
            .map((word, idx) => firstLetters[idx] + word)
            .join(' ')

        refactoredObject[refactoredKey] = obj[key]
    }
    return refactoredObject
}

// returns an array of values as objects
// { data: 'string', link?: 'string', type?: 'string' }
export function refactorDataValues(obj: DataObjectProps) {
    const values = Object.values(obj)
    const refactoredValues = values.map((value) => {
        if (typeof value === 'string' || typeof value === 'number') {
            return { data: value }
        } else if (!!value && typeof value === 'object' && value?.type) {
            // type date
            if (value.type === 'date') {
                return { data: formatUnixTimestamp(value.data), type: 'date' }
            }
            return { ...value }
        } else if (!!value && typeof value === 'object') {
            const nameVal = Object.entries(value).find(([key]) => {
                const lastFour = key.slice(-4)
                return lastFour === 'name' || lastFour === 'Name'
            })
            const uriVal = Object.entries(value).find(([key]) => {
                const lastThree = key.slice(-3)
                return lastThree === 'uri' || lastThree === 'Uri'
            })

            const obj: ValueProps = {
                data: nameVal ? `${nameVal[1]}` : '',
            }
            if (!!uriVal) {
                obj['link'] = `${uriVal[1]}`
            }

            return obj
        }
    })
    return refactoredValues
}

// returns an array of objects with values mapped by JSON file
export function mapTableColumns(
    data: DataProps,
    table: string,
    json: { [key: string]: any } = tableDataFilter
) {
    const tableColsMap: { [key: string]: any } = json
    const columnsMap: { [key: string]: any } = tableColsMap[table]?.map

    const findValue = (key: string, val: any) => {
        if (!val) {
            if (key === 'status')
                return { type: 'status', data: StatusType.UNKNOWN }
            return { data: '' }
        }
        const getValueFromPath = (path: string) => {
            const pathArray = path.split('.') || []
            let value = val
            for (let i = 0; i < pathArray.length; i++) {
                if (!!value[pathArray[i]]) {
                    value = value[pathArray[i]]
                }
            }
            return `${value}`
        }
        if (typeof columnsMap[key] === 'string')
            return { data: getValueFromPath(columnsMap[key]) }

        // for config object in json
        const returnValue: { [key: string]: string } = {}

        for (const mapKey in columnsMap[key]) {
            if (mapKey === 'type' || mapKey === 'title') {
                returnValue[mapKey] = columnsMap[key][mapKey]
            } else {
                returnValue[mapKey] = getValueFromPath(columnsMap[key][mapKey])
            }
        }
        return returnValue
    }

    data = data.map((obj) => {
        const filteredObj: DataObjectProps = {}
        Object.keys(columnsMap).forEach((key) => {
            if (key === 'labels')
                return (filteredObj[key] = {
                    data: obj[key],
                    type: ValueTypes.Labels,
                })
            filteredObj[key] = findValue(key, obj[key])
        })

        return filteredObj
    })

    return data
}

export function extractTableData(obj: DataObjectProps) {
    if (!obj && typeof obj !== 'object') return null
    const values = refactorDataValues(obj)
    const refactoredObj: { [key: string]: ValueProps } = {}
    Object.keys(obj).forEach((key: string, idx: number) => {
        refactoredObj[key] = {
            data: `${values[idx]?.data}`,
        }
        if (values[idx] && !!values[idx]?.link) {
            refactoredObj[key].link = `${values[idx]?.link}`
        }
        if (values[idx] && values[idx]?.type in ValueTypes) {
            refactoredObj[key].type = values[idx].type
        }
        if (key === 'Status') {
            refactoredObj[key] = {
                data: `${values[idx]}`,
                type: ValueTypes.Status,
            }
        }
    })
    return refactoredObj
}

// returns an array of objects with object props with data prop
export function addHiddenTableColumnProp(
    data: DataProps,
    table: string,
    json: { [key: string]: any } = tableDataFilter
) {
    const tableColsMap: { [key: string]: any } = json
    const hiddenColsList: { [key: string]: any } =
        tableColsMap[table]?.hiddenTableColumns

    data = data.map((obj) => {
        const filteredObj: DataObjectProps = {}
        Object.entries(obj).forEach(([key, val]) => {
            if (hiddenColsList.includes(key)) {
                filteredObj[key] = {
                    ...val,
                    hiddenTableColumn: true,
                }
            } else {
                filteredObj[key] = val
            }
        })
        return filteredObj
    })

    return data
}

export function refactorDataForTableView(
    data: DataProps,
    table?: string,
    json: { [key: string]: any } = tableDataFilter
) {
    //data = testRuns
    if (!data) return { data: [] }
    if (!!table && json[table] && json[table].map) {
        data = mapTableColumns(data, table)
    } else {
        data = data.map(extractTableData) as DataProps
    }
    if (!!table && json[table] && json[table].hiddenTableColumns) {
        data = addHiddenTableColumnProp(data, table)
    }
    data = data.map(refactorDataKeys)
    return data.map((item) => ({ data: item }))
}
