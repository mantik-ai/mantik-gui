import {
    DeleteOutlined,
    EditOutlined,
    VisibilityOutlined,
} from '@mui/icons-material'
import {
    Box,
    IconButton,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TablePagination,
    TableRow,
    Tooltip,
    Typography,
} from '@mui/material'
import Link from 'next/link'
import { useRouter } from 'next/router'
import React, { JSX, useEffect, useState } from 'react'
import { PAGE_LENGTH_OPTIONS } from '../../constants'
import { MenuItemProps } from '../../types/MenuItemProps'
import { RenderElement } from '../../utils/renderResourceActions'
import { parseUserRole, toSemanticUserRole } from '../../utils/userRole'
import { DetailsDialogLayout } from '../DetailsDialogLayout'
import { DetailsDialogList } from '../DetailsDialogList'
import { MoreVerticalIconMenu } from '../MoreVerticalIconMenu'
import { CustomCell } from './CustomCell'
// NOTE Refactoring for "CustomCellProps" ended here
// use "CustomCellProps" from "./CustomCell" in the future
interface CustomCellProps {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    data: any
    component?: React.ReactElement
    detail?: boolean
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    handleRefreshClick?: any
    userActions?: RenderElement[]
    link?: string
}

export type TablesLayoutRowData = { [key: string]: CustomCellProps }
export type TablesLayoutRow<Extra = undefined> = Extra extends undefined
    ? {
          data: TablesLayoutRowData
      }
    : {
          data: TablesLayoutRowData
          extra: Extra
      }

export interface ActionProps {
    delete?: (idx: number, type: RenderElement) => void
    edit?: (idx: number, type: RenderElement) => void
}

export interface TableProps<ExtraRowData> {
    data: Array<TablesLayoutRow<ExtraRowData>>
    action?: ActionProps
    menuItemsForRow?: (row: TablesLayoutRow<ExtraRowData>) => MenuItemProps[]
    page?: {
        rowsPerPage: number
        page: number
        totalRecords: number
        handleRowsChange: (type: 'rowsPerPage' | 'page', num: number) => void
    }
    tableFor?: string
    detailsComponent?: (
        currentRow: number | null,
        detailsDialogOpen: boolean,
        handleDetailsDialog: (idx?: number | undefined) => void
    ) => React.ReactNode
}

export enum TableColumn {
    MLFLOW_EXPERIMENT_ID = 'MLflow Experiment ID',
    NAME = 'Name',
    ID = 'ID',
    EXPERIMENT_REPOSITORY = 'Experiment Repository',
    ROLE = 'Role',
    STATUS = 'Status',
    SAVEDMODEl = 'SavedModel',
}
enum Key {
    STATUS = 'Status',
    NAME = 'Name',
    ROLE = 'Role',
}

export const TablesLayout = <ExtraRowData = undefined,>({
    data,
    action,
    page,
    tableFor,
    detailsComponent,
    menuItemsForRow,
}: TableProps<ExtraRowData>) => {
    const [detailsDialogOpen, setDetailsDialogOpen] = useState<boolean>(false)
    const [currentRow, setCurrentRow] = useState<number | null>(null)
    const [projectId, setProjectId] = useState<string | null>(null)
    const router = useRouter()
    useEffect(() => {
        if (router) {
            const { id } = router.query
            const project = String(id)
            setProjectId(project)
        }
    }, [router])

    const actions: {
        type: RenderElement
        action?: (idx: number, type: RenderElement) => void
        icon: JSX.Element
        tooltipText: string
        color?: 'primary' | 'error' | 'info'
    }[] = [
        {
            type: RenderElement.SHOW_DETAILS,
            action: handleDetailsDialog,
            icon: <VisibilityOutlined fontSize="small" />,
            tooltipText: 'Details',
        },
        {
            type: RenderElement.EDIT,
            action: action?.edit,
            icon: <EditOutlined fontSize="small" />,
            tooltipText: 'Edit',
        },
        {
            type: RenderElement.DELETE,
            action: action?.delete,
            icon: <DeleteOutlined fontSize="small" />,
            tooltipText: 'Delete',
            color: 'error',
        },
    ]

    function handleDetailsDialog(idx?: number) {
        if (idx === undefined) {
            setDetailsDialogOpen(false)
            setCurrentRow(null)
        } else {
            setDetailsDialogOpen(true)
            setCurrentRow(null)
            setCurrentRow(idx)
        }
    }

    function getCustomCell(
        idx: number,
        i: number,
        key: string,
        val: CustomCellProps
    ) {
        if (tableFor === 'members' && key === Key.ROLE) {
            return (
                <CustomCell
                    key={`${idx}-${i}`}
                    data={{
                        data: toSemanticUserRole(
                            parseUserRole(val.data.toString())
                        ),
                    }}
                />
            )
        }
        return <CustomCell key={`${idx}-${i}`} data={val} />
    }
    return (
        <>
            {data.length ? (
                <TableContainer component={Paper}>
                    <Table>
                        <TableHead>
                            <TableRow>
                                {Object.entries(data[0].data).map(
                                    ([column, val], idx) =>
                                        !Object.keys(val).includes(
                                            'hiddenTableColumn'
                                        ) && (
                                            <TableCell key={idx}>
                                                {column}
                                            </TableCell>
                                        )
                                )}
                                <TableCell>Action</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {data.map((row, idx) => {
                                const menuItems = menuItemsForRow?.(row) ?? []

                                const isExperimentsTable =
                                    tableFor === 'experiments'
                                const { data: id = null } =
                                    row.data[
                                        TableColumn.MLFLOW_EXPERIMENT_ID
                                    ] || {}
                                const { data: linkName = null } =
                                    isExperimentsTable
                                        ? row.data[TableColumn.NAME]
                                        : {}
                                return (
                                    <TableRow key={idx}>
                                        {Object.entries(row.data).map(
                                            ([key, val], i) => {
                                                if (
                                                    !Object.keys(val).includes(
                                                        'hiddenTableColumn'
                                                    )
                                                ) {
                                                    return (
                                                        <React.Fragment
                                                            key={`${idx}-${i}`}
                                                        >
                                                            <TableCell>
                                                                {isExperimentsTable &&
                                                                id &&
                                                                linkName &&
                                                                key ===
                                                                    Key.NAME ? (
                                                                    <Link
                                                                        href={`/mlflow/project/${projectId}/experiments/${id}`}
                                                                        passHref
                                                                        key={
                                                                            'mlflow'
                                                                        }
                                                                        legacyBehavior
                                                                    >
                                                                        <Typography
                                                                            variant="inherit"
                                                                            color="primary.dark"
                                                                            sx={{
                                                                                textDecorationLine:
                                                                                    'underline',
                                                                                cursor: 'pointer',
                                                                            }}
                                                                        >
                                                                            {
                                                                                linkName
                                                                            }
                                                                        </Typography>
                                                                    </Link>
                                                                ) : val.component ? (
                                                                    val.component
                                                                ) : (
                                                                    getCustomCell(
                                                                        idx,
                                                                        i,
                                                                        key,
                                                                        val
                                                                    )
                                                                )}
                                                            </TableCell>
                                                        </React.Fragment>
                                                    )
                                                }
                                            }
                                        )}
                                        <TableCell
                                            sx={{
                                                whiteSpace: 'nowrap',
                                            }}
                                        >
                                            <Box
                                                style={{
                                                    display: 'flex',
                                                    gap: '8px',
                                                    alignItems: 'center',
                                                }}
                                            >
                                                {actions &&
                                                    actions.map(
                                                        (
                                                            {
                                                                type,
                                                                action,
                                                                icon,
                                                                tooltipText,
                                                                color,
                                                            },
                                                            i
                                                        ) => (
                                                            <Tooltip
                                                                key={i}
                                                                title={
                                                                    tooltipText
                                                                }
                                                                color="primary"
                                                                placement="top"
                                                            >
                                                                <div>
                                                                    <IconButton
                                                                        size="small"
                                                                        color={
                                                                            color
                                                                                ? color
                                                                                : 'primary'
                                                                        }
                                                                        onClick={() =>
                                                                            action
                                                                                ? action(
                                                                                      idx,
                                                                                      type
                                                                                  )
                                                                                : null
                                                                        }
                                                                        disabled={
                                                                            !action
                                                                        }
                                                                        data-testid={`rud-button-${tooltipText}`}
                                                                    >
                                                                        {icon}
                                                                    </IconButton>
                                                                </div>
                                                            </Tooltip>
                                                        )
                                                    )}

                                                {menuItemsForRow && (
                                                    <MoreVerticalIconMenu
                                                        menuItems={[
                                                            ...menuItems,
                                                            ...actions,
                                                        ]}
                                                        runIndex={idx}
                                                    />
                                                )}
                                            </Box>
                                        </TableCell>
                                    </TableRow>
                                )
                            })}
                        </TableBody>
                    </Table>
                    {data.length &&
                        !!page &&
                        page.totalRecords >
                            PAGE_LENGTH_OPTIONS[
                                PAGE_LENGTH_OPTIONS.length - 1
                            ] && (
                            <TablePagination
                                rowsPerPageOptions={PAGE_LENGTH_OPTIONS}
                                component="div"
                                count={page.totalRecords}
                                rowsPerPage={page.rowsPerPage}
                                page={page.page}
                                onPageChange={(e, newPage) =>
                                    page.handleRowsChange('page', newPage)
                                }
                                onRowsPerPageChange={(e) => {
                                    page.handleRowsChange(
                                        'rowsPerPage',
                                        Number(e?.target.value)
                                    )
                                    page.handleRowsChange('page', 0)
                                }}
                            />
                        )}
                </TableContainer>
            ) : (
                <div>No data available</div>
            )}
            {detailsComponent ? (
                detailsComponent(
                    currentRow,
                    detailsDialogOpen,
                    handleDetailsDialog
                )
            ) : (
                <DetailsDialogLayout
                    open={detailsDialogOpen}
                    setOpen={handleDetailsDialog}
                    title={'Details'}
                >
                    <DetailsDialogList
                        data={currentRow !== null ? data[currentRow].data : {}}
                    />
                </DetailsDialogLayout>
            )}
        </>
    )
}
