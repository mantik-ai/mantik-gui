import * as React from 'react'
import IconButton from '@mui/material/IconButton'
import Menu from '@mui/material/Menu'
import MenuItem from '@mui/material/MenuItem'
import MoreVertIcon from '@mui/icons-material/MoreVert'
import { ActionButton } from '../utils/generateCustomActionButtonsArray'

interface MoreActionMenuProps {
    menuItems: ActionButton[]
}
export default function MoreActionMenu({ menuItems }: MoreActionMenuProps) {
    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null)
    const open = Boolean(anchorEl)
    const handleClick = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorEl(event.currentTarget)
    }
    const handleClose = () => {
        setAnchorEl(null)
    }

    return (
        <>
            <IconButton
                aria-label="more"
                id="long-button"
                aria-controls={open ? 'long-menu' : undefined}
                aria-expanded={open ? 'true' : undefined}
                aria-haspopup="true"
                onClick={handleClick}
                size="small"
            >
                <MoreVertIcon fontSize="small" />
            </IconButton>
            <Menu
                id="long-menu"
                MenuListProps={{
                    'aria-labelledby': 'long-button',
                }}
                anchorEl={anchorEl}
                open={open}
                onClose={handleClose}
            >
                {menuItems.map((option) => (
                    <MenuItem
                        key={option.title}
                        onClick={() => {
                            handleClose()
                            option.actionFunction && option.actionFunction()
                        }}
                        disabled={!option.actionFunction}
                    >
                        <IconButton
                            size="small"
                            color={'primary'}
                            disabled={!option.actionFunction}
                        >
                            {option.iconComponent}
                        </IconButton>
                        <span style={{ paddingBottom: '.1em' }}>
                            {option.title}
                        </span>
                    </MenuItem>
                ))}
            </Menu>
        </>
    )
}
