import React from 'react'
import { Box, Divider, Paper, Typography, useTheme } from '@mui/material'
import { Spacing } from './Spacing'
import Markdown from 'markdown-to-jsx'

interface MarkdownComponentProps {
    content: string
    title: string
}
const MarkdownComponent: React.FC<MarkdownComponentProps> = ({
    content,
    title,
}) => {
    const theme = useTheme()
    return (
        <Box
            sx={{
                m: theme.spacing(4),
            }}
        >
            <Typography variant="h2" fontWeight={600} gutterBottom>
                {/* Why is the font weight different here? Can it be defined globally? */}
                {title}
            </Typography>
            <Spacing horizontal value={theme.spacing(2)}></Spacing>
            <Divider />
            <Box
                sx={{
                    display: 'flex',
                    justifyContent: 'center',
                }}
            >
                <Paper
                    sx={{
                        m: theme.spacing(5),
                        p: theme.spacing(7),
                        maxWidth: '50em',
                    }}
                >
                    <Markdown>{content}</Markdown>
                </Paper>
            </Box>
        </Box>
    )
}
export default MarkdownComponent
