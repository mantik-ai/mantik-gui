import { Info } from '@mui/icons-material'
import { Box, CircularProgress, Paper, Typography } from '@mui/material'
import React from 'react'
import { QueryStatus } from '@tanstack/react-query'

interface DataStateIndicatorProps {
    status?: QueryStatus
    usePaper?: boolean
    text: string
    children: React.ReactNode
    error?: string
    errorMessage?: string
}
export const DataStateIndicator = ({
    status,
    usePaper = false,
    text,
    children,
    error,
    errorMessage = 'Error',
}: DataStateIndicatorProps) => {
    const content = (
        <>
            {status === 'error' ? (
                <div
                    style={{
                        display: 'flex',
                        gap: 3,
                        width: '13.75em',
                        margin: '0.5em',
                    }}
                >
                    <Info /> {error ?? errorMessage}
                </div>
            ) : (
                <>
                    <Typography variant="body1">{text}</Typography>
                    <CircularProgress />
                </>
            )}
        </>
    )

    const styling = {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        height: '100%',
    }

    if (status !== 'success') {
        return (
            <>
                {usePaper ? (
                    <Paper sx={styling}>{content}</Paper>
                ) : (
                    <Box sx={styling}>{content}</Box>
                )}
            </>
        )
    }
    return <>{children}</>
}
