import { Help } from '@mui/icons-material'
import {
    CircularProgress,
    FormControl,
    FormHelperText,
    InputLabel,
    MenuItem,
    Select,
    SelectChangeEvent,
    Skeleton,
    Tooltip,
    Typography,
    useTheme,
} from '@mui/material'
import Link from 'next/link'
import { useContext, useEffect, useMemo, useState } from 'react'
import AuthContext from '../../context/AuthContext'
import {
    Connection,
    Platform,
    useUsersUserIdSettingsConnectionsGetUsersUserIdSettingsConnectionsGet,
} from '../../queries'
import { Spacing } from '../Spacing'

interface ConnectionSelectionProps {
    currentConnection?: { connectionId: string } | null
    setCurrentConnection: (connection: Connection | null) => void
    isRequired?: boolean
    isEdit?: boolean
    platform?: Platform | null | 'S3'
}

export const ConnectionSelection = ({
    currentConnection = null,
    setCurrentConnection,
    isRequired = true,
    isEdit = false,
    platform,
}: ConnectionSelectionProps) => {
    const theme = useTheme()
    const userContext = useContext(AuthContext)
    const userId = String(userContext.usersData?.userId)
    const {
        data,
        isPending: isLoading,
        isSuccess,
    } = useUsersUserIdSettingsConnectionsGetUsersUserIdSettingsConnectionsGet(
        userId
    )

    const [currentConnectionHint, setCurrentConnectionHint] =
        useState<string>('')
    const [modified, setModified] = useState<boolean>(false)

    // Extract connectionId array
    const connectionIds =
        data && data.connections
            ? data.connections.map((el) => el.connectionId)
            : []

    // Add a default empty value to the connectionId array in case connection is not required
    const extendedConnectionIds = connectionIds
    extendedConnectionIds.unshift('None selected')

    // set index of current select value
    function handleConnectionsChange(e: SelectChangeEvent<string>): void {
        if (!data?.connections) return
        setModified(true)
        const { value } = e.target
        let idx = 0
        const connection = extendedConnectionIds.find((el) => el === value)
        if (connection && connection !== 'None selected') {
            idx = extendedConnectionIds.indexOf(connection)

            setCurrentConnection(data.connections[idx - 1])
        } else {
            setCurrentConnection(null)
        }
    }

    const filteredAllowedConnections = useMemo(() => {
        const allowedProvider = platform
        return allowedProvider
            ? data?.connections?.filter(
                  (connection) =>
                      connection.connectionProvider === allowedProvider
              )
            : data?.connections
    }, [data?.connections, platform])

    useEffect(() => {
        if (!isSuccess) {
            return
        }
        if (!data.connections || data.connections.length === 0) {
            return
        }

        if (modified) {
            setCurrentConnectionHint('')
            return
        }

        if (!isEdit && isRequired && !currentConnection) {
            setCurrentConnection(data.connections[0])
            setCurrentConnectionHint(
                'We have automatically set this to one of your stored connections.'
            )
            return
        }

        if (!currentConnection) {
            return
        }

        const validConnection = filteredAllowedConnections?.find(
            (connection) =>
                connection.connectionId === currentConnection?.connectionId
        )
        if (!validConnection) {
            setCurrentConnectionHint(
                'We have automatically unset connection because platform and connection provider do not match. Please submit to apply the change.'
            )
            setCurrentConnection(null)
        }
    }, [
        data?.connections,
        currentConnection,
        filteredAllowedConnections,
        isSuccess,
        isEdit,
        isRequired,
        setCurrentConnection,
        modified,
    ])

    return (
        <FormControl required={isRequired} sx={{ width: '100%' }}>
            <div
                style={{
                    display: 'flex',
                    flexDirection: 'row',
                    alignItems: 'center',
                    gap: '0.5em',
                }}
            >
                <InputLabel htmlFor="connections" id="connections-label">
                    Stored Connection
                </InputLabel>
                {isLoading && (
                    <Skeleton variant="rounded" sx={{ height: '3.5em' }}>
                        <input
                            type="hidden"
                            id="connections"
                            value={currentConnection?.connectionId}
                            required={isRequired}
                        />
                    </Skeleton>
                )}
                {isSuccess && (
                    <Select
                        labelId="connections-label"
                        id="connections"
                        label="StoredConnection"
                        data-testid="connectionSelector"
                        sx={{ flexGrow: 1 }}
                        value={
                            filteredAllowedConnections?.find(
                                (connection) =>
                                    connection.connectionId ===
                                    currentConnection?.connectionId
                            )?.connectionId || 'None selected'
                        }
                        onChange={handleConnectionsChange}
                        required={isRequired}
                    >
                        <MenuItem value={'None selected'}>
                            None selected
                        </MenuItem>
                        {filteredAllowedConnections?.length ? (
                            filteredAllowedConnections.map((item, index) => (
                                <MenuItem key={index} value={item.connectionId}>
                                    {`${item.connectionName}`}
                                </MenuItem>
                            ))
                        ) : (
                            <MenuItem value="">No connections found</MenuItem>
                        )}
                    </Select>
                )}
                {isLoading ? (
                    <CircularProgress size={20} />
                ) : (
                    <Tooltip
                        title={
                            <>
                                <Typography>
                                    Access private repositories using your{' '}
                                    <strong>Username-Password</strong> or{' '}
                                    <strong>Token</strong> credentials
                                </Typography>
                                <Typography>
                                    Save these credentials within your{' '}
                                    <Link
                                        href={'/settings/connections'}
                                        passHref
                                        legacyBehavior
                                    >
                                        <strong
                                            style={{
                                                textDecorationLine: 'underline',
                                                cursor: 'pointer',
                                                color: 'lightsteelblue',
                                            }}
                                        >
                                            Connections profile
                                        </strong>
                                    </Link>{' '}
                                    (within account settings) to access them
                                    here.
                                </Typography>
                            </>
                        }
                    >
                        <Help color="info" />
                    </Tooltip>
                )}
            </div>
            {isSuccess && data.connections?.length === 0 && (
                <FormHelperText color="warning">
                    Cannot find any connections for this user. Please go to
                    Settings &gt; Connections &gt; Add
                </FormHelperText>
            )}
            <FormHelperText color="warning">
                {currentConnectionHint}
            </FormHelperText>
            <Spacing value={theme.spacing(1)} />
            {!currentConnection && isRequired && (
                <>
                    <FormHelperText error>
                        {`You must select a connection in order to submit a run`}
                    </FormHelperText>
                    <Spacing value={theme.spacing(2)} />
                </>
            )}
        </FormControl>
    )
}
