import {
    Box,
    Button,
    Checkbox,
    Chip,
    FormControl,
    FormControlLabel,
    Grid,
    InputLabel,
    MenuItem,
    Select,
    SelectChangeEvent,
    TextField,
    Typography,
} from '@mui/material'
import { FormAction, FormState } from '../../utils/forms'
import { useState } from 'react'
import { Add, Delete } from '@mui/icons-material'

// Simple Components to handle simplest cases of text fields when FormAction is used to dispatch change
// Designed to be used with a reducer with (state: FormState, action: FormAction<AddFormState>) => FormState

// These are very baic with limited type checking! It's up to you (so far) to ensure the appropriate component is used!

interface DispatchingTextFieldProps<X extends FormState> {
    state: X
    target: keyof X
    label?: string
    dispatch: (action: FormAction<X>) => void
    required?: boolean
}

export const BasicTextField = <X extends FormState>({
    state,
    target,
    dispatch,
    label,
    required = true,
}: DispatchingTextFieldProps<X>) => (
    <TextField
        margin="normal"
        error={state[target.toString()] === 'bam'}
        fullWidth
        required={required}
        id={target.toString()} // TODO: improve typing: target should always be a string
        name={target.toString()}
        label={label ?? target.toString()}
        value={state[target]}
        type="text"
        onChange={(e) => {
            dispatch({
                target: target.toString(),
                payload: e.target.value,
            } as FormAction<X>)
        }}
    />
)

export const BasicCheckbox = <X extends FormState>({
    state,
    target: targetFixTyping,
    dispatch,
    label,
}: DispatchingTextFieldProps<X>) => {
    const target = targetFixTyping.toString()
    return (
        <FormControlLabel
            control={
                <Checkbox
                    checked={state[target]}
                    onClick={() => {
                        dispatch({
                            target,
                            payload: !state[target],
                        } as FormAction<X>)
                    }}
                ></Checkbox>
            }
            style={{ marginBottom: '25px' }}
            label={label ?? target}
        />
    )
}

export const BasicSelector = <X extends FormState>({
    state,
    target: targetFixTyping,
    label,
    options,
    onChange,
}: DispatchingTextFieldProps<X> & {
    options: string[]
    onChange: (e: SelectChangeEvent) => void
}) => {
    const target = targetFixTyping.toString()
    return (
        <FormControl fullWidth margin="normal">
            <InputLabel id="add-datarepo-select-platform-label">
                {label}
            </InputLabel>
            <Select
                labelId="add-datarepo-select-platform-label"
                id="add-datarepo-select-platform"
                value={state[target]}
                label={label}
                onChange={onChange}
            >
                {options.map((option, key) => (
                    <MenuItem key={key} value={option}>
                        {option}
                    </MenuItem>
                ))}
            </Select>
        </FormControl>
    )
}

export const BasicLabels = <X extends FormState>({
    state,
    target: targetFixTyping,
    dispatch,
    label,
}: DispatchingTextFieldProps<X>) => {
    const target = targetFixTyping.toString()
    const [labels, setLabels] = useState<string>('')
    return (
        <FormControl fullWidth={true}>
            <Box py="1em">
                <Typography>{label}</Typography>
                <TextField
                    fullWidth
                    required={false}
                    id={target}
                    name={target}
                    label={'Add fields separated by comma'}
                    value={labels}
                    type="text"
                    onChange={(e) => {
                        if (e.target.value.includes(',')) {
                            const newLabel = e.target.value.split(',')[0]
                            if (
                                !state[target].includes(newLabel) &&
                                newLabel !== ''
                            ) {
                                dispatch({
                                    target,
                                    payload: [...state[target], newLabel],
                                } as FormAction<X>)
                            }
                            setLabels('')
                        } else {
                            setLabels(e.target.value)
                        }
                    }}
                />
                {(state[target] as string[]).map((label, key) => (
                    <Chip
                        key={key}
                        label={label}
                        onDelete={() =>
                            dispatch({
                                target,
                                payload: state[target].filter(
                                    (l: string) => l !== label
                                ),
                            } as FormAction<X>)
                        }
                    ></Chip>
                ))}
            </Box>
        </FormControl>
    )
}

interface RecordLabels {
    keyLabel: string
    valueLabel: string
}
export const BasicStringStringRecord = <X extends FormState>({
    state,
    target: targetFixTyping,
    dispatch,
    label,
    keyLabel,
    valueLabel,
}: DispatchingTextFieldProps<X> & RecordLabels) => {
    const target = targetFixTyping.toString()
    const [currentKey, setCurrentKey] = useState<string>('')
    const [currentValue, setCurrentValue] = useState<string>('')
    const handleAddRecord = () => {
        if (currentKey !== '' && currentValue !== '') {
            const keys = Object.keys(state[target])
            if (!keys.includes(currentKey)) {
                const payload = {
                    ...state[target],
                    [currentKey]: currentValue,
                }
                dispatch({
                    target,
                    payload,
                } as FormAction<X>)
                setCurrentKey('')
                setCurrentValue('')
            }
        }
    }
    const handleDelete = (record: { key: string; value: string }) => {
        const { key } = record
        const newState = { ...state[target] }
        delete newState[key]
        dispatch({
            target,
            payload: newState,
        } as FormAction<X>)
    }
    return (
        <Box sx={{ border: 1 }} p={1}>
            <Typography>{label}</Typography>
            <Grid
                container
                sm={12}
                alignItems="center"
                justifyContent={'space-evenly'}
            >
                <Grid item sm={5}>
                    <Box px="1em">
                        <TextField
                            fullWidth
                            margin="normal"
                            required={false}
                            id={target}
                            name={target}
                            label={keyLabel}
                            value={currentKey}
                            type="text"
                            onChange={(e) => {
                                setCurrentKey(e.target.value)
                            }}
                        />
                    </Box>
                </Grid>
                <Grid item sm={5} mx={'2'}>
                    <Box px="1em">
                        <TextField
                            fullWidth
                            margin="normal"
                            required={false}
                            id={target}
                            name={target}
                            label={valueLabel}
                            value={currentValue}
                            type="text"
                            onChange={(e) => {
                                setCurrentValue(e.target.value)
                            }}
                        />
                    </Box>
                </Grid>
                <Grid item sm={2}>
                    <Button variant="outlined" onClick={handleAddRecord}>
                        <Add />
                    </Button>
                </Grid>
            </Grid>
            {Object.entries(state[target]).map(([k, v]) => (
                <Grid
                    key={k}
                    container
                    sm={12}
                    alignItems="center"
                    justifyContent="space-evenly"
                >
                    <Grid item sm={5}>
                        <Box px="1em">
                            <TextField
                                disabled
                                color="primary"
                                fullWidth
                                margin="normal"
                                required={false}
                                id={target}
                                name={target}
                                value={k}
                                type="text"
                            />
                        </Box>
                    </Grid>
                    <Grid item sm={5}>
                        <Box px="1em">
                            <TextField
                                disabled
                                color="primary"
                                fullWidth
                                margin="normal"
                                required={false}
                                id={target}
                                name={target}
                                value={v}
                                type="text"
                            />
                        </Box>
                    </Grid>
                    <Grid item sm={2}>
                        <Button variant="outlined">
                            <Delete
                                onClick={() =>
                                    handleDelete({
                                        key: k,
                                        value: v as string,
                                    })
                                }
                            />
                        </Button>
                    </Grid>
                </Grid>
            ))}
        </Box>
    )
}
