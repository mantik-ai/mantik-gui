import { Fragment } from 'react'
import CheckIcon from '@mui/icons-material/Check'
import CloseIcon from '@mui/icons-material/Close'

const iconStyle = {
    fontSize: '100%',
    mr: 1,
    mb: -0.2,
}
interface RequirementsChecklistProps {
    requirements: {
        requirement: string
        error: boolean
    }[]
}
export const RequirementsChecklist = ({
    requirements,
}: RequirementsChecklistProps) => {
    return (
        <>
            {requirements.map(({ requirement, error }, index) => (
                <Fragment key={index}>
                    {error ? (
                        <CloseIcon color="error" sx={iconStyle} />
                    ) : (
                        <CheckIcon color="success" sx={iconStyle} />
                    )}
                    {requirement}
                    <br />
                </Fragment>
            ))}
        </>
    )
}
