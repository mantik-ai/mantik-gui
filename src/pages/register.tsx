import React, { ChangeEvent, KeyboardEvent, useState } from 'react'
import type { NextPage } from 'next'
import { AxiosError } from 'axios'
import { useRouter } from 'next/router'
import PersonOutlineIcon from '@mui/icons-material/PersonOutline'
import { IconButton, InputAdornment, TextField } from '@mui/material'
import { AuthCard, AuthCardTypes } from '../modules/auth/AuthCard'
import { getSignUpExceptionMessage } from '../modules/auth/utils'
import Visibility from '@mui/icons-material/Visibility'
import VisibilityOff from '@mui/icons-material/VisibilityOff'
import { formRulesChecker } from '../helpers'
import { AddUser, useUsersPostUsersPost } from '../queries'
import { RequirementsChecklist } from '../components/forms/RequirementsChecklist'
import { usePasswordValidator } from '../hooks/usePasswordValidator'

const Register: NextPage = () => {
    const router = useRouter()
    const [loading, setLoading] = useState(false)
    const [globalError, setGlobalError] = useState('')
    const [errorState, setErrorState] = useState({
        email: '',
        username: '',
        password: '',
        confirmPassword: '',
    })
    const [formState, setFormState] = useState({
        email: '',
        username: '',
        password: '',
        confirmPassword: '',
    })
    const [showPassword, setShowPassword] = useState(false)
    const handleClickShowPassword = () => setShowPassword(!showPassword)
    const handleMouseDownPassword = () => setShowPassword(!showPassword)
    const { passwordRequirements } = usePasswordValidator(formState.password)
    const [showConfirmPassword, setShowConfirmPassword] = useState(false)
    const handleClickConfirmPassword = () =>
        setShowConfirmPassword(!showConfirmPassword)
    const handleMouseDownConfirmPassword = () =>
        setShowConfirmPassword(!showConfirmPassword)

    const isDisabled =
        Object.values(errorState).some((value) => value !== '') ||
        Object.values(formState).some((value) => value === '') ||
        globalError !== ''

    const onChange = (
        e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
    ) => {
        const { name, value } = e.target
        setFormState((prev) => ({ ...prev, [name]: value }))
        setGlobalError('')
        setErrorState((prev) => {
            const stateObj = { ...prev, [name]: '' }
            formRulesChecker(name, value, stateObj, formState.password)
            return stateObj
        })
    }
    const { mutate: createUser } = useUsersPostUsersPost()
    const handleCreateUser = async (userData: AddUser) => {
        try {
            await createUser({ data: userData })
        } catch (e: unknown) {
            const resError = e as AxiosError<{
                message: {
                    name: string
                    $fault: string
                }
            }>
            setGlobalError(
                getSignUpExceptionMessage(
                    resError.response?.data?.message.name ?? ''
                )
            )
        } finally {
            setLoading(false)
        }
    }

    const handleFormSubmit = async () => {
        try {
            setLoading(true)
            await handleCreateUser({
                name: formState.username,
                password: formState.password,
                email: formState.email,
            })
            await router.push({
                pathname: '/confirm-email',
                query: { username: formState.username },
            })
        } catch (error) {
            console.log(error)
        }
    }

    const handleKeyDown = async (event: KeyboardEvent<HTMLInputElement>) => {
        if (event.key === 'Enter' && !isDisabled) {
            await handleFormSubmit()
        }
    }

    return (
        <AuthCard
            icon={PersonOutlineIcon}
            loading={loading}
            globalError={globalError === '' ? undefined : globalError}
            onClick={handleFormSubmit}
            disabled={isDisabled}
            fields={[
                <TextField
                    key="register-email"
                    name="email"
                    label="email"
                    size="small"
                    error={errorState.email !== ''}
                    helperText={
                        errorState.email === '' ? null : errorState.email
                    }
                    value={formState.email}
                    onChange={onChange}
                    onKeyDown={handleKeyDown}
                    required
                />,
                <TextField
                    key="register-username"
                    name="username"
                    label="username"
                    size="small"
                    error={errorState.username !== ''}
                    helperText={
                        errorState.username === '' ? null : errorState.username
                    }
                    value={formState.username}
                    onChange={onChange}
                    onKeyDown={handleKeyDown}
                    required
                />,
                <TextField
                    className="password"
                    key="register-password"
                    name="password"
                    label="password"
                    size="small"
                    type={showPassword ? 'text' : 'password'}
                    error={errorState.password !== ''}
                    helperText={
                        !!formState.password.length && (
                            <RequirementsChecklist
                                requirements={passwordRequirements}
                            />
                        )
                    }
                    value={formState.password}
                    onKeyDown={handleKeyDown}
                    onChange={onChange}
                    InputProps={{
                        endAdornment: (
                            <InputAdornment position="end">
                                <IconButton
                                    aria-label="toggle password visibility"
                                    onClick={handleClickShowPassword}
                                    onMouseDown={handleMouseDownPassword}
                                >
                                    {showPassword ? (
                                        <Visibility />
                                    ) : (
                                        <VisibilityOff />
                                    )}
                                </IconButton>
                            </InputAdornment>
                        ),
                    }}
                    required
                />,
                <TextField
                    key="register-password-confirm"
                    name="confirmPassword"
                    label="password confirmation"
                    size="small"
                    type={showConfirmPassword ? 'text' : 'password'}
                    error={errorState.confirmPassword !== ''}
                    helperText={
                        errorState.confirmPassword === ''
                            ? null
                            : errorState.confirmPassword
                    }
                    value={formState.confirmPassword}
                    onChange={onChange}
                    onKeyDown={handleKeyDown}
                    InputProps={{
                        endAdornment: (
                            <InputAdornment position="end">
                                <IconButton
                                    aria-label="toggle password visibility"
                                    onClick={handleClickConfirmPassword}
                                    onMouseDown={handleMouseDownConfirmPassword}
                                >
                                    {showConfirmPassword ? (
                                        <Visibility />
                                    ) : (
                                        <VisibilityOff />
                                    )}
                                </IconButton>
                            </InputAdornment>
                        ),
                    }}
                    required
                />,
            ]}
            type={AuthCardTypes.REGISTER}
        />
    )
}

export default Register
