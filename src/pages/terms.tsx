import React from 'react'
import termsContent from '../content/terms-content.md'
import MarkdownComponent from '../components/MarkdownComponent'

function Terms() {
    return <MarkdownComponent content={termsContent} title={'Terms'} />
}

export default Terms
