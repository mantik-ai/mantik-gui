import Groups from './Groups'
import * as React from 'react'
import { NextPageWithNestedLayout } from '../_app'
import { CollaborationsLayout } from '../../modules/collaborations/components/CollaborationsLayout'

const Collaboration: NextPageWithNestedLayout = () => {
    return <Groups />
}
Collaboration.getNestedLayout = (page) => {
    return <CollaborationsLayout>{page}</CollaborationsLayout>
}
export default Collaboration
