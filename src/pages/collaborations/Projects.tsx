import React, { useState } from 'react'
import { Button, Box } from '@mui/material'
import { useSession } from 'next-auth/react'
import { NextPageWithNestedLayout } from '../_app'
import { CollaborationsLayout } from '../../modules/collaborations/components/CollaborationsLayout'
import { mantikApi } from '../../modules/auth/axios'

const MyProjects: NextPageWithNestedLayout = () => {
    const session = useSession()
    const [myProjects, setMyProjects] = useState([])
    async function fetchUserProjects() {
        try {
            const response = await mantikApi.get(`/projects`, {
                headers: {
                    Authorization: `Bearer ${session.data?.user.accessToken}`,
                    Accept: 'application/json',
                },
            })
            if (response.status === 200) {
                setMyProjects(response.data['projects'])
            }
        } catch (err: unknown) {
            console.error('Error: ', err)
        }
    }

    return (
        <Box m={5}>
            <Button onClick={fetchUserProjects}>Click to view Projects</Button>
            {myProjects &&
                myProjects.map((project: any) => (
                    <Box key={project.name}>Project Name: {project.name}</Box>
                ))}
        </Box>
    )
}
MyProjects.getNestedLayout = (page) => {
    return <CollaborationsLayout>{page}</CollaborationsLayout>
}
export default MyProjects
