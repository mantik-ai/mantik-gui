import { Add, DeleteOutlined, Edit, ForwardToInbox } from '@mui/icons-material'
import {
    Box,
    Button,
    Divider,
    Paper,
    FormControlLabel,
    Switch,
    Tooltip,
    IconButton,
} from '@mui/material'
import React, { useContext, useState } from 'react'
import OrganizationDialogContext from '../../modules/collaborations/contexts/OrganizationDialogContext'
import { Organization, User } from '../../queries/models'
import { OrganizationDialog } from '../../modules/collaborations/CreateOrganizationDialog'
import { useSession } from 'next-auth/react'
import { NextPageWithNestedLayout } from '../_app'
import { CollaborationsLayout } from '../../modules/collaborations/components/CollaborationsLayout'
import { DataStateIndicator } from '../../components/DataStateIndicator'
import { EditOrganizationDialog } from '../../modules/collaborations/EditOrganizationDialog'
import {
    AddCollaboratorDialog,
    InvitedToType,
    InvitedType,
} from '../../modules/collaborations/AddCollaboratorDialog'
import { DeleteDialogLayout } from '../../components/DeleteDialogLayout'
import { StatusProps } from '../../types/statusProps'
import {
    useOrganizationsOrganizationIdDeleteOrganizationsOrganizationIdDelete,
    useOrganizationsOrganizationMemberIdDeleteOrganizationsOrganizationIdMembersUserIdDelete,
    useUsersUserIdOrganizationsGetUsersUserIdOrganizationsGet,
} from '../../queries'
import { renderApiErrorMessage } from '../../errors'
import AuthContext from '../../context/AuthContext'
import { PageHeading } from '../../components/PageHeading'

const Organizations: NextPageWithNestedLayout = () => {
    const [showMembers, setShowMembers] = useState(false)
    const [openCreateOrganizationDialog, setOpenCreateOrganizationDialog] =
        useState(false)
    const [openEditOrganizationDialog, setOpenEditOrganizationDialog] =
        useState<{
            [organizationId: string]: boolean
        }>({})
    const [
        openAddMemberOrganizationDialog,
        setOpenAddMemberOrganizationDialog,
    ] = useState<{
        [organizationId: string]: boolean
    }>({})
    const session = useSession()
    const userContext = useContext(AuthContext)
    const userId = userContext.usersData?.userId
    const organizationContext = useContext(OrganizationDialogContext)
    const { data, status, refetch } =
        useUsersUserIdOrganizationsGetUsersUserIdOrganizationsGet(
            userId as string
        )
    const [deleteDialogOpen, setDeleteDialogOpen] = useState<boolean>(false)
    const [deleteData, setDeleteData] = useState({
        name: '',
        organizationId: '',
        userId: '',
        type: '', //delete types are 'organization' or 'member'
    })
    const [deleteStatus, setDeleteStatus] = useState<StatusProps>({
        status: false,
        type: '',
        text: '',
    })

    const handleShowMembers = () => {
        setShowMembers((showMembers) => !showMembers)
    }
    const openDialog = () => {
        setOpenCreateOrganizationDialog(true)
    }
    const openEditDialog = (organizationId: string) => {
        setOpenEditOrganizationDialog((prevState) => ({
            ...prevState,
            [organizationId]: true,
        }))
    }
    const handleCloseEditDialog = (organizationId: string) => {
        setOpenEditOrganizationDialog((prevState) => ({
            ...prevState,
            [organizationId]: false,
        }))
    }
    const openAddMemberDialog = (organizationId: string) => {
        setOpenAddMemberOrganizationDialog((prevState) => ({
            ...prevState,
            [organizationId]: true,
        }))
    }
    const handleCloseAddMemberDialog = (organizationId: string) => {
        setOpenAddMemberOrganizationDialog((prevState) => ({
            ...prevState,
            [organizationId]: false,
        }))
    }
    function handleDeleteOpen() {
        setDeleteDialogOpen(true)
    }

    function handleDeleteClose() {
        setDeleteDialogOpen(false)
        setDeleteStatus({ status: false, type: '', text: '' })
    }
    function handleDataRefetch() {
        refetch() // Refresh the data
    }
    function handleDeleteSubmit() {
        const organizationId = String(deleteData.organizationId)
        const userId = String(deleteData.userId)
        if (deleteData.type == 'organization') {
            mutate(
                { organizationId },
                {
                    // Optional: You can provide extra configuration here if needed
                    // e.g., onSuccess, onError, onSettled, etc.
                }
            )
        } else {
            deleteOrganizationMember.mutate({
                organizationId,
                userId,
            })
        }
    }
    /* #############################################
                    API CALLS
    ############################################# */

    const { mutate, isPending: isLoading } =
        useOrganizationsOrganizationIdDeleteOrganizationsOrganizationIdDelete({
            mutation: {
                onSuccess: () => {
                    setDeleteStatus({
                        status: true,
                        type: 'success',
                        text: 'Organization was deleted successfully!',
                    })
                    handleDataRefetch()
                },
                onError: (error) => {
                    setDeleteStatus({
                        status: true,
                        type: 'error',
                        text: renderApiErrorMessage(error),
                    })
                },
            },
        })
    const deleteOrganizationMember =
        useOrganizationsOrganizationMemberIdDeleteOrganizationsOrganizationIdMembersUserIdDelete(
            {
                mutation: {
                    onSuccess: () => {
                        setDeleteStatus({
                            status: true,
                            type: 'success',
                            text: 'Member was removed successfully!',
                        })
                        handleDataRefetch()
                    },
                    onError: (error) => {
                        setDeleteStatus({
                            status: true,
                            type: 'error',
                            text: renderApiErrorMessage(error),
                        })
                    },
                },
            }
        )

    return (
        <Paper>
            <Box p={4}>
                <Box
                    sx={{
                        display: 'flex',
                        flexDirection: 'column',
                    }}
                >
                    <PageHeading
                        description="Search and create your organizations."
                        right={
                            session.data ? (
                                <Box>
                                    <Button
                                        variant="outlined"
                                        onClick={() => openDialog()}
                                    >
                                        <Add />
                                        CREATE ORGANIZATION
                                    </Button>
                                </Box>
                            ) : (
                                'Please log in to create an organization'
                            )
                        }
                    >
                        My Organizations
                    </PageHeading>
                    <OrganizationDialog
                        open={openCreateOrganizationDialog}
                        setOpen={setOpenCreateOrganizationDialog}
                        refetchData={handleDataRefetch}
                    />
                </Box>
                <DataStateIndicator
                    status={
                        session.status !== 'loading' && status !== 'pending'
                            ? 'success'
                            : 'pending'
                    }
                    text="Loading Organizations..."
                >
                    <Box
                        sx={{
                            display: 'flex',
                            flexDirection: 'column',
                        }}
                    >
                        {data?.organizations && (
                            <FormControlLabel
                                value="start"
                                control={<Switch color="primary" />}
                                label={
                                    !showMembers
                                        ? 'Show members'
                                        : 'Hide members'
                                }
                                labelPlacement="start"
                                onChange={handleShowMembers}
                            />
                        )}
                        {data?.organizations ? (
                            data?.organizations.map(
                                (organization: Organization, index: number) => (
                                    <Box key={organization.organizationId}>
                                        <h3
                                            style={{
                                                display: 'flex',
                                                alignItems: 'center',
                                                justifyContent: 'space-between',
                                            }}
                                        >
                                            <span>
                                                {index + 1}. Organization Name:{' '}
                                                {organization.name}
                                            </span>
                                            <div>
                                                <Tooltip
                                                    title="Invite"
                                                    color="primary"
                                                    placement="top"
                                                >
                                                    <IconButton
                                                        onClick={() =>
                                                            openAddMemberDialog(
                                                                organization.organizationId
                                                            )
                                                        }
                                                        size="small"
                                                    >
                                                        <ForwardToInbox fontSize="small" />
                                                    </IconButton>
                                                </Tooltip>
                                                <Tooltip
                                                    title="Edit"
                                                    color="primary"
                                                    placement="top"
                                                >
                                                    <IconButton
                                                        onClick={() =>
                                                            openEditDialog(
                                                                organization.organizationId
                                                            )
                                                        }
                                                        size="small"
                                                    >
                                                        <Edit fontSize="small" />
                                                    </IconButton>
                                                </Tooltip>
                                                <Tooltip
                                                    title="Delete"
                                                    color="primary"
                                                    placement="top"
                                                >
                                                    <IconButton
                                                        onClick={() => {
                                                            handleDeleteOpen()
                                                            setDeleteData({
                                                                ...deleteData,
                                                                name: String(
                                                                    `Are you sure you want to delete the ${organization?.name} organization?`
                                                                ),
                                                                organizationId:
                                                                    String(
                                                                        organization.organizationId
                                                                    ),
                                                                type: 'organization',
                                                            })
                                                        }}
                                                        size="small"
                                                    >
                                                        <DeleteOutlined fontSize="small" />
                                                    </IconButton>
                                                </Tooltip>
                                            </div>
                                        </h3>
                                        <EditOrganizationDialog
                                            id={organization.organizationId}
                                            name={organization.name}
                                            contact={organization.contact}
                                            members={organization.members}
                                            groups={organization.groups}
                                            open={
                                                openEditOrganizationDialog[
                                                    organization.organizationId
                                                ] || false
                                            }
                                            setOpen={() =>
                                                handleCloseEditDialog(
                                                    organization.organizationId
                                                )
                                            }
                                        />
                                        <AddCollaboratorDialog
                                            id={organization.organizationId}
                                            users={
                                                organizationContext.users
                                                    ?.users || []
                                            }
                                            invitedType={InvitedType.USER}
                                            invitedToType={
                                                InvitedToType.ORGANIZATION
                                            }
                                            open={
                                                openAddMemberOrganizationDialog[
                                                    organization.organizationId
                                                ] || false
                                            }
                                            setOpen={() =>
                                                handleCloseAddMemberDialog(
                                                    organization.organizationId
                                                )
                                            }
                                        />
                                        <p>
                                            Organization Contact:{' '}
                                            {organization.contact.name}
                                        </p>

                                        {showMembers && (
                                            <ul>
                                                {organization.members?.map(
                                                    (member: User) => (
                                                        <li key={member.userId}>
                                                            <p>
                                                                Name:{' '}
                                                                {member.name}
                                                                <Tooltip
                                                                    title="Delete"
                                                                    color="error"
                                                                    placement="top"
                                                                >
                                                                    <IconButton
                                                                        onClick={() => {
                                                                            handleDeleteOpen()
                                                                            setDeleteData(
                                                                                {
                                                                                    ...deleteData,
                                                                                    name: String(
                                                                                        `Are you sure you want to remove ${member.name} from the ${organization.name} organization?`
                                                                                    ),
                                                                                    organizationId:
                                                                                        String(
                                                                                            organization.organizationId
                                                                                        ),
                                                                                    userId: String(
                                                                                        member.userId
                                                                                    ),
                                                                                    type: 'member',
                                                                                }
                                                                            )
                                                                        }}
                                                                        size="small"
                                                                        style={{
                                                                            marginLeft:
                                                                                '15px',
                                                                        }} // Adjust the value as needed
                                                                    >
                                                                        <DeleteOutlined fontSize="small" />
                                                                    </IconButton>
                                                                </Tooltip>
                                                            </p>
                                                        </li>
                                                    )
                                                )}
                                            </ul>
                                        )}
                                        <Divider variant="middle" />
                                    </Box>
                                )
                            )
                        ) : (
                            <Box>You have no organizations yet.</Box>
                        )}
                    </Box>
                </DataStateIndicator>
            </Box>
            <DeleteDialogLayout
                open={deleteDialogOpen}
                title="Delete Organization"
                text={deleteData?.name}
                warning="All access rights to projects based on organization membership will be revoked!"
                handleSubmit={handleDeleteSubmit}
                handleClose={handleDeleteClose}
                isLoading={isLoading}
                status={deleteStatus}
            ></DeleteDialogLayout>
            <Divider variant="middle" />
        </Paper>
    )
}
Organizations.getNestedLayout = (page) => {
    return <CollaborationsLayout>{page}</CollaborationsLayout>
}
export default Organizations
