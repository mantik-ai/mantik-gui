import {
    Box,
    Button,
    Divider,
    FormControlLabel,
    IconButton,
    Paper,
    Switch,
    Tooltip,
} from '@mui/material'
import React, { useContext, useState } from 'react'
import { Add, DeleteOutlined, Edit, ForwardToInbox } from '@mui/icons-material'
import { useSession } from 'next-auth/react'
import { EditGroupDialog } from '../../modules/collaborations/EditGroupDialog'
import { CreateGroupDialog } from '../../modules/collaborations/CreateGroupDialog'
import GroupDialogContext from '../../modules/collaborations/contexts/GroupDialogContext'
import { NextPageWithNestedLayout } from '../_app'
import { CollaborationsLayout } from '../../modules/collaborations/components/CollaborationsLayout'
import {
    User,
    UserGroup,
    useGroupsGroupIdDeleteGroupsGroupIdDelete,
    useGroupsGroupMemberIdDeleteGroupsGroupIdMembersUserIdDelete,
    useUsersUserIdGroupsGetUsersUserIdGroupsGet,
} from '../../queries'
import { DataStateIndicator } from '../../components/DataStateIndicator'
import {
    AddCollaboratorDialog,
    InvitedToType,
    InvitedType,
} from '../../modules/collaborations/AddCollaboratorDialog'
import { StatusProps } from '../../types/statusProps'
import { DeleteDialogLayout } from '../../components/DeleteDialogLayout'
import { renderApiErrorMessage } from '../../errors'
import AuthContext from '../../context/AuthContext'
import { PageHeading } from '../../components/PageHeading'

const Groups: NextPageWithNestedLayout = () => {
    const [showMembers, setShowMembers] = useState(false)
    const [openCreateGroupDialog, setOpenCreateGroupDialog] = useState(false)
    const [openEditGroupDialog, setOpenEditGroupDialog] = useState<{
        [groupId: string]: boolean
    }>({})
    const [openAddMemberGroupDialog, setOpenAddMemberGroupDialog] = useState<{
        [groupId: string]: boolean
    }>({})
    const session = useSession()
    const userContext = useContext(AuthContext)
    const userId = String(userContext.usersData?.userId)
    const groupContext = useContext(GroupDialogContext)
    const { data, status, refetch } =
        useUsersUserIdGroupsGetUsersUserIdGroupsGet(userId as string)
    const [deleteDialogOpen, setDeleteDialogOpen] = useState<boolean>(false)
    const [deleteData, setDeleteData] = useState({
        name: '',
        userGroupId: '',
        userId: '',
        type: '',
    })
    const [deleteStatus, setDeleteStatus] = useState<StatusProps>({
        status: false,
        type: '',
        text: '',
    })

    const handleShowMembers = () => {
        setShowMembers((showMembers) => !showMembers)
    }

    const openDialog = () => {
        setOpenCreateGroupDialog(true)
    }
    const openEditDialog = (groupId: string) => {
        setOpenEditGroupDialog((prevState) => ({
            ...prevState,
            [groupId]: true,
        }))
    }
    const handleCloseEditDialog = (groupId: string) => {
        setOpenEditGroupDialog((prevState) => ({
            ...prevState,
            [groupId]: false,
        }))
    }
    const openAddMemberDialog = (groupId: string) => {
        setOpenAddMemberGroupDialog((prevState) => ({
            ...prevState,
            [groupId]: true,
        }))
    }
    const handleCloseAddMemberDialog = (groupId: string) => {
        setOpenAddMemberGroupDialog((prevState) => ({
            ...prevState,
            [groupId]: false,
        }))
    }
    function handleDeleteOpen() {
        setDeleteDialogOpen(true)
    }

    function handleDeleteClose() {
        setDeleteDialogOpen(false)
        setDeleteStatus({ status: false, type: '', text: '' })
    }
    function handleDataRefetch() {
        refetch() // Refresh the data
    }
    function handleDeleteSubmit() {
        const userGroupId = String(deleteData.userGroupId)
        const userId = String(deleteData.userId)
        if (deleteData.type == 'group') {
            mutate(
                { groupId: userGroupId },
                {
                    // Optional: You can provide extra configuration here if needed
                    // e.g., onSuccess, onError, onSettled, etc.
                }
            )
        } else {
            deleteMember.mutate({
                groupId: userGroupId,
                userId,
            })
        }
    }

    const { mutate, isPending: isLoading } =
        useGroupsGroupIdDeleteGroupsGroupIdDelete({
            mutation: {
                onSuccess: () => {
                    setDeleteStatus({
                        status: true,
                        type: 'success',
                        text: 'Group was deleted successfully!',
                    })
                    handleDataRefetch()
                },
                onError: (error) => {
                    setDeleteStatus({
                        status: true,
                        type: 'error',
                        text: renderApiErrorMessage(error),
                    })
                },
            },
        })
    const deleteMember =
        useGroupsGroupMemberIdDeleteGroupsGroupIdMembersUserIdDelete({
            mutation: {
                onSuccess: () => {
                    setDeleteStatus({
                        status: true,
                        type: 'success',
                        text: 'Member was removed successfully!',
                    })
                    handleDataRefetch()
                },
                onError: (error) => {
                    setDeleteStatus({
                        status: true,
                        type: 'error',
                        text: renderApiErrorMessage(error),
                    })
                },
            },
        })
    return (
        <Paper>
            <Box p={4}>
                <Box
                    sx={{
                        display: 'flex',
                        flexDirection: 'column',
                    }}
                >
                    <PageHeading
                        description="Search and create your groups."
                        right={
                            session.data ? (
                                <Button
                                    variant="outlined"
                                    onClick={() => openDialog()}
                                >
                                    <Add />
                                    CREATE GROUP
                                </Button>
                            ) : (
                                'Please log in to create a group'
                            )
                        }
                    >
                        My Groups
                    </PageHeading>
                    <CreateGroupDialog
                        open={openCreateGroupDialog}
                        setOpen={setOpenCreateGroupDialog}
                        refetchData={handleDataRefetch}
                    />
                </Box>
                <DataStateIndicator
                    status={
                        session.status !== 'loading' && status !== 'pending'
                            ? 'success'
                            : 'pending'
                    }
                    text="Loading Groups..."
                >
                    <Box
                        sx={{
                            display: 'flex',
                            flexDirection: 'column',
                        }}
                    >
                        {data?.userGroups && (
                            <FormControlLabel
                                value="start"
                                control={<Switch color="primary" />}
                                label={
                                    !showMembers
                                        ? 'Show members'
                                        : 'Hide members'
                                }
                                labelPlacement="start"
                                onChange={handleShowMembers}
                            />
                        )}
                        {data?.userGroups ? (
                            data?.userGroups.map(
                                (group: UserGroup, index: number) => (
                                    <Box key={group.userGroupId}>
                                        <h3
                                            style={{
                                                display: 'flex',
                                                alignItems: 'center',
                                                justifyContent: 'space-between',
                                            }}
                                        >
                                            <span>
                                                {index + 1 + '  '}. Group Name:{' '}
                                                {group.name}
                                            </span>
                                            <div>
                                                <Tooltip
                                                    title="Invite"
                                                    color="primary"
                                                    placement="top"
                                                >
                                                    <IconButton
                                                        onClick={() =>
                                                            openAddMemberDialog(
                                                                group.userGroupId
                                                            )
                                                        }
                                                        size="small"
                                                    >
                                                        <ForwardToInbox fontSize="small" />
                                                    </IconButton>
                                                </Tooltip>
                                                <Tooltip
                                                    title="Edit"
                                                    color="primary"
                                                    placement="top"
                                                >
                                                    <IconButton
                                                        onClick={() =>
                                                            openEditDialog(
                                                                group.userGroupId
                                                            )
                                                        }
                                                        size="small"
                                                    >
                                                        <Edit fontSize="small" />
                                                    </IconButton>
                                                </Tooltip>
                                                <Tooltip
                                                    title="Delete"
                                                    color="primary"
                                                    placement="top"
                                                >
                                                    <IconButton
                                                        onClick={() => {
                                                            handleDeleteOpen()
                                                            setDeleteData({
                                                                ...deleteData,
                                                                name: String(
                                                                    `Are you sure you want to delete the ${group?.name} group?`
                                                                ),
                                                                userGroupId:
                                                                    String(
                                                                        group.userGroupId
                                                                    ),
                                                                type: 'group',
                                                            })
                                                        }}
                                                        size="small"
                                                    >
                                                        <DeleteOutlined fontSize="small" />
                                                    </IconButton>
                                                </Tooltip>
                                            </div>
                                        </h3>
                                        <EditGroupDialog
                                            groupId={group.userGroupId}
                                            groupName={group.name}
                                            groupAdmin={group.admin}
                                            groupMembers={group.members || []}
                                            open={
                                                openEditGroupDialog[
                                                    group.userGroupId
                                                ] || false
                                            }
                                            setOpen={() =>
                                                handleCloseEditDialog(
                                                    group.userGroupId
                                                )
                                            }
                                        />

                                        <AddCollaboratorDialog
                                            id={group.userGroupId}
                                            users={
                                                groupContext.users?.users || []
                                            }
                                            invitedType={InvitedType.USER}
                                            invitedToType={InvitedToType.GROUP}
                                            open={
                                                openAddMemberGroupDialog[
                                                    group.userGroupId
                                                ] || false
                                            }
                                            setOpen={() =>
                                                handleCloseAddMemberDialog(
                                                    group.userGroupId
                                                )
                                            }
                                        />
                                        <Box>
                                            <p>
                                                {' '}
                                                Group Admin: {group.admin.name}
                                            </p>

                                            {showMembers && (
                                                <Box>
                                                    <h5>Group Members:-</h5>
                                                    <ul>
                                                        {group.members?.map(
                                                            (member: User) => (
                                                                <li
                                                                    key={
                                                                        member.userId
                                                                    }
                                                                >
                                                                    <p>
                                                                        Name:{' '}
                                                                        {
                                                                            member.name
                                                                        }
                                                                        <Tooltip
                                                                            title="Delete"
                                                                            color="error"
                                                                            placement="top"
                                                                        >
                                                                            <IconButton
                                                                                onClick={() => {
                                                                                    handleDeleteOpen()
                                                                                    setDeleteData(
                                                                                        {
                                                                                            ...deleteData,
                                                                                            name: String(
                                                                                                `Are you sure you want to remove ${member.name} from the ${group.name} group?`
                                                                                            ),
                                                                                            userGroupId:
                                                                                                String(
                                                                                                    group.userGroupId
                                                                                                ),
                                                                                            userId: String(
                                                                                                member.userId
                                                                                            ),
                                                                                            type: 'member',
                                                                                        }
                                                                                    )
                                                                                }}
                                                                                size="small"
                                                                                style={{
                                                                                    marginLeft:
                                                                                        '15px',
                                                                                }} // Adjust the value as needed
                                                                            >
                                                                                <DeleteOutlined fontSize="small" />
                                                                            </IconButton>
                                                                        </Tooltip>
                                                                    </p>
                                                                </li>
                                                            )
                                                        )}
                                                    </ul>
                                                </Box>
                                            )}
                                            <Divider variant="middle" />
                                        </Box>
                                    </Box>
                                )
                            )
                        ) : (
                            <Box>You have no groups yet.</Box>
                        )}
                    </Box>
                    <DeleteDialogLayout
                        open={deleteDialogOpen}
                        title="Delete Group"
                        text={deleteData?.name}
                        warning="All access rights to projects based on group membership will be revoked!"
                        handleSubmit={handleDeleteSubmit}
                        handleClose={handleDeleteClose}
                        isLoading={isLoading}
                        status={deleteStatus}
                    ></DeleteDialogLayout>
                </DataStateIndicator>
            </Box>
        </Paper>
    )
}
Groups.getNestedLayout = (page) => {
    return <CollaborationsLayout>{page}</CollaborationsLayout>
}
export default Groups
