import React, { KeyboardEvent, useState } from 'react'
import type { NextPage } from 'next'
import { useRouter } from 'next/router'
import PersonOutlineIcon from '@mui/icons-material/PersonOutline'
import { TextField } from '@mui/material'
import { AuthCard, AuthCardTypes } from '../modules/auth/AuthCard'
import { useSignupResendConfirmationCodePostSignupResendConfirmationCodePost as resendConfirmationCode } from '../queries'
import { renderApiErrorMessage } from '../errors'
import { useSetUsernameOrEmail } from '../hooks/useSetUsernameOrEmail'

const ResendCode: NextPage = () => {
    const router = useRouter()
    const [globalError, setGlobalError] = useState('')

    const { confirmationType, helperText, userNameOrEmail, onChange } =
        useSetUsernameOrEmail()
    const { mutate: resendCodeMutation, isPending: isLoading } =
        resendConfirmationCode({
            mutation: {
                onSuccess: async (response) => {
                    router.push({
                        pathname: '/confirm-email',
                        query: { username: response.userName },
                    })
                },
                onError: (error) => {
                    setGlobalError(renderApiErrorMessage(error))
                },
            },
        })

    const handleSubmit = () => {
        resendCodeMutation({
            data: {
                [confirmationType]: userNameOrEmail,
            },
        })
    }

    const handleKeyDown = (event: KeyboardEvent<HTMLInputElement>) => {
        if (event.key === 'Enter' && userNameOrEmail && helperText === '') {
            handleSubmit()
        }
    }

    return (
        <AuthCard
            icon={PersonOutlineIcon}
            loading={isLoading}
            globalError={globalError}
            onClick={handleSubmit}
            disabled={!userNameOrEmail || helperText !== ''}
            fields={[
                <TextField
                    key="pwd-recovery-username"
                    name="username"
                    label="username or email"
                    size="small"
                    error={helperText !== ''}
                    helperText={helperText}
                    value={userNameOrEmail}
                    onChange={onChange}
                    onKeyDown={handleKeyDown}
                    required
                />,
            ]}
            type={AuthCardTypes.RESEND_VERIFICATION_CODE}
        />
    )
}

export default ResendCode
