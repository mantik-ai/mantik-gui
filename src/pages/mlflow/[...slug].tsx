import React, { useEffect, useState } from 'react'
import IFrame from 'react-iframe'
import type { NextPage } from 'next'
import { useSession } from 'next-auth/react'
import {
    MAIN_LAYOUT_MARGIN_BOTTOM,
    MAIN_LAYOUT_MARGIN_TOP,
    MLFLOW_UI_URL,
} from '../../constants'
import { MessageHandler, useIFrameApi } from '../../hooks/useIFrameApi'
import { useProjectsProjectIdGetProjectsProjectIdGet as useGetProject } from '../../queries'
import { Project } from '../../queries/models'
import { DataStateIndicator } from '../../components/DataStateIndicator'

type ProjectLink = [string, string]
type ExperimentLink = [string, string, string, string]
type RunLink = [string, string, string, string, string, string, string]

const isProjectLink = (slug: string[]): slug is ProjectLink =>
    slug.length === 2 || slug.length === 3
const isExperimentLink = (slug: string[]): slug is ExperimentLink =>
    slug.length === 4 || slug.length === 5
const isRunLink = (slug: string[]): slug is RunLink => slug.length === 6

const getExperimentIds = ({ experimentRepositories: experiment }: Project) =>
    experiment ? experiment.map((e) => e.mlflowExperimentId) : []

const MlflowUi: NextPage = () => {
    const { status, data } = useSession({ required: true })
    const [experimentIds, setExperimentIds] = useState<number[] | undefined>()
    const [projectId, setProjectId] = useState<string | null>(null)
    const [experimentId, setExperimentId] = useState<string | null>(null)
    const [runId, setRunId] = useState<string | null>(null)
    const [initialMount, setInitialMount] = useState<boolean>(true)
    const [slug, setSlug] = useState<string[] | null>(null)
    useEffect(() => {
        // Access window.location for the browser url
        const currentUrl = window.location.pathname
        const splitUrl = currentUrl.split('/').slice(2, 8)
        setSlug(splitUrl)
    }, [])
    useEffect(() => {
        if (slug) {
            if (isRunLink(slug)) {
                const [
                    _projects,
                    project,
                    _experiments,
                    experiment,
                    _runs,
                    run,
                ] = slug
                setProjectId(project)
                setExperimentId(experiment)
                setRunId(run)
            } else if (isExperimentLink(slug)) {
                const [_projects, project, _experiments, experiment] = slug
                setProjectId(project)
                setExperimentId(experiment)
            } else if (isProjectLink(slug)) {
                const [_projects, project] = slug
                setProjectId(project)
            }
        }
    }, [slug])

    const setExperimentIdsFromProject = (project: Project) => {
        const ids = getExperimentIds(project)
        setExperimentIds(ids)
        // If navigating to mlflow UI via project & without a selected experiment, navigate to the first experiment of that project
        if (slug && isProjectLink(slug)) setExperimentId(ids[0].toString())
    }

    const { data: project } = useGetProject(projectId as string)

    useEffect(() => {
        if (project) {
            setExperimentIdsFromProject(project)
        }
    }, [project])

    const exampleProcessor: MessageHandler = async (payload) => {
        // Technically the iFrame only loads when the user is authenticated
        // but still, this is explicit
        if (status !== 'authenticated') {
            return { data: 'unauthenticated' }
        }
        switch (payload.message) {
            case 'getUsername':
                return { data: data.user.name ?? 'Unknown' }
            case 'getAccessToken':
                return { data: data.user.accessToken ?? 'Unknown' }
            case 'getExperimentIds':
                return { data: experimentIds }
            case 'getProjectId':
                return { data: projectId }
            case 'getExperimentId':
                return { data: experimentId }
            case 'getRunId':
                return { data: runId }
            case 'getInitialMount':
                return { data: initialMount }
            case 'setInitialMount':
                if (initialMount) {
                    setInitialMount(false)
                    return { data: false }
                }
                return { data: true }
            default:
                return { data: null }
        }
    }
    useIFrameApi(exampleProcessor)

    return (
        <div
            style={{
                height: `calc(100vh - ${MAIN_LAYOUT_MARGIN_TOP} - ${MAIN_LAYOUT_MARGIN_BOTTOM})`,
            }}
        >
            <DataStateIndicator
                status={!!experimentIds ? 'success' : 'pending'}
                text="Loading Experiments..."
                usePaper
            >
                <IFrame
                    url={MLFLOW_UI_URL}
                    width="100%"
                    height="100%"
                    styles={{ border: 0 }}
                />
            </DataStateIndicator>
        </div>
    )
}
export default MlflowUi
