import React from 'react'
import type { NextPage } from 'next'
import { Box, Button, Stack, Typography } from '@mui/material'
import ArrowForwardIcon from '@mui/icons-material/ArrowForward'
import PersonOutlineIcon from '@mui/icons-material/PersonOutline'
import { useSession } from 'next-auth/react'

function FooterContainer({ children }: { children: React.ReactNode }) {
    return (
        <Box
            style={{
                width: '100%',
                marginTop: 'auto',
                position: 'relative',
                bottom: 0,
                left: 0,
                height: 'fit-content',
                lineHeight: 0,
                overflow: 'hidden',
            }}
        >
            {children}
        </Box>
    )
}

function FooterNavigation({ children }: { children: React.ReactNode }) {
    return (
        <Box
            sx={{
                display: 'flex',
                justifyContent: 'space-between',
                alignItems: 'center',
                width: '100%',
                minHeight: '60px',
            }}
        >
            {children}
        </Box>
    )
}

const sponsorImages = [
    { name: 'ambrosys', src: '/sponsors/ambrosys.png' },
    { name: 'eu', src: '/sponsors/eu.png' },
    { name: 'euro-hpc', src: '/sponsors/eurohpc.png' },
    { name: 'bmbf', src: '/sponsors/bmbf.png' },
]

function HomePageFooter() {
    return (
        <FooterContainer>
            <FooterNavigation>
                <Stack
                    direction={'row'}
                    flexWrap={'wrap'}
                    alignItems={'center'}
                    width={'100%'}
                    pb={4}
                    justifyContent={'center'}
                    style={{ height: '100%' }}
                    display={{ xs: 'none', sm: 'flex' }}
                    spacing={6}
                    gap={'2em'}
                >
                    {sponsorImages.map((image) => (
                        <Box
                            key={image.name}
                            component="img"
                            src={image.src}
                            alt={image.name}
                            height={{
                                xl: '90px',
                                lg: '75px',
                                xs: '60px',
                            }}
                        />
                    ))}
                </Stack>
            </FooterNavigation>
        </FooterContainer>
    )
}

const Home: NextPage = () => {
    const session = useSession()

    return (
        <Stack
            alignItems={'center'}
            justifyContent={'center'}
            flexGrow={1}
            spacing={4}
            sx={{
                background: {
                    lg: `
                    url(/images/footer.svg) no-repeat bottom 0 right 0% / contain,
                        url(/images/balloon.svg) no-repeat bottom 0 right 30%,
                        url(/images/cloud1.svg) no-repeat top 12% left -90px,
                        url(/images/cloud2.svg) no-repeat bottom 30% left 16%,
                        url(/images/cloud3.svg) no-repeat top 6% right 40%,
                        url(/images/cloud4.svg) no-repeat right 2% bottom 55%
                    `,
                    md: `
                    url(/images/footer.svg) no-repeat bottom 0 right 0% / contain,
                        url(/images/balloon.svg) no-repeat bottom 0 right 30%,
                        url(/images/cloud1.svg) no-repeat top 16% left -90px,
                        url(/images/cloud2.svg) no-repeat bottom 30% left 16%,
                        url(/images/cloud3.svg) no-repeat top 6% right 40%,
                        url(/images/cloud4.svg) no-repeat right -15% bottom 65%
                    `,
                    sm: `
                    url(/images/footer.svg) no-repeat bottom 0 right 0% / contain,
                        url(/images/balloon.svg) no-repeat bottom 5% right 30%,
                        url(/images/cloud1.svg) no-repeat top 7% left -90px,
                        url(/images/cloud2.svg) no-repeat bottom 6% left 20%,
                        url(/images/cloud4.svg) no-repeat right -15% top 32%,
                    `,
                    xs: `
                    url(/images/footer.svg) no-repeat bottom 0 right 0% / contain,
                        url(/images/balloon.svg) no-repeat bottom 0 right 30%,
                        url(/images/cloud1.svg) no-repeat top 12% left -90px
                    `,
                },
                height: '100%',
            }}
        >
            <Typography
                variant="h1"
                className="fontWeightBold"
                style={{
                    fontSize: 'calc(3.5em + 0.75vw)',
                    paddingBottom: 0,
                    marginTop: 'auto',
                }}
            >
                Mantik
            </Typography>

            <Typography variant="h4" textAlign="center">
                Enhance the AI Modeler&apos;s life,
                <br />
                by solving BIG problems.
            </Typography>

            <Stack spacing={4} direction={{ xs: 'column', sm: 'row' }}>
                <Button
                    style={{
                        color: 'white',
                        minWidth: '260px',
                    }}
                    endIcon={<ArrowForwardIcon />}
                    color="primary"
                    variant="contained"
                    href={'/projects'}
                >
                    Discover
                </Button>
                {!session.data && (
                    <Button
                        style={{
                            minWidth: '260px',
                        }}
                        endIcon={<PersonOutlineIcon />}
                        color="primary"
                        variant="outlined"
                        href={'/register'}
                    >
                        register
                    </Button>
                )}
            </Stack>
            <HomePageFooter />
        </Stack>
    )
}

export default Home
