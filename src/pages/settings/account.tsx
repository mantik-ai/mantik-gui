import React from 'react'
import { SettingsLayout } from '../../modules/settings/layouts/SettingsLayout'
import { NextPageWithNestedLayout } from '../_app'
import AccountContext, { AccountProvider } from '../../context/AccountContext'
import DetailsToolbar from '../../components/DetailsToolbar'
import { Profile } from '../../modules/settings/account/Profile'
import { Email } from '../../modules/settings/account/Email'
import { PasswordReset } from '../../modules/settings/account/PasswordReset'
import { DeleteAccount } from '../../modules/settings/account/DeleteAccount'

const AccountPage: NextPageWithNestedLayout = () => {
    return (
        <AccountProvider>
            <AccountContext.Consumer>
                {(context) => (
                    <>
                        <DetailsToolbar
                            data={context.accountDetailsData}
                            title="Account"
                        />
                        <Profile />
                        <Email />
                        <PasswordReset />
                        <DeleteAccount />
                    </>
                )}
            </AccountContext.Consumer>
        </AccountProvider>
    )
}

AccountPage.getNestedLayout = (page) => {
    return <SettingsLayout>{page}</SettingsLayout>
}

export default AccountPage
