import React from 'react'
import { SettingsLayout } from '../../modules/settings/layouts/SettingsLayout'
import { NextPageWithNestedLayout } from '../_app'
import { ConnectionsOverview } from '../../modules/settings/connections/ConnectionsOverview'

const ConnectionsPage: NextPageWithNestedLayout = () => {
    return <ConnectionsOverview />
}

ConnectionsPage.getNestedLayout = (page) => {
    return <SettingsLayout>{page}</SettingsLayout>
}

export default ConnectionsPage
