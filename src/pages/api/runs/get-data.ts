import { NextApiRequest, NextApiResponse } from 'next'
import { mantikApi } from '../../../modules/auth/axios'

export default async function handler(
    req: NextApiRequest,
    res: NextApiResponse
) {
    if (req.method !== 'POST') return res.status(405).send({})

    try {
        const response = await mantikApi.get(req.body.url, {
            headers: {
                accept: req.headers?.accept as string,
                authorization: req.headers?.authorization as string,
            },
        })
        return res.status(response.status).json({
            ...response.data,
        })
    } catch (err: unknown) {
        return res.status(500).json({ message: err })
    }
}
