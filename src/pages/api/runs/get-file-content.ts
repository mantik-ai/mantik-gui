import { NextApiRequest, NextApiResponse } from 'next'
import axios from 'axios'

export default async function handler(
    req: NextApiRequest,
    res: NextApiResponse
) {
    if (req.method !== 'POST') return res.status(405).send({})

    try {
        const response = await axios.get(req.body.url, {
            headers: {
                Accept: '*/*',
            },
        })
        return res.status(response.status).json({
            data_response: response.data,
        })
    } catch (err: unknown) {
        return res.status(500).json({ message: err })
    }
}
