import {
    CognitoIdentityProviderClient,
    GetUserCommand,
} from '@aws-sdk/client-cognito-identity-provider'
import { NextApiRequest, NextApiResponse } from 'next'
import { AuthPayload } from '../../../modules/auth/authPayload'
import axios from '../../../modules/auth/axios'

export default async function handler(
    req: NextApiRequest,
    res: NextApiResponse
) {
    if (req.method !== 'POST') return res.status(405).send({})

    const cognitoClient = new CognitoIdentityProviderClient({
        region: process.env.COGNITO_REGION,
    })
    try {
        const response = await axios.post(
            `${process.env.NEXT_PUBLIC_API_URL}/mantik/tokens/create`,
            {
                username: req.body.username as string,
                password: req.body.password as string,
            }
        )
        const payload: AuthPayload = response.data
        if (response.status === 201 && payload) {
            const userCmd = new GetUserCommand({
                AccessToken: response.data.AccessToken,
            })
            //The below usage of cognitoClient could be removed when an endpoint fetches the user with the access token at its header.
            payload.User = await cognitoClient.send(userCmd)
        }
        return res.status(response.status).json({
            ...payload,
        })
    } catch (err: any) {
        return res.status(500).json({
            detail: err.response?.data?.detail,
            status: err.response?.status,
        })
    }
}
