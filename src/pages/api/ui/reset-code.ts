import {
    CognitoIdentityProviderClient,
    ForgotPasswordCommand,
} from '@aws-sdk/client-cognito-identity-provider'
import { NextApiRequest, NextApiResponse } from 'next'

const { COGNITO_REGION, COGNITO_CLIENT_ID } = process.env

export default async function handler(
    req: NextApiRequest,
    res: NextApiResponse
) {
    if (req.method !== 'POST') return res.status(405).send({})

    const params = {
        ClientId: COGNITO_CLIENT_ID,
        Username: req.body.username as string | undefined,
    }

    const cognitoClient = new CognitoIdentityProviderClient({
        region: COGNITO_REGION,
    })
    const forgotPasswordCommand = new ForgotPasswordCommand(params)

    try {
        const response = await cognitoClient.send(forgotPasswordCommand)
        return res.status(response['$metadata'].httpStatusCode!).send({})
    } catch (err: unknown) {
        console.log(err)
        if (
            err instanceof Error &&
            '$metadata' in err &&
            typeof err['$metadata'] === 'object' &&
            err['$metadata'] !== null &&
            'httpStatusCode' in err['$metadata'] &&
            typeof err['$metadata'].httpStatusCode === 'number'
        ) {
            return res
                .status(err['$metadata'].httpStatusCode)
                .json({ message: toString() })
        }
        throw err
    }
}
