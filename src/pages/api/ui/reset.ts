import {
    CognitoIdentityProviderClient,
    ConfirmForgotPasswordCommand,
    ConfirmForgotPasswordCommandInput,
} from '@aws-sdk/client-cognito-identity-provider'
import { NextApiRequest, NextApiResponse } from 'next'

const { COGNITO_REGION, COGNITO_CLIENT_ID } = process.env

export default async function handler(
    req: NextApiRequest,
    res: NextApiResponse
) {
    if (req.method !== 'POST') return res.status(405).send({})

    const params = {
        ClientId: COGNITO_CLIENT_ID,
        ConfirmationCode: req.body.code as string | undefined,
        Password: req.body.password as string | undefined,
        Username: req.body.username as string | undefined,
    } satisfies ConfirmForgotPasswordCommandInput

    const cognitoClient = new CognitoIdentityProviderClient({
        region: COGNITO_REGION,
    })
    const confirmForgotPasswordCommand = new ConfirmForgotPasswordCommand(
        params
    )

    try {
        const response = await cognitoClient.send(confirmForgotPasswordCommand)
        return res.status(response['$metadata'].httpStatusCode!).send({})
    } catch (err: unknown) {
        console.log(err)
        if (
            err instanceof Error &&
            '$metadata' in err &&
            typeof err['$metadata'] === 'object' &&
            err['$metadata'] !== null &&
            'httpStatusCode' in err['$metadata'] &&
            typeof err['$metadata'].httpStatusCode === 'number'
        ) {
            return res
                .status(err['$metadata'].httpStatusCode)
                .json({ message: err.toString() })
        }
        throw err
    }
}
