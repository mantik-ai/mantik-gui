import nextAuth, { Account, ISODateString, Session, User } from 'next-auth'
import credentialsProvider from 'next-auth/providers/credentials'
import axios from '../../../modules/auth/axios'
import { MANTIK_PROVIDER_ID } from '../../../constants'
import { isTokenExpired } from '../../../helpers'
import { JWT } from 'next-auth/jwt'
import { GetUserCommandOutput } from '@aws-sdk/client-cognito-identity-provider'

export async function refreshAccessToken(token: JWT) {
    try {
        const response = await axios.post(
            `${process.env.NEXT_PUBLIC_API_URL}/mantik/tokens/refresh`,
            {
                username: token.name,
                refresh_token: token.refreshToken,
            }
        )

        const refreshedTokens: { AccessToken: string; ExpiresAt: string } =
            await response.data

        if (response.status !== 201) {
            throw refreshedTokens
        }

        return {
            ...token,
            accessToken: refreshedTokens.AccessToken,
            accessTokenExpires: refreshedTokens.ExpiresAt,
        }
        // eslint-disable-next-line unused-imports/no-unused-vars
    } catch (error) {
        return {
            ...token,
            error: 'RefreshTokenError' as const,
        }
    }
}

export async function handleJWT({
    token,
    user,
    account,
}: {
    token: JWT
    user?: User
    account: Account | null
}): Promise<JWT> {
    if (account && user) {
        return {
            ...token,
            name: user.User.Username ?? token.name,
            accessToken: user.AccessToken,
            refreshToken: user.RefreshToken,
            accessTokenExpires: user.ExpiresAt,
        }
    }

    if (!isTokenExpired(token.accessToken as string)) {
        return token
    }

    if (!token.refreshToken) throw new TypeError('Missing refresh_token')

    const refreshedToken = await refreshAccessToken(token)

    return refreshedToken
}
export async function handleSession({
    session,
    token,
}: {
    session: Session
    token: JWT
}) {
    session.user.name = token.name
    session.user.accessToken = String(token.accessToken)
    session.user.refreshToken = String(token.refreshToken)
    session.user.accessTokenExpires = String(token.accessTokenExpires)
    session.error = token.error
    return session
}

export default nextAuth({
    providers: [
        credentialsProvider({
            id: MANTIK_PROVIDER_ID,
            name: 'Credentials',
            credentials: {
                username: {
                    label: 'Username',
                    type: 'text',
                },
                password: { label: 'Password', type: 'password' },
            },
            async authorize(credentials, req) {
                try {
                    const res = await axios.post(
                        '/api/ui/login',
                        {
                            username: credentials?.username,
                            password: credentials?.password,
                        },
                        {
                            headers: {
                                accept: '*/*',
                                'Content-Type': 'application/json',
                                cookie: req.headers?.cookie ?? '',
                            },
                        }
                    )

                    if (res.status !== 201) return null
                    const cognitoTokens = res.data as User
                    return cognitoTokens
                    // eslint-disable-next-line unused-imports/no-unused-vars
                } catch (e: unknown) {
                    return null
                }
            },
        }),
    ],
    secret: process.env.NEXTAUTH_SECRET,
    pages: {
        signIn: '/login',
    },
    callbacks: {
        jwt: handleJWT,
        session: handleSession,
    },
})
declare module 'next-auth' {
    /**
     * Returned by `useSession`, `getSession` and received as a prop on the `SessionProvider` React Context
     */
    interface Session {
        user: {
            name?: string | null
            accessToken: string
            refreshToken: string | null
            accessTokenExpires: string | null
        }
        expires: ISODateString
        error?: 'RefreshTokenError'
    }

    interface User {
        AccessToken: string
        ExpiresAt: string
        RefreshToken: string
        User: GetUserCommandOutput
    }
}
// type from the namespace is overwritten as it doesn't reflect our actual extended token that we use for our functionality
declare module 'next-auth/jwt' {
    interface JWT {
        name: string
        accessToken: string
        refreshToken: string
        accessTokenExpires: string
        error?: 'RefreshTokenError'
        iat?: number
        exp?: number
        jti?: string
    }
}
