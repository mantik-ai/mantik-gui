import React, { ChangeEvent, KeyboardEvent, useState } from 'react'
import { useForgotPasswordInitiatePostForgotPasswordInitiatePost as forgotPasswordInitiativePost } from '../queries'
import type { NextPage } from 'next'
import { useRouter } from 'next/router'
import PersonOutlineIcon from '@mui/icons-material/PersonOutline'
import { TextField } from '@mui/material'
import { AuthCard, AuthCardTypes } from '../modules/auth/AuthCard'
import { renderApiErrorMessage } from '../errors'
import { useSetUsernameOrEmail } from '../hooks/useSetUsernameOrEmail'

const PasswordRecovery: NextPage = () => {
    const router = useRouter()
    const [globalError, setGlobalError] = useState('')
    const [userNameOrEmailEdited, setUserNameOrEmailEdited] = useState(false)

    const {
        confirmationType,
        helperText,
        userNameOrEmail,
        onChange: changeUserNameOrEmail,
    } = useSetUsernameOrEmail()

    const { mutate: mutateForgotPasswordInitiative, isPending: isLoading } =
        forgotPasswordInitiativePost({
            mutation: {
                onSuccess: async (response) => {
                    setUserNameOrEmailEdited(false)
                    setGlobalError('')
                    changeUserNameOrEmail({
                        target: { value: '' },
                    } as ChangeEvent<HTMLInputElement>)
                    await router.push({
                        pathname: '/password-reset',
                        query: {
                            username: response.userName,
                        },
                    })
                },
                onError: (error) => {
                    if (error.response?.status === 424) {
                        setGlobalError(
                            'You must verify your email to reset your password.'
                        )
                        return
                    }
                    setGlobalError(renderApiErrorMessage(error))
                },
            },
        })

    const handleClickSendPassword = () => {
        mutateForgotPasswordInitiative({
            data: {
                [confirmationType]: userNameOrEmail,
            },
        })
    }

    const handleKeyDown = (event: KeyboardEvent<HTMLInputElement>) => {
        if (event.key === 'Enter' && !!userNameOrEmail) {
            handleClickSendPassword()
        }
    }

    return (
        <AuthCard
            icon={PersonOutlineIcon}
            loading={isLoading}
            globalError={globalError === '' ? undefined : globalError}
            onClick={handleClickSendPassword}
            disabled={!userNameOrEmail}
            fields={[
                <TextField
                    key="pwd-recovery-username"
                    name="username"
                    label="username or email"
                    size="small"
                    error={userNameOrEmailEdited && !userNameOrEmail}
                    helperText={helperText}
                    value={userNameOrEmail}
                    onKeyDown={handleKeyDown}
                    onChange={(e) => {
                        changeUserNameOrEmail(e)
                        setUserNameOrEmailEdited(true)
                    }}
                    required
                />,
            ]}
            type={AuthCardTypes.FORGOT_PWD}
        />
    )
}

export default PasswordRecovery
