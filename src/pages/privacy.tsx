import React from 'react'
import MarkdownComponent from '../components/MarkdownComponent'
import privacyContent from '../content/privacy-content.md'

function Privacy() {
    return <MarkdownComponent content={privacyContent} title={'Privacy'} />
}

export default Privacy
