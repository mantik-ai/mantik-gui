import React from 'react'
import MarkdownComponent from '../components/MarkdownComponent'
import aboutContent from '../content/about-content.md'

function About() {
    return <MarkdownComponent content={aboutContent} title={'About'} />
}

export default About
