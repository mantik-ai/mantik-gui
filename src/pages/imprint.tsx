import React from 'react'
import imprintContent from '../content/imprint-content.md'
import MarkdownComponent from '../components/MarkdownComponent'

function Imprint() {
    return <MarkdownComponent content={imprintContent} title={'Imprint'} />
}
export default Imprint
