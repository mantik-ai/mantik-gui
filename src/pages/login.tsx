import React, { ChangeEvent, useEffect, useState } from 'react'
import type { NextPage } from 'next'
import { useRouter } from 'next/router'
import { IconButton, InputAdornment, TextField } from '@mui/material'
import LockOutlinedIcon from '@mui/icons-material/LockOutlined'
import { signIn, useSession } from 'next-auth/react'
import { MANTIK_PROVIDER_ID } from '../constants'
import { AuthCard, AuthCardTypes } from '../modules/auth/AuthCard'
import Visibility from '@mui/icons-material/Visibility'
import VisibilityOff from '@mui/icons-material/VisibilityOff'
import { generateUserIdFromAccessToken } from '../helpers'
import axios from '../modules/auth/axios'
import { renderApiErrorMessage } from '../errors'

enum AwsErrors {
    LIMIT = 'Password attempts exceeded',
}

export const ErrorType: { [key: string]: string } = {
    EMAIL_UNVERIFIED: 'You must verify your email to log in.',
    LIMIT: 'Password attempts exceeded. Please wait 30 seconds and try again.',
    PASSWORD_EMPTY: 'Password cannot be empty.',
    USERNAME_INCORRECT: 'Please enter a valid email/username.',
    OTHER: 'Something went wrong. Please check and try again.',
}

interface Credentials {
    username: string
    password: string
}

const blankCredentials: Credentials = {
    username: '',
    password: '',
}

const Login: NextPage = () => {
    const router = useRouter()
    const session = useSession()
    const [loading, setLoading] = useState(false)
    const [globalError, setGlobalError] = useState('')
    const [unverifiedEmail, setUnverifiedEmail] = useState(false)
    const [errorState, setErrorState] = useState<Credentials>(blankCredentials)
    const [formState, setFormState] = useState<Credentials>(blankCredentials)
    const [showPassword, setShowPassword] = useState(false)
    const handleClickShowPassword = () => setShowPassword(!showPassword)
    const handleMouseDownPassword = () => setShowPassword(!showPassword)
    const onChange = (
        e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
    ) => {
        const { name, value } = e.target
        setFormState((prev) => ({ ...prev, [name]: value }))
        if (globalError !== ErrorType.LIMIT || name === 'username') {
            // reduces excessive password attempt block but allow changing username
            setGlobalError('')
        }
        setUnverifiedEmail(false)
        setErrorState((prev) => {
            const stateObj = { ...prev, [name]: '' }
            switch (name) {
                case 'username':
                    if (value === '') {
                        stateObj[name] = ErrorType.USERNAME_INCORRECT
                    }
                    break
                case 'password':
                    if (value === '') {
                        stateObj[name] = ErrorType.PASSWORD_EMPTY
                    }
                    break
                default:
                    break
            }
            return stateObj
        })
    }
    const handleFormSubmit = async () => {
        setLoading(true)
        const result = await signIn(MANTIK_PROVIDER_ID, {
            ...formState,
            redirect: false,
        })
        if (result?.ok) {
            await router.push('/')
        } else {
            // possible to replace with orval function
            try {
                await axios.post('/api/ui/login', formState, {
                    headers: {
                        accept: '*/*',
                        'Content-Type': 'application/json',
                    },
                })
            } catch (e: any) {
                const responseData = e?.response?.data
                const detail = responseData?.detail
                const status = responseData?.status

                if (status === 403) {
                    setGlobalError(ErrorType.EMAIL_UNVERIFIED)
                    setUnverifiedEmail(true)
                } else if (typeof detail === 'string') {
                    if (detail === AwsErrors.LIMIT) {
                        setGlobalError(ErrorType.LIMIT)
                        // TODO: use backend-generated error messages when ready
                        setTimeout(() => setGlobalError(''), 30000) // reduces user block from excessive password attempts for 30 seconds.
                    } else {
                        setGlobalError(renderApiErrorMessage(e))
                    }
                } else {
                    setGlobalError(ErrorType.OTHER)
                }
            }
        }
        setLoading(false)
    }
    const handleKeyDown = (event: React.KeyboardEvent<HTMLInputElement>) => {
        // reduces user block from excessive password attempts
        if (globalError !== ErrorType.LIMIT) {
            if (event.key === 'Enter') {
                handleFormSubmit()
            }
        }
    }
    useEffect(() => {
        if (session.status === 'authenticated') {
            const userId = generateUserIdFromAccessToken(
                session.data?.user.accessToken
            )
            if (userId) {
                sessionStorage.setItem('userId', userId)
            } else {
                console.log('Failed to extract sub from access token.')
            }
        }
    }, [session.status])
    return (
        <AuthCard
            icon={LockOutlinedIcon}
            loading={loading}
            globalError={globalError === '' ? undefined : globalError}
            disabled={
                Object.values(errorState).some((value) => value !== '') ||
                Object.values(formState).some((value) => value === '') ||
                globalError !== ''
            }
            unverifiedEmail={unverifiedEmail}
            onClick={handleFormSubmit}
            fields={[
                <TextField
                    key="login-username"
                    name="username"
                    label="username or email"
                    size="small"
                    error={errorState.username !== ''}
                    helperText={
                        errorState.username === '' ? null : errorState.username
                    }
                    value={formState.username}
                    onChange={onChange}
                    onKeyDown={handleKeyDown}
                    required
                />,
                <TextField
                    key="login-password"
                    name="password"
                    label="password"
                    size="small"
                    type={showPassword ? 'text' : 'password'}
                    error={errorState.password !== ''}
                    helperText={
                        errorState.password === '' ? null : errorState.password
                    }
                    value={formState.password}
                    onChange={onChange}
                    onKeyDown={handleKeyDown}
                    InputProps={{
                        endAdornment: (
                            <InputAdornment position="end">
                                <IconButton
                                    aria-label="toggle password visibility"
                                    onClick={handleClickShowPassword}
                                    onMouseDown={handleMouseDownPassword}
                                >
                                    {showPassword ? (
                                        <Visibility />
                                    ) : (
                                        <VisibilityOff />
                                    )}
                                </IconButton>
                            </InputAdornment>
                        ),
                    }}
                    required
                />,
            ]}
            type={AuthCardTypes.LOGIN}
        />
    )
}

export default Login
