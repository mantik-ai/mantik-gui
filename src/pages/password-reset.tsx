import React, { KeyboardEvent, useState } from 'react'
import type { NextPage } from 'next'
import { useRouter } from 'next/router'
import PersonOutlineIcon from '@mui/icons-material/PersonOutline'
import { Button, FormHelperText, TextField } from '@mui/material'
import { AuthCard, AuthCardTypes } from '../modules/auth/AuthCard'
import { usePasswordValidator } from '../hooks/usePasswordValidator'
import { renderApiErrorMessage } from '../errors'
import { useForgotPasswordConfirmPutForgotPasswordConfirmPut } from '../queries'
import { useSetNewPassword } from '../hooks/useSetNewPassword'
import { NewPasswordFieldgroup } from '../components/NewPasswordFieldgroup'

const PasswordReset: NextPage = () => {
    const router = useRouter()
    const [globalError, setGlobalError] = useState('')
    const [globalSuccess, setGlobalSuccess] = useState('')
    const [confirmationCode, setConfirmationCode] = useState('')
    const [confirmationCodeError, setConfirmationCodeError] = useState('')
    const [resetPassword, setResetPassword] = useState(false)
    const newPasswordProps = useSetNewPassword(resetPassword)
    const { password, confirmPassword, confirmPasswordErrorMessage } =
        newPasswordProps
    const { passwordRequirements, error: passwordError } = usePasswordValidator(
        password,
        resetPassword
    )
    const isDisabled =
        passwordError ||
        !!confirmPasswordErrorMessage ||
        !confirmPassword ||
        !password ||
        !confirmationCode

    let timeoutId: NodeJS.Timeout

    const { mutate: mutatePasswordConfirm, isPending: isLoading } =
        useForgotPasswordConfirmPutForgotPasswordConfirmPut({
            mutation: {
                onSuccess: async () => {
                    setGlobalSuccess('Password changed successfully!')
                    setResetPassword(true)
                    setConfirmationCode('')
                    timeoutId = setTimeout(() => {
                        router.push('/login')
                    }, 10000)
                },
                onError: (error) => {
                    setGlobalError(renderApiErrorMessage(error))
                },
            },
        })

    const handleKeyDown = (event: KeyboardEvent<HTMLInputElement>) => {
        if (event.key === 'Enter' && !isDisabled) {
            handleSubmit()
        }
    }

    const handleSubmit = () => {
        mutatePasswordConfirm({
            data: {
                userName: router.query.username as string,
                confirmationCode,
                newPassword: confirmPassword,
            },
        })
    }

    const goToLogin = (
        <>
            <FormHelperText>{globalSuccess}</FormHelperText>
            <Button
                variant="contained"
                onClick={() => {
                    clearTimeout(timeoutId)
                    router.push('/login')
                }}
            >
                Return to login
            </Button>
        </>
    )

    return (
        <AuthCard
            icon={PersonOutlineIcon}
            loading={isLoading}
            globalError={globalError}
            globalSuccess={globalSuccess === '' ? undefined : goToLogin}
            onClick={handleSubmit}
            disabled={isDisabled}
            fields={
                !resetPassword
                    ? [
                          <TextField
                              key="pwd-recovery-code"
                              name="code"
                              label="code"
                              size="small"
                              error={confirmationCodeError !== ''}
                              helperText={confirmationCodeError}
                              value={confirmationCode}
                              onChange={(e) => {
                                  setConfirmationCode(e.target.value)
                                  setConfirmationCodeError(
                                      !e.target.value
                                          ? 'Code must not be empty'
                                          : ''
                                  )
                              }}
                              onKeyDown={handleKeyDown}
                              required
                          />,
                          <NewPasswordFieldgroup
                              key="password-reset"
                              handleKeyDown={handleKeyDown}
                              {...newPasswordProps}
                              passwordRequirements={passwordRequirements}
                              passwordError={passwordError}
                          />,
                      ]
                    : []
            }
            type={AuthCardTypes.PASSWORD_RESET}
        />
    )
}

export default PasswordReset
