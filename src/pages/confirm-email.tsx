import React, { ChangeEvent, KeyboardEvent, useState } from 'react'
import type { NextPage } from 'next'
import { useRouter } from 'next/router'
import PersonOutlineIcon from '@mui/icons-material/PersonOutline'
import { Button, FormHelperText, TextField } from '@mui/material'
import { AuthCard, AuthCardTypes } from '../modules/auth/AuthCard'
import { useSignupVerifyEmailPostSignupVerifyEmailPost as verifyEmail } from '../queries'
import { renderApiErrorMessage } from '../errors'

const ConfirmEmail: NextPage = () => {
    const router = useRouter()
    const [globalError, setGlobalError] = useState('')
    const [globalSuccess, setGlobalSuccess] = useState('')
    const [confirmationCode, setConfirmationCode] = useState('')
    let timeoutId: NodeJS.Timeout

    const { mutate: confirmEmail, isPending: isLoading } = verifyEmail({
        mutation: {
            onSuccess: () => {
                setConfirmationCode('')
                setGlobalSuccess('Email verification successful!')
                timeoutId = setTimeout(() => {
                    router.push('/login')
                }, 10000)
            },
            onError: (error) => {
                setConfirmationCode('')
                setGlobalError(renderApiErrorMessage(error))
            },
        },
    })

    const goToLogin = (
        <>
            <FormHelperText>{globalSuccess}</FormHelperText>
            <Button
                variant="contained"
                onClick={() => {
                    clearTimeout(timeoutId)
                    router.push('/login')
                }}
            >
                Return to login
            </Button>
        </>
    )

    const onChange = (
        e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
    ) => {
        const { value } = e.target
        setGlobalError('')
        setConfirmationCode(value)
    }

    const handleSubmit = () => {
        confirmEmail({
            data: {
                confirmationCode,
                userName: router.query.username as string,
            },
        })
    }

    const handleKeyDown = (event: KeyboardEvent<HTMLInputElement>) => {
        if (event.key === 'Enter' && confirmationCode) {
            handleSubmit()
        }
    }

    return (
        <AuthCard
            icon={PersonOutlineIcon}
            loading={isLoading}
            globalError={globalError === '' ? undefined : globalError}
            globalSuccess={globalSuccess === '' ? undefined : goToLogin}
            onClick={handleSubmit}
            disabled={!confirmationCode}
            fields={[
                <TextField
                    key="register-code"
                    name="code"
                    label="code"
                    size="small"
                    value={confirmationCode}
                    onChange={onChange}
                    onKeyDown={handleKeyDown}
                    required
                />,
            ]}
            type={AuthCardTypes.CONFIRM}
        />
    )
}

export default ConfirmEmail
