import React from 'react'
import MarkdownComponent from '../components/MarkdownComponent'
import contactContent from '../content/contact-page.md'

function Contact() {
    return <MarkdownComponent content={contactContent} title={'Contact'} />
}

export default Contact
