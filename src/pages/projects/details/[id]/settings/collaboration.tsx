import React from 'react'
import { NextPageWithNestedLayout } from '../../../../_app'
import { DetailsLayout } from '../../../../../modules/project_details/layouts/DetailsLayout'
import { Paper } from '@mui/material'
import { TabLayout } from '../../../../../layouts/TabLayout'
import MembersSettings from '../../../../../modules/project_details/settings/components/MembersSettings'
import { GroupsAndOrganizationsSettings } from '../../../../../modules/project_details/settings/components/GroupsAndOrganizationsSettings'

const CollaborationSettings: NextPageWithNestedLayout = () => {
    const tabsData = [
        { label: 'Members', index: 0, component: <MembersSettings /> },
        {
            label: 'Groups/Organizations',
            index: 1,
            component: <GroupsAndOrganizationsSettings />,
        },
    ]
    return (
        <Paper sx={{ padding: '1em' }}>
            <TabLayout tabsData={tabsData} />
        </Paper>
    )
}

CollaborationSettings.getNestedLayout = (page) => {
    return <DetailsLayout>{page}</DetailsLayout>
}

export default CollaborationSettings
