import React from 'react'
import { NextPageWithNestedLayout } from '../../../../_app'
import { DetailsLayout } from '../../../../../modules/project_details/layouts/DetailsLayout'
import ProjectSettings from '../../../../../modules/project_details/settings/ProjectSettings'
import { ProjectDialogProvider } from '../../../../../modules/projects_overview/contexts/ProjectDialogContext'

const SettingsDetails: NextPageWithNestedLayout = () => {
    return <ProjectSettings />
}

SettingsDetails.getNestedLayout = (page) => {
    return (
        <DetailsLayout>
            <ProjectDialogProvider>{page}</ProjectDialogProvider>
        </DetailsLayout>
    )
}

export default SettingsDetails
