import React from 'react'
import { NextPageWithNestedLayout } from '../../../../_app'
import { DetailsLayout } from '../../../../../modules/project_details/layouts/DetailsLayout'
import { RunSchedulesTable } from '../../../../../modules/project_details/runs/runSchedules/RunSchedulesTable'

const RunSchedules: NextPageWithNestedLayout = () => {
    return <RunSchedulesTable></RunSchedulesTable>
}

RunSchedules.getNestedLayout = (page) => {
    return <DetailsLayout>{page}</DetailsLayout>
}

export default RunSchedules
