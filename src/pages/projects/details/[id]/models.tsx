import React from 'react'
import { NextPageWithNestedLayout } from '../../../_app'
import { DetailsLayout } from '../../../../modules/project_details/layouts/DetailsLayout'
import { ModelDetailsView } from '../../../../modules/project_details/models/ModelDetailsView'

const ModelDetails: NextPageWithNestedLayout = () => {
    return <ModelDetailsView />
}

ModelDetails.getNestedLayout = (page) => {
    return <DetailsLayout>{page}</DetailsLayout>
}

export default ModelDetails
