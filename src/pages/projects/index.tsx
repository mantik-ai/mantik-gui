import React, { useContext } from 'react'
import type { NextPage } from 'next'
import { Box, Button, useTheme } from '@mui/material'
import { SearchSideBar } from '../../modules/projects_overview/components/SearchSideBar'
import { ProjectList } from '../../modules/projects_overview/components/ProjectList'
import { SearchParameterProvider } from '../../modules/projects_overview/contexts/SearchParameterContext'
import { Add } from '@mui/icons-material'
import { AddProject } from '../../queries'
import ProjectDialogContext, {
    ProjectDialogProvider,
} from '../../modules/projects_overview/contexts/ProjectDialogContext'
import { ProjectDialog } from '../../modules/projects_overview/ProjectDialog'
import { useSession } from 'next-auth/react'
import { PageHeading } from '../../components/PageHeading'

const Projects: NextPage = () => {
    const { data: session } = useSession()
    const theme = useTheme()

    const [openProjectDialog, setOpenProjectDialog] = React.useState(false)
    const projectContext = useContext(ProjectDialogContext)
    const openDialog = (project?: AddProject) => {
        setOpenProjectDialog(true)
        if (project && projectContext.setProject)
            projectContext.setProject(project)
    }
    return (
        <SearchParameterProvider>
            <Box
                sx={{
                    m: theme.spacing(4),
                    display: 'flex',
                    flexDirection: 'column',
                }}
            >
                <PageHeading
                    description="Search through all projects accessible to you."
                    right={
                        session ? (
                            <Button
                                variant="outlined"
                                onClick={() => openDialog()}
                            >
                                <Add></Add>CREATE PROJECT
                            </Button>
                        ) : (
                            'Please log in to create a project'
                        )
                    }
                >
                    Projects
                </PageHeading>

                <Box
                    sx={{
                        display: 'flex',
                        flexDirection: { xs: 'column', md: 'row' },
                    }}
                >
                    <Box flex={1}>
                        <SearchSideBar></SearchSideBar>
                    </Box>
                    <Box
                        sx={{
                            display: 'flex',
                            flexDirection: 'column',
                            p: 4,
                            pr: 0,
                        }}
                        flex={3}
                        color={'blue'}
                    >
                        <ProjectList></ProjectList>
                    </Box>
                </Box>
            </Box>
            <ProjectDialogProvider>
                <ProjectDialog
                    projectId={''} //TODO: set programmatically after api-spec fix
                    open={openProjectDialog}
                    setOpen={setOpenProjectDialog}
                ></ProjectDialog>
            </ProjectDialogProvider>
        </SearchParameterProvider>
    )
}

export default Projects
