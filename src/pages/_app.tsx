import React, { ReactElement, ReactNode } from 'react'
import { NextPage } from 'next'
import type { AppProps } from 'next/app'
import { QueryClient, QueryClientProvider } from '@tanstack/react-query'
import { ReactQueryDevtools } from '@tanstack/react-query-devtools'
import { SessionProvider } from 'next-auth/react'
import { CssBaseline, ThemeProvider } from '@mui/material'
import axios from 'axios'
import getConfig from 'next/config'
import MainLayout from '../layouts/MainLayout'
import { defaultTheme } from '../themes/default'
import '@fontsource/blinker/300.css'
import '@fontsource/blinker/400.css'
import '@fontsource/blinker/600.css'
import '@fontsource/blinker/700.css'
import { AuthProvider } from '../context/AuthContext'
import { useRUMMonitoring } from '../utils/handleRumMonitoring'
import { useCookieConsent } from '../hooks/useCookieConsent'
import * as CookieConsent from 'vanilla-cookieconsent'

export type NextPageWithNestedLayout = NextPage & {
    getNestedLayout?: (page: ReactElement) => ReactNode
}

type AppPropsWithNestedLayout = AppProps & {
    Component: NextPageWithNestedLayout
}

const { publicRuntimeConfig } = getConfig()
axios.defaults.baseURL = publicRuntimeConfig.apiBaseUrl

if (publicRuntimeConfig.mockDynamically) {
    axios.defaults.headers.common.dynamic = true
    axios.defaults.headers.common.Prefer = 'code=200, dynamic=true'
}

function MantikApp({
    Component,
    pageProps: { session, ...pageProps },
}: AppPropsWithNestedLayout) {
    const initialConsentValue = CookieConsent.acceptedCategory('analytics')
    const { enable, disable } = useRUMMonitoring(initialConsentValue)

    useCookieConsent({ enableAnalytics: enable, disableAnalytics: disable })

    const [queryClient] = React.useState(
        () =>
            new QueryClient({
                defaultOptions: {
                    queries: {
                        refetchOnWindowFocus: false, // Disable automatic refetching on window focus
                        retry: 1, // Re-fetch attempts after initial fetch fails
                    },
                },
            })
    )
    const getNestedLayout = Component.getNestedLayout ?? ((page) => page)

    return (
        <QueryClientProvider client={queryClient}>
            <ThemeProvider theme={defaultTheme}>
                <SessionProvider session={session}>
                    <CssBaseline />
                    <AuthProvider>
                        <MainLayout>
                            {getNestedLayout(<Component {...pageProps} />)}
                        </MainLayout>
                    </AuthProvider>
                    <ReactQueryDevtools initialIsOpen={false} />
                </SessionProvider>
            </ThemeProvider>
        </QueryClientProvider>
    )
}

export default MantikApp
