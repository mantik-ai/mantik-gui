# Terms and Conditions

This agreement (‘Terms’) describes your ('User') rights and responsibilities when accessing the Services of Mantik MLOps development platform ('Mantik') consisting of the web-browser application available under the domain https://cloud.mantik.ai/ and the Mantik client available via PyPi under https://pypi.org/project/mantik/ (‘Mantik’ for web-browser and client). By indicating your acceptance to these Terms, or enabling or using a any Mantik Service, you agree to the following terms.

This Agreement pertains to the utilization of Mantik as supplied by 'Ambrosys' (Ambrosys GmbH, Gesellschaft für Management komplexer Systeme, Marlene-Dietrich-Allee 16, 14482 Potsdam). The user shall be responsible for any additional services required, including but not limited to other cloud services, data storages, and computing facilities. Such additional services shall be at the expense of the user.

## No performance or uptime warranties

User expressly acknowledges and agrees that the applicable Services licensed hereunder may contain defects, and are provided on an "as-is" and "as-available" basis with respect to their performance, speed, functionality, support, and availability. Customers must safeguard important data, use caution, and not rely in any way on the correct functioning and performance of the Services. Ambrosys disclaim all warranties, express or implied, including, without limitation, all implied warranties of merchantability, fitness for a particular purpose, title, and non-infringement. No information or advice, whether oral or written, provided by Ambrosys shall create any warranty not expressly made herein. Ambrosys will have no liability or obligation for any harm or damage arising from deficiencies therewith.

## Feedback and Feature request

The User may provide ongoing feedback to Ambrosys regarding the Services and reports the malfunctioning via the channels provided by Ambrosys. Further, Ambrosys grants the User the right to submit feature requests for consideration by Ambrosys. The User is encouraged to submit feature requests for consideration through the designated channels provided by Ambrosys. Feature requests need to include a detailed description of the proposed feature, its potential benefits and use cases for the User. Ambrosys may communicate the User regarding their feature requests for clarification or additional information. Ambrosys retains the sole discretion to review, prioritize with regard to the product roadmap, and implement feature requests. Updates on the status of feature requests may be provided at the discretion of Ambrosys. Ambrosys is not obligated to implement any feature requests. Ambrosys is not liable for any damages or losses arising from the non-implementation of feature requests.

The User grants Ambrosys an unlimited, irrevocable, perpetual, sublicensable, royalty-free licence to use any such feedback or suggestions for any purpose without any obligation or compensation to the User.

## Modifications

As our business evolves, we may change these Terms. If we make a material change to the Terms, we will provide the User with reasonable notice prior to the change taking effect by emailing the email address associated with the User’s account. The materially revised Contract will become effective on the date set forth in our notice, and all other changes will become effective upon posting of the change. If the User accesses or uses a Service after the effective date, that use will constitute the Customer’s acceptance of any revised terms and conditions.
