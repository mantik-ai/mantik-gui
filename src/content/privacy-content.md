# Data Privacy

This data protection declaration explains to you the type, scope and purpose of the processing of personal data (hereinafter referred to as "data") within our online offer and the associated websites, functions and contents as well as external online presences, e.g. our social media profile. (hereinafter jointly referred to as "online offer"). With regard to the terms used, such as "processing" or "person responsible", we refer to the definitions in Art. 4 of the Basic Data Protection Ordinance (DSGVO).

## Person responsible

Ambrosys GmbH, Marlene-Dietrich-Allee 16, 14482 Potsdam
Fon: +49 331 27319540<br>
info@ambrosys.de<br>
General manager: Dr. Markus Abel, Dr. Markus Quade

## Types of data processed:

- inventory data (e.g., names, addresses).
- Contact details (e.g., e-mail, telephone numbers).
- Content data (e.g., text input, photographs, videos).
- Usage data (e.g., visited websites, interest in content, access times).
- Meta/communication data (e.g., device information, IP addresses).
- Mantik User and Customer Data: data relating to the use of our Machine Learning Production Platform or any part thereof (the “Platform”) by customers (or on behalf of customers) (“Customer Users”). This data may include:
    - Account and activity data relating to individuals who use the Platform.
    - Data which we analyze, process and manage on our customers’ behalf, in connection with the Platform. Specifically, this is the data contained in e-mails and other communications and content, as well as data submitted by our customers when using the Platform.

## Categories of affected persons

Visitors and users of the online offer (hereinafter we refer to the affected persons in summary also as "users").

## Purpose of the processing

- Provision of the online offer, its functions and contents.
- Response to contact requests and communication with users.
- Security measures.
- Reach measurement/Marketing

## Terms used

"personal data" are all information relating to an identified or identifiable natural person (hereinafter referred to as "data subject"); a natural person is considered as identifiable, which can be identified directly or indirectly, in particular by means of assignment to an identifier such as a name, to an identification number, to location data, to an online identifier (eg cookie) or to one or more special features, that express the physical, physiological, genetic, mental, economic, cultural or social identity of this natural person. "Processing" means any process performed with or without the aid of automated procedures or any such process associated with personal data. The term goes far and includes virtually every handling of data. "Responsible person" means the natural or legal person, public authority, body or body that decides, alone or in concert with others, on the purposes and means of processing personal data.

## Relevant legal bases

In accordance with Art. 13 GDPR, we inform you about the legal basis of our data processing. Unless the legal basis in the data protection declaration is mentioned, the following applies: The legal basis for obtaining consent is Article 6 (1) lit. a and Art. 7 DSGVO, the legal basis for the processing for the performance of our services and the execution of contractual measures as well as the response to inquiries is Art. 6 (1) lit. b DSGVO, the legal basis for processing in order to fulfill our legal obligations is Art. 6 (1) lit. c DSGVO, and the legal basis for processing in order to safeguard our legitimate interests is Article 6 (1) lit. f DSGVO. In the event that vital interests of the data subject or another natural person require the processing of personal data, Art. 6 para. 1 lit. d DSGVO as legal basis.

## Relevant legal bases

In accordance with Art. 13 DSGVO, we inform you of the legal bases of our data processing. If the legal basis is not mentioned in the data protection declaration, the following applies: The legal basis for obtaining consents is Art. 6 para. 1 lit. a and Art. 7 DSGVO, the legal basis for processing for the performance of our services and performance of contractual measures as well as for answering inquiries is Art. 6 para. 1 lit. b DSGVO, the legal basis for processing to fulfil our legal obligations is Art. 6 para. 1 lit. c DSGVO, and the legal basis for processing to protect our legitimate interests is Art. 6 para. 1 lit. f DSGVO. In the event that the vital interests of the data subject or another natural person require the processing of personal data, Article 6(1)(d) DSGVO serves as the legal basis.

## Collaboration with processors and third parties

If, in the course of our processing, we disclose data to other persons and companies (contract processors or third parties), transmit them to them or otherwise grant access to the data, this is done only on the basis of a legal permit (e.g. if a transmission the data to third parties, in accordance with Article 6 paragraph 1 letter b DSGVO is required to fulfill the contract), you have consented to a legal obligation or based on our legitimate interests (e.g. the use of agents, webhosters , etc.). We note at this point that Mantik services are cloud hosted using services offered by AWS. If we commission third parties to process data on the basis of a so-called "contract processing contract", this is done on the basis of Art. 28 GDPR.

## Transmission to third countries

We process data in European Union (EU) or the European Economic Area (EEA). If we process data in a third country (i.e. outside EU or EEA) or if this occurs in the context of the use of third-party services or disclosure or transfer of data to third parties, this shall only take place if it occurs in order to fulfil our (pre)contractual obligations, on the basis of your consent, on the basis of a legal obligation or on the basis of our legitimate interests. Subject to legal or contractual permissions, we process or leave the data in a third country only if the special requirements of Art. 44 ff. Process DSGVO. This means, for example, processing is carried out on the basis of special guarantees, such as the officially recognised determination of a data protection level corresponding to the EU (e.g. for the USA by the "Privacy Shield") or compliance with officially recognised special contractual obligations (so-called "standard contractual clauses").

## Rights of data subjects

You have the right to request confirmation as to whether the data concerned are being processed and to request information about these data as well as further information and a copy of the data in accordance with Art. 15 DSGVO. In accordance with Article 16 of the DSBER, you have the right to request the completion of data concerning you or the correction of inaccurate data concerning you. You have the right, in accordance with Art. 17 DSGVO, to demand that relevant data be deleted immediately or, alternatively, to demand a restriction on the processing of the data in accordance with Art. 18 DSGVO. You have the right to request that the data concerning you that you have provided to us be received in accordance with Art. 20 DSGVO and to request its transmission to other persons responsible. You also have the right to file a complaint with the competent supervisory authority pursuant to Art. 77 DSGVO.

## Withdrawal

You have the right to consent granted in accordance with. Revoke Art. 7 para. 3 GDPR with effect for the future
Right to object
You can object to the future processing of your data in accordance with Art. 21 GDPR at any time. The objection may in particular be made against processing for direct marketing purposes.

## Cookies

"Cookies" are small files that are stored on users' computers. Different data can be stored within the cookies. A cookie is primarily used to store information about a user (or the device on which the cookie is stored) during or after his or her visit to an online offer. Temporary cookies, or "session cookies" or "transient cookies", are cookies that are deleted after a user leaves an online offer and closes his browser. In such a cookie, for example, the content of a shopping basket can be stored in an online shop or a login jam. Cookies are referred to as "permanent" or "persistent" and remain stored even after the browser is closed. For example, the login status can be saved when users visit it after several days. Likewise, the interests of users used for range measurement or marketing purposes may be stored in such a cookie."Third-party cookies" are cookies that are offered by providers other than the person responsible for operating the online offer (otherwise, if they are only its cookies, they are referred to as "first-party cookies"). Wir may use temporary and permanent cookies and clarify this within the scope of our data protection declaration. If users do not want cookies to be stored on their computer, they are asked to deactivate the corresponding option in the system settings of their browser. Stored cookies can be deleted in the system settings of the browser. The exclusion of cookies can lead to functional restrictions of this online offer. Furthermore, the storage of cookies can be achieved by deactivating them in the browser settings. Please note that in this case not all functions of this online offer can be used.

We do not make use of cookies for online marketing purposes.

## Deletion of data

The data processed by us will be deleted or their processing restricted in accordance with Articles 17 and 18 DSGVO. Unless expressly stated in this data protection declaration, the data stored by us will be deleted as soon as it is no longer required for its intended purpose and the deletion does not conflict with any statutory storage obligations. If the data are not deleted because they are necessary for other and legally permissible purposes, their processing is restricted. This means that the data is blocked and not processed for other purposes. This applies, for example, to data that must be retained for commercial or tax reasons. In particular in Germany for 6 years in accordance with § 257 (1) HGB (trading books, inventories, opening balance sheets, annual financial statements, commercial letters, accounting documents, etc.) and for 10 years in accordance with § 147 (1) AO (books, records, management reports, accounting documents, commercial and business letters, documents relevant to taxation, etc.). According to § 132 para. 1 BAO (accounting documents, receipts/invoices, accounts, receipts, business papers, statement of income and expenses, etc.), for 22 years in connection with real estate and for 10 years for documents in connection with electronically provided services, telecommunications, radio and television services, which are provided to non-entrepreneurs in EU member states and for which the Mini-One-Stop-Shop (MOSS) is used.

## Hosting

We use Amazon Web Service for our service. The hosting service we use serve to provide the following services: Infrastructure and platform services, computing capacity, storage space and database services, security services and technical maintenance services that we use for the purpose of operating this online offering. For this, we and our hosting provider process inventory data, contact data, content data, contract data, usage data, meta- and communication data of customers, interested parties and visitors of this online offer on the basis of our legitimate interests in an efficient and secure provision of this online offer according to Art. 6 para. 1 lit. f DSGVO in connection with. Art. 28 DSGVO (conclusion of order processing contract).

## Collection of access data and log files

We, or our hosting provider, collect data on the basis of our legitimate interests within the meaning of Art. 6 (1) (f). DSGVO data on each access to the server on which this service is located (so-called server log files). Access data includes the name of the accessed website, file, date and time of access, transferred data volume, notification of successful access, browser type and version, the user's operating system, referrer URL (the previously visited page), IP address and the requesting provider. Logfile information is stored for a maximum of 7 days for security reasons (e.g. to clarify misuse or fraud) and then deleted. Data whose further storage is required for evidentiary purposes are excluded from deletion until the respective incident has been finally clarified.

## Contact

When contacting us (e.g. via contact form, e-mail, telephone or social media), the user's details are processed for processing the contact enquiry and its processing in accordance with Art. 6 para. 1 lit. b) DSGVO. User information can be stored in a customer relationship management system ("CRM system") or comparable request organization. Wir will delete the requests if they are no longer necessary. We review this requirement every two years; the statutory archiving obligations also apply.

## Integration of services and contents of third parties

Within our online offer, we set out on the basis of our legitimate interests (i.e. interest in the analysis, optimisation and economic operation of our online offer within the meaning of Art. 6 para. 1 lit. f. DSGVO) content or service offerings of third parties to incorporate their content and services, such as videos or fonts (hereinafter uniformly referred to as "content"). This always requires that the third party providers of this content perceive the IP address of the users, since without the IP address they could not send the content to their browser. The IP address is therefore required for the display of this content. We make every effort to use only those contents whose respective providers use the IP address only for the delivery of the contents.
