**Ambrosys GmbH** - Gesellschaft für Management komplexer Systeme<br>
Marlene-Dietrich-Allee 16<br>
14482 Potsdam<br>
Phone: +49 331 27319540<br>
Email: info@ambrosys.de

**Managing directors**: Dr. Markus Abel, Dr. Markus Quade<br>
**Commercial register entry/Handelsregister-Eintrag**: HRB 21228 P, Amtsgericht Potsdam<br>
Tax identification number/Umsatzsteuer-Identifikationsnummer gem. § 27 a Umsatzsteuergesetz: DE 264386402

Responsible for the content of the web page in accordance with §5 TMG and §55 II RStV:
Dr. Markus Abel, Dr. Markus Quade

Mantik is developed in the framework of the research projects [MAELSTROM](https://eurohpc-ju.europa.eu/research-innovation/our-projects/maelstrom_en) and [KISTE](https://kiste-project.de/).
