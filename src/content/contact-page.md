Mantik is a community-driven open-source project.

For requests, consult info@mantik.ai or our [helpdesk](https://mantik-ai.gitlab.io/mantik/helpdesk.html).

For further details on the project, we refer to our [documentation](https://mantik-ai.gitlab.io/mantik/about.html).
