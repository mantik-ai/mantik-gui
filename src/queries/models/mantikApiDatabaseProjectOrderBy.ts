/**
 * Generated by orval v7.4.1 🍺
 * Do not edit manually.
 * Mantik minimal API
 * The API serves the front end with all necessary information on projects, and users.
 * OpenAPI spec version: v0.2.0
 */

/**
 * An enumeration.
 */
export type MantikApiDatabaseProjectOrderBy =
    (typeof MantikApiDatabaseProjectOrderBy)[keyof typeof MantikApiDatabaseProjectOrderBy]

// eslint-disable-next-line @typescript-eslint/no-redeclare
export const MantikApiDatabaseProjectOrderBy = {
    name: 'name',
    created: 'created',
    updated: 'updated',
} as const
