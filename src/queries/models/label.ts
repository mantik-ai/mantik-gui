/**
 * Generated by orval v7.4.1 🍺
 * Do not edit manually.
 * Mantik minimal API
 * The API serves the front end with all necessary information on projects, and users.
 * OpenAPI spec version: v0.2.0
 */
import type { Scope } from './scope'
import type { Category } from './category'
import type { SubCategory } from './subCategory'

export interface Label {
    labelId: string
    scopes: Scope[]
    category: Category
    subCategory?: SubCategory
    name: string
    createdAt: string
    updatedAt?: string
}
