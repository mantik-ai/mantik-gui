/**
 * Generated by orval v7.4.1 🍺
 * Do not edit manually.
 * Mantik minimal API
 * The API serves the front end with all necessary information on projects, and users.
 * OpenAPI spec version: v0.2.0
 */
import type { MantikApiDatabaseRoleDetailsProjectRole } from './mantikApiDatabaseRoleDetailsProjectRole'
import type { RunConfiguration } from './runConfiguration'

export interface ProjectsProjectIdRunConfigurationsGet200Response {
    totalRecords: number
    pageRecords: number
    userRole?: MantikApiDatabaseRoleDetailsProjectRole
    runConfigurations?: RunConfiguration[]
}
