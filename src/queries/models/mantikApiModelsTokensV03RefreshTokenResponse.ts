/**
 * Generated by orval v7.4.1 🍺
 * Do not edit manually.
 * Mantik minimal API
 * The API serves the front end with all necessary information on projects, and users.
 * OpenAPI spec version: v0.2.0
 */

/**
 * Refreshed access token and it's expiration date.
 */
export interface MantikApiModelsTokensV03RefreshTokenResponse {
    accessToken: string
    expiresAt: string
}
