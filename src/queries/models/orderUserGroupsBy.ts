/**
 * Generated by orval v7.4.1 🍺
 * Do not edit manually.
 * Mantik minimal API
 * The API serves the front end with all necessary information on projects, and users.
 * OpenAPI spec version: v0.2.0
 */

/**
 * An enumeration.
 */
export type OrderUserGroupsBy =
    (typeof OrderUserGroupsBy)[keyof typeof OrderUserGroupsBy]

// eslint-disable-next-line @typescript-eslint/no-redeclare
export const OrderUserGroupsBy = {
    role: 'role',
    added: 'added',
    updated: 'updated',
} as const
