/**
 * Generated by orval v7.4.1 🍺
 * Do not edit manually.
 * Mantik minimal API
 * The API serves the front end with all necessary information on projects, and users.
 * OpenAPI spec version: v0.2.0
 */
import { faker } from '@faker-js/faker'
import { HttpResponse, delay, http } from 'msw'
import {
    Category,
    MantikApiDatabaseRoleDetailsProjectRole,
    Platform,
    Scope,
    SubCategory,
} from '.././models'
import type {
    CodeRepository,
    ProjectsProjectIdCodeGet200Response,
    ProjectsProjectIdCodeGet201Response,
} from '.././models'

export const getProjectsProjectIdCodeCodeRepositoryIdGetProjectsProjectIdCodeCodeRepositoryIdGetResponseMock =
    (overrideResponse: Partial<CodeRepository> = {}): CodeRepository => ({
        userRole: faker.helpers.arrayElement([
            faker.helpers.arrayElement(
                Object.values(MantikApiDatabaseRoleDetailsProjectRole)
            ),
            undefined,
        ]),
        codeRepositoryId: faker.string.uuid(),
        codeRepositoryName: faker.helpers.arrayElement([
            faker.string.alpha(20),
            undefined,
        ]),
        uri: faker.string.alpha(20),
        connectionId: faker.helpers.arrayElement([
            faker.string.uuid(),
            undefined,
        ]),
        description: faker.helpers.arrayElement([
            faker.string.alpha(20),
            undefined,
        ]),
        labels: faker.helpers.arrayElement([
            Array.from(
                { length: faker.number.int({ min: 1, max: 10 }) },
                (_, i) => i + 1
            ).map(() => ({
                labelId: faker.string.uuid(),
                scopes: faker.helpers.arrayElements(Object.values(Scope)),
                category: faker.helpers.arrayElement(Object.values(Category)),
                subCategory: faker.helpers.arrayElement([
                    faker.helpers.arrayElement(Object.values(SubCategory)),
                    undefined,
                ]),
                name: faker.string.alpha(20),
                createdAt: `${faker.date.past().toISOString().split('.')[0]}Z`,
                updatedAt: faker.helpers.arrayElement([
                    `${faker.date.past().toISOString().split('.')[0]}Z`,
                    undefined,
                ]),
            })),
            undefined,
        ]),
        createdAt: `${faker.date.past().toISOString().split('.')[0]}Z`,
        updatedAt: faker.helpers.arrayElement([
            `${faker.date.past().toISOString().split('.')[0]}Z`,
            undefined,
        ]),
        platform: faker.helpers.arrayElement(Object.values(Platform)),
        ...overrideResponse,
    })

export const getProjectsProjectIdCodeGetProjectsProjectIdCodeGetResponseMock = (
    overrideResponse: Partial<ProjectsProjectIdCodeGet200Response> = {}
): ProjectsProjectIdCodeGet200Response => ({
    totalRecords: faker.number.int({ min: undefined, max: undefined }),
    pageRecords: faker.number.int({ min: undefined, max: undefined }),
    userRole: faker.helpers.arrayElement([
        faker.helpers.arrayElement(
            Object.values(MantikApiDatabaseRoleDetailsProjectRole)
        ),
        undefined,
    ]),
    codeRepositories: faker.helpers.arrayElement([
        Array.from(
            { length: faker.number.int({ min: 1, max: 10 }) },
            (_, i) => i + 1
        ).map(() => ({
            userRole: faker.helpers.arrayElement([
                faker.helpers.arrayElement(
                    Object.values(MantikApiDatabaseRoleDetailsProjectRole)
                ),
                undefined,
            ]),
            codeRepositoryId: faker.string.uuid(),
            codeRepositoryName: faker.helpers.arrayElement([
                faker.string.alpha(20),
                undefined,
            ]),
            uri: faker.string.alpha(20),
            connectionId: faker.helpers.arrayElement([
                faker.string.uuid(),
                undefined,
            ]),
            description: faker.helpers.arrayElement([
                faker.string.alpha(20),
                undefined,
            ]),
            labels: faker.helpers.arrayElement([
                Array.from(
                    { length: faker.number.int({ min: 1, max: 10 }) },
                    (_, i) => i + 1
                ).map(() => ({
                    labelId: faker.string.uuid(),
                    scopes: faker.helpers.arrayElements(Object.values(Scope)),
                    category: faker.helpers.arrayElement(
                        Object.values(Category)
                    ),
                    subCategory: faker.helpers.arrayElement([
                        faker.helpers.arrayElement(Object.values(SubCategory)),
                        undefined,
                    ]),
                    name: faker.string.alpha(20),
                    createdAt: `${faker.date.past().toISOString().split('.')[0]}Z`,
                    updatedAt: faker.helpers.arrayElement([
                        `${faker.date.past().toISOString().split('.')[0]}Z`,
                        undefined,
                    ]),
                })),
                undefined,
            ]),
            createdAt: `${faker.date.past().toISOString().split('.')[0]}Z`,
            updatedAt: faker.helpers.arrayElement([
                `${faker.date.past().toISOString().split('.')[0]}Z`,
                undefined,
            ]),
            platform: faker.helpers.arrayElement(Object.values(Platform)),
        })),
        undefined,
    ]),
    ...overrideResponse,
})

export const getProjectsProjectIdCodePostProjectsProjectIdCodePostResponseMock =
    (
        overrideResponse: Partial<ProjectsProjectIdCodeGet201Response> = {}
    ): ProjectsProjectIdCodeGet201Response => ({
        codeRepositoryId: faker.string.uuid(),
        ...overrideResponse,
    })

export const getProjectsProjectIdCodeCodeRepositoryIdGetV02ProjectsProjectIdCodeCodeRepositoryIdGetResponseMock =
    (overrideResponse: Partial<CodeRepository> = {}): CodeRepository => ({
        userRole: faker.helpers.arrayElement([
            faker.helpers.arrayElement(
                Object.values(MantikApiDatabaseRoleDetailsProjectRole)
            ),
            undefined,
        ]),
        codeRepositoryId: faker.string.uuid(),
        codeRepositoryName: faker.helpers.arrayElement([
            faker.string.alpha(20),
            undefined,
        ]),
        uri: faker.string.alpha(20),
        connectionId: faker.helpers.arrayElement([
            faker.string.uuid(),
            undefined,
        ]),
        description: faker.helpers.arrayElement([
            faker.string.alpha(20),
            undefined,
        ]),
        labels: faker.helpers.arrayElement([
            Array.from(
                { length: faker.number.int({ min: 1, max: 10 }) },
                (_, i) => i + 1
            ).map(() => ({
                labelId: faker.string.uuid(),
                scopes: faker.helpers.arrayElements(Object.values(Scope)),
                category: faker.helpers.arrayElement(Object.values(Category)),
                subCategory: faker.helpers.arrayElement([
                    faker.helpers.arrayElement(Object.values(SubCategory)),
                    undefined,
                ]),
                name: faker.string.alpha(20),
                createdAt: `${faker.date.past().toISOString().split('.')[0]}Z`,
                updatedAt: faker.helpers.arrayElement([
                    `${faker.date.past().toISOString().split('.')[0]}Z`,
                    undefined,
                ]),
            })),
            undefined,
        ]),
        createdAt: `${faker.date.past().toISOString().split('.')[0]}Z`,
        updatedAt: faker.helpers.arrayElement([
            `${faker.date.past().toISOString().split('.')[0]}Z`,
            undefined,
        ]),
        platform: faker.helpers.arrayElement(Object.values(Platform)),
        ...overrideResponse,
    })

export const getProjectsProjectIdCodeGetV02ProjectsProjectIdCodeGetResponseMock =
    (
        overrideResponse: Partial<ProjectsProjectIdCodeGet200Response> = {}
    ): ProjectsProjectIdCodeGet200Response => ({
        totalRecords: faker.number.int({ min: undefined, max: undefined }),
        pageRecords: faker.number.int({ min: undefined, max: undefined }),
        userRole: faker.helpers.arrayElement([
            faker.helpers.arrayElement(
                Object.values(MantikApiDatabaseRoleDetailsProjectRole)
            ),
            undefined,
        ]),
        codeRepositories: faker.helpers.arrayElement([
            Array.from(
                { length: faker.number.int({ min: 1, max: 10 }) },
                (_, i) => i + 1
            ).map(() => ({
                userRole: faker.helpers.arrayElement([
                    faker.helpers.arrayElement(
                        Object.values(MantikApiDatabaseRoleDetailsProjectRole)
                    ),
                    undefined,
                ]),
                codeRepositoryId: faker.string.uuid(),
                codeRepositoryName: faker.helpers.arrayElement([
                    faker.string.alpha(20),
                    undefined,
                ]),
                uri: faker.string.alpha(20),
                connectionId: faker.helpers.arrayElement([
                    faker.string.uuid(),
                    undefined,
                ]),
                description: faker.helpers.arrayElement([
                    faker.string.alpha(20),
                    undefined,
                ]),
                labels: faker.helpers.arrayElement([
                    Array.from(
                        { length: faker.number.int({ min: 1, max: 10 }) },
                        (_, i) => i + 1
                    ).map(() => ({
                        labelId: faker.string.uuid(),
                        scopes: faker.helpers.arrayElements(
                            Object.values(Scope)
                        ),
                        category: faker.helpers.arrayElement(
                            Object.values(Category)
                        ),
                        subCategory: faker.helpers.arrayElement([
                            faker.helpers.arrayElement(
                                Object.values(SubCategory)
                            ),
                            undefined,
                        ]),
                        name: faker.string.alpha(20),
                        createdAt: `${faker.date.past().toISOString().split('.')[0]}Z`,
                        updatedAt: faker.helpers.arrayElement([
                            `${faker.date.past().toISOString().split('.')[0]}Z`,
                            undefined,
                        ]),
                    })),
                    undefined,
                ]),
                createdAt: `${faker.date.past().toISOString().split('.')[0]}Z`,
                updatedAt: faker.helpers.arrayElement([
                    `${faker.date.past().toISOString().split('.')[0]}Z`,
                    undefined,
                ]),
                platform: faker.helpers.arrayElement(Object.values(Platform)),
            })),
            undefined,
        ]),
        ...overrideResponse,
    })

export const getProjectsProjectIdCodePostV02ProjectsProjectIdCodePostResponseMock =
    (
        overrideResponse: Partial<ProjectsProjectIdCodeGet201Response> = {}
    ): ProjectsProjectIdCodeGet201Response => ({
        codeRepositoryId: faker.string.uuid(),
        ...overrideResponse,
    })

export const getProjectsProjectIdCodeIsUnlockedGetProjectsProjectIdCodeIsUnlockedGetMockHandler =
    (
        overrideResponse?:
            | unknown
            | ((
                  info: Parameters<Parameters<typeof http.get>[1]>[0]
              ) => Promise<unknown> | unknown)
    ) => {
        return http.get(
            'https://api.dev2.cloud.mantik.ai/projects/:projectId/code/is-unlocked',
            async (info) => {
                await delay(1000)
                if (typeof overrideResponse === 'function') {
                    await overrideResponse(info)
                }
                return new HttpResponse(null, { status: 200 })
            }
        )
    }

export const getProjectsProjectIdCodeCodeRepositoryIdGetProjectsProjectIdCodeCodeRepositoryIdGetMockHandler =
    (
        overrideResponse?:
            | CodeRepository
            | ((
                  info: Parameters<Parameters<typeof http.get>[1]>[0]
              ) => Promise<CodeRepository> | CodeRepository)
    ) => {
        return http.get(
            'https://api.dev2.cloud.mantik.ai/projects/:projectId/code/:codeRepositoryId',
            async (info) => {
                await delay(1000)

                return new HttpResponse(
                    JSON.stringify(
                        overrideResponse !== undefined
                            ? typeof overrideResponse === 'function'
                                ? await overrideResponse(info)
                                : overrideResponse
                            : getProjectsProjectIdCodeCodeRepositoryIdGetProjectsProjectIdCodeCodeRepositoryIdGetResponseMock()
                    ),
                    {
                        status: 200,
                        headers: { 'Content-Type': 'application/json' },
                    }
                )
            }
        )
    }

export const getProjectsProjectIdCodeCodeRepositoryIdPutProjectsProjectIdCodeCodeRepositoryIdPutMockHandler =
    (
        overrideResponse?:
            | void
            | ((
                  info: Parameters<Parameters<typeof http.put>[1]>[0]
              ) => Promise<void> | void)
    ) => {
        return http.put(
            'https://api.dev2.cloud.mantik.ai/projects/:projectId/code/:codeRepositoryId',
            async (info) => {
                await delay(1000)
                if (typeof overrideResponse === 'function') {
                    await overrideResponse(info)
                }
                return new HttpResponse(null, { status: 204 })
            }
        )
    }

export const getProjectsProjectIdCodeCodeRepositoryIdDeleteProjectsProjectIdCodeCodeRepositoryIdDeleteMockHandler =
    (
        overrideResponse?:
            | void
            | ((
                  info: Parameters<Parameters<typeof http.delete>[1]>[0]
              ) => Promise<void> | void)
    ) => {
        return http.delete(
            'https://api.dev2.cloud.mantik.ai/projects/:projectId/code/:codeRepositoryId',
            async (info) => {
                await delay(1000)
                if (typeof overrideResponse === 'function') {
                    await overrideResponse(info)
                }
                return new HttpResponse(null, { status: 204 })
            }
        )
    }

export const getProjectsProjectIdCodeGetProjectsProjectIdCodeGetMockHandler = (
    overrideResponse?:
        | ProjectsProjectIdCodeGet200Response
        | ((
              info: Parameters<Parameters<typeof http.get>[1]>[0]
          ) =>
              | Promise<ProjectsProjectIdCodeGet200Response>
              | ProjectsProjectIdCodeGet200Response)
) => {
    return http.get(
        'https://api.dev2.cloud.mantik.ai/projects/:projectId/code',
        async (info) => {
            await delay(1000)

            return new HttpResponse(
                JSON.stringify(
                    overrideResponse !== undefined
                        ? typeof overrideResponse === 'function'
                            ? await overrideResponse(info)
                            : overrideResponse
                        : getProjectsProjectIdCodeGetProjectsProjectIdCodeGetResponseMock()
                ),
                { status: 200, headers: { 'Content-Type': 'application/json' } }
            )
        }
    )
}

export const getProjectsProjectIdCodePostProjectsProjectIdCodePostMockHandler =
    (
        overrideResponse?:
            | ProjectsProjectIdCodeGet201Response
            | ((
                  info: Parameters<Parameters<typeof http.post>[1]>[0]
              ) =>
                  | Promise<ProjectsProjectIdCodeGet201Response>
                  | ProjectsProjectIdCodeGet201Response)
    ) => {
        return http.post(
            'https://api.dev2.cloud.mantik.ai/projects/:projectId/code',
            async (info) => {
                await delay(1000)

                return new HttpResponse(
                    JSON.stringify(
                        overrideResponse !== undefined
                            ? typeof overrideResponse === 'function'
                                ? await overrideResponse(info)
                                : overrideResponse
                            : getProjectsProjectIdCodePostProjectsProjectIdCodePostResponseMock()
                    ),
                    {
                        status: 201,
                        headers: { 'Content-Type': 'application/json' },
                    }
                )
            }
        )
    }

export const getProjectsProjectIdCodeCodeRepositoryIdConnectionProjectsProjectIdCodeCodeRepositoryIdConnectionPostMockHandler =
    (
        overrideResponse?:
            | void
            | ((
                  info: Parameters<Parameters<typeof http.post>[1]>[0]
              ) => Promise<void> | void)
    ) => {
        return http.post(
            'https://api.dev2.cloud.mantik.ai/projects/:projectId/code/:codeRepositoryId/connection',
            async (info) => {
                await delay(1000)
                if (typeof overrideResponse === 'function') {
                    await overrideResponse(info)
                }
                return new HttpResponse(null, { status: 204 })
            }
        )
    }

export const getProjectsProjectIdCodeCodeRepositoryIdConnectionIdDeleteProjectsProjectIdCodeCodeRepositoryIdConnectionConnectionIdDeleteMockHandler =
    (
        overrideResponse?:
            | void
            | ((
                  info: Parameters<Parameters<typeof http.delete>[1]>[0]
              ) => Promise<void> | void)
    ) => {
        return http.delete(
            'https://api.dev2.cloud.mantik.ai/projects/:projectId/code/:codeRepositoryId/connection/:connectionId',
            async (info) => {
                await delay(1000)
                if (typeof overrideResponse === 'function') {
                    await overrideResponse(info)
                }
                return new HttpResponse(null, { status: 204 })
            }
        )
    }

export const getProjectsProjectIdCodeIsUnlockedGetV02ProjectsProjectIdCodeIsUnlockedGetMockHandler =
    (
        overrideResponse?:
            | unknown
            | ((
                  info: Parameters<Parameters<typeof http.get>[1]>[0]
              ) => Promise<unknown> | unknown)
    ) => {
        return http.get(
            'https://api.dev2.cloud.mantik.ai/v0-2/projects/:projectId/code/is-unlocked',
            async (info) => {
                await delay(1000)
                if (typeof overrideResponse === 'function') {
                    await overrideResponse(info)
                }
                return new HttpResponse(null, { status: 200 })
            }
        )
    }

export const getProjectsProjectIdCodeCodeRepositoryIdGetV02ProjectsProjectIdCodeCodeRepositoryIdGetMockHandler =
    (
        overrideResponse?:
            | CodeRepository
            | ((
                  info: Parameters<Parameters<typeof http.get>[1]>[0]
              ) => Promise<CodeRepository> | CodeRepository)
    ) => {
        return http.get(
            'https://api.dev2.cloud.mantik.ai/v0-2/projects/:projectId/code/:codeRepositoryId',
            async (info) => {
                await delay(1000)

                return new HttpResponse(
                    JSON.stringify(
                        overrideResponse !== undefined
                            ? typeof overrideResponse === 'function'
                                ? await overrideResponse(info)
                                : overrideResponse
                            : getProjectsProjectIdCodeCodeRepositoryIdGetV02ProjectsProjectIdCodeCodeRepositoryIdGetResponseMock()
                    ),
                    {
                        status: 200,
                        headers: { 'Content-Type': 'application/json' },
                    }
                )
            }
        )
    }

export const getProjectsProjectIdCodeCodeRepositoryIdPutV02ProjectsProjectIdCodeCodeRepositoryIdPutMockHandler =
    (
        overrideResponse?:
            | void
            | ((
                  info: Parameters<Parameters<typeof http.put>[1]>[0]
              ) => Promise<void> | void)
    ) => {
        return http.put(
            'https://api.dev2.cloud.mantik.ai/v0-2/projects/:projectId/code/:codeRepositoryId',
            async (info) => {
                await delay(1000)
                if (typeof overrideResponse === 'function') {
                    await overrideResponse(info)
                }
                return new HttpResponse(null, { status: 204 })
            }
        )
    }

export const getProjectsProjectIdCodeCodeRepositoryIdDeleteV02ProjectsProjectIdCodeCodeRepositoryIdDeleteMockHandler =
    (
        overrideResponse?:
            | void
            | ((
                  info: Parameters<Parameters<typeof http.delete>[1]>[0]
              ) => Promise<void> | void)
    ) => {
        return http.delete(
            'https://api.dev2.cloud.mantik.ai/v0-2/projects/:projectId/code/:codeRepositoryId',
            async (info) => {
                await delay(1000)
                if (typeof overrideResponse === 'function') {
                    await overrideResponse(info)
                }
                return new HttpResponse(null, { status: 204 })
            }
        )
    }

export const getProjectsProjectIdCodeGetV02ProjectsProjectIdCodeGetMockHandler =
    (
        overrideResponse?:
            | ProjectsProjectIdCodeGet200Response
            | ((
                  info: Parameters<Parameters<typeof http.get>[1]>[0]
              ) =>
                  | Promise<ProjectsProjectIdCodeGet200Response>
                  | ProjectsProjectIdCodeGet200Response)
    ) => {
        return http.get(
            'https://api.dev2.cloud.mantik.ai/v0-2/projects/:projectId/code',
            async (info) => {
                await delay(1000)

                return new HttpResponse(
                    JSON.stringify(
                        overrideResponse !== undefined
                            ? typeof overrideResponse === 'function'
                                ? await overrideResponse(info)
                                : overrideResponse
                            : getProjectsProjectIdCodeGetV02ProjectsProjectIdCodeGetResponseMock()
                    ),
                    {
                        status: 200,
                        headers: { 'Content-Type': 'application/json' },
                    }
                )
            }
        )
    }

export const getProjectsProjectIdCodePostV02ProjectsProjectIdCodePostMockHandler =
    (
        overrideResponse?:
            | ProjectsProjectIdCodeGet201Response
            | ((
                  info: Parameters<Parameters<typeof http.post>[1]>[0]
              ) =>
                  | Promise<ProjectsProjectIdCodeGet201Response>
                  | ProjectsProjectIdCodeGet201Response)
    ) => {
        return http.post(
            'https://api.dev2.cloud.mantik.ai/v0-2/projects/:projectId/code',
            async (info) => {
                await delay(1000)

                return new HttpResponse(
                    JSON.stringify(
                        overrideResponse !== undefined
                            ? typeof overrideResponse === 'function'
                                ? await overrideResponse(info)
                                : overrideResponse
                            : getProjectsProjectIdCodePostV02ProjectsProjectIdCodePostResponseMock()
                    ),
                    {
                        status: 201,
                        headers: { 'Content-Type': 'application/json' },
                    }
                )
            }
        )
    }

export const getProjectsProjectIdCodeCodeRepositoryIdConnectionV02ProjectsProjectIdCodeCodeRepositoryIdConnectionPostMockHandler =
    (
        overrideResponse?:
            | void
            | ((
                  info: Parameters<Parameters<typeof http.post>[1]>[0]
              ) => Promise<void> | void)
    ) => {
        return http.post(
            'https://api.dev2.cloud.mantik.ai/v0-2/projects/:projectId/code/:codeRepositoryId/connection',
            async (info) => {
                await delay(1000)
                if (typeof overrideResponse === 'function') {
                    await overrideResponse(info)
                }
                return new HttpResponse(null, { status: 204 })
            }
        )
    }

export const getProjectsProjectIdCodeCodeRepositoryIdConnectionIdDeleteV02ProjectsProjectIdCodeCodeRepositoryIdConnectionConnectionIdDeleteMockHandler =
    (
        overrideResponse?:
            | void
            | ((
                  info: Parameters<Parameters<typeof http.delete>[1]>[0]
              ) => Promise<void> | void)
    ) => {
        return http.delete(
            'https://api.dev2.cloud.mantik.ai/v0-2/projects/:projectId/code/:codeRepositoryId/connection/:connectionId',
            async (info) => {
                await delay(1000)
                if (typeof overrideResponse === 'function') {
                    await overrideResponse(info)
                }
                return new HttpResponse(null, { status: 204 })
            }
        )
    }
export const getCodeRepositoryMock = () => [
    getProjectsProjectIdCodeIsUnlockedGetProjectsProjectIdCodeIsUnlockedGetMockHandler(),
    getProjectsProjectIdCodeCodeRepositoryIdGetProjectsProjectIdCodeCodeRepositoryIdGetMockHandler(),
    getProjectsProjectIdCodeCodeRepositoryIdPutProjectsProjectIdCodeCodeRepositoryIdPutMockHandler(),
    getProjectsProjectIdCodeCodeRepositoryIdDeleteProjectsProjectIdCodeCodeRepositoryIdDeleteMockHandler(),
    getProjectsProjectIdCodeGetProjectsProjectIdCodeGetMockHandler(),
    getProjectsProjectIdCodePostProjectsProjectIdCodePostMockHandler(),
    getProjectsProjectIdCodeCodeRepositoryIdConnectionProjectsProjectIdCodeCodeRepositoryIdConnectionPostMockHandler(),
    getProjectsProjectIdCodeCodeRepositoryIdConnectionIdDeleteProjectsProjectIdCodeCodeRepositoryIdConnectionConnectionIdDeleteMockHandler(),
    getProjectsProjectIdCodeIsUnlockedGetV02ProjectsProjectIdCodeIsUnlockedGetMockHandler(),
    getProjectsProjectIdCodeCodeRepositoryIdGetV02ProjectsProjectIdCodeCodeRepositoryIdGetMockHandler(),
    getProjectsProjectIdCodeCodeRepositoryIdPutV02ProjectsProjectIdCodeCodeRepositoryIdPutMockHandler(),
    getProjectsProjectIdCodeCodeRepositoryIdDeleteV02ProjectsProjectIdCodeCodeRepositoryIdDeleteMockHandler(),
    getProjectsProjectIdCodeGetV02ProjectsProjectIdCodeGetMockHandler(),
    getProjectsProjectIdCodePostV02ProjectsProjectIdCodePostMockHandler(),
    getProjectsProjectIdCodeCodeRepositoryIdConnectionV02ProjectsProjectIdCodeCodeRepositoryIdConnectionPostMockHandler(),
    getProjectsProjectIdCodeCodeRepositoryIdConnectionIdDeleteV02ProjectsProjectIdCodeCodeRepositoryIdConnectionConnectionIdDeleteMockHandler(),
]
