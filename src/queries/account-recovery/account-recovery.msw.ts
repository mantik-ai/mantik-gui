/**
 * Generated by orval v7.4.1 🍺
 * Do not edit manually.
 * Mantik minimal API
 * The API serves the front end with all necessary information on projects, and users.
 * OpenAPI spec version: v0.2.0
 */
import { faker } from '@faker-js/faker'
import { HttpResponse, delay, http } from 'msw'
import type { InitiateForgotPasswordUserDetailsPost200Response } from '.././models'

export const getForgotPasswordInitiatePostForgotPasswordInitiatePostResponseMock =
    (
        overrideResponse: Partial<InitiateForgotPasswordUserDetailsPost200Response> = {}
    ): InitiateForgotPasswordUserDetailsPost200Response => ({
        userName: faker.string.alpha(20),
        ...overrideResponse,
    })

export const getForgotPasswordInitiatePostV02ForgotPasswordInitiatePostResponseMock =
    (
        overrideResponse: Partial<InitiateForgotPasswordUserDetailsPost200Response> = {}
    ): InitiateForgotPasswordUserDetailsPost200Response => ({
        userName: faker.string.alpha(20),
        ...overrideResponse,
    })

export const getForgotPasswordInitiatePostForgotPasswordInitiatePostMockHandler =
    (
        overrideResponse?:
            | InitiateForgotPasswordUserDetailsPost200Response
            | ((
                  info: Parameters<Parameters<typeof http.post>[1]>[0]
              ) =>
                  | Promise<InitiateForgotPasswordUserDetailsPost200Response>
                  | InitiateForgotPasswordUserDetailsPost200Response)
    ) => {
        return http.post(
            'https://api.dev2.cloud.mantik.ai/forgot-password/initiate',
            async (info) => {
                await delay(1000)

                return new HttpResponse(
                    JSON.stringify(
                        overrideResponse !== undefined
                            ? typeof overrideResponse === 'function'
                                ? await overrideResponse(info)
                                : overrideResponse
                            : getForgotPasswordInitiatePostForgotPasswordInitiatePostResponseMock()
                    ),
                    {
                        status: 200,
                        headers: { 'Content-Type': 'application/json' },
                    }
                )
            }
        )
    }

export const getForgotPasswordConfirmPutForgotPasswordConfirmPutMockHandler = (
    overrideResponse?:
        | void
        | ((
              info: Parameters<Parameters<typeof http.put>[1]>[0]
          ) => Promise<void> | void)
) => {
    return http.put(
        'https://api.dev2.cloud.mantik.ai/forgot-password/confirm',
        async (info) => {
            await delay(1000)
            if (typeof overrideResponse === 'function') {
                await overrideResponse(info)
            }
            return new HttpResponse(null, { status: 204 })
        }
    )
}

export const getForgotPasswordInitiatePostV02ForgotPasswordInitiatePostMockHandler =
    (
        overrideResponse?:
            | InitiateForgotPasswordUserDetailsPost200Response
            | ((
                  info: Parameters<Parameters<typeof http.post>[1]>[0]
              ) =>
                  | Promise<InitiateForgotPasswordUserDetailsPost200Response>
                  | InitiateForgotPasswordUserDetailsPost200Response)
    ) => {
        return http.post(
            'https://api.dev2.cloud.mantik.ai/v0-2/forgot-password/initiate',
            async (info) => {
                await delay(1000)

                return new HttpResponse(
                    JSON.stringify(
                        overrideResponse !== undefined
                            ? typeof overrideResponse === 'function'
                                ? await overrideResponse(info)
                                : overrideResponse
                            : getForgotPasswordInitiatePostV02ForgotPasswordInitiatePostResponseMock()
                    ),
                    {
                        status: 200,
                        headers: { 'Content-Type': 'application/json' },
                    }
                )
            }
        )
    }

export const getForgotPasswordConfirmPutV02ForgotPasswordConfirmPutMockHandler =
    (
        overrideResponse?:
            | void
            | ((
                  info: Parameters<Parameters<typeof http.put>[1]>[0]
              ) => Promise<void> | void)
    ) => {
        return http.put(
            'https://api.dev2.cloud.mantik.ai/v0-2/forgot-password/confirm',
            async (info) => {
                await delay(1000)
                if (typeof overrideResponse === 'function') {
                    await overrideResponse(info)
                }
                return new HttpResponse(null, { status: 204 })
            }
        )
    }
export const getAccountRecoveryMock = () => [
    getForgotPasswordInitiatePostForgotPasswordInitiatePostMockHandler(),
    getForgotPasswordConfirmPutForgotPasswordConfirmPutMockHandler(),
    getForgotPasswordInitiatePostV02ForgotPasswordInitiatePostMockHandler(),
    getForgotPasswordConfirmPutV02ForgotPasswordConfirmPutMockHandler(),
]
