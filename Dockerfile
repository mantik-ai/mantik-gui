# Based on the official example: https://nextjs.org/docs/app/building-your-application/deploying
FROM node:20-alpine AS base

FROM base AS dependencies

# Check https://github.com/nodejs/docker-node/tree/b4117f9333da4138b03a546ec926ef50a31506c3#nodealpine to understand why libc6-compat might be needed.
#RUN apk add --no-cache libc6-compat

WORKDIR /app

# Install dependencies based on the preferred package manager
COPY package.json package-lock.json* ./
RUN npm install

FROM base AS builder

WORKDIR /app

COPY --from=dependencies /app/node_modules ./node_modules
COPY . .

# Next.js collects completely anonymous telemetry data about general usage (see https://nextjs.org/telemetry).
# Disable telemetry during the build.
ENV NEXT_TELEMETRY_DISABLED 1

# NEXT_PUBLIC_API_URL has to be passed in at build time,
# since it is used client side as well.
ARG NEXT_PUBLIC_API_URL
ARG NEXT_PUBLIC_MANTIK_MLFLOW_UI_URL
ARG NEXT_PUBLIC_RUM_IDENTITY_POOL_ID
ARG NEXT_PUBLIC_RUM_APPLICATION_ID
ARG NEXT_PUBLIC_RUM_APPLICATION_REGION

RUN npm run build

# Production image, copy all the files and run next
FROM base AS runner

ARG PORT=3002

ENV PORT=$PORT
ENV NODE_ENV production

EXPOSE $PORT

WORKDIR /app

# Disable telemetry during runtime.
ENV NEXT_TELEMETRY_DISABLED 1

RUN addgroup --system --gid 1001 nodejs
RUN adduser --system --uid 1001 nextjs

COPY --from=builder /app/public ./public

# Set the correct permission for prerender cache
RUN mkdir .next
RUN chown nextjs:nodejs .next

# Automatically leverage output traces to reduce image size
# https://nextjs.org/docs/advanced-features/output-file-tracing
COPY --from=builder --chown=nextjs:nodejs /app/.next/standalone ./
COPY --from=builder --chown=nextjs:nodejs /app/.next/static ./.next/static

USER nextjs

# set hostname to localhost
ENV HOSTNAME="0.0.0.0"

# server.js is created by next build from the standalone output
# https://nextjs.org/docs/pages/api-reference/next-config-js/output
CMD ["node", "server.js"]
