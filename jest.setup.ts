import { configure } from '@testing-library/react'
import '@testing-library/jest-dom'
import server from './__mockserver__/mockserver'

// enable request validation during tests
// api.register('validationFail', (c, res, ctx) =>
//     res(ctx.status(400), ctx.json({ error: c.validation.errors }))
// )
configure({ asyncUtilTimeout: 3000 })

server.resetHandlers()

beforeAll(() => server.listen({ onUnhandledRequest: 'warn' }))

afterEach(() => {
    server.resetHandlers()
    jest.clearAllMocks()
})
afterAll(() => server.close())

jest.mock('next/config', () => () => ({
    publicRuntimeConfig: {},
}))

// mock useRouter
jest.mock('next/router', () => ({
    useRouter: jest.fn(),
}))

global.console = {
    ...console,
    warn: jest.fn(), // temporarily disabled console.warn in test runs
    error: jest.fn(), // temporarily disabled console.error in test runs
}

const BASE_API_URL = 'https://api.dev2.cloud.mantik.ai'

export { server, BASE_API_URL }
