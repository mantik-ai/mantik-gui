import { defineConfig, devices } from '@playwright/test'
import dotenv from 'dotenv'
import path from 'path'

// When running local tests, ensure .env file is populated with the correct passwords for accounts.
// see the variables in .gitlab-ci.yml for the required fields.
// The corresponding values are in the repository settings in GitLab.
// Either fetch from there, or ask someone.
// TODO: also make projectIds configurable:
//    this will allow a user without access to GitLab settings to run tests.

if (!process.env.CI) {
    dotenv.config({ path: path.resolve(__dirname, '.env') })
}

const PORT = 3000

/**
 * See https://playwright.dev/docs/test-configuration.
 */
export default defineConfig({
    testDir: './tests/playwright',
    /* Run tests in files in parallel */
    fullyParallel: true,
    /* Fail the build on CI if you accidentally left test.only in the source code. */
    forbidOnly: !!process.env.CI,
    /* Retry on CI only */
    retries: process.env.CI ? 2 : 0,
    /* Opt out of parallel tests on CI. */
    workers: process.env.CI ? 1 : undefined,
    /* Reporter to use. See https://playwright.dev/docs/test-reporters */
    reporter: 'html',
    /* Shared settings for all the projects below. See https://playwright.dev/docs/api/class-testoptions. */
    use: {
        /* Base URL to use in actions like `await page.goto('/')`. */
        baseURL: `http://localhost:3000/`,

        /* Collect trace when retrying the failed test. See https://playwright.dev/docs/trace-viewer */
        trace: 'on-first-retry',
        /* Record videoes */
        video: process.env.PLAYWRIGHT_VIDEO === 'on' ? 'on' : 'off',
        timezoneId: 'Europe/Berlin',
    },

    /* Configure projects for major browsers */
    projects: [
        {
            name: 'chromium',
            use: { ...devices['Desktop Chrome'] },
        },
    ],

    webServer: {
        command: `npm run build && npm run start -p ${PORT}`,
        url: `http://127.0.0.1:${PORT}`,
        reuseExistingServer: !process.env.CI,
        timeout: 400 * 1000,
    },
    expect: {
        timeout: 10_000, // Working tests were failing on this point, cf. known issue https://gitlab.com/mantik-ai/mantik-gui/-/issues/403
    },
    timeout: 60_000,
})
