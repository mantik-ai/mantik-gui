import { waitFor, renderHook } from '@testing-library/react'
import { getUsersGetUsersGetMockHandler } from '../../src/queries/users/users.msw'

// test component that uses mantikApi axios instance hook for data fetching
describe('TestMSW', () => {
    // test data fetching hook that uses mantikApi axios instance
    it('should get data from custom hook', async () => {
        const { result } = renderHook(getUsersGetUsersGetMockHandler)
        await waitFor(() => {
            expect(result.current).toBeDefined() // check why it does not get data --> b/c component not rendered?
        })
    })
})
