import { useEffect, useState } from 'react'
import { mantikApi } from '../../src/modules/auth/axios'
import {
    useUsersGetUsersGet,
    useUsersUserIdGetUsersUserIdGet,
} from '../../src/queries'

export const API = 'https://api.dev2.cloud.mantik.ai'

export const useFetchData = (query: string, accessToken?: string) => {
    const [data, setData] = useState<{ [k: string]: any }>({})
    useEffect(() => {
        const headers = accessToken
            ? {
                  headers: {
                      Authorization: `Bearer ${accessToken}`,
                      Accept: 'application/json',
                  },
              }
            : {}
        const getData = async () => {
            const url = API + query
            try {
                const response = await mantikApi.get(url, headers)
                if (response.status === 200) {
                    setData(response.data)
                }
                return response
            } catch (err: any) {
                console.error(err)
            }
        }
        getData()
    }, [query, accessToken])
    return data
}

const renderUserList = (
    users: { [k: string]: any }[],
    componentName: string
) => {
    if (!users) return <p>loading...</p>
    return (
        <div>
            <h1>{componentName}</h1>
            <p style={{ margin: '1em' }}>length: {users?.length}</p>
            <ul>
                {users?.map((user: any, index: number) => (
                    <li
                        key={index}
                        style={{ margin: '1em' }}
                        data-testid="listelement-user"
                    >
                        <p style={{ margin: 0 }}>
                            name: {user?.name && user.name}
                        </p>
                        <p style={{ margin: 0 }}>
                            userId: {user?.userId && user.userId}
                        </p>
                        <p style={{ margin: 0 }}>
                            jobTitle: {user?.jobTitle && user.jobTitle}
                        </p>
                    </li>
                ))}
            </ul>
        </div>
    )
}

export const TestMSW = () => {
    const data = useFetchData('/users')
    return renderUserList(data?.users, 'TestMSW')
}

export const TestMSWwithDataAsProps = ({ data }: any) => {
    return renderUserList(data, 'TestMSWwithDataAsProps')
}

export const TestMSWwithReactQuery = () => {
    const { data } = useUsersGetUsersGet()
    const users = data?.users as any[]
    return <TestMSWwithDataAsProps data={users} />
}

export const TestMSWwithReactQueryAndAuthRequired = () => {
    const { data } = useUsersUserIdGetUsersUserIdGet(
        '09f29cc7-4ea6-4521-92ac-4e5ce72ab011'
    )
    return renderUserList(
        [data as any],
        'TestMSW with React Query and Authentication required'
    )
}
