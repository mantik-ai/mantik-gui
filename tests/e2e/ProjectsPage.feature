Feature: Projects page

    # 1. execute Test with chrome utilize dev tools "performance" tab
    # - compare LCP with page load summary
    # - check screen shots if the timing is accurate
    # 2. open AWS RUM and check LCP events for the page
    # - select "/projects" and check LCP
    # - compare if LCP matches the relevant metrics from "performance" tab, once RUM metrics are giving useful results, we can remove the dev tools check
    @issue:mantik-gui#447
    Rule: The LCP for "/projects" should be less than 500 ms more expensive than the most expensive api

        Scenario: User opens "/projects" for first directly and wants to see projects
            Given I have opened a new tab
            When I open "/projects"
            Then the projects are visible after less than 500 ms plus the time measured for most expensive api "/projects"

        Scenario: User revisits "/projects" and wants to see projects
            Given I have "/projects" before
            And I have opened "/account"
            When I open "/projects"
            Then the projects are visible after less than 500 ms
