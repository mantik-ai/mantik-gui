# End to End Testing

## Automated tests

All automated ui tests are implemented with playwright and located in the '../playwright/'

## Manual tests

All Gherkin files in this folder shall be executed manually. This means execute
each step by hand as described.
