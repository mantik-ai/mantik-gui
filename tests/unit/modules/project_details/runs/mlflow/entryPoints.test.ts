import {
    getFromMlprojectConfig,
    prepareParamsForRequest,
} from '../../../../../../src/modules/project_details/runs/mlflow/entryPoints'
import { EntryPoints } from '../../../../../../src/modules/project_details/runs/types/runDialogProps'

describe('get MLflow entry points from parsed YAML', () => {
    it('returns empty array if no entry points given', () => {
        const yaml = {}

        const expected: EntryPoints[] = []

        const result = getFromMlprojectConfig(yaml)

        expect(result).toStrictEqual(expected)
    })
    it('returns empty parameters if no parameters given', () => {
        const yaml = {
            main: {
                parameters: {},
            },
        }

        const expected = [
            {
                entryPoint: 'main',
                parameters: [],
            },
        ]

        const result = getFromMlprojectConfig(yaml)

        expect(result).toStrictEqual(expected)
    })

    it('returns one required parameter', () => {
        const yaml = {
            main: {
                parameters: {
                    alpha: 'float',
                },
            },
        }

        const expected = [
            {
                entryPoint: 'main',
                parameters: [
                    {
                        name: 'alpha',
                        label: 'alpha (type: float)',
                        value: null,
                        required: true,
                    },
                ],
            },
        ]

        const result = getFromMlprojectConfig(yaml)

        expect(result).toStrictEqual(expected)
    })

    it('returns one required nested parameter', () => {
        const yaml = {
            main: {
                parameters: {
                    alpha: { type: 'float' },
                },
            },
        }

        const expected = [
            {
                entryPoint: 'main',
                parameters: [
                    {
                        name: 'alpha',
                        label: 'alpha (type: float)',
                        value: null,
                        required: true,
                    },
                ],
            },
        ]

        const result = getFromMlprojectConfig(yaml)

        expect(result).toStrictEqual(expected)
    })

    it('returns one nested optional parameter', () => {
        const yaml = {
            main: {
                parameters: {
                    alpha: {
                        type: 'float',
                        default: '0.1',
                    },
                },
            },
        }
        const expected = [
            {
                entryPoint: 'main',
                parameters: [
                    {
                        name: 'alpha',
                        label: 'alpha (type: float, default: 0.1)',
                        value: null,
                        required: false,
                    },
                ],
            },
        ]

        const result = getFromMlprojectConfig(yaml)

        expect(result).toStrictEqual(expected)
    })

    it('returns three parameters', () => {
        const yaml = {
            main: {
                parameters: {
                    alpha: 'float',
                    beta: {
                        type: 'float',
                    },
                    gamma: {
                        type: 'float',
                        default: '0.1',
                    },
                },
            },
        }
        const expected = [
            {
                entryPoint: 'main',
                parameters: [
                    {
                        name: 'alpha',
                        label: 'alpha (type: float)',
                        value: null,
                        required: true,
                    },
                    {
                        name: 'beta',
                        label: 'beta (type: float)',
                        value: null,
                        required: true,
                    },
                    {
                        name: 'gamma',
                        label: 'gamma (type: float, default: 0.1)',
                        value: null,
                        required: false,
                    },
                ],
            },
        ]

        const result = getFromMlprojectConfig(yaml)

        expect(result).toStrictEqual(expected)
    })

    it('returns three entry points', () => {
        const yaml = {
            main: {
                parameters: {
                    alpha: 'float',
                },
            },
            second: {
                parameters: {
                    beta: 'string',
                },
            },
            third: {
                parameters: {
                    gamma: 'int',
                },
            },
        }

        const expected = [
            {
                entryPoint: 'main',
                parameters: [
                    {
                        name: 'alpha',
                        label: 'alpha (type: float)',
                        value: null,
                        required: true,
                    },
                ],
            },
            {
                entryPoint: 'second',
                parameters: [
                    {
                        name: 'beta',
                        label: 'beta (type: string)',
                        value: null,
                        required: true,
                    },
                ],
            },
            {
                entryPoint: 'third',
                parameters: [
                    {
                        name: 'gamma',
                        label: 'gamma (type: int)',
                        value: null,
                        required: true,
                    },
                ],
            },
        ]

        const result = getFromMlprojectConfig(yaml)

        expect(result).toStrictEqual(expected)
    })
})

describe('prepare MLflow entry point parameters for request', () => {
    it('returns an empty object for no entry points', () => {
        const entryPoints: EntryPoints = []

        const expected = {}

        const result = prepareParamsForRequest(entryPoints, 0)

        expect(result).toStrictEqual(expected)
    })

    it('returns an empty object for no entry point parameters', () => {
        const entryPoints: EntryPoints = [
            {
                entryPoint: 'main',
                parameters: [],
            },
        ]

        const expected = {}

        const result = prepareParamsForRequest(entryPoints, 0)

        expect(result).toStrictEqual(expected)
    })

    it('only returns not null values', () => {
        const entryPoints = [
            {
                entryPoint: 'main',
                parameters: [
                    {
                        name: 'alpha',
                        label: 'alpha (type: float)',
                        value: '0.1',
                        required: true,
                    },
                    {
                        name: 'beta',
                        label: 'beta (type: float, default: 0.1)',
                        value: null,
                        required: false,
                    },
                ],
            },
        ]
        const expected = {
            alpha: '0.1',
        }

        const result = prepareParamsForRequest(entryPoints, 0)

        expect(result).toStrictEqual(expected)
    })
})
