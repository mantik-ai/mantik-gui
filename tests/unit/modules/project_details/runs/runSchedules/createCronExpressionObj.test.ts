import { createCronExpressionObj } from '../../../../../../src/modules/project_details/runs/runSchedules/createCronExpressionObj'

describe('creates an Object from a string', () => {
    it('creates an Object from a string', () => {
        const cronExpression = '0 0 1 * ?'

        const expected: {
            minute: string
            hour: string
            dayOfMonth: string
            month: string
            dayOfWeek: string
        } = {
            minute: '0',
            hour: '0',
            dayOfMonth: '1',
            month: '*',
            dayOfWeek: '?',
        }

        const result = createCronExpressionObj(cronExpression)

        expect(result).toStrictEqual(expected)
    })
})
