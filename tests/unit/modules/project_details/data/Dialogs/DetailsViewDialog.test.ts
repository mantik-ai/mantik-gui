import { faker } from '@faker-js/faker'
import { transformData } from '../../../../../../src/modules/project_details/data/Dialogs/DetailsViewDialog'
import {
    Category as CategoryType,
    Category,
    DataRepository,
    SubCategory as SubCategoryType,
    SubCategory,
    Scope,
    Scope as ScopeType,
} from '../../../../../../src/queries'

describe('Data repository labels should be transformed correctly', () => {
    describe('mantik#287 Data repo labels should be shown in the Details view', () => {
        it('transforms data repository to proper labels object ', () => {
            const dataRepo: Pick<DataRepository, 'labels'> = {
                labels: [
                    {
                        labelId: faker.string.uuid(),
                        scopes: faker.helpers.arrayElements(
                            Object.keys(Scope) as ScopeType[],
                            2
                        ),
                        category: faker.helpers.arrayElement(
                            Object.keys(Category) as CategoryType[]
                        ),
                        subCategory: faker.helpers.arrayElement(
                            Object.keys(SubCategory) as SubCategoryType[]
                        ),
                        name: faker.string.alphanumeric(),
                        createdAt: faker.date.anytime().toDateString(),
                    },
                ],
            }

            const result = transformData(dataRepo as DataRepository)

            expect(result).toStrictEqual({
                Labels: {
                    data: { data: dataRepo.labels, type: 'labels' },
                },
            })
        })
    })

    describe('mantik#287 Data repo versions should NOT be shown in the Details view', () => {
        it('transforms data repository without versions', () => {
            const dataRepo: Pick<DataRepository, 'versions'> = {
                versions: { test: 'should not be saved' },
            }

            const result = transformData(dataRepo as DataRepository)

            expect(result).toStrictEqual({})
        })
    })

    it('transforms empty input data to empty data', () => {
        const dataRepo = {
            someField: null,
            someOtherField: undefined,
        }

        const result = transformData(dataRepo as unknown as DataRepository)

        expect(result).toStrictEqual({
            'Some Field': { data: null },
            'Some Other Field': { data: null },
        })
    })

    it('transforms boolean input data to spoken language', () => {
        const dataRepo1 = {
            isDvcEnabled: true,
        }
        const dataRepo2 = {
            isDvcEnabled: false,
        }

        const result1 = transformData(dataRepo1 as unknown as DataRepository)
        const result2 = transformData(dataRepo2 as unknown as DataRepository)

        expect(result1).toStrictEqual({
            'Is Dvc Enabled': { data: 'Yes' },
        })
        expect(result2).toStrictEqual({
            'Is Dvc Enabled': { data: 'No' },
        })
    })
})
