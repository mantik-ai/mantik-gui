import { faker } from '@faker-js/faker/.'
import { DataTag, QueryKey, UseQueryResult } from '@tanstack/react-query'
import { render, screen } from '@testing-library/react'
import { NextRouter, useRouter } from 'next/router'
import AuthContext from '../../../src/context/AuthContext'
import { ProjectProvider } from '../../../src/context/ProjectContext'
import { useProjectsProjectIdGetProjectsProjectIdGet } from '../../../src/queries'

jest.mock('.../../../src/queries')
jest.mock('next/router', () => ({ useRouter: jest.fn() }))

describe('ProjectContext', () => {
    const mockUser = { accessToken: 'mockToken' }

    const mockRouter = { query: { id: faker.string.uuid() } }

    beforeEach(() => {
        jest.mocked(useRouter).mockReturnValue(
            mockRouter as unknown as NextRouter
        )
    })
    it('should render content', async () => {
        jest.mocked(
            useProjectsProjectIdGetProjectsProjectIdGet
        ).mockReturnValue({
            data: {},
            status: 'success',
        } as UseQueryResult & { queryKey: DataTag<QueryKey, { id: string }> })

        render(
            <AuthContext.Provider value={mockUser}>
                <ProjectProvider>
                    <div>Project details page</div>
                </ProjectProvider>
            </AuthContext.Provider>
        )

        expect(await screen.findByText('Project details page')).toBeVisible()
    })

    it('shows loading indicator when status is `pending`', async () => {
        jest.mocked(
            useProjectsProjectIdGetProjectsProjectIdGet
        ).mockReturnValue({
            data: undefined,
            status: 'pending',
        } as UseQueryResult & { queryKey: DataTag<QueryKey, { id: string }> })

        render(
            <AuthContext.Provider value={mockUser}>
                <ProjectProvider>
                    <div>Project details page</div>
                </ProjectProvider>
            </AuthContext.Provider>
        )

        expect(await screen.findByText('Loading Project...')).toBeVisible()
    })

    it('renders nothing when status is `error`', async () => {
        jest.mocked(
            useProjectsProjectIdGetProjectsProjectIdGet
        ).mockReturnValue({
            data: undefined,
            status: 'error',
        } as UseQueryResult & { queryKey: DataTag<QueryKey, { id: string }> })

        const { container } = render(
            <AuthContext.Provider value={mockUser}>
                <ProjectProvider>
                    <div>Project details page</div>
                </ProjectProvider>
            </AuthContext.Provider>
        )

        expect(container.firstChild).toBeNull()
    })
})
