import { renderApiErrorMessage } from '../../src/errors'

const Expected = 'should be rendered'

describe('API errors should be rendered correctly', () => {
    it('renders the error message given in the response data message field', () => {
        const error = {
            message: 'shoult not be rendered',
            response: {
                data: {
                    message: 'should be rendered',
                },
            },
        }

        const result = renderApiErrorMessage(error)

        expect(result).toBe(Expected)
    })

    it('renders the error message given in the response data detail field', () => {
        const error = {
            message: 'shoult not be rendered',
            response: {
                data: {
                    detail: 'should be rendered',
                },
            },
        }

        const result = renderApiErrorMessage(error)

        expect(result).toBe(Expected)
    })

    it('renders the error message given in the message field', () => {
        const error = {
            message: 'should be rendered',
            response: {
                data: {},
            },
        }

        const result = renderApiErrorMessage(error)

        expect(result).toBe(Expected)
    })
})
