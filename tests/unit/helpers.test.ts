import {
    extractExpiryTime,
    formatTimestamp,
    formatUnixTimestamp,
    isTokenExpired,
    shortenString,
} from '../../src/helpers'

describe('formatUnixTimestamp', () => {
    it('should return an error message if no valid timestamp is provided', () => {
        const date = formatUnixTimestamp(undefined)
        const errorMessage = 'Invalid timestamp'
        expect(date).toBe(errorMessage)
    })
    it('should return a formatted date', () => {
        const date = formatUnixTimestamp(1626274800)
        expect(date).toEqual(
            expect.stringMatching(/^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}/)
        )
    })
})

describe('extractExpiryTime', () => {
    it('should return null if no expiration time is present in the token', () => {
        const tokenWithoutExp =
            'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.FKgMwxSGMeiFMAETj-K13_PVmdaKVv4jPv4lU03Ev8U'
        const expiryTime = extractExpiryTime(tokenWithoutExp)
        expect(expiryTime).toBeNull()
    })

    it('should extract and return the correct expiry time from the token', () => {
        const validToken =
            'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyLCJleHAiOjE1MTYyMzkwMjJ9.rv-MnDjESa5UDsxYzKq1I3D4P-UVN5M2kZG9iMGR7A0'
        const expiryTime = extractExpiryTime(validToken)
        const expectedExpiryTime = new Date('2018-01-18T01:30:22.000Z')
        expect(expiryTime?.toISOString()).toEqual(
            expectedExpiryTime.toISOString()
        )
    })
})
describe('isTokenExpired', () => {
    it('should return false if no expiration time is present in the token', () => {
        const tokenWithoutExp =
            'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.FKgMwxSGMeiFMAETj-K13_PVmdaKVv4jPv4lU03Ev8U'
        const expiryTime = isTokenExpired(tokenWithoutExp)
        expect(expiryTime).toBe(false)
    })
    it('should return a boolean if a valid token is provided', () => {
        const validToken =
            'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyLCJleHAiOjE1MTYyMzkwMjJ9.rv-MnDjESa5UDsxYzKq1I3D4P-UVN5M2kZG9iMGR7A0'
        const isExpired = isTokenExpired(validToken)
        const expectedValue = new Date() >= extractExpiryTime(validToken)!
        expect(isExpired).toEqual(expectedValue)
    })
})

describe('formatTimestamp', () => {
    it('should format ISO timestamp correctly', () => {
        const isoTimestamp = '2023-11-09T12:34:56'
        const formatted = formatTimestamp(isoTimestamp)
        expect(formatted).toMatch(/^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}$/)
    })

    it('should format UNIX timestamp correctly', () => {
        const unixTimestamp = '1678509296' // Replace with a valid UNIX timestamp
        const formatted = formatTimestamp(unixTimestamp)
        expect(formatted).toMatch(/^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}$/)
    })

    it('should handle invalid timestamp', () => {
        const invalidTimestamp = 'invalid'
        const formatted = formatTimestamp(invalidTimestamp)
        expect(formatted).toBe('Invalid timestamp')
    })
})
describe('shortenString', () => {
    it("should return the given string if the string's length is smaller than or equal to the minimum length", () => {
        const str = '20 character string.'
        const expected = str
        const result = shortenString(str, 20, 100, 2000)
        expect(result).toBe(expected)
    })
    it('should return the given string if the viewport width is smaller than 1600', () => {
        const str = '20 character string.'
        const expected = str
        const result = shortenString(str, 20, 100, 1400)
        expect(result).toBe(expected)
    })
    it("should return a shortened string if the string's length exceeds the maximum length", () => {
        const str = 'A very long 57 character string that should be shortened.'
        const expected = 'A very long 57 chara...'
        const result = shortenString(str, 10, 20, 2400)
        expect(result).toBe(expected)
    })
    it("should return a shortened string if the viewports width is larger than 1600 and the string's length is longer than min", () => {
        const str = 'A very long 57 character string that should be shortened.'
        const expected = 'A very long 57 character strin...'
        const result = shortenString(str, 20, 50, 2100)
        expect(result).toBe(expected)
    })
})
