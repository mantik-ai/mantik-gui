import {
    refactorDataKeys,
    refactorDataValues,
    refactorDataForTableView,
    mapTableColumns,
    addHiddenTableColumnProp,
} from '../../../src/components/TablesLayout/extractTableData'

describe('Data from API should be refactored in order to be displayed in a table or list', () => {
    /* mapTableColumns */
    it('Returns an array of key value pairs according to json map prop ', () => {
        const arrayOfObjects = [
            {
                prop1: 'A String Value',
                prop2: {
                    name: 'A Name Value',
                    uri: 'https://www.url.com',
                    someOtherKey: 'Some Other Value',
                },
                prop3: {
                    nestedObject: {
                        name: 'Nested Object Name Value',
                        uri: 'https://www.url.com',
                        someOtherKey: 'Some Other Value',
                    },
                },
            },
        ]
        const json = {
            componentName: {
                map: {
                    prop1: {
                        data: 'name',
                    },
                    prop2: {
                        data: 'prop2.name',
                        link: 'prop2.uri',
                        type: 'link',
                    },
                    prop3: {
                        data: 'prop3.nestedObject.name',
                    },
                },
            },
        }
        const expected = [
            {
                prop1: {
                    data: 'A String Value',
                },
                prop2: {
                    data: 'A Name Value',
                    link: 'https://www.url.com',
                    type: 'link',
                },
                prop3: {
                    data: 'Nested Object Name Value',
                },
            },
        ]
        const result = mapTableColumns(arrayOfObjects, 'componentName', json)

        expect(result).toEqual(expected)
    })

    /* addHiddenTableColumnProp */
    it('Returns an array of nested objects with hiddenTableColumn prop added to each nested object', () => {
        const arrayOfObjects = [
            {
                prop1: {
                    data: 'A String Value',
                    link: 'https://www.url.com',
                },
                prop2: {
                    data: 'Another String Value',
                },
            },
        ]
        const json = {
            componentName: {
                hiddenTableColumns: ['prop1'],
            },
        }
        const expected = [
            {
                prop1: {
                    data: 'A String Value',
                    link: 'https://www.url.com',
                    hiddenTableColumn: true,
                },
                prop2: {
                    data: 'Another String Value',
                },
            },
        ]
        const result = addHiddenTableColumnProp(
            arrayOfObjects,
            'componentName',
            json
        )

        expect(result).toEqual(expected)
    })

    it('Turns camelCased keys into human readable format', () => {
        const obj = {
            someKey: 'Some Value',
            someOtherKey: 'Some Other Value',
            someObject: {
                aKey: 'A Value',
                name: 'A Name',
                someOtherKey: 'Some Other Value',
            },
            someLinkObject: {
                uri: 'https://gitlab.com/mantik-ai/tutorials',
                name: 'Tutorials',
            },
        }
        const result = refactorDataKeys(obj)
        const expected = {
            'Some Key': 'Some Value',
            'Some Other Key': 'Some Other Value',
            'Some Object': {
                aKey: 'A Value',
                name: 'A Name',
                someOtherKey: 'Some Other Value',
            },
            'Some Link Object': {
                uri: 'https://gitlab.com/mantik-ai/tutorials',
                name: 'Tutorials',
            },
        }

        expect(result).toEqual(expected)
    })

    it('Returns array of objects with data key and value if value is a string or a number', () => {
        const shallowObj = {
            someKey: 'Some Value',
            someOtherKey: 0,
        }
        const result = refactorDataValues(shallowObj)
        const expected = [{ data: 'Some Value' }, { data: 0 }]

        expect(result).toEqual(expected)
    })

    it('Returns array of objects with data key and name value if "name" prop is found', () => {
        const nestedObj = {
            someObject: {
                aKey: 'A Value',
                name: 'A Name',
                someOtherKey: 'Some Other Value',
            },
        }
        const result = refactorDataValues(nestedObj)
        const expected = [{ data: 'A Name' }]

        expect(result).toEqual(expected)
    })

    it('Returns array of objects with data and link key if "uri" prop is found', () => {
        const nestedObj = {
            someLinkObject: {
                uri: 'https://gitlab.com/mantik-ai/tutorials',
                name: 'Tutorials',
            },
        }
        const result = refactorDataValues(nestedObj)
        const expected = [
            {
                data: 'Tutorials',
                link: 'https://gitlab.com/mantik-ai/tutorials',
            },
        ]
        expect(result).toEqual(expected)
    })

    it('Changes the Object key if title prop is found in json file', () => {
        const arr = {
            someKey: {
                title: 'Title',
                data: 'data',
            },
        }
        const result = refactorDataKeys(arr)
        const expected = {
            Title: {
                data: 'data',
            },
        }
        expect(result).toEqual(expected)
    })

    it('Returns an array of objects with string values in data prop', () => {
        const arr = [
            {
                someKey: 'Some Value',
                someOtherKey: 'Some Other Value',
            },
        ]
        const result = refactorDataForTableView(arr)
        const expected = [
            {
                data: {
                    'Some Key': { data: 'Some Value' },
                    'Some Other Key': { data: 'Some Other Value' },
                },
            },
        ]
        expect(result).toEqual(expected)
    })
})
