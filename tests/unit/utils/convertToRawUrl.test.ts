import { Platform } from '../../../src/queries'
import { convertToRawUrl } from '../../../src/utils/convertToRawUrl'

describe('convertToRawUrl returns an URL leading to a raw file', () => {
    /**
     * parameters:
     *
     * platform: Platform,
     * repoUri: string,
     * commit: string,
     * path: string
     *
     */

    it('On GitLab on main branch', () => {
        const platform: Platform = Platform.GitLab
        const repoUri = 'https://gitlab.com/username/repo'
        const commit = 'main'
        const path = 'path/to/file.txt'

        const result = convertToRawUrl(platform, repoUri, commit, path)
        const expected =
            'https://gitlab.com/username/repo/raw/main/path/to/file.txt'

        expect(result).toBe(expected)
    })
    it('On GitHub with commit hash', () => {
        const platform: Platform = Platform.GitHub
        const repoUri = 'https://github.com/4castRenewables/maelstrom-a2'
        const commit = 'ceb8d1e3ffd6410e8a646bdd7abbecf2155614d2'
        const path = '.pre-commit-config.yaml'

        const result = convertToRawUrl(platform, repoUri, commit, path)
        const expected =
            'https://raw.github.com/4castRenewables/maelstrom-a2/ceb8d1e3ffd6410e8a646bdd7abbecf2155614d2/.pre-commit-config.yaml'

        expect(result).toBe(expected)
    })
    it('On Bitbucket on main branch', () => {
        const platform: Platform = Platform.Bitbucket
        const repoUri =
            'https://bitbucket.org/kristian4castmantik/mantikminimalproject'
        const commit = 'main'
        const path = 'mlflow/MLproject'

        const result = convertToRawUrl(platform, repoUri, commit, path)
        const expected =
            'https://bitbucket.org/kristian4castmantik/mantikminimalproject/raw/main/mlflow/MLproject'

        expect(result).toBe(expected)
    })
})
