import { LabelsGet200Response } from '../../../src/queries'
import { autoCompleteLabelOption } from '../../../src/utils/autoCompleteLabelOption'

describe('calculateOptions function', () => {
    it('Retrun options values with category correctly', () => {
        const data: LabelsGet200Response = {
            labels: [
                {
                    labelId: '1',
                    scopes: [],
                    name: 'Label 1',
                    category: 'Tasks',
                    subCategory: 'Multimodal',
                    createdAt: '2023-11-23T09:10:36.216Z',
                },
                {
                    labelId: '2',
                    scopes: [],
                    name: 'Label 2',
                    category: 'Software',
                    subCategory: undefined,
                    createdAt: '2023-11-23T09:10:36.216Z',
                },
                // Add more mock data as needed
            ],
            totalRecords: 2,
            pageRecords: 2,
        }

        const expectedOptions = [
            {
                labelId: '1',
                scopes: [],
                name: 'Label 1',
                category: 'Tasks',
                subCategory: 'Multimodal',
                categoryWithSubCategory: 'Tasks: Multimodal',
                createdAt: '2023-11-23T09:10:36.216Z',
            },
            {
                labelId: '2',
                scopes: [],
                name: 'Label 2',
                category: 'Software',
                subCategory: undefined,
                categoryWithSubCategory: 'Software',
                createdAt: '2023-11-23T09:10:36.216Z',
            },
            // Add expected options for additional mock data
        ]

        const options = autoCompleteLabelOption(data)

        // Check if the calculated options match the expected options
        expect(options).toEqual(expectedOptions)
    })
})
