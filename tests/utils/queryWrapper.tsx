import { QueryClient, QueryClientProvider } from '@tanstack/react-query'
import { JSX } from 'react'

// call this function with component as argument in @testing-library/react render function for component with queryClient
export const queryWrapper = (children: JSX.Element) => {
    const queryClient = new QueryClient({
        defaultOptions: {
            queries: {
                staleTime: Infinity,
                retry: false,
            },
        },
    })
    return (
        <QueryClientProvider client={queryClient}>
            {children}
        </QueryClientProvider>
    )
}

// call this function in @testing-library/react renderHook function for hook with queryClient
export const createWrapper = () => {
    const queryClient = new QueryClient()
    return function Wrapper({ children }: { children: React.ReactElement }) {
        return (
            <QueryClientProvider client={queryClient}>
                {children}
            </QueryClientProvider>
        )
    }
}
