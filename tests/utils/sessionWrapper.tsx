import { SessionProvider } from 'next-auth/react'
import { JSX } from 'react'

const session = {
    data: {
        user: {
            name: 'testadmin',
            accessToken: '',
            refreshToken: '',
            accessTokenExpires: new Date(Date.now() + 2 * 86400).toISOString(),
        },
        expires: new Date(Date.now() + 2 * 86400).toISOString(),
    },
    status: 'authenticated',
}

export const sessionWrapper = (children: JSX.Element) => (
    <SessionProvider session={session.data}>{children}</SessionProvider>
)
