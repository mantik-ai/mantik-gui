import { APIRequest, APIRequestContext, Page } from '@playwright/test'

export let authRequestContext: APIRequestContext | undefined = undefined

export async function authFetch<T>(url: string) {
    if (!authRequestContext) throw Error('auth context not initialized!')

    const response = await authRequestContext.fetch(url)
    return (await response.json()) as T
}

export async function initializeAuthRequestContext(
    page: Page,
    request: APIRequest
) {
    const session = await page.evaluate(() => {
        return window.accessToken
    })
    authRequestContext = await request.newContext({
        baseURL: process.env.API_URL,
        extraHTTPHeaders: {
            Authorization: `Bearer ${session}`,
        },
    })
}
