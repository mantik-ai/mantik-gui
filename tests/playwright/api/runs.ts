import { faker } from '@faker-js/faker'
import {
    AddRun,
    CodeRepository,
    DataRepository,
    ExperimentRepository,
    ProjectsGet200Response,
    Run,
    User,
} from '../../../src/queries'
import { authFetch } from '../api/api'
import { Context } from '../navigate'

export async function fetchProjectByName(projectName: string) {
    const projectResponse = await authFetch<ProjectsGet200Response>(`/projects`)
    return projectResponse.projects?.find(({ name }) => name === projectName)
}

export async function fetchExperiment(
    projectId: string,
    experimentRepositoryId: string
) {
    return await authFetch<ExperimentRepository>(
        `/projects/${projectId}/experiments/${experimentRepositoryId}`
    )
}

export async function fetchCodeRepository(
    projectId: string,
    codeRepositoryId: string
) {
    return await authFetch<CodeRepository>(
        `/projects/${projectId}/code/${codeRepositoryId}`
    )
}

export async function fetchDataRepository(
    projectId: string,
    dataRepositoryId: string
) {
    return await authFetch<DataRepository>(
        `/projects/${projectId}/data/${dataRepositoryId}`
    )
}

async function fetchUserDetails() {
    return await authFetch<User>(`/user-details`)
}

let runs: Run[] = []

export function getFirstRun(): Run {
    if (!runs[0]) throw new Error('Run is expected to be set with addRun')
    return runs[0]
}

function getRuns(): Run[] {
    return runs
}

export function addRun(data: Run) {
    runs.unshift(data)
}

export function cleanRuns() {
    runs = []
}

export async function interceptGetRuns(context: Context) {
    await context.page.route(/^.*\/runs\?.*$/, async (route) => {
        const response = await route.fetch()
        const json = await response.json()

        if (route.request().method() !== 'GET') {
            await route.fallback()
            return
        }

        const runs = getRuns()
        await route.fulfill({
            response,
            json: { ...json, runs: [...runs, ...json.runs] },
        })
    })
}

export async function interceptPostRun(context: Context, projectName: string) {
    await context.page.route(/runs$/, async (route) => {
        if (route.request().method() !== 'POST') {
            await route.fallback()
            return
        }
        const postData = route.request().postData()
        if (!postData) throw new Error('No run data submitted')
        const run = await transformToValidRun(JSON.parse(postData), projectName)
        addRun(run)

        await route.fulfill({
            status: 200,
            contentType: 'application/json',
            body: JSON.stringify({ runId: run.runId }),
        })
    })
}

export async function transformToValidRun(
    data: AddRun,
    projectName: string
): Promise<Run> {
    const user = await fetchUserDetails()
    const project = await fetchProjectByName(projectName)
    if (!project) throw new Error(`Could not find project ${projectName}`)
    const experimentRepository = await fetchExperiment(
        project.projectId,
        data.experimentRepositoryId
    )
    const codeRepository = await fetchCodeRepository(
        project.projectId,
        data.codeRepositoryId
    )
    return {
        ...data,
        createdAt: new Date().toISOString(),
        runId: faker.string.uuid(),
        mlflowRunId: faker.string.uuid(),
        experimentRepository,
        modelRepository: {
            createdAt: faker.date.past().toISOString(),
            modelRepositoryId: faker.string.uuid(),
            codeRepository,
        },
        user,
        dataRepository: data.dataRepositoryId
            ? await fetchDataRepository(
                  project.projectId,
                  data.dataRepositoryId
              )
            : undefined,
    }
}

export async function getValidRunData(projectName: string): Promise<Run> {
    const project = await fetchProjectByName(projectName)
    if (!project) throw new Error(`Could not find project ${projectName}`)

    const experimentRepository = await fetchExperiment(
        project.projectId,
        '831bc10b-1351-47d2-b215-468cdcf83deb'
    )
    const codeRepository = await fetchCodeRepository(
        project.projectId,
        'e48fbbb2-97b7-4453-a4ad-e1c60cae40b1'
    )

    const dataRepository = await fetchDataRepository(
        project.projectId,
        'd2da0a96-c66f-493a-9b3e-ccfb6badb249'
    )

    return {
        name: faker.word.noun(),
        createdAt: new Date().toISOString(),
        runId: faker.string.uuid(),
        experimentRepository,
        modelRepository: {
            createdAt: faker.date.past().toISOString(),
            modelRepositoryId: 'bb6ba424-a7bf-4939-a476-17cab4324bdb',
            codeRepository,
            branch: 'main',
        },
        user: {
            createdAt: faker.date.past().toISOString(),
            name: faker.string.sample(),
            userId: faker.string.uuid(),
        },
        mlflowRunId: '16a98400-9fb7-400c-8d7f-90a6aaef7334',
        dataRepository,
        backendConfig: {
            Exclude: ['recipe.def', '*.sif', 'Dockerfile'],
            Resources: {
                Nodes: 1,
                Queue: 'develop',
            },
            Environment: {
                Apptainer: {
                    Path: '<PATH TO THE APPTAINER IMAGE>/wine-quality-estimator.sif',
                    Type: 'remote',
                },
            },
            UnicoreApiUrl:
                'https://zam2125.zam.kfa-juelich.de:9112/JUWELS/rest/core',
        },
        entryPoint: 'main',
        mlflowMlprojectFilePath:
            'wine-quality-estimator/mlproject-docker/MLproject',
        dataBranch: 'main',
        computeBudgetAccount: 'trial',
        mlflowParameters: { alpha: 'example' },
        connection: {
            connectionId: 'fa0b5d87-c8b3-421a-92a7-4cc7bfa22e42',
            user: {
                userId: '50358336-053a-4e7d-9b97-ca6bf6e84bfd',
                name: 'playwright',
                createdAt: '2024-06-26T07:21:54.381809+00:00',
            },
            connectionName: 'JSC Token',
            connectionProvider: 'Jülich Supercomputing Centre (JSC)',
            authMethod: 'Token',
            createdAt: '2024-07-25T08:59:29.824540+00:00',
        },
    }
}
