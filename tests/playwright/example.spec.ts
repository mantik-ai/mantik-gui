import { expect, test } from '@playwright/test'

test.describe('Example Tests:', () => {
    test('has Mantik heading on the landing page', async ({ page }) => {
        await page.goto('/')

        await expect(
            page.getByRole('heading', { name: 'Mantik' })
        ).toBeVisible()
    })

    test('can navigate via Discover button on home page', async ({ page }) => {
        await page.goto('/')

        await page.getByRole('link', { name: 'Discover' }).click()
        await page.waitForURL('/projects')

        await expect(
            page.getByRole('heading', {
                level: 1,
                name: 'Projects',
                exact: true,
            })
        ).toBeVisible()
    })

    test('projects page renders public projects', async ({ page }) => {
        // Marked test as slow, relates to known issue:
        // https://gitlab.com/mantik-ai/mantik-gui/-/issues/403
        test.slow()
        await page.goto('/projects', { waitUntil: 'networkidle' })

        // Example public project: Minimal Mantik Project
        await expect(
            page.locator('span:has-text("Minimal Mantik Project")')
        ).toBeVisible()
    })
})
