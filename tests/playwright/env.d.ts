declare global {
    namespace NodeJS {
        interface ProcessEnv {
            PLAYWRIGHT_TEST_ACCOUNT_GUEST: string
            PLAYWRIGHT_TEST_ACCOUNT_REPORTER: string
            PLAYWRIGHT_TEST_ACCOUNT_RESEARCHER: string
            PLAYWRIGHT_TEST_ACCOUNT_MAINTAINER: string
            PLAYWRIGHT_TEST_ACCOUNT_OWNER: string
            PLAYWRIGHT_TEST_ACCOUNT_1_PASSWORD: string
        }
    }
}
export {}
