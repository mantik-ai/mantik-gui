import { Page, expect } from '@playwright/test'
import dayjs from 'dayjs'

type Credentials = {
    username: string
    password: string
}

export const credentials: Record<string, Credentials> = {
    guest: {
        username: 'Playwright-Guest',
        password: process.env.PLAYWRIGHT_TEST_ACCOUNT_GUEST,
    },
    reporter: {
        username: 'Playwright-Reporter',
        password: process.env.PLAYWRIGHT_TEST_ACCOUNT_REPORTER,
    },
    researcher: {
        username: 'Playwright-Researcher',
        password: process.env.PLAYWRIGHT_TEST_ACCOUNT_RESEARCHER,
    },
    maintainer: {
        username: 'Playwright-Maintainer',
        password: process.env.PLAYWRIGHT_TEST_ACCOUNT_MAINTAINER,
    },
    owner: {
        username: 'Playwright-Owner',
        password: process.env.PLAYWRIGHT_TEST_ACCOUNT_OWNER,
    },
    owner_1: {
        username: 'playwright',
        password: process.env.PLAYWRIGHT_TEST_ACCOUNT_1_PASSWORD,
    },
}

export async function setCookieConsent(page: Page) {
    const value = {
        categories: ['necessary', 'analytics'],
        revision: 0,
        data: null,
        consentTimestamp: new Date().toISOString(),
        consentId: '5183b1bb-1a8c-4b07-8549-1a5793608fc9',
        services: {
            necessary: [],
            analytics: [],
        },
        lastConsentTimestamp: new Date().toISOString(),
        expirationTime: dayjs().add(6, 'month').valueOf(),
    }
    await page.context().addCookies([
        {
            name: 'cc_cookie',
            value: encodeURIComponent(JSON.stringify(value)),
            domain: 'localhost',
            path: '/',
            expires: -1,
            httpOnly: false,
            secure: false,
            sameSite: 'Lax',
        },
    ])
}

export async function unsetCookieConsent(page: Page) {
    await page.context().clearCookies({
        name: 'cc_cookie',
        domain: 'localhost',
        path: '/',
    })
}

export const login = async (page: Page, credentials: Credentials) => {
    // Navigate to the login page
    await page.goto('/login')
    // Log in
    await page.getByLabel('Username').fill(credentials.username)
    await page.getByLabel('Password').first().fill(credentials.password)
    await page.getByRole('button', { name: 'Login' }).click()

    await page.waitForURL('/', { waitUntil: 'networkidle' })
}

export const logout = async (page: Page) => {
    // Navigate to account dropdown menu
    await page.locator('button[aria-label="Your Account"]').click()
    // Log out
    await page.locator('li.MuiMenuItem-root:has-text("Logout")').click()
    await page.waitForURL('/', { waitUntil: 'networkidle' }) // The URL you expect after login

    await expect(page.getByRole('heading', { name: 'Mantik' })).toBeVisible()
}

/* 

  Permissions are defined here:
    https://mantik-ai.gitlab.io/mantik/user_ui/roles_permissions.html#project-roles-and-permission

  and the relevant permissions for data repositories are reproduced here in tabular form for convenience:

    ['Action'       , 'VISITOR', 'GUEST', 'REPORTER', 'RESEARCHER', 'MAINTAINER', 'OWNER']
    ['READ'         ,    YES   ,   YES  ,    YES    ,    YES      ,    YES      ,   YES  ]
    ['CREATE'       ,    ___   ,   ___  ,    ___    ,    YES      ,    YES      ,   YES  ]
    ['UPDATE'       ,    ___   ,   ___  ,    ___    ,    YES      ,    YES      ,   YES  ]
    ['DELETE'       ,    ___   ,   ___  ,    ___    ,    YES      ,    YES      ,   YES  ]

    In summary,
      * "Guests" and "Reporters" can only view data repositories
      * "Researchers", "Maintainers" and "Owners" can do everything
      
    The ability of "Owners" to perform actions, completely, is tested in other test files.

*/
