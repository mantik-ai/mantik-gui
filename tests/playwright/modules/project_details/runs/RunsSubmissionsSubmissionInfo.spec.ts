import { test, expect, Page } from '@playwright/test'
import { credentials, login, logout } from '../../../credentials'
import {
    navigateToProjectByName,
    navigateToSubmissionsList,
} from '../../../navigate'

const PROJECT_NAME = 'Test Project: Run - Submission'
const RUN_NAME = 'Local: wine quality estimator'

async function whenIOpenTheSubmissionInfoForARun(
    { page }: { page: Page },
    { project, run }: { project: string; run: string }
) {
    await navigateToProjectByName({ page }, project)
    await navigateToSubmissionsList({ page })

    const tableRow = page.locator('tr', { has: page.getByText(run) })
    await tableRow
        .locator('svg[data-testid="VisibilityOutlinedIcon"]')
        .first()
        .click()
    await page.getByText(/Submission Info/i).click()
}

async function thenISeeTheSubmissionInfoOfTheRun(
    { page }: { page: Page },
    {
        infrastructure,
        logs,
    }: { infrastructure: RegExp | string; logs: RegExp | string }
) {
    const infrastructureInfo = page.locator('li', {
        has: page.getByText(/infrastructure/),
    })
    await expect(infrastructureInfo).toBeVisible()
    await expect(infrastructureInfo.getByText(infrastructure)).toBeVisible()
    await expect(page.getByText(logs).first()).toBeVisible()
    await page.getByText('Close').click()
}

// Given there are previously executed Runs under PROJECT_NAME
test.describe('Submission Info', () => {
    test.beforeEach('login', async ({ page }) =>
        login(page, credentials.owner_1)
    )
    test.afterEach('logout', async ({ page }) => logout(page))

    test('can read submission info of a local run', async ({ page }) => {
        await whenIOpenTheSubmissionInfoForARun(
            { page },
            { run: RUN_NAME, project: PROJECT_NAME }
        )
        await thenISeeTheSubmissionInfoOfTheRun(
            { page },
            { infrastructure: /local/i, logs: /Building docker image/i }
        )
    })
})
