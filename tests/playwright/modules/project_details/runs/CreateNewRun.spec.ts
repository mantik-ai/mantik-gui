import { faker } from '@faker-js/faker'
import { expect, Locator, test } from '@playwright/test'
import { initializeAuthRequestContext } from '../../../api/api'
import {
    addRun,
    cleanRuns,
    getFirstRun,
    getValidRunData,
    interceptGetRuns,
    interceptPostRun,
} from '../../../api/runs'
import { credentials, login, logout } from '../../../credentials'
import {
    Context,
    navigateToProjectByName,
    navigateToSubmissionsList,
} from '../../../navigate'

const PROJECT_NAME = 'Test Project: Run - Submission'
const newRunInputData = {
    name: faker.word.noun(),
    experimentRepository: {
        name: 'An Experiment',
    },
    codeRepository: {
        name: 'local run test code',
    },
    mlflowMlprojectFilePath: 'wine-quality-estimator/mlproject-docker',
    backendConfigFile:
        'wine-quality-estimator/mlproject-docker/compute-backend-config.yaml',
    connection: {
        name: 'JSC Token',
    },
    computeBudgetAccount: 'trial',
    dataRepository: {
        name: 'Https repo',
    },
    dataBranchName: 'main',
    parameters: {
        alpha: faker.string.numeric(),
    },
}

async function selectFromDropdown(
    { page }: Context,
    {
        label,
        optionName,
    }: {
        label: string | RegExp
        optionName: string | RegExp
    }
) {
    const selectElement = page.getByLabel(label)
    await selectElement.click()
    const selectedOption = page.getByRole('option', {
        name: optionName,
    })
    await selectedOption.click()
}

async function whenIOpenTheCreateNewRunDialog(
    context: Context,
    project: string
) {
    await navigateToProjectByName(context, project)
    await navigateToSubmissionsList(context)
    const addButton = context.page.getByRole('button', { name: /add/i })
    await expect(addButton).toBeEnabled()
    await addButton.click()
    await expect(context.page.getByRole('dialog')).toBeVisible()
}

async function whenIFillInRequiredFields(context: Context) {
    const { page } = context
    await page.getByLabel(/^name/i).fill(newRunInputData.name)

    await selectFromDropdown(context, {
        label: /Experiment Repository/i,
        optionName: new RegExp(newRunInputData.experimentRepository.name, 'i'),
    })
    await selectFromDropdown(context, {
        label: /Code Repository/i,
        optionName: new RegExp(newRunInputData.codeRepository.name, 'i'),
    })
    await page
        .getByLabel(/relative path to mlflow project directory/i)
        .fill(newRunInputData.mlflowMlprojectFilePath)
    await page.getByLabel(/alpha/).fill(newRunInputData.parameters.alpha)
    await page
        .getByLabel(/relative path to compute backend config file/i)
        .fill(newRunInputData.backendConfigFile)

    await selectFromDropdown(context, {
        label: /Stored Connection/i,
        optionName: new RegExp(newRunInputData.connection.name, 'i'),
    })
    await expect(
        page.getByLabel(/unicore compute budget account/i)
    ).toBeVisible()
    await page
        .getByLabel(/unicore compute budget account/i)
        .fill(newRunInputData.computeBudgetAccount)
}

async function whenIChangeBranchToCommitHash(context: Context) {
    const { page } = context
    const dataRepositoryInputs = page.getByTestId('dataRepositoryInputs')
    await expect(dataRepositoryInputs).toBeVisible()
    await dataRepositoryInputs
        .getByLabel('Commit Hash', { exact: true })
        .click()
    await dataRepositoryInputs
        .locator('input[type=text]')
        .fill('48906ba621f82a04b810a20694c9654f0c53a0c9')
}

async function whenISelectADataRepository(context: Context) {
    await selectFromDropdown(context, {
        label: /Data Repository/i,
        optionName: new RegExp(newRunInputData.dataRepository.name, 'i'),
    })
}

async function whenISelectBranchNameForDataRepository(context: Context) {
    const { page } = context

    const dataRepositoryInputs = page.getByTestId('dataRepositoryInputs')
    await expect(dataRepositoryInputs).toBeVisible()
    await dataRepositoryInputs.getByLabel('Branch', { exact: true }).click()
    await dataRepositoryInputs
        .getByLabel('Branch Name')
        .fill(newRunInputData.dataBranchName)
}

async function whenISubmitTheForm(
    context: Context,
    withGetRunsIntercept?: boolean
) {
    await interceptPostRun(context, PROJECT_NAME)
    if (withGetRunsIntercept) {
        await interceptGetRuns(context)
    }

    const submitButton = context.page.getByRole('button', { name: /run/i })
    await expect(submitButton).toBeEnabled({ timeout: 3000 })

    await submitButton.click()

    await expect(context.page.getByText(/success/)).toBeVisible({
        timeout: 15_000,
    })
    await context.page.getByRole('button', { name: /close/i }).click()
    await expect(context.page.getByRole('dialog')).not.toBeVisible()
}

async function openRunDetails(context: Context) {
    const run = getFirstRun()
    await expect(
        context.page.getByText(run.name, { exact: true })
    ).toBeVisible()
    const detailsButton = context.page.getByTestId('rud-button-Details').first()
    await detailsButton.click()
    const dialog = context.page.getByRole('dialog')
    await expect(dialog).toBeVisible()
    await expect(dialog.getByText(run.name)).toBeVisible()
    return dialog
}

async function checkDataRepositoryIsVisible(scope: Locator) {
    const run = getFirstRun()
    if (!run.dataRepository) {
        throw new Error('Data Repository is empty')
    }

    const repositoryContainer = scope.locator('li', {
        hasText: 'Data',
    })
    await expect(
        repositoryContainer.getByText(run.dataRepository.dataRepositoryId)
    ).toBeVisible()
    return repositoryContainer
}

async function thenICanSeeDataRepositoryAndBranchNameInRunDetails(
    context: Context
) {
    const run = getFirstRun()
    const dialog = await openRunDetails(context)
    const repositoryContainer = await checkDataRepositoryIsVisible(dialog)
    if (!run.dataBranch) {
        throw new Error('Data Branch is empty')
    }

    await expect(repositoryContainer.getByText(run.dataBranch)).toBeVisible()
    await closeDialog(dialog)
}

async function thenICanSeeDataRepositoryAndCommitHashInRunDetails(
    context: Context
) {
    const run = getFirstRun()
    const dialog = await openRunDetails(context)
    const repositoryContainer = await checkDataRepositoryIsVisible(dialog)
    if (!run.dataCommit) {
        throw new Error('Data Commit is empty')
    }

    await expect(repositoryContainer.getByText(run.dataCommit)).toBeVisible()
    await closeDialog(dialog)
}

async function givenIHaveCreatedARun(context: Context) {
    const { page } = context

    const run = await getValidRunData(PROJECT_NAME)
    addRun(run)

    await interceptGetRuns({ page })
}

async function givenIHaveCreatedARunWithCommitHash(context: Context) {
    const { page } = context

    const run = await getValidRunData(PROJECT_NAME)
    addRun({
        ...run,
        dataBranch: undefined,
        dataCommit: '48906ba621f82a04b810a20694c9654f0c53a0c9',
    })

    await interceptGetRuns({ page })
}

async function whenIOpenTheReRunDialog(context: Context, project: string) {
    await navigateToProjectByName(context, project)
    await navigateToSubmissionsList(context)
    const viewMoreButton = context.page
        .getByRole('button', { name: /more/i })
        .first() // FIXME: we do not want this
    await expect(viewMoreButton).toBeEnabled()
    await viewMoreButton.click()
    const reSubmitRunButton = context.page.getByRole('menuitem', {
        name: /re-submit run/i,
    })

    await reSubmitRunButton.click()
    await expect(context.page.getByRole('dialog')).toBeVisible()

    const progressBars = context.page.getByRole('progressbar')
    for (const bar of await progressBars.all()) {
        await expect(bar).not.toBeVisible()
    }
}

async function thenICanSeePreFilledDataRepoAndBranch(context: Context) {
    const run = getFirstRun()
    const { page } = context
    const selectElement = page.getByLabel(/Data Repository/)
    if (!run?.dataRepository?.dataRepositoryName || !run.dataBranch) {
        throw new Error(
            'Data Repository name or data branch name is not defined'
        )
    }

    await expect(
        selectElement.getByText(run.dataRepository.dataRepositoryName)
    ).toBeVisible()

    const dataRepositoryInputs = page.getByTestId('dataRepositoryInputs')
    await expect(
        dataRepositoryInputs.getByRole('textbox', { name: /Branch Name/ })
    ).toHaveValue(run.dataBranch)
}

async function thenICanSeePreFilledDataRepoAndCommitHash(context: Context) {
    const { page } = context
    const run = getFirstRun()
    const selectElement = page.getByLabel(/Data Repository/)

    if (!run?.dataRepository?.dataRepositoryName || !run.dataCommit) {
        throw new Error('Data Repository name or commit hash is not defined')
    }
    await expect(
        selectElement.getByText(run.dataRepository.dataRepositoryName)
    ).toBeVisible()

    const dataRepositoryInputs = page.getByTestId('dataRepositoryInputs')
    await expect(
        dataRepositoryInputs.getByRole('textbox', { name: /Commit Hash/ })
    ).toHaveValue(run.dataCommit)
}

async function thenICanSeeNewRunInTheList(context: Context) {
    const run = getFirstRun()
    await expect(context.page.getByText(run.name)).toHaveCount(2)
}

async function closeDialog(dialog: Locator) {
    const closeButton = dialog.getByText(/close/i)
    await closeButton.click()
    await expect(dialog).not.toBeVisible()
}

async function givenIHaveCreatedARunWithAJupyterNotebook(context: Context) {
    const { page } = context

    const run = await getValidRunData(PROJECT_NAME)
    addRun({
        ...run,
        notebookSource: {
            location: faker.internet.url(),
            provider: 'Jupyter',
        },
    })

    await interceptGetRuns({ page })
}

async function thenICannotSeeAResubmitRunButtonToOpenTheReRunDialog(
    context: Context,
    project: string
) {
    await navigateToProjectByName(context, project)
    await navigateToSubmissionsList(context)
    const viewMoreButton = context.page
        .getByRole('button', { name: /more/i })
        .first()
    await expect(viewMoreButton).toBeEnabled()
    await viewMoreButton.click()
    const reSubmitRunButton = context.page.getByRole('menuitem', {
        name: /re-submit run/i,
    })

    await expect(reSubmitRunButton).not.toBeVisible()
    // to close dropdown menu
    await context.page.getByRole('presentation').click()
}

test.describe('Create New Run', () => {
    test.beforeEach('login', async ({ page, playwright: { request } }) => {
        await login(page, credentials.owner_1)
        await initializeAuthRequestContext(page, request)
        cleanRuns()
    })

    test.afterEach('logout', async ({ page }) => logout(page))

    test.describe('mantik-api#315 Data repo and data version can be submitted with the run form.', () => {
        test('user creates a run with a data repo plus branch and submits the form', async ({
            page,
        }) => {
            await whenIOpenTheCreateNewRunDialog({ page }, PROJECT_NAME)
            await whenIFillInRequiredFields({ page })
            await whenISelectADataRepository({ page })
            await whenISelectBranchNameForDataRepository({ page })
            await whenISubmitTheForm({ page }, true)
            await thenICanSeeDataRepositoryAndBranchNameInRunDetails({ page })
        })

        test('user creates a run with data repo plus commit hash and submits the form', async ({
            page,
        }) => {
            await whenIOpenTheCreateNewRunDialog({ page }, PROJECT_NAME)
            await whenIFillInRequiredFields({ page })
            await whenISelectADataRepository({ page })
            await whenIChangeBranchToCommitHash({ page })
            await whenISubmitTheForm({ page }, true)
            await thenICanSeeDataRepositoryAndCommitHashInRunDetails({ page })
        })
    })

    test.describe('mantik-api#316 Data repo and data version can be pre-filled in the re-run form.', () => {
        test('user re-submits a run with a data repo plus branch and submits the form', async ({
            page,
        }) => {
            await givenIHaveCreatedARun({ page })
            await whenIOpenTheReRunDialog({ page }, PROJECT_NAME)
            await thenICanSeePreFilledDataRepoAndBranch({ page })
            await whenISubmitTheForm({ page })
            await thenICanSeeNewRunInTheList({ page })
        })

        test('user re-submits a run with data repo plus commit hash and submits the form', async ({
            page,
        }) => {
            await givenIHaveCreatedARunWithCommitHash({ page })
            await whenIOpenTheReRunDialog({ page }, PROJECT_NAME)
            await thenICanSeePreFilledDataRepoAndCommitHash({ page })
            await whenISubmitTheForm({ page })
            await thenICanSeeNewRunInTheList({ page })
        })
    })

    test.describe('mantik-gui#437 Rerun disabled for runs from jupyter notebooks.', () => {
        test('user cannot re-submit a run that has been executed on a jupyter notebook', async ({
            page,
        }) => {
            await givenIHaveCreatedARunWithAJupyterNotebook({ page })
            await thenICannotSeeAResubmitRunButtonToOpenTheReRunDialog(
                { page },
                PROJECT_NAME
            )
        })
    })
})
