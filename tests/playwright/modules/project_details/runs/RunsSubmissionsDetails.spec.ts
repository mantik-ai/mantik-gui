import { faker } from '@faker-js/faker'
import { expect, Page, test } from '@playwright/test'
import { NoteBookSource } from '../../../../../src/queries'
import { initializeAuthRequestContext } from '../../../api/api'
import {
    addRun,
    cleanRuns,
    getFirstRun,
    getValidRunData,
    interceptGetRuns,
} from '../../../api/runs'
import { credentials, login, logout } from '../../../credentials'
import {
    Context,
    navigateToProjectByName,
    navigateToSubmissionsList,
} from '../../../navigate'

const PROJECT_NAME = 'Test Project: Run - Submission'

type NotebookSource = Required<NoteBookSource>

const notebookSource: NotebookSource = {
    provider: 'Colab',
    version: faker.string.alphanumeric(),
    location: 'https://www.google.com',
}

async function updateRunWithJupyterNotebook(
    context: Context,
    notebookSource: NotebookSource | undefined
) {
    const { page } = context

    const run = await getValidRunData(PROJECT_NAME)
    const runWithNotebookSource = {
        ...run,
        notebookSource,
    }
    addRun(runWithNotebookSource)

    await interceptGetRuns({ page })

    return runWithNotebookSource
}

async function givenIHaveCreatedARunWithAJupyterNotebook(context: Context) {
    return await updateRunWithJupyterNotebook(context, notebookSource)
}

async function givenIHaveCreatedARunThatHasNoNotebookSource(context: Context) {
    return await updateRunWithJupyterNotebook(context, undefined)
}

async function thenICanNotViewTheNotebookSource({ page }: { page: Page }) {
    const repoInfo = page.locator('li', {
        has: page.getByText(`Code`),
    })
    await expect(repoInfo).toBeVisible()

    await expect(repoInfo.getByText(/Notebook Source/)).not.toBeVisible()

    await closeDialog({ page })
}

async function whenIOpenTheDetailsForARun(
    { page }: { page: Page },
    { project, run }: { project: string; run?: string }
) {
    const runName = run ?? getFirstRun().name
    await navigateToProjectByName({ page }, project)
    await navigateToSubmissionsList({ page })

    const tableRow = page.locator('tr', { has: page.getByText(runName) })
    await tableRow
        .locator('svg[data-testid="VisibilityOutlinedIcon"]')
        .first()
        .click()
    await page.getByRole('tab', { name: /Details/i }).click()
}

async function thenISeeTheInfrastructureOfTheRun({ page }: { page: Page }) {
    const infrastructureInfo = page.locator('li', {
        has: page.getByText(/Infrastructure/),
    })
    await expect(infrastructureInfo).toBeVisible()

    await expect(infrastructureInfo.getByText('cpuCores')).toBeVisible()
    await expect(infrastructureInfo.getByText('gpuCount')).toBeVisible()
    await expect(infrastructureInfo.getByText('hostname')).toBeVisible()
    await expect(infrastructureInfo.getByText('memoryGb')).toBeVisible()
    await expect(infrastructureInfo.getByText('os').first()).toBeVisible()
    await expect(infrastructureInfo.getByText('platform')).toBeVisible()
    await expect(infrastructureInfo.getByText('processor')).toBeVisible()
    await expect(infrastructureInfo.getByText('pythonExecutable')).toBeVisible()
    await expect(infrastructureInfo.getByText('pythonVersion')).toBeVisible()

    await closeDialog({ page })
}

async function thenICanViewTheInfoOfTheRepo({
    page,
    repoType,
}: {
    page: Page
    repoType: 'Code' | 'Data'
}) {
    const repoInfo = page.locator('li', {
        has: page.getByText(`${repoType}`),
    })
    await expect(repoInfo).toBeVisible()

    const repoName =
        repoType === 'Code' ? 'Local Run Test Code' : 'Local Run Test Data 2'
    const repoUri =
        repoType === 'Code'
            ? 'https://gitlab.com/mantik-ai/tutorials'
            : 'git@gitlab.com:mantik-ai/tutorials.git'
    const branchName = 'mantik-showcase'

    await expect(repoInfo.getByText('Name')).toBeVisible()
    await expect(repoInfo.getByText('ID')).toBeVisible()
    await expect(repoInfo.getByText('URI')).toBeVisible()
    await expect(repoInfo.getByText('Branch')).toBeVisible()

    await expect(repoInfo.getByText(repoName)).toBeVisible()
    await expect(repoInfo.getByText(repoUri)).toBeVisible()
    await expect(repoInfo.getByText(branchName)).toBeVisible()

    await closeDialog({ page })
}

async function closeDialog(context: Context) {
    await context.page.getByRole('button', { name: /close/i }).click()
    await expect(context.page.getByRole('dialog')).not.toBeVisible()
}

async function thenICanViewTheNotebookSource({ page }: { page: Page }) {
    const run = getFirstRun()
    if (
        !run.notebookSource ||
        (run.notebookSource && !('location' in run.notebookSource))
    ) {
        throw new Error('Notebook source url is not provided')
    }
    const repoInfo = page.locator('li', {
        has: page.getByText(`Code`),
    })
    await expect(repoInfo).toBeVisible()

    await expect(repoInfo.getByText(run.notebookSource.location)).toBeVisible()
    if (run.notebookSource.version) {
        await expect(
            repoInfo.getByText(run.notebookSource.version, { exact: true })
        ).toBeVisible()
    }

    await closeDialog({ page })
}

async function thenICanSeeTheNotebookSourcePage({ page }: { page: Page }) {
    const run = getFirstRun()

    if (
        !run.notebookSource ||
        (run.notebookSource && !('location' in run.notebookSource))
    ) {
        throw new Error('Notebook source url is not provided')
    }

    const newTabPromise = page.waitForEvent('popup')
    await page.getByText(run.notebookSource.location).click()
    const newTab = await newTabPromise

    await newTab.waitForLoadState()
    await expect(newTab).toHaveURL(run.notebookSource.location)
    await newTab.close()

    await closeDialog({ page })
}

test.describe('Run Details', () => {
    test.beforeEach('login', async ({ page, playwright: { request } }) => {
        await login(page, credentials.owner_1)
        await initializeAuthRequestContext(page, request)
        cleanRuns()
    })

    test.afterEach('logout', async ({ page }) => logout(page))

    test.describe('mantik-api#317 Infrastructure details are shown in run details', () => {
        test('can read infrastructure detail of a local run', async ({
            page,
        }) => {
            // Given there is a Run with infrastructure configured
            await whenIOpenTheDetailsForARun(
                { page },
                { run: 'infrastructure test run', project: PROJECT_NAME }
            )
            await thenISeeTheInfrastructureOfTheRun({ page })
        })
    })

    test.describe('mantik-api#318 code repo and version used for a run are shown in run details', () => {
        test('can read code repository detail of a run', async ({ page }) => {
            // Given I have a submitted run (any status)
            // And I specified a branch, uri and name for the related code repository
            await whenIOpenTheDetailsForARun(
                { page },
                { run: 'repositories test run', project: PROJECT_NAME }
            )
            await thenICanViewTheInfoOfTheRepo({ page, repoType: 'Code' })
        })
    })

    test.describe('mantik-api#318 data repo and version used for a run are shown in run details', () => {
        test('can read data repository detail of a run', async ({ page }) => {
            // Given I have a submitted run (any status)
            // And I specified a branch, uri and name for the related data repository
            await whenIOpenTheDetailsForARun(
                { page },
                { run: 'repositories test run', project: PROJECT_NAME }
            )
            await thenICanViewTheInfoOfTheRepo({ page, repoType: 'Data' })
        })
    })

    test.describe('mantik-gui#436 notebook source and version used for a run are shown in run details', () => {
        test('can read notebook source of a run', async ({ page }) => {
            await givenIHaveCreatedARunWithAJupyterNotebook({
                page,
            })
            await whenIOpenTheDetailsForARun(
                { page },
                { project: PROJECT_NAME }
            )
            await thenICanViewTheNotebookSource({ page })
        })

        test('can open url of notebook source', async ({ page }) => {
            await givenIHaveCreatedARunWithAJupyterNotebook({
                page,
            })
            await whenIOpenTheDetailsForARun(
                { page },
                { project: PROJECT_NAME }
            )
            await thenICanSeeTheNotebookSourcePage({ page })
        })

        test('can not see notebook source if it doesn`t exist', async ({
            page,
        }) => {
            await givenIHaveCreatedARunThatHasNoNotebookSource({
                page,
            })
            await whenIOpenTheDetailsForARun(
                { page },
                { project: PROJECT_NAME }
            )
            await thenICanNotViewTheNotebookSource({ page })
        })
    })
})
