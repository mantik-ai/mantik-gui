import { expect, Page, test } from '@playwright/test'
import { credentials, login, logout } from '../../../credentials'

// projects
const projectWithTwoCodeRepositories = 'a032bb33-7a76-40ec-b2e9-f2834d1aab21'
const projectEmpty = 'c4476001-38d2-4649-8515-20c952709ab3'
const projectEmptyTestCRD = 'ec3ac48f-65bb-445c-a8d2-e992704e96f1'
const projectTestRUD = '99084ef5-4093-49de-bbd8-34156da891ae'
// paths to projects
const pathToCodeRepositories = `/projects/details/${projectWithTwoCodeRepositories}/code`
const pathToEmptyCodeRepositories = `/projects/details/${projectEmpty}/code`
const pathToTestCRD = `/projects/details/${projectEmptyTestCRD}/code`
const pathToTestRUD = `/projects/details/${projectTestRUD}/code`

test.describe.configure({ mode: 'parallel' })

async function closeDialog({ page }: { page: Page }) {
    const dialog = page.getByRole('dialog')
    const cancelButton = page.getByRole('button', {
        name: 'Cancel',
    })
    await cancelButton.click()
    await expect(dialog).not.toBeVisible()
}

async function whenIOpenTheEditDialog({ page }: { page: Page }) {
    const dialog = page.getByRole('dialog')
    await expect(dialog).not.toBeVisible()
    const editIcon = page.locator('svg[data-testid="EditOutlinedIcon"]').last()
    await expect(editIcon).not.toBeDisabled()

    await editIcon.click()
    await expect(dialog).toBeVisible()
    await expect(page.getByRole('progressbar')).not.toBeVisible()
}

async function thenICanSeeTheSavedConnection({
    page,
    connectionValue,
}: {
    page: Page
    connectionValue: string
}) {
    const dialog = page.getByRole('dialog')

    const connection = dialog.getByLabel('Stored Connection')
    await expect(connection).toHaveText(connectionValue)
    await closeDialog({ page })
}

async function whenIRemoveSelectedConnection({ page }: { page: Page }) {
    const dialog = page.getByRole('dialog')
    const connection = dialog.getByLabel('Stored Connection')
    await expect(connection.getByText('GitHub Connection')).toBeVisible()
    await connection.click()
    const noneSelectedOption = page.getByRole('option', {
        name: 'None selected',
    })
    await expect(noneSelectedOption).toBeVisible()
    await noneSelectedOption.click()
}

async function whenISubmitTheEditForm({ page }: { page: Page }) {
    const dialog = page.getByRole('dialog')
    const updateButton = page.getByRole('button', {
        name: 'Update',
    })
    await updateButton.click()
    await expect(dialog).toBeVisible()

    const closeButton = page.getByRole('button', {
        name: 'Close',
    })
    await closeButton.click()
    await expect(dialog).not.toBeVisible()
}

async function whenISelectConnectionValue({ page }: { page: Page }) {
    const dialog = page.getByRole('dialog')
    const connection = dialog.getByLabel('Stored Connection')
    await expect(connection.getByText('None selected')).toBeVisible()
    await connection.click()
    const githubConnectionOption = page.getByRole('option', {
        name: 'GitHub Connection',
    })
    await expect(githubConnectionOption).toBeVisible()
    await githubConnectionOption.click()
}

async function revertChange({ page }: { page: Page }) {
    await whenIOpenTheEditDialog({ page })
    await whenISelectConnectionValue({
        page,
    })
    await whenISubmitTheEditForm({ page })
}

test.describe('Overview', () => {
    test('cards display with information', async ({ page }) => {
        await page.goto(pathToCodeRepositories, { waitUntil: 'networkidle' })
        expect(await page.textContent('#tableTitle')).toContain(
            'Testing Mock Data Double Populated'
        )
        const GitHubCard = page.locator('[data-testid="card-content"]').last()
        await expect(GitHubCard).toContainText('GitHub Description')
        const GitLabCard = page.locator('[data-testid="card-content"]').first()
        await expect(GitLabCard).toContainText('GitLab Description')
        const captionTypography = page
            .locator('[data-testid="timestamp-caption"]')
            .first()
        await expect(captionTypography).toContainText(
            'Created At: 2024-07-25 11:39:39'
        )
    })

    test('no cards display if none exist', async ({ page }) => {
        await page.goto(pathToEmptyCodeRepositories, {
            waitUntil: 'networkidle',
        })
        expect(await page.textContent('#tableTitle')).toContain(
            'Testing Mock Data Empty'
        )
        const card = page.locator('[data-testid="card-content"]')
        await expect(card).not.toBeVisible()
    })
})

test.describe('CRUD actions', () => {
    test.describe('Project Owner', () => {
        test.beforeEach('login', async ({ page }) =>
            login(page, credentials.owner_1)
        )
        test.afterEach('logout', async ({ page }) => logout(page))

        test.describe('ADD', () => {
            test.beforeEach('navigate', async ({ page }) => {
                await page.goto(pathToTestCRD, { waitUntil: 'networkidle' })
                expect(await page.textContent('#tableTitle')).toContain(
                    'Testing Mock ADD'
                )
            })

            test.afterEach('delete', async ({ page }) => {
                // Click the delete button on the third card we just created
                const dialog = page.getByRole('dialog')
                await expect(dialog).not.toBeVisible()
                const deleteIcon = page.locator(
                    'svg[data-testid="DeleteOutlineIcon"]'
                )
                await deleteIcon.click()
                await expect(dialog).toBeVisible()

                // Confirm Delete
                const deleteButton = page.getByRole('button', {
                    name: 'Delete',
                })
                await deleteButton.click()

                // Close the dialog & assert deletion
                const closeButton = page.getByRole('button', { name: 'Close' })
                await closeButton.click()
                await expect(dialog).not.toBeVisible()
                await expect(deleteIcon).not.toBeVisible()
            })

            test('can add a code repo with a code connection', async ({
                page,
            }) => {
                const dialog = page.getByRole('dialog')
                await expect(dialog).not.toBeVisible()
                const addButton = page.getByRole('button', { name: 'Add' })
                await expect(addButton).toBeVisible()
                await addButton.click()
                await expect(dialog).toBeVisible()
                await page.getByLabel('Name').fill('Creating trial code repo')
                await page
                    .getByLabel('URL')
                    .fill('https://gitlab.com/mantik-ai/tutorials')
                const connection = dialog.getByLabel('Stored Connection')
                await connection.click()

                const githubOption = page.getByRole('option', {
                    name: 'GitHub Connection',
                })
                await expect(githubOption).toBeVisible()
                await githubOption.click()

                const createButton = page.getByRole('button', {
                    name: 'Create',
                })
                await createButton.click()
                await expect(dialog).toBeVisible()

                const closeButton = page.getByRole('button', {
                    name: 'Close',
                })
                await closeButton.click()
                await expect(dialog).not.toBeVisible()

                const editIcon = page
                    .locator('svg[data-testid="EditOutlinedIcon"]')
                    .last()
                await editIcon.click()
                await expect(connection).toHaveText('GitHub Connection')
                const cancelButton = page.getByRole('button', {
                    name: 'Cancel',
                })
                await cancelButton.click()
                await expect(dialog).not.toBeVisible()
            })
        })

        test.describe('EDIT', () => {
            test.beforeEach('navigate', async ({ page }) => {
                await page.goto(pathToTestRUD, { waitUntil: 'networkidle' })
                expect(await page.textContent('#tableTitle')).toContain(
                    'Testing Mock EDIT'
                )
                const GitHubCard = page.locator('[data-testid="card-content"]')
                await expect(GitHubCard).toContainText('GitHub Description')
            })

            test('can edit code connection', async ({ page }) => {
                await whenIOpenTheEditDialog({ page })
                await whenIRemoveSelectedConnection({
                    page,
                })
                await whenISubmitTheEditForm({ page })
                await whenIOpenTheEditDialog({ page })
                await thenICanSeeTheSavedConnection({
                    page,
                    connectionValue: 'None selected',
                })

                await revertChange({ page })
            })

            test.describe('mantik-gui#438 can see connection pre-filled once Edit dialog is re-opened', () => {
                test('can see connection pre-filled', async ({ page }) => {
                    await whenIOpenTheEditDialog({ page })
                    await closeDialog({ page })
                    await whenIOpenTheEditDialog({ page })
                    await thenICanSeeTheSavedConnection({
                        page,
                        connectionValue: 'GitHub Connection',
                    })
                })

                test('can see pre-filled saved connection after selected another in the field and not save', async ({
                    page,
                }) => {
                    await whenIOpenTheEditDialog({ page })
                    await whenIRemoveSelectedConnection({
                        page,
                    })
                    await closeDialog({ page })
                    await whenIOpenTheEditDialog({ page })
                    await thenICanSeeTheSavedConnection({
                        page,
                        connectionValue: 'GitHub Connection',
                    })
                })
            })
        })
    })

    test.describe('Project Guest', () => {
        test.describe('Overview', () => {
            test('cannot create code repository', async ({ page }) => {
                await page.goto(pathToEmptyCodeRepositories, {
                    waitUntil: 'networkidle',
                })
                expect(await page.textContent('#tableTitle')).toContain(
                    'Testing Mock Data Empty'
                )
                const dialog = page.getByRole('dialog')
                await expect(dialog).not.toBeVisible()
                const addButton = page.getByRole('button', { name: 'Add' })
                await expect(addButton).not.toBeVisible()
            })
        })

        test.describe('Card', () => {
            test.beforeEach('navigate', async ({ page }) => {
                await page.goto(pathToCodeRepositories, {
                    waitUntil: 'networkidle',
                })
                expect(await page.textContent('#tableTitle')).toContain(
                    'Testing Mock Data Double Populated'
                )
            })

            test('cannot edit code repository', async ({ page }) => {
                const dialog = page.getByRole('dialog')
                await expect(dialog).not.toBeVisible()

                const editIcon = page
                    .locator('svg[data-testid="EditOutlinedIcon"]')
                    .last()
                await editIcon.waitFor({ state: 'visible' })
                await expect(editIcon).toBeDisabled()
                await editIcon.click({ force: true })
                await expect(dialog).not.toBeVisible()
            })

            test('cannot delete code repository', async ({ page }) => {
                const dialog = page.getByRole('dialog')
                await expect(dialog).not.toBeVisible()

                const deleteIcon = page
                    .locator('svg[data-testid="DeleteOutlineIcon"]')
                    .last()
                await deleteIcon.waitFor({ state: 'visible' })
                await expect(deleteIcon).toBeDisabled()
                await deleteIcon.click({ force: true })
                // dialog remains not visible after trying to click disabled edit button
                await expect(dialog).not.toBeVisible()
            })
        })
    })
})
