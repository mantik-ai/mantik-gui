import { expect, test } from '@playwright/test'

// Use project with 2 prefilled data repositories
const PROJECT_ID = 'a032bb33-7a76-40ec-b2e9-f2834d1aab21'

test.describe('mantik#287 Data repo details are shown in the Details view', () => {
    test('user can read details of a public data repository', async ({
        page,
    }) => {
        await page.goto(`/projects/details/${PROJECT_ID}/data`)

        const dialog = page.getByRole('dialog')
        await expect(dialog).not.toBeVisible()
        // Click on the details 'eye' icon
        const detailsIcon = page
            .locator('svg[data-testid="VisibilityOutlinedIcon"]')
            .last()
        await detailsIcon.waitFor({ state: 'visible' })
        await detailsIcon.click()
        await expect(dialog).toBeVisible()
        await expect(dialog).toHaveText(/User Role-1/)
        await expect(dialog).toHaveText(
            /Data Repository Id71759558-d004-44c2-9c4b-fbbeef6a2089/
        )
        await expect(dialog).toHaveText(/Uriwww.first-example.com/)
        await expect(dialog).toHaveText(
            /DescriptionFirst Data Repo Description/
        )
        await expect(dialog).toHaveText(/Is Dvc EnabledNo/)
        await expect(dialog).toHaveText(/PlatformGitHub/)
        await expect(dialog).toHaveText(/Created At2024-07-25T08:04:08.436600/)
        await expect(dialog).toHaveText(/LabelsSTL/)
        await expect(dialog).toHaveText(
            /Updated At2025-01-20T06:44:42\.497227\+00:00Close/
        )

        await page.getByRole('button', { name: 'Close' }).click()
        await expect(dialog).not.toBeVisible()
    })
})

test.describe('mantik#287 Data repo labels are shown in the tiles', () => {
    test('user can read card with label of a public data repository', async ({
        page,
    }) => {
        await page.goto(`/projects/details/${PROJECT_ID}/data`)

        const lastCard = page
            .locator('[data-testid="card-content"]')
            .last()
            .locator('..')
        await lastCard.waitFor({ state: 'visible' })
        await expect(lastCard.getByRole('heading')).toHaveText(
            'First Data Repo'
        )
        await expect(lastCard).toHaveText(/STL/)
    })
})
