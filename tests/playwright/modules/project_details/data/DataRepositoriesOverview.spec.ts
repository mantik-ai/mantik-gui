import { expect, test } from '@playwright/test'

test.describe.configure({ mode: 'parallel' })

test.describe('Page UI Renders', () => {
    test('card displays full information when a project has data repositories', async ({
        page,
    }) => {
        // Using Playwright Test Account 1 project with 2 data repos
        await page.goto(
            '/projects/details/a032bb33-7a76-40ec-b2e9-f2834d1aab21/data',
            { waitUntil: 'networkidle' }
        )
        expect(await page.textContent('#tableTitle')).toContain(
            'Testing Mock Data Double Populated'
        )

        // Card menubar will not be tested here, rather within RBAC tests per CRUD action
        const firstCard = page.locator('[data-testid="card-content"]').last()
        await firstCard.waitFor({ state: 'visible' })
        await expect(firstCard.getByRole('heading')).toHaveText(
            'First Data Repo'
        )
        await expect(firstCard.getByRole('paragraph')).toContainText(
            'First Data Repo Description'
        )

        const captionTypography = page
            .locator('[data-testid="timestamp-caption"]')
            .first()
        await expect(captionTypography).toContainText(
            'Created At: 2024-07-25 10:04:18'
        )
    })

    test('no cards display when a project has no data repositories', async ({
        page,
    }) => {
        // Using Playwright Test Account 1 project with no data repos
        await page.goto(
            'projects/details/c4476001-38d2-4649-8515-20c952709ab3/data',
            { waitUntil: 'networkidle' }
        )
        expect(await page.textContent('#tableTitle')).toContain(
            'Testing Mock Data Empty'
        )
        const card = page.locator('[data-testid="card-content"]')
        await expect(card).not.toBeVisible()
    })
})

test.describe('Create Data Repositories', () => {
    test.describe('Project Owner', () => {
        test.beforeEach('login', async ({ page }) => {
            if (!process.env.PLAYWRIGHT_TEST_ACCOUNT_1_PASSWORD)
                throw new Error(
                    'Expect to have variable defined: PLAYWRIGHT_TEST_ACCOUNT_1_PASSWORD'
                )
            // Navigate to the login page
            await page.goto('/login')
            // Log in
            await page.getByLabel('Username').fill('playwright')
            await page
                .getByLabel('Password')
                .first()
                .fill(process.env.PLAYWRIGHT_TEST_ACCOUNT_1_PASSWORD)
            await page.getByRole('button', { name: 'Login' }).click()

            await page.waitForURL('/', { waitUntil: 'networkidle' })
        })

        test.afterEach('logout', async ({ page }) => {
            // Navigate to account dropdown menu
            await page.locator('button[aria-label="Your Account"]').click()
            // Log out
            await page.locator('li.MuiMenuItem-root:has-text("Logout")').click()
            await page.waitForURL('/', { waitUntil: 'networkidle' }) // The URL you expect after login

            await expect(
                page.getByRole('heading', { name: 'Mantik' })
            ).toBeVisible()
        })

        test.describe('creates', () => {
            test.afterEach('delete', async ({ page }) => {
                // Click the delete button on the third card we just created
                const dialog = page.getByRole('dialog')
                await expect(dialog).not.toBeVisible()
                const firstCard = page
                    .locator('svg[data-testid="DeleteOutlineIcon"]')
                    .first()
                await firstCard.waitFor({ state: 'visible' })
                await firstCard.click()
                await expect(dialog).toBeVisible()

                // Confirm Delete
                const deleteButton = page.getByRole('button', {
                    name: 'Delete',
                })
                await deleteButton.click()
                await expect(dialog).toBeVisible()

                // Close the dialog
                const closeButton = page.getByRole('button', { name: 'Close' })
                await closeButton.click()
                await expect(dialog).not.toBeVisible()

                // Assert third card was deleted
                const secondCard = page
                    .locator('[data-testid="card-content"]')
                    .first()
                await secondCard.waitFor({ state: 'visible' })
                await expect(secondCard.getByRole('heading')).toHaveText(
                    'Second Data Repo'
                )
            })

            test('with minimum required fields', async ({ page }) => {
                await page.goto(
                    '/projects/details/b3f01854-1ad7-4068-be37-dbaf82ff602d/data',
                    { waitUntil: 'networkidle' }
                )
                const secondCard = page
                    .locator('[data-testid="card-content"]')
                    .first()
                await secondCard.waitFor({ state: 'visible' })
                await expect(secondCard.getByRole('heading')).toHaveText(
                    'Second Data Repo'
                )

                const dialog = page.getByRole('dialog')
                await expect(dialog).not.toBeVisible()
                const addButton = page.getByRole('button', { name: 'Add' })
                await expect(addButton).toBeVisible()
                await expect(addButton).not.toBeDisabled()
                await addButton.click()
                await expect(dialog).toBeVisible()
                await page.getByLabel('Name').fill('Third Data Repo')
                await page
                    .getByLabel('Data Repository URL')
                    .fill('www.third-example.com')

                // Create the new data repository
                const createButton = page.getByRole('button', {
                    name: 'Create',
                })
                await createButton.click()
                await expect(dialog).toBeVisible()

                // Close the dialog
                const closeButton = page.getByRole('button', { name: 'Close' })
                await closeButton.click()
                await expect(dialog).not.toBeVisible()

                // Assert the third card was created
                const mostRecentlyCreatedCard = page
                    .locator('[data-testid="card-content"]')
                    .first()
                await mostRecentlyCreatedCard.waitFor({ state: 'visible' })
                await expect(
                    mostRecentlyCreatedCard.getByRole('heading')
                ).toHaveText('Third Data Repo')
            })

            test('with a stored connection of a correct platform/connection match', async ({
                page,
            }) => {
                await page.goto(
                    '/projects/details/b3f01854-1ad7-4068-be37-dbaf82ff602d/data',
                    { waitUntil: 'networkidle' }
                )
                const dialog = page.getByRole('dialog')
                const addButton = page.getByRole('button', { name: 'Add' })
                await addButton.click()
                await expect(dialog).toBeVisible()
                await page.getByLabel('Name').fill('Third Data Repo')
                await page
                    .getByLabel('Data Repository URL')
                    .fill('www.third-example.com')
                const platform = dialog.getByLabel('Platform')
                await platform.click()
                const githubOption = page.getByRole('option', {
                    name: 'GitHub',
                })
                await githubOption.click()
                const checkbox = page.getByRole('checkbox', { name: 'is DVC' })
                await checkbox.click()
                // Add a connection
                const connection = dialog
                    .getByLabel('Stored Connection')
                    .first()
                await connection.click()

                const gitHubOption = page.getByRole('option', {
                    name: 'GitHub Connection',
                })
                await gitHubOption.click()
                // Add a dvc connection
                const dvcConnection = dialog
                    .getByLabel('Stored Connection')
                    .last()
                await dvcConnection.click()

                const dvcConnectionOption = page.getByRole('option', {
                    name: 'DVC Connection',
                })
                await dvcConnectionOption.click()

                const createButton = page.getByRole('button', {
                    name: 'Create',
                })
                await createButton.click()
                await expect(dialog).toBeVisible()

                // Close the dialog
                const closeButton = page.getByRole('button', { name: 'Close' })
                await closeButton.click()
                await expect(dialog).not.toBeVisible()

                // Assert the third card was created
                const mostRecentlyCreatedCard = page
                    .locator('[data-testid="card-content"]')
                    .first()
                await mostRecentlyCreatedCard.waitFor({ state: 'visible' })
                await expect(
                    mostRecentlyCreatedCard.getByRole('heading')
                ).toHaveText('Third Data Repo')
                const detailsIcon = page
                    .locator('svg[data-testid="VisibilityOutlinedIcon"]')
                    .first()
                await detailsIcon.click()
                await expect(dialog).toBeVisible()
                await expect(dialog).toHaveText(
                    /82d0ea49-e1bd-42d1-a0fb-08edda2a2200/
                )
                await closeButton.click()
            })
        })

        test.describe('cannot create', () => {
            test('when minimum required fields not met', async ({ page }) => {
                await page.goto(
                    '/projects/details/b3f01854-1ad7-4068-be37-dbaf82ff602d/data',
                    { waitUntil: 'networkidle' }
                )

                const dialog = page.getByRole('dialog')
                await expect(dialog).not.toBeVisible()
                const addButton = page.getByRole('button', { name: 'Add' })
                await expect(addButton).toBeVisible()
                await expect(addButton).not.toBeDisabled()
                await addButton.click()
                await expect(dialog).toBeVisible()
                await page.getByLabel('Name').fill('Third Data Repo')

                // Create the new data repository
                const createButton = page.getByRole('button', {
                    name: 'Create',
                })
                await createButton.click()
                await expect(dialog).toBeVisible()

                // Close the dialog
                const closeButton = page.getByRole('button', { name: 'Cancel' })
                await closeButton.click()
                await expect(dialog).not.toBeVisible()

                // Assert the third card was created
                const mostRecentlyCreatedCard = page
                    .locator('[data-testid="card-content"]')
                    .first()
                await mostRecentlyCreatedCard.waitFor({ state: 'visible' })
                await expect(
                    mostRecentlyCreatedCard.getByRole('heading')
                ).toHaveText('Second Data Repo')
            })
        })
    })

    test.describe('Project Guest', () => {
        test('guest user cannot create a data repository instance', async ({
            page,
        }) => {
            await page.goto(
                '/projects/details/5bd4c13d-7750-4fe4-8093-2e33ad839751/data',
                { waitUntil: 'networkidle' }
            )
            const dialog = page.getByRole('dialog')
            await expect(dialog).not.toBeVisible()
            const addButton = page.getByRole('button', { name: 'Add' })
            await expect(addButton).toBeVisible()
            await expect(addButton).toBeDisabled()

            // Attempt to click the button and expect it to not trigger any action
            await addButton.click({ force: true }) // Use force: true to attempt clicking even if disabled
            // Hover over the button to trigger the Tooltip
            try {
                // Hover over the button
                await addButton.hover({ timeout: 500 })
            } catch (e) {
                console.warn(
                    'Hovering over the button timed out, but continuing...',
                    e
                )
                const tooltip = await page.waitForSelector('[role="tooltip"]', {
                    state: 'visible',
                })
                const tooltipText = await tooltip.textContent()
                expect(tooltipText).toBe(
                    "You don't have the required permissions for this action"
                )
            }

            // Move the mouse away from the button to simulate unhovering */
            await page.mouse.move(0, 0)

            // Expect both neither tooltip text, nor dialogs
            await page.waitForSelector('[role="tooltip"]', { state: 'hidden' })
            await expect(dialog).not.toBeVisible()
        })
    })
})
