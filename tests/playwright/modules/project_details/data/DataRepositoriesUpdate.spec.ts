import { expect, test } from '@playwright/test'

test.describe('Edit Data Repositories', () => {
    test.describe('Project Owner ', () => {
        test.beforeEach('login', async ({ page }) => {
            if (!process.env.PLAYWRIGHT_TEST_ACCOUNT_1_PASSWORD)
                throw new Error(
                    'Expect to have variable defined: PLAYWRIGHT_TEST_ACCOUNT_1_PASSWORD'
                )
            // Navigate to the login page
            await page.goto('/login')
            // Log in
            await page.getByLabel('Username').fill('playwright')
            await page
                .getByLabel('Password')
                .first()
                .fill(process.env.PLAYWRIGHT_TEST_ACCOUNT_1_PASSWORD)
            await page.getByRole('button', { name: 'Login' }).click()

            await page.waitForURL('/', { waitUntil: 'networkidle' })
        })

        test.afterEach('logout', async ({ page }) => {
            // Navigate to account dropdown menu
            await page.locator('button[aria-label="Your Account"]').click()
            // Log out
            await page.locator('li.MuiMenuItem-root:has-text("Logout")').click()
            await page.waitForURL('/', { waitUntil: 'networkidle' }) // The URL you expect after login

            await expect(
                page.getByRole('heading', { name: 'Mantik' })
            ).toBeVisible()
            page.close()
        })

        test.describe('can update a repo ', () => {
            test('with new information', async ({ page }) => {
                await page.goto(
                    '/projects/details/242c7c1d-7a3b-4545-a4f7-1a4aaf7b1e2c/data',
                    { waitUntil: 'networkidle' }
                )
                // Find the Edit repository button for the data repo that pre-exists
                const openUpdateDialogButton = page
                    .getByLabel('Edit')
                    .getByRole('button')
                await expect(openUpdateDialogButton).toBeVisible()
                const dialog = page.getByRole('dialog')
                await expect(dialog).not.toBeVisible()
                // Click on the Edit repository button
                openUpdateDialogButton.click()
                await expect(dialog).toBeVisible()
                // Fill in the new information
                await page.getByLabel('Name').fill('Modified data repo name')
                await page
                    .getByLabel('Description')
                    .fill('Modified data repo description')
                await page
                    .getByLabel('Data Repository URL')
                    .fill('www.example.com/modified')

                // Add a connection
                const connection = dialog
                    .getByLabel('Stored Connection')
                    .first()
                await connection.click()
                const gitHubOption = page.getByRole('option', {
                    name: 'GitHub Connection',
                })
                await gitHubOption.click()
                const checkbox = page.getByRole('checkbox', { name: 'is DVC' })
                await checkbox.uncheck()
                await checkbox.click()
                expect(checkbox).toBeChecked()
                const dvcConnection = dialog
                    .getByLabel('Stored Connection')
                    .last()
                await dvcConnection.click()
                const dvcOption = page.getByRole('option', {
                    name: 'DVC Connection',
                })
                await dvcOption.click()

                const updateButton = page.getByRole('button', {
                    name: 'Update',
                })
                await expect(updateButton).toBeVisible()
                await expect(updateButton).not.toBeDisabled()
                // Update the data repository
                await updateButton.click()

                // Assert the success dialog
                const successMessage = page.getByText(/success/i)
                await expect(successMessage).toBeVisible()

                // Close the success dialog
                const closeButton = page.getByRole('button', { name: 'Close' })
                await closeButton.click()
                await expect(dialog).not.toBeVisible()

                // Expect the changes to be reflected in the card
                const onlyCard = page
                    .locator('[data-testid="card-content"]')
                    .first()
                await onlyCard.waitFor({ state: 'visible' })
                await expect(onlyCard.getByRole('heading')).toHaveText(
                    'Modified data repo name'
                )
                await expect(
                    onlyCard.getByText('Modified data repo description')
                ).toBeVisible()
                // check that saved connections are prefilled
                await openUpdateDialogButton.click()
                await expect(page.getByText('GitHub Connection')).toBeVisible()
                await expect(page.getByText('DVC Connection')).toBeVisible()

                // Undo the changes to restore the original state
                await page.getByLabel('Name').fill('Unmodified data repo name')
                await page
                    .getByLabel('Description')
                    .fill('Unmodified data repo description')
                await page
                    .getByLabel('Data Repository URL')
                    .fill('www.example.com/unmodified')
                await updateButton.click()
                // Close the dialog
                await closeButton.click()
                await expect(dialog).not.toBeVisible()
                // Expect the card to be as it was originally
                await onlyCard.waitFor({ state: 'visible' })
                await expect(onlyCard.getByRole('heading')).toHaveText(
                    'Unmodified data repo name'
                )
                await expect(
                    onlyCard.getByText('Unmodified data repo description')
                ).toBeVisible()
            })

            test('and remove dvc connection', async ({ page }) => {
                await page.goto(
                    '/projects/details/242c7c1d-7a3b-4545-a4f7-1a4aaf7b1e2c/data',
                    { waitUntil: 'networkidle' }
                )
                // Find the Edit repository button for the data repo that pre-exists
                const openUpdateDialogButton = page
                    .getByLabel('Edit')
                    .getByRole('button')
                await expect(openUpdateDialogButton).toBeVisible()
                const dialog = page.getByRole('dialog')
                await expect(dialog).not.toBeVisible()
                // Click on the Edit repository button
                openUpdateDialogButton.click()
                await expect(dialog).toBeVisible()

                //check that dvc connection is prefilled
                const checkbox = page.getByRole('checkbox', { name: 'is DVC' })
                await expect(checkbox).toBeChecked()
                const dvcConnection = dialog
                    .getByLabel('Stored Connection')
                    .last()

                await expect(page.getByText('DVC Connection')).toBeVisible()

                // Remove dvc connection
                await checkbox.click()

                const updateButton = page.getByRole('button', {
                    name: 'Update',
                })
                expect(updateButton).toBeVisible()
                expect(updateButton).not.toBeDisabled()
                // Update the data repository
                await updateButton.click()
                // Close the dialog
                const closeButton = page.getByRole('button', { name: 'Close' })
                await closeButton.click()
                await expect(dialog).not.toBeVisible()

                // open dialog again
                openUpdateDialogButton.click()
                await expect(dialog).toBeVisible()

                // check that dvc connection is not prefilled
                await expect(checkbox).not.toBeChecked()
                await expect(page.getByText('DVC Connection')).not.toBeVisible()
                await checkbox.click()
                await expect(
                    page.getByText('None Selected').last()
                ).toBeVisible()
                await page
                    .getByRole('button', {
                        name: 'Cancel',
                    })
                    .click()
                await expect(page.getByRole('dialog')).not.toBeVisible()

                // roll back changes
                openUpdateDialogButton.click()
                const connection = dialog
                    .getByLabel('Stored Connection')
                    .first()
                await connection.click()
                const gitHubOption = page.getByRole('option', {
                    name: 'GitHub Connection',
                })
                await gitHubOption.click()

                await checkbox.click()

                await dvcConnection.click()
                const dvcOption = page.getByRole('option', {
                    name: 'DVC Connection',
                })
                await dvcOption.click()
                await updateButton.click()
                await closeButton.click()
                await expect(page.getByRole('dialog')).not.toBeVisible()
            })
        })
    })
})
