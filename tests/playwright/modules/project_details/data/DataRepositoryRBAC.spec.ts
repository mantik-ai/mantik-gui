import { expect, test } from '@playwright/test'
import { credentials, login, logout } from '../../../credentials'

const projectWithInvitedMembers = '9ab3db78-b052-4873-805f-97b69115fea3'
const pathToDataRepositories = `/projects/details/${projectWithInvitedMembers}/data`

test.describe.configure({ mode: 'parallel' })

test.describe('Data Repositories RBAC Checks', () => {
    test.describe('Project Guest', () => {
        test.beforeEach('login', async ({ page }) =>
            login(page, credentials.guest)
        )
        test.afterEach('logout', async ({ page }) => logout(page))

        test('has correct permissions', async ({ page }) => {
            await page.goto(pathToDataRepositories, {
                waitUntil: 'networkidle',
            })

            const addButton = page.getByRole('button', { name: 'Add' })
            await expect(addButton).toBeDisabled()
            for (const card of await page.getByTestId('card').all()) {
                const detailsButton = card
                    .getByLabel('Details')
                    .getByRole('button')
                await expect(detailsButton).not.toBeDisabled()
                const editButton = card.getByLabel('Edit').getByRole('button')
                await expect(editButton).toBeDisabled()
                const deleteButton = card
                    .getByLabel('Delete')
                    .getByRole('button')
                await expect(deleteButton).toBeDisabled()
            }
        })
    })

    test.describe('Project Reporter', () => {
        test.beforeEach('login', async ({ page }) =>
            login(page, credentials.reporter)
        )
        test.afterEach('logout', async ({ page }) => logout(page))

        test('has correct permissions', async ({ page }) => {
            await page.goto(pathToDataRepositories, {
                waitUntil: 'networkidle',
            })

            const addButton = page.getByRole('button', { name: 'Add' })
            await expect(addButton).toBeDisabled()

            for (const card of await page.getByTestId('card').all()) {
                const detailsButton = card
                    .getByLabel('Details')
                    .getByRole('button')
                await expect(detailsButton).not.toBeDisabled()
                const editButton = card.getByLabel('Edit').getByRole('button')
                await expect(editButton).toBeDisabled()
                const deleteButton = card
                    .getByLabel('Delete')
                    .getByRole('button')
                await expect(deleteButton).toBeDisabled()
            }
        })
    })

    test.describe('Project Researcher', () => {
        test.beforeEach('login', async ({ page }) =>
            login(page, credentials.researcher)
        )
        test.afterEach('logout', async ({ page }) => logout(page))

        test('has correct permissions', async ({ page }) => {
            await page.goto(pathToDataRepositories, {
                waitUntil: 'networkidle',
            })

            const addButton = page.getByRole('button', { name: 'Add' })
            await expect(addButton).not.toBeDisabled()

            for (const card of await page.getByTestId('card').all()) {
                const detailsButton = card
                    .getByLabel('Details')
                    .getByRole('button')
                await expect(detailsButton).not.toBeDisabled()
                const editButton = card.getByLabel('Edit').getByRole('button')
                await expect(editButton).not.toBeDisabled()
                const deleteButton = card
                    .getByLabel('Delete')
                    .getByRole('button')
                await expect(deleteButton).not.toBeDisabled()
            }
        })
    })

    test.describe('Project Maintainer', () => {
        test.beforeEach('login', async ({ page }) =>
            login(page, credentials.maintainer)
        )
        test.afterEach('logout', async ({ page }) => logout(page))

        test('has correct permissions', async ({ page }) => {
            await page.goto(pathToDataRepositories, {
                waitUntil: 'networkidle',
            })

            const addButton = page.getByRole('button', { name: 'Add' })
            await expect(addButton).not.toBeDisabled()

            for (const card of await page.getByTestId('card').all()) {
                const detailsButton = card
                    .getByLabel('Details')
                    .getByRole('button')
                await expect(detailsButton).not.toBeDisabled()
                const editButton = card.getByLabel('Edit').getByRole('button')
                await expect(editButton).not.toBeDisabled()
                const deleteButton = card
                    .getByLabel('Delete')
                    .getByRole('button')
                await expect(deleteButton).not.toBeDisabled()
            }
        })
    })
})
