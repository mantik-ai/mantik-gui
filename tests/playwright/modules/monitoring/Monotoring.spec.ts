import { expect, test } from '@playwright/test'
import { unsetCookieConsent, setCookieConsent } from '../../credentials'
import { Context } from '../../navigate'

async function givenIHaveNoMonitoringCookies({ page }: Context) {
    await unsetCookieConsent(page)
}

async function whenIVisitTheLandingPage({ page }: Context) {
    await page.goto('/')
}

async function whenIConsentToMonitoringCookies({ page }: Context) {
    const acceptBtn = page.getByRole('button', { name: /Accept all/i })
    await expect(acceptBtn).toBeVisible()
    await acceptBtn.click()
}

async function thenMonitoringDataIsSent({ page }: Context) {
    await page.route('https://dataplane**/*', async (route) => {
        await route.abort('aborted')
    })
    await expect(async () => {
        await page.waitForRequest(/https:\/\/dataplane.*/, {
            timeout: 10000,
        })
    }).toPass({ timeout: 11000 })
}

async function thenMonitoringDataIsNotSent({ page }: Context) {
    await page.route('https://dataplane**/*', async (route) => {
        await route.abort('aborted')
    })

    await expect(async () => {
        await page.waitForRequest(/https:\/\/dataplane.*/, {
            timeout: 10000,
        })
    }).not.toPass({ timeout: 11000 })
}

async function whenIDenyConsentToMonitoringCookies({ page }: Context) {
    const denyBtn = page.getByRole('button', { name: /Reject all/i })
    await expect(denyBtn).toBeVisible()
    await denyBtn.click()
}

async function givenIConsentedToMonitoringCookies({ page }: Context) {
    await setCookieConsent(page)
}

async function whenIWithdrawConsentForMonitoringCookies({ page }: Context) {
    const openPreferencesBtn = page.getByRole('button', {
        name: /Show Cookie Preferences/i,
    })
    await expect(openPreferencesBtn).toBeVisible()
    await openPreferencesBtn.click()

    const denyBtn = page.getByRole('button', { name: /Reject all/i })
    await expect(denyBtn).toBeVisible()
    await denyBtn.click()
}

test.describe('mantik-api#374 Consent to Monitoring Cookies', () => {
    test.beforeEach(async ({ page }) => {
        await page.addInitScript(
            "Object.defineProperty(navigator, 'webdriver', {get: () => undefined})"
        )
    })
    test.afterEach(async ({ page }) => {
        await page.addInitScript(
            "Object.defineProperty(navigator, 'webdriver', {get: () => true})"
        )
    })
    test.describe('mantik-api#374 Users can be individually excluded from monitoring.', () => {
        test('user consents to monitoring cookies', async ({ page }) => {
            await givenIHaveNoMonitoringCookies({ page })
            await whenIVisitTheLandingPage({ page })
            await whenIConsentToMonitoringCookies({ page })
            await thenMonitoringDataIsSent({ page })
        })

        test('user denies consent to monitoring cookies', async ({ page }) => {
            await givenIHaveNoMonitoringCookies({ page })
            await whenIVisitTheLandingPage({ page })
            await whenIDenyConsentToMonitoringCookies({ page })
            await thenMonitoringDataIsNotSent({ page })
        })

        test('user withdraws consent to monitoring cookies', async ({
            page,
        }) => {
            await givenIConsentedToMonitoringCookies({ page })
            await whenIVisitTheLandingPage({ page })
            await whenIWithdrawConsentForMonitoringCookies({ page })
            await thenMonitoringDataIsNotSent({ page })
        })

        test('user visits page with previous consent to monitoring cookies', async ({
            page,
        }) => {
            await givenIConsentedToMonitoringCookies({ page })
            await whenIVisitTheLandingPage({ page })
            await thenMonitoringDataIsSent({ page })
        })
    })
})
