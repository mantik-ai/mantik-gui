import { Page } from '@playwright/test'

export type Context = { page: Page }

export async function navigateToProjectByName({ page }: Context, name: string) {
    await page.getByRole('link', { name: /Projects/i }).click()
    await page.getByRole('button', { name }).click()
}

export async function navigateToSubmissionsList({ page }: Context) {
    await page.getByRole('button', { name: /Runs/i }).first().click()
    await page.getByText(/Submissions/i).click()
}
