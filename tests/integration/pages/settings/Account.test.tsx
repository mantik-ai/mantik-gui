import * as React from 'react'
import { render, renderHook, screen, waitFor } from '@testing-library/react'
import { createWrapper, queryWrapper } from '../../../utils/queryWrapper'
import { useQuery } from '@tanstack/react-query'
import userEvent from '@testing-library/user-event'
import { FormTextfieldGroup } from '../../../../src/modules/settings/account/Profile'
import AccountPage from '../../../../src/pages/settings/account'
import {
    getUserDetailsGetUserDetailsGetMockHandler,
    getUsersUserIdGetUsersUserIdGetMockHandler,
} from '../../../../src/queries/users/users.msw'
import { server } from '../../../../jest.setup'

const mockResponse = {
    updatedAt: '',
    createdAt: '',
    userId: '123',
    name: 'jane.doe',
    fullName: 'Jane Doe',
    info: 'This is a test user',
    company: 'Test Company',
    jobTitle: 'Test Job',
    websiteUrl: 'https://test.com',
    email: 'jdoe@test.de',
}

function useCustomHook() {
    return useQuery({ queryKey: ['customHook'], queryFn: () => mockResponse })
}

describe('Account', () => {
    it('renders own account details if logged in with name field disabled', () => {
        server.use(getUserDetailsGetUserDetailsGetMockHandler())
        server.use(getUsersUserIdGetUsersUserIdGetMockHandler())
        render(queryWrapper(<AccountPage />))
        const nameField = screen.getByLabelText('Name')
        expect(nameField).toBeInTheDocument()
        expect(nameField).toBeDisabled()
    })
    it('renders full name with capitalized first letter label and response full name as value', async () => {
        const { result } = renderHook(() => useCustomHook(), {
            wrapper: createWrapper() as React.JSXElementConstructor<unknown>,
        })
        await waitFor(() => expect(result.current.isSuccess).toBe(true))
        render(
            <FormTextfieldGroup
                data={{ ...result.current.data }}
                setData={() => {}}
            />
        )
        await waitFor(() => {
            const fullnamefield = screen.getByLabelText('Full name')
            expect(fullnamefield).toHaveValue('Jane Doe')
        })
    })
    it('changes the rendered value on input change', async () => {
        render(queryWrapper(<AccountPage />))
        const user = userEvent.setup()
        const fullnamefield = screen.getByLabelText('Full name')
        await user.type(fullnamefield, 'John Doe')
        await waitFor(() => {
            expect(fullnamefield).toHaveValue('John Doe')
        })
    })
})
