import * as React from 'react'
import { render, screen, waitFor } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import { queryWrapper } from '../../utils/queryWrapper'
import ConfirmEmail from '../../../src/pages/confirm-email'
import { HttpResponse, delay, http } from 'msw'
import { server } from '../../../jest.setup'

// Mock useRouter
jest.mock('next/router', () => ({
    useRouter: () => ({
        query: {
            username: 'testuser',
        },
    }),
}))

const mock400 = http.post(
    'https://api.dev2.cloud.mantik.ai/signup/verify-email',
    async () => {
        await delay(1000)
        return new HttpResponse(
            JSON.stringify('Request failed with status code 400'),
            {
                status: 400,
                headers: {
                    'Content-Type': 'application/json',
                },
            }
        )
    }
)

describe('ConfirmEmail', () => {
    it('disables confirm button if no code was provided', () => {
        render(queryWrapper(<ConfirmEmail />))
        const confirmButton = screen.getByRole('button', { name: /confirm/i })
        expect(confirmButton).toBeDisabled()
    })
    it('enables confirm button if code was provided', async () => {
        render(queryWrapper(<ConfirmEmail />))
        const user = userEvent.setup()
        const inputField = screen.getByLabelText(/code/i)
        const confirmButton = screen.getByRole('button', { name: /confirm/i })
        await user.type(inputField, '123456')
        expect(confirmButton).toBeEnabled()
    })
    it('calls confirmEmail with value from router.query and confirmation code', async () => {
        const user = userEvent.setup()
        server.use(mock400)
        render(queryWrapper(<ConfirmEmail />))
        const inputField = screen.getByLabelText(/code/i)
        const confirmButton = screen.getByRole('button', { name: /confirm/i })
        await user.type(inputField, '123456')
        await user.click(confirmButton)
        waitFor(() => {
            expect(inputField).toHaveValue('123456')
            expect(confirmButton).toBeEnabled()
            expect(mock400).toHaveBeenCalledWith({
                confirmationCode: '123456',
                userName: undefined,
            })
        })
    })
})
