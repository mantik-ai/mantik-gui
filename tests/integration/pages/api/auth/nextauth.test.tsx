import { delay, http, HttpResponse } from 'msw'
import { server } from '../../../../../jest.setup'
import {
    handleJWT,
    handleSession,
    refreshAccessToken,
} from '../../../../../src/pages/api/auth/[...nextauth]'
import { faker } from '@faker-js/faker'
import { Session, User } from 'next-auth'

jest.mock('next-auth', () => {
    return jest.fn()
})

const expiredToken = {
    name: 'mantik_test_account',
    accessToken:
        'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI1YzUyMTUzNy0yODBkLTQxMDYtYWFlNi1jYTRmYWJlOThlNjQiLCJpc3MiOiJodHRwczovL2NvZ25pdG8taWRwLmV1LWNlbnRyYWwtMS5hbWF6b25hd3MuY29tL2V1LWNlbnRyYWwtMV8wc1RHQ1dpWVMiLCJjbGllbnRfaWQiOiI1NGp1aGt2YW9janFxNGkzaGp1b3U4cWZmIiwib3JpZ2luX2p0aSI6ImFjY2JkNTM1LTQ5NTQtNDA5Ni1hYzU3LTI0ZjNkY2M1MDM2NSIsImV2ZW50X2lkIjoiZTc5YTAyYTYtY2QxYi00Y2QzLTliZDQtNTI5Y2I5ZTEwY2Q2IiwidG9rZW5fdXNlIjoiYWNjZXNzIiwic2NvcGUiOiJhd3MuY29nbml0by5zaWduaW4udXNlci5hZG1pbiIsImF1dGhfdGltZSI6MTczNzAxNzc1NywiZXhwIjoxNzM2ODU4Njg4LCJpYXQiOjE3MzcwMTc3NTcsImp0aSI6ImQyYzI1Njc5LTg3ZTctNGJlMS1iMzRkLTU3ZWMwYmMwMzIwMiIsInVzZXJuYW1lIjoibWFudGlrX3Rlc3RfYWNjb3VudCJ9.A8-oEiXuwkmJ84PjS5JdGUzJi657S3cwE98tIvN4tAf-yfZEYAXCM0Lk7mNi1L6-1uNxIwdm1CrlFRaGYorktlfjHJwAKRracz7ulkUzuEWQMVC3wGk60ps3ptRXmbHzrl34IEf6jiro7PZO4WsPKtb3RbBQwRTDAuU1AuNHpT_lHSVJ74ZUUfDoqJQ0pvxpAamr9tRxAwlVg0X0HW6wcMzErLbeo5ww0IXFKwyMc7E6LDT3P0j0WUScAN1qfNsG4_IEZQpDDNBd8kZQ4i6swh7w33t0KPLI72zr6_3Aioa99P68mFmrpS58AbrAQIGNclE-e8RYjC7bV4Nrs7PYlA',
    refreshToken:
        'eyJjdHkiOiJKV1QiLCJlbmMiOiJBMjU2R0NNIiwiYWxnIjoiUlNBLU9BRVAifQ.pO9XZKttC4DnHrOkrMrFo73C7rzOmLRqJCK-DLEQNM3YGe_xUb4bQ0MmTij0qO3rEO8OW_Q81Gj_p2CKPfhmTUn6weyyNYuEEWo6KpMOoV_geyUvz5aO-YryU0E7j4CRcq6-yszvaxBVm8HcSsOFcxx1JD4RqEApI6faew2tmzeuuPdMXYBJJJgDc0Ikdhjbxvbs3Sj2QlR-7XfQ63IzUAEoFaA62s1-mbrGRJfddexabWb1tOH1EvPg2ecM4IrDr8vBEhMYuK-WySXwDsmMyUVZd8IXWve2-1WYrCNxU-MKJZD4O02L3q47HnYSHmpIJQpuP4S2oyWs12cUyTUdPw.v9iYLqDgQIbrclSh.CO14bj8U2bKxrdKdG2vHwfvygkQH2MA1df-8KdANmH0PMoaziVZiULODWLevD0usriuCT8b_Vl8dtAb7V8vsQ1oPLO8XVIkXRgFYy1C5ygMV-8p0hV8-eeN69RdIO9UBxuy-1z4oqEvuTMkQmPBY_2ELO4PxrXN8mULqiGr3tEA4jcrmuSdZIljSSLOyEW5b23fptP9uZWCaljki22tl8Kxgi8fWCizSQM5GR9bRdvSZvnzPG-wM9sC4C6anubaslh0UPeWXinM_LgB4JWPevxx5-AtribyOU1c14ty0BJqoKaFhRH8mU5ZVoyOxpYSxrpzUFU4UeXJInkUXwWqZ9gq6zx-wvKjx6yAjH3KWRitorvqJ6ZnL5apx7VhgouQwyk4vjz2N0KwCtoBYZOXYktSrOM1S09HjA2jFU3xR5IU5Nazke6RjlhAi-OlQFzv12FnE5BFJN7TN08PL_AKid4h1we2a_NnqC1IqAQagJ2aeaGETzQmwGY9y-9dHg57XxYxuARVb9uHXG6Ah2pS0Qw41B2tspq0uJcsoj8XvHpbmU4wNEsFTNug7nY97dfxNILTg1eal-otreZsSpLOYc--VHzLmmnfYK3ybEhnULcGCYvJxdtdQ38lC95-2IYDR1nE6lE_82Zx3xUbWkahGvKhQyXfVbFl-07WtwYd-46FDUTVDEl5TlgoAOxr_VE3HI9Uqy2Mo64l2yRzbR8yFCxeaf3b9MdeJu8lnJL2jl6CABAYYzyq1sXMFAycgSiMqr7nYDKfjcaXlytBu-Wc1ZhrmDcBqvbs03-3ks48EeT-j2wyDVbRjuLpEpbkwQYkZZP1_CnanV8I4lHMPKZnrS0FpOJaXCXitnyaGDyfe61zr2aYB81MccXD4hO0ErTV74Nptma1e2Chb75btOuk5BEY3jhdKnQqb9Ui2MHq-kwukBu2LcEC-7_irIIH7K0BwAZ17bLSBTea-56LpD3TPF5neuWRaKhfz-M4vVFfSSt15VT4PTx9YNmY6cPWiJefZCsrsOZlqZcFSXCZSAcYywMNRapUfmWRySa5g9QOuclEmUeJrBpesQug6mC78GA4-CeA1zbFMsixX5f28xtGwbCbvK5HjjxufP-tCaUCOMayVD9UT0IZFlB4jBVKluRTurfZOFx6_xWgDktWlaPgN72SEk2CCoaMXJgGCOctyLkCdS7cu_JMke6vkN3D3-MPH91w7mk8tHLue3WzuSYwE5UJm2wpXxsWwjOuuhBhidgQzYP0xAEhAd39dCsWVYzgtGD28ue0NCsLqrBuPfE2hzI4E2mJKd8RS64kr8Q.avbbzI9NGSM_xS9nGC2qMw',
    accessTokenExpires: '2022-01-14T12:44:48+00:00',
    exp: 1736858688,
}

const validToken = {
    name: 'mantik_test_account',
    accessToken:
        'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI1YzUyMTUzNy0yODBkLTQxMDYtYWFlNi1jYTRmYWJlOThlNjQiLCJpc3MiOiJodHRwczovL2NvZ25pdG8taWRwLmV1LWNlbnRyYWwtMS5hbWF6b25hd3MuY29tL2V1LWNlbnRyYWwtMV8wc1RHQ1dpWVMiLCJjbGllbnRfaWQiOiI1NGp1aGt2YW9janFxNGkzaGp1b3U4cWZmIiwib3JpZ2luX2p0aSI6ImFjY2JkNTM1LTQ5NTQtNDA5Ni1hYzU3LTI0ZjNkY2M1MDM2NSIsImV2ZW50X2lkIjoiZTc5YTAyYTYtY2QxYi00Y2QzLTliZDQtNTI5Y2I5ZTEwY2Q2IiwidG9rZW5fdXNlIjoiYWNjZXNzIiwic2NvcGUiOiJhd3MuY29nbml0by5zaWduaW4udXNlci5hZG1pbiIsImF1dGhfdGltZSI6MTczNzAxNzc1NywiZXhwIjoxNzk5NzU3ODg4LCJpYXQiOjE3MzcwMTc3NTcsImp0aSI6ImQyYzI1Njc5LTg3ZTctNGJlMS1iMzRkLTU3ZWMwYmMwMzIwMiIsInVzZXJuYW1lIjoibWFudGlrX3Rlc3RfYWNjb3VudCJ9.at4P2VVKhOmJUTC3hX9Dm8AVa07FtId-hALKQulPhfp_I7DvPWfG5O4IxVQNOteHWMsdlW1yIQzjWRW1ImZYsgYCwr3IvfaN0rsLiXg-yKqDXFUCTYllaWYiYQQ3KmD_mt_ENrXHacV8_7-MoHF3hEhKNQ2CmYOWTm4k6D-Pgo203YqiWxq8fycvb65SHBUo9qoC6MZ0zBj1zFsGvKDofeN1fxi8zDkAn-CeDZyVgXEUMo-FFmUgSpD5ScnRXZiARWJlX4A6Y9dNuHI_V8ttKU-dvIe_igVGjziFwF5ChMDpoeVhSdDqP8He9u9lRUGihIxtHei8EQacDOreBjVvvw',
    refreshToken:
        'eyJjdHkiOiJKV1QiLCJlbmMiOiJBMjU2R0NNIiwiYWxnIjoiUlNBLU9BRVAifQ.pO9XZKttC4DnHrOkrMrFo73C7rzOmLRqJCK-DLEQNM3YGe_xUb4bQ0MmTij0qO3rEO8OW_Q81Gj_p2CKPfhmTUn6weyyNYuEEWo6KpMOoV_geyUvz5aO-YryU0E7j4CRcq6-yszvaxBVm8HcSsOFcxx1JD4RqEApI6faew2tmzeuuPdMXYBJJJgDc0Ikdhjbxvbs3Sj2QlR-7XfQ63IzUAEoFaA62s1-mbrGRJfddexabWb1tOH1EvPg2ecM4IrDr8vBEhMYuK-WySXwDsmMyUVZd8IXWve2-1WYrCNxU-MKJZD4O02L3q47HnYSHmpIJQpuP4S2oyWs12cUyTUdPw.v9iYLqDgQIbrclSh.CO14bj8U2bKxrdKdG2vHwfvygkQH2MA1df-8KdANmH0PMoaziVZiULODWLevD0usriuCT8b_Vl8dtAb7V8vsQ1oPLO8XVIkXRgFYy1C5ygMV-8p0hV8-eeN69RdIO9UBxuy-1z4oqEvuTMkQmPBY_2ELO4PxrXN8mULqiGr3tEA4jcrmuSdZIljSSLOyEW5b23fptP9uZWCaljki22tl8Kxgi8fWCizSQM5GR9bRdvSZvnzPG-wM9sC4C6anubaslh0UPeWXinM_LgB4JWPevxx5-AtribyOU1c14ty0BJqoKaFhRH8mU5ZVoyOxpYSxrpzUFU4UeXJInkUXwWqZ9gq6zx-wvKjx6yAjH3KWRitorvqJ6ZnL5apx7VhgouQwyk4vjz2N0KwCtoBYZOXYktSrOM1S09HjA2jFU3xR5IU5Nazke6RjlhAi-OlQFzv12FnE5BFJN7TN08PL_AKid4h1we2a_NnqC1IqAQagJ2aeaGETzQmwGY9y-9dHg57XxYxuARVb9uHXG6Ah2pS0Qw41B2tspq0uJcsoj8XvHpbmU4wNEsFTNug7nY97dfxNILTg1eal-otreZsSpLOYc--VHzLmmnfYK3ybEhnULcGCYvJxdtdQ38lC95-2IYDR1nE6lE_82Zx3xUbWkahGvKhQyXfVbFl-07WtwYd-46FDUTVDEl5TlgoAOxr_VE3HI9Uqy2Mo64l2yRzbR8yFCxeaf3b9MdeJu8lnJL2jl6CABAYYzyq1sXMFAycgSiMqr7nYDKfjcaXlytBu-Wc1ZhrmDcBqvbs03-3ks48EeT-j2wyDVbRjuLpEpbkwQYkZZP1_CnanV8I4lHMPKZnrS0FpOJaXCXitnyaGDyfe61zr2aYB81MccXD4hO0ErTV74Nptma1e2Chb75btOuk5BEY3jhdKnQqb9Ui2MHq-kwukBu2LcEC-7_irIIH7K0BwAZ17bLSBTea-56LpD3TPF5neuWRaKhfz-M4vVFfSSt15VT4PTx9YNmY6cPWiJefZCsrsOZlqZcFSXCZSAcYywMNRapUfmWRySa5g9QOuclEmUeJrBpesQug6mC78GA4-CeA1zbFMsixX5f28xtGwbCbvK5HjjxufP-tCaUCOMayVD9UT0IZFlB4jBVKluRTurfZOFx6_xWgDktWlaPgN72SEk2CCoaMXJgGCOctyLkCdS7cu_JMke6vkN3D3-MPH91w7mk8tHLue3WzuSYwE5UJm2wpXxsWwjOuuhBhidgQzYP0xAEhAd39dCsWVYzgtGD28ue0NCsLqrBuPfE2hzI4E2mJKd8RS64kr8Q.avbbzI9NGSM_xS9nGC2qMw',
    accessTokenExpires: '2027-01-12T12:44:48+00:00',
    exp: 1799757888,
}

const user = {
    AccessToken: validToken.accessToken,
    RefreshToken: validToken.refreshToken,
    ExpiresAt: validToken.accessTokenExpires,
    User: {
        Username: validToken.name,
    },
    id: faker.string.uuid(),
}

function interceptRefreshTokenWithError() {
    server.use(
        http.post(`**/mantik/tokens/refresh`, async () => {
            await delay(500)
            return HttpResponse.json(
                { detail: 'Unable to refresh token' },
                { status: 400 }
            )
        })
    )
}

function getExistingSessionConfigurationWithExpiredToken() {
    return {
        token: expiredToken,
        account: null,
        user: undefined,
    }
}

function getExistingSessionConfigurationWithValidToken() {
    return {
        token: validToken,
        account: null,
        user: undefined,
    }
}

function getSessionConfigurationAfterFirstSignIn() {
    return {
        token: validToken,
        account: {
            providerAccountId: faker.string.uuid(),
            userId: faker.string.uuid(),
            provider: 'credentials' as const,
            type: 'credentials' as const,
        },
        user: user as User,
    }
}

describe('next auth', () => {
    beforeEach(() => {
        server.use(
            http.post(`**/mantik/tokens/refresh`, async () => {
                await delay(500)
                return HttpResponse.json(
                    {
                        AccessToken: validToken.accessToken,
                        ExpiresAt: validToken.accessTokenExpires,
                    },
                    { status: 201 }
                )
            })
        )
    })
    describe('refreshAccessToken', () => {
        it('should return error if refresh token request failed', async () => {
            interceptRefreshTokenWithError()

            await expect(
                refreshAccessToken(expiredToken)
            ).resolves.toMatchObject({
                ...expiredToken,
                error: 'RefreshTokenError',
            })
        })
    })

    describe('jwt', () => {
        it('should return refreshed token if access token has expired', async () => {
            const initialConfiguration =
                getExistingSessionConfigurationWithExpiredToken()
            const refreshedToken = await handleJWT(initialConfiguration)

            const isTokenExpired =
                new Date(Date.now()).toISOString() >
                new Date(refreshedToken.accessTokenExpires).toISOString()

            expect(refreshedToken.error).toBeFalsy()
            expect(isTokenExpired).toBeFalsy()
        })

        it('should return token with error if access token refreshing fails', async () => {
            interceptRefreshTokenWithError()

            const initialConfiguration =
                getExistingSessionConfigurationWithExpiredToken()
            const refreshedToken = await handleJWT(initialConfiguration)

            expect(refreshedToken.error).toBe('RefreshTokenError')
        })

        it('should return previous access token if token is not expired yet', async () => {
            const initialConfiguration =
                getExistingSessionConfigurationWithValidToken()
            const refreshedToken = await handleJWT(initialConfiguration)

            expect(refreshedToken).toMatchObject(validToken)
        })

        it('should return valid token if user just logged in', async () => {
            const initialConfiguration =
                getSessionConfigurationAfterFirstSignIn()
            const refreshedToken = await handleJWT(initialConfiguration)

            expect(refreshedToken).toMatchObject(validToken)
        })
    })

    describe('session', () => {
        it('should return error if token has error', async () => {
            const session = await handleSession({
                token: { ...validToken, error: 'RefreshTokenError' },
                session: { user: {} } as Session,
            })

            expect(session).toMatchObject({
                user: {
                    name: validToken.name,
                    accessToken: validToken.accessToken,
                    refreshToken: validToken.refreshToken,
                    accessTokenExpires: validToken.accessTokenExpires,
                },
                error: 'RefreshTokenError',
            })
        })
    })
})
