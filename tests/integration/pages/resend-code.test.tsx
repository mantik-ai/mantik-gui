import * as React from 'react'
import { render, screen, waitFor } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import { queryWrapper } from '../../utils/queryWrapper'
import ResendCode from '../../../src/pages/resend-code'

describe('ResendCode', () => {
    it('disables resend button if username or email field is empty', () => {
        render(queryWrapper(<ResendCode />))
        expect(screen.getByRole('button', { name: /resend/i })).toBeDisabled()
    })
    it('detects whether username or email is given', async () => {
        const user = userEvent.setup()
        const mockHandler = jest.fn((c, res, ctx) => res(ctx.json('testuser')))
        render(queryWrapper(<ResendCode />))
        const inputField = screen.getByLabelText(/username or email/i)
        const resendButton = screen.getByRole('button', { name: /resend/i })
        await user.type(inputField, 'testuser')
        await user.click(resendButton)
        waitFor(() => {
            expect(inputField).toHaveValue('testuser')
            expect(resendButton).toBeEnabled()
            expect(mockHandler).toHaveBeenCalledWith({ userName: 'testuser' })
        })
    })
    it('calls resendCodeMutation with data: {email: userNameOrEmail} if value in userNameOrEmail is an email', async () => {
        const user = userEvent.setup()
        const mockHandler = jest.fn((c, res, ctx) => res(ctx.json('testuser')))
        render(queryWrapper(<ResendCode />))
        const inputField = screen.getByLabelText(/username or email/i)
        const resendButton = screen.getByRole('button', { name: /resend/i })
        await user.type(inputField, 'testuser@test.com')
        await user.click(resendButton)
        waitFor(() => {
            expect(inputField).toHaveValue('testuser@test.com')
            expect(resendButton).toBeEnabled()
            expect(mockHandler).toHaveBeenCalledWith({
                email: 'testuser@test.com',
            })
        })
    })
})
