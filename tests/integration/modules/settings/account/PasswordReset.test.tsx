import * as React from 'react'
import { render, renderHook, screen, waitFor } from '@testing-library/react'
import { usePasswordValidator } from '../../../../../src/hooks/usePasswordValidator'
import PasswordReset from '../../../../../src/pages/password-reset'
import { queryWrapper } from '../../../../utils/queryWrapper'
import userEvent from '@testing-library/user-event'

describe('PasswordReset', () => {
    it('changes from true to false if password requirements are finally met', async () => {
        render(queryWrapper(<PasswordReset />))
        const newPwField = screen.getAllByLabelText(
            /password/i
        )[0] as HTMLInputElement
        const { result, rerender } = renderHook(
            () => usePasswordValidator(newPwField.value),
            { initialProps: 'shortPw' }
        )
        expect(result.current.error).toBe(false)
        const user = userEvent.setup()
        await user.type(newPwField, 'Mantik_1234')
        rerender(newPwField.value)
        await waitFor(() => {
            expect(result.current.error).toBe(false)
        })
    })
    it('save new password button is initially disabled', () => {
        render(queryWrapper(<PasswordReset />))
        const saveButton = screen.getByRole('button', { name: /reset/i })
        expect(saveButton).toBeDisabled()
    })
})
