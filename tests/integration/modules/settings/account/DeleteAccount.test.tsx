import * as React from 'react'
import { render, screen, waitFor } from '@testing-library/react'
import { queryWrapper } from '../../../../utils/queryWrapper'
import userEvent from '@testing-library/user-event'
import { DeleteAccount } from '../../../../../src/modules/settings/account/DeleteAccount'
import { AccountProvider } from '../../../../../src/context/AccountContext'

import { server } from '../../../../../jest.setup'
import { HttpResponse, delay, http } from 'msw'
import AuthContext from '../../../../../src/context/AuthContext'

let deleted = false

const mockDelete = http.delete(
    'https://api.dev2.cloud.mantik.ai/users/:userId',
    async () => {
        deleted = true
        await delay(100)
        return new HttpResponse(null, {
            status: 204,
            headers: {
                'Content-Type': 'application/json',
            },
        })
    }
)

const mockGet = http.get(
    'https://api.dev2.cloud.mantik.ai/users/:userId',
    async () => {
        await delay(1000)
        if (deleted) return new HttpResponse(null, { status: 404 })
        return new HttpResponse(
            JSON.stringify({
                userId: '1234567',
                name: 'jane.doe',
                createdAt: '2021-10-10',
            }),
            {
                status: 200,
                headers: {
                    'Content-Type': 'application/json',
                },
            }
        )
    }
)

describe('DeleteAccount', () => {
    it('deletes account on click on "delete" if no data is related to the account', async () => {
        server.use(mockDelete)
        server.use(mockGet)
        const user = userEvent.setup()
        render(
            queryWrapper(
                <AuthContext.Provider
                    value={{
                        usersData: {
                            name: 'jane.doe',
                            userId: '1234567',
                            createdAt: new Date().toISOString(),
                        },
                        accessToken: 'accessToken',
                        queryStatus: 'success',
                    }}
                >
                    <AccountProvider>
                        <DeleteAccount />
                    </AccountProvider>
                </AuthContext.Provider>
            )
        )

        const deleteButton = screen.getByRole('button', {
            name: /delete account/i,
        })
        expect(deleteButton).toBeInTheDocument()
        await user.click(deleteButton)
        await waitFor(
            () => {
                expect(screen.queryByText('jane.doe')).toBeInTheDocument()
            },
            { timeout: 3000 }
        )
        const confirmButton = screen.getByRole('button', { name: 'Delete' })
        expect(confirmButton).toBeInTheDocument()
        expect(confirmButton).toBeEnabled()
        await user.click(confirmButton)
        await waitFor(() => {
            const successMessage = screen.getByText(/account was deleted/i)
            expect(successMessage).toBeInTheDocument()
        })
    })
})
