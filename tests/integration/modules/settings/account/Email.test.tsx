import * as React from 'react'
import { render, screen, waitFor } from '@testing-library/react'
import { Email } from '../../../../../src/modules/settings/account/Email'
import { queryWrapper } from '../../../../utils/queryWrapper'
import userEvent from '@testing-library/user-event'

describe('Email', () => {
    it('disables save button if email is invalid', async () => {
        render(queryWrapper(<Email />))
        const emailfield = screen.getByLabelText(/email/i)
        const submitButton = screen.getByRole('button', { name: /save/i })
        const user = userEvent.setup()
        await user.type(emailfield, 'hello')
        await waitFor(() => {
            expect(submitButton).toBeDisabled()
        })
    })
    it('enables save button if email is valid', async () => {
        render(queryWrapper(<Email />))
        const emailfield = screen.getByLabelText(/email/i)
        const submitButton = screen.getByRole('button', { name: /save/i })
        const user = userEvent.setup()
        await user.type(emailfield, 'test@test.de')
        await waitFor(() => {
            expect(submitButton).toBeEnabled()
        })
    })
    it('opens dialog on click on "Enter confirmation code" with submit button disabled', async () => {
        render(queryWrapper(<Email />))
        const submitButton = screen.getByRole('button', {
            name: /Enter confirmation code/i,
        })
        const user = userEvent.setup()
        await user.click(submitButton)
        await waitFor(() => {
            const dialog = screen.getByRole('dialog')
            const submitCodeButton = screen.getByRole('button', {
                name: /submit/i,
            })
            expect(dialog).toBeInTheDocument()
            expect(submitCodeButton).toBeDisabled()
        })
    })
    it('renders error message on wrong code entry', async () => {
        render(queryWrapper(<Email />))
        const submitButton = screen.getByRole('button', {
            name: /enter confirmation code/i,
        })
        const user = userEvent.setup()
        await user.click(submitButton)
        const confirmationCodeField = screen.getByRole('textbox', {
            name: /confirmation code/i,
        })
        await user.type(confirmationCodeField, '123456')
        const submitCodeButton = screen.getByRole('button', { name: /submit/i })
        await waitFor(() => expect(submitCodeButton).not.toBeDisabled())
        await user.click(submitCodeButton)
        await waitFor(() => {
            const errorMessage = screen.getByRole('alert')
            expect(errorMessage).toBeInTheDocument()
        })
    })
})
