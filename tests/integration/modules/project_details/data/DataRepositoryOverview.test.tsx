import { faker } from '@faker-js/faker'
import { render, screen, waitFor, within } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import { delay, http, HttpResponse } from 'msw'
import { server } from '../../../../../jest.setup'
import { DataRepositoriesOverview } from '../../../../../src/modules/project_details/data/DataRepositoriesOverview'
import { AuthMethod, ConnectionProvider } from '../../../../../src/queries'
import {
    getProjectsProjectIdDataDataRepositoryIdConnectionProjectsProjectIdDataDataRepositoryIdConnectionPostMockHandler as createConnectionMockHandler,
    getProjectsProjectIdDataPostProjectsProjectIdDataPostMockHandler as createDataRepositoryMockHandler,
} from '../../../../../src/queries/data-repository/data-repository.msw'
import { getUsersUserIdSettingsConnectionsGetUsersUserIdSettingsConnectionsGetMockHandler as getUserConnectionsMockHandler } from '../../../../../src/queries/user-connections/user-connections.msw'
import { queryWrapper } from '../../../../utils/queryWrapper'

const projectId = faker.string.uuid()
jest.mock('../../../../../src/context/ProjectContext', () => {
    return {
        ...jest.requireActual<
            typeof import('../../../../../src/context/ProjectContext')
        >('../../../../../src/context/ProjectContext'),
        useProject: jest.fn(() => ({ projectId })),
        usePermission: jest.fn(() => 'MAINTAINER'),
    }
})

const dummyConnections = () => ({
    connections: [
        {
            authMethod: {} as AuthMethod,
            connectionId: '12',
            connectionName: 'Test Connection',
            connectionProvider: 'GitHub' as ConnectionProvider,
            createdAt: '',
            user: {
                createdAt: '',
                name: 'Test User',
                userId: faker.string.uuid(),
            },
        },
        {
            authMethod: {} as AuthMethod,
            connectionId: '134',
            connectionName: 'Test Connection2',
            connectionProvider: 'S3' as ConnectionProvider,
            createdAt: '',
            user: {
                createdAt: '',
                name: 'Test User2',
                userId: faker.string.uuid(),
            },
        },
        {
            authMethod: {} as AuthMethod,
            connectionId: '13456',
            connectionName: 'Test Connection3',
            connectionProvider: 'GitHub' as ConnectionProvider,
            createdAt: '',
            user: {
                createdAt: '',
                name: 'Test User3',
                userId: faker.string.uuid(),
            },
        },
    ],
    pageRecords: 1,
    totalRecords: 1,
})

async function renderCardAndOpenEditDialog() {
    const user = userEvent.setup()
    render(queryWrapper(<DataRepositoriesOverview />))

    const addButton = await screen.findByRole('button', {
        name: 'Add',
    })

    await user.click(addButton)

    await waitFor(
        () => {
            expect(screen.getByRole('dialog')).toBeVisible()
            expect(screen.queryByRole('progressbar')).toBeNull()
        },
        { timeout: 3000 }
    )
}

async function clickButton(text: 'Create') {
    const user = userEvent.setup()
    const submitButton = await screen.findByRole('button', {
        name: text,
    })
    expect(submitButton).toBeEnabled()

    await user.click(submitButton)
}

async function fillInRequiredFields() {
    const user = userEvent.setup()
    await user.type(await screen.findByLabelText(/Name/i), 'Test name')
    await user.type(await screen.findByLabelText(/url/i), 'Test url')
}

async function selectConnectionByText(text: string) {
    const user = userEvent.setup()
    const selectCompoEl = screen.getAllByTestId('connectionSelector').at(0)
    if (!selectCompoEl) {
        throw new Error('Expect selectCompoEl not to be null')
    }
    const button = within(selectCompoEl).getByRole('combobox')
    await user.click(button)
    const listbox = await screen.findByRole('listbox')
    await user.click(within(listbox).getByText(text))
    expect(await screen.findByText(text)).toBeVisible()
}

async function selectDvcConnectionByText(text: string) {
    const user = userEvent.setup()
    const selectCompoEl = screen.getAllByTestId('connectionSelector').at(1)
    if (!selectCompoEl) {
        throw new Error(
            'Expect selectCompoEl not to be null, maybe dvc is not checked.'
        )
    }
    const button = within(selectCompoEl).getByRole('combobox')
    await user.click(button)
    const listbox = await screen.findByRole('listbox')
    await user.click(within(listbox).getByText(text))
    expect(await within(selectCompoEl).findByText(text)).toBeVisible()
}

describe('DataRepositoryOverview', () => {
    beforeEach(() => {
        server.use(
            getUserConnectionsMockHandler(dummyConnections()),
            createDataRepositoryMockHandler(),
            createConnectionMockHandler()
        )
    })

    it('should show success dialog if data repo was created', async () => {
        await renderCardAndOpenEditDialog()

        await fillInRequiredFields()
        await clickButton('Create')

        expect(await screen.findByText(/created successfully/i)).toBeVisible()
    }, 10_000)

    it('should show success dialog if data repo and connections were created', async () => {
        await renderCardAndOpenEditDialog()

        await fillInRequiredFields()
        await selectConnectionByText(
            dummyConnections().connections[0].connectionName
        )
        const user = userEvent.setup()
        const checkboxLabel = await screen.findByText(/Is DVC/i)
        await user.click(checkboxLabel)

        await selectDvcConnectionByText(
            dummyConnections().connections[1].connectionName
        )

        await clickButton('Create')

        expect(
            await screen.findAllByText(/created successfully/i)
        ).toHaveLength(3)
        expect(screen.queryAllByText(/failed to be created/i)).toHaveLength(0)
    }, 10_000)

    it('should show error if connection creation fails', async () => {
        server.use(
            http.post(
                'https://api.dev2.cloud.mantik.ai/projects/:projectId/data/:dataRepositoryId/connection',
                async () => {
                    await delay(1000)
                    return HttpResponse.json(
                        {
                            detail: "Connection platform 'and repository platform do not match.",
                        },
                        { status: 422 }
                    )
                }
            )
        )

        await renderCardAndOpenEditDialog()

        await fillInRequiredFields()
        await selectConnectionByText(
            dummyConnections().connections[0].connectionName
        )
        await clickButton('Create')

        expect(await screen.findByText(/created successfully/i)).toBeVisible()
        expect(await screen.findByText(/failed to be created/i)).toBeVisible()
    }, 10_000)

    it('should show error if data repository creation fails', async () => {
        server.use(
            http.post(
                'https://api.dev2.cloud.mantik.ai/projects/:projectId/data',
                async () => {
                    await delay(500)
                    return HttpResponse.json(
                        { detail: 'Configured incorrectly' },
                        { status: 400 }
                    )
                }
            )
        )
        await renderCardAndOpenEditDialog()

        await fillInRequiredFields()
        await clickButton('Create')

        await waitFor(() => {
            expect(screen.getByText(/Configured incorrectly/i)).toBeVisible()
        })
    })
})
