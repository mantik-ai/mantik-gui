import {
    render,
    screen,
    waitFor,
    waitForElementToBeRemoved,
    within,
} from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import { server } from '../../../../../../jest.setup'
import { EditRepositoryDialog } from '../../../../../../src/modules/project_details/data/Dialogs/EditRepositoryDialog'
import {
    AuthMethod,
    ConnectionProvider,
    DataRepository,
    HTTPError,
    HTTPValidationError,
} from '../../../../../../src/queries'
import { getUsersUserIdSettingsConnectionsGetUsersUserIdSettingsConnectionsGetMockHandler } from '../../../../../../src/queries/user-connections/user-connections.msw'
import { queryWrapper } from '../../../../../utils/queryWrapper'
import { faker } from '@faker-js/faker'
import { ErrorType } from '../../../../../../src/modules/auth/mantikApi'
import { AxiosRequestHeaders } from 'axios'

const dummyConnections = {
    connections: [
        {
            authMethod: {} as AuthMethod,
            connectionId: '12',
            connectionName: 'Test Connection',
            connectionProvider: 'GitHub' as ConnectionProvider,
            createdAt: '',
            user: {
                createdAt: '',
                name: 'Test User',
                userId: faker.string.uuid(),
            },
        },
        {
            authMethod: {} as AuthMethod,
            connectionId: '1234',
            connectionName: 'Test Connection2',
            connectionProvider: 'GitHub' as ConnectionProvider,
            createdAt: '',
            user: {
                createdAt: '',
                name: 'Test User',
                userId: faker.string.uuid(),
            },
        },
    ],
    pageRecords: 1,
    totalRecords: 1,
}
const dummyDataRepositoryData: DataRepository = {
    createdAt: Date.now().toString(),
    platform: 'GitHub',
    uri: 'https://',
    dataRepositoryId: faker.string.uuid(),
    connectionId: faker.string.uuid(),
    isDvcEnabled: false,
}

const renderEditRepositoryDialog = (
    {
        currentConnection,
        isDvcEnabled,
        networkError,
    }: {
        currentConnection?: { connectionId: string } | null
        isDvcEnabled?: boolean
        networkError?: ErrorType<HTTPValidationError | HTTPError> | null
    } = { currentConnection: null, isDvcEnabled: false, networkError: null }
) => {
    const setCurrentConnectionHandler = jest.fn()
    render(
        queryWrapper(
            <EditRepositoryDialog
                setCurrentConnection={setCurrentConnectionHandler}
                currentConnection={currentConnection ?? null}
                updateDataRepositoryError={networkError ?? null}
                setCurrentDvcConnection={jest.fn()}
                currentDvcConnection={{ connectionId: '' }}
                data={{ ...dummyDataRepositoryData, isDvcEnabled }}
                onSubmit={jest.fn()}
                onCancel={jest.fn()}
            />
        )
    )
    return { setCurrentConnectionHandler }
}

describe('EditRepositoryDialog', () => {
    it('should show connection error', async () => {
        const errorMessage = 'Some error from backend'

        renderEditRepositoryDialog({
            networkError: {
                response: {
                    status: 422,
                    statusText: 'Invalid format',
                    headers: {},
                    config: {
                        headers: undefined as unknown as AxiosRequestHeaders,
                    },
                    data: {
                        detail: errorMessage,
                    },
                },
                config: {
                    headers: undefined as unknown as AxiosRequestHeaders,
                },
                isAxiosError: true,
                toJSON: jest.fn(),
                name: faker.string.sample(),
                message: faker.string.sample(),
            },
        })

        expect(await screen.findByText(errorMessage)).toBeVisible()
    })

    it('should show connection selector if dvc checkbox is ticked', async () => {
        server.use(
            getUsersUserIdSettingsConnectionsGetUsersUserIdSettingsConnectionsGetMockHandler(
                dummyConnections
            )
        )
        renderEditRepositoryDialog({ isDvcEnabled: true })
        const progressbar = await screen.findAllByRole('progressbar')
        await waitForElementToBeRemoved(progressbar)

        expect(
            await screen.findAllByLabelText('Stored Connection')
        ).toHaveLength(2)
    })

    it('should show default connection value', async () => {
        server.use(
            getUsersUserIdSettingsConnectionsGetUsersUserIdSettingsConnectionsGetMockHandler(
                dummyConnections
            )
        )

        renderEditRepositoryDialog({
            currentConnection: {
                connectionId: dummyConnections.connections[0].connectionId,
            },
        })
        const progressbar = await screen.findAllByRole('progressbar')
        await waitForElementToBeRemoved(progressbar)

        await waitFor(
            () => {
                expect(
                    screen.getByText(
                        dummyConnections.connections[0].connectionName
                    )
                ).toBeVisible()
            },
            { timeout: 3000 }
        )
    })

    it('connection can be selected from the list', async () => {
        const user = userEvent.setup()
        server.use(
            getUsersUserIdSettingsConnectionsGetUsersUserIdSettingsConnectionsGetMockHandler(
                dummyConnections
            )
        )

        const { setCurrentConnectionHandler } = renderEditRepositoryDialog({
            currentConnection: {
                connectionId: dummyConnections.connections[0].connectionId,
            },
        })
        const progressbar = await screen.findAllByRole('progressbar')
        await waitForElementToBeRemoved(progressbar)

        const selectCompoEl = await screen.findByTestId('connectionSelector')
        const button = within(selectCompoEl).getByRole('combobox')
        await userEvent.click(button)
        const listbox = await screen.findByRole('listbox')

        await user.click(
            await within(listbox).findByText(
                dummyConnections.connections[1].connectionName
            )
        )
        expect(setCurrentConnectionHandler).toBeCalledWith(
            dummyConnections.connections[1]
        )
    })
})
