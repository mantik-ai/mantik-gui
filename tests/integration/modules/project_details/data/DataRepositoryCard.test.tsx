import { faker } from '@faker-js/faker'
import { render, screen, waitFor, within } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import { delay, http, HttpResponse } from 'msw'
import { BASE_API_URL, server } from '../../../../../jest.setup'
import { DataRepositoryCard } from '../../../../../src/modules/project_details/data/DataRepositoryCard'
import {
    getProjectsProjectIdDataDataRepositoryIdConnectionIdDeleteProjectsProjectIdDataDataRepositoryIdConnectionConnectionIdDeleteMockHandler as deleteStoredConnectionForDataRepositoryMockHandler,
    getProjectsProjectIdDataDataRepositoryIdGetProjectsProjectIdDataDataRepositoryIdGetMockHandler as getDataRepositoryMockHandler,
    getProjectsProjectIdDataDataRepositoryIdPutProjectsProjectIdDataDataRepositoryIdPutMockHandler as updateDataRepositoryMockHandler,
} from '../../../../../src/queries/data-repository/data-repository.msw'
import {
    AuthMethod,
    ConnectionProvider,
    DataRepository,
} from '../../../../../src/queries/models'
import { getUsersUserIdSettingsConnectionsGetUsersUserIdSettingsConnectionsGetMockHandler as getUserConnectionsMockHandler } from '../../../../../src/queries/user-connections/user-connections.msw'
import { queryWrapper } from '../../../../utils/queryWrapper'

const dummyConnections = () => ({
    connections: [
        {
            authMethod: {} as AuthMethod,
            connectionId: '12',
            connectionName: 'Test Connection',
            connectionProvider: 'GitHub' as ConnectionProvider,
            createdAt: '',
            user: {
                createdAt: '',
                name: 'Test User',
                userId: faker.string.uuid(),
            },
        },
        {
            authMethod: {} as AuthMethod,
            connectionId: '134',
            connectionName: 'Test Connection2',
            connectionProvider: 'S3' as ConnectionProvider,
            createdAt: '',
            user: {
                createdAt: '',
                name: 'Test User2',
                userId: faker.string.uuid(),
            },
        },
        {
            authMethod: {} as AuthMethod,
            connectionId: '13456',
            connectionName: 'Test Connection3',
            connectionProvider: 'GitHub' as ConnectionProvider,
            createdAt: '',
            user: {
                createdAt: '',
                name: 'Test User3',
                userId: faker.string.uuid(),
            },
        },
        {
            authMethod: {} as AuthMethod,
            connectionId: '1345678',
            connectionName: 'Test Connection4',
            connectionProvider: 'S3' as ConnectionProvider,
            createdAt: '',
            user: {
                createdAt: '',
                name: 'Test User2',
                userId: faker.string.uuid(),
            },
        },
    ],
    pageRecords: 1,
    totalRecords: 1,
})
const projectId = faker.string.uuid()
const dataRepositoryId = faker.string.uuid()
jest.mock('../../../../../src/context/ProjectContext', () => {
    return {
        ...jest.requireActual<
            typeof import('../../../../../src/context/ProjectContext')
        >('../../../../../src/context/ProjectContext'),
        useProject: jest.fn(() => ({ projectId })),
        usePermission: jest.fn(() => 'MAINTAINER'),
    }
})

const dummyDataRepositoryDataTable = (): DataRepository => ({
    createdAt: Date.now().toString(),
    platform: 'GitHub',
    uri: 'https://',
    dataRepositoryId: dataRepositoryId,
    connectionId: dummyConnections().connections[0].connectionId,
    isDvcEnabled: false,
})

const dummyDataRepositoryData = () => ({
    ...dummyDataRepositoryDataTable(),
    dataRepositoryName: 'The Name' as const,
})

async function renderCardAndOpenEditDialog() {
    const user = userEvent.setup()
    render(
        queryWrapper(
            <DataRepositoryCard
                id={dataRepositoryId}
                initialData={dummyDataRepositoryDataTable()}
                parentQueryKey={['']}
            />
        )
    )
    await waitFor(
        () => {
            expect(
                screen.getByText(dummyDataRepositoryData().dataRepositoryName)
            ).toBeVisible()
        },
        { timeout: 3000 }
    )

    const editButton = await screen.findByRole('button', {
        name: 'Edit',
    })

    await user.click(editButton)

    await waitFor(
        () => {
            expect(screen.getByRole('dialog')).toBeVisible()
            expect(screen.queryByRole('progressbar')).toBeNull()
        },
        { timeout: 3000 }
    )
}

async function selectConnectionByText(text: string) {
    const user = userEvent.setup()
    const selectCompoEl = screen.getAllByTestId('connectionSelector').at(0)
    if (!selectCompoEl) {
        throw new Error('Expect selectCompoEl not to be null')
    }
    const button = within(selectCompoEl).getByRole('combobox')
    await user.click(button)
    const listbox = await screen.findByRole('listbox')
    await user.click(within(listbox).getByText(text))
    expect(await screen.findByText(text)).toBeVisible()
}

async function selectDvcConnectionByText(text: string) {
    const user = userEvent.setup()
    const selectCompoEl = screen.getAllByTestId('connectionSelector').at(1)
    if (!selectCompoEl) {
        throw new Error(
            'Expect selectCompoEl not to be null, maybe dvc is not checked.'
        )
    }
    const button = within(selectCompoEl).getByRole('combobox')
    await user.click(button)
    const listbox = await screen.findByRole('listbox')
    await user.click(within(listbox).getByText(text))
    expect(await within(selectCompoEl).findByText(text)).toBeVisible()
}

async function clickButton(text: 'Update') {
    const user = userEvent.setup()
    const submitButton = await screen.findByRole('button', {
        name: text,
    })
    expect(submitButton).toBeEnabled()

    await user.click(submitButton)
}

describe('DataRepositoryCard', () => {
    describe('Edit Data Repository', () => {
        beforeEach(() => {
            server.use(
                getUserConnectionsMockHandler(dummyConnections()),
                updateDataRepositoryMockHandler(),
                deleteStoredConnectionForDataRepositoryMockHandler(),
                getDataRepositoryMockHandler(dummyDataRepositoryData())
            )
        })

        it('should show success dialog if data was updated', async () => {
            await renderCardAndOpenEditDialog()
            await clickButton('Update')

            expect(
                await screen.findByText(/updated successfully/)
            ).toBeVisible()
        }, 10_000)

        it('should set default connection value', async () => {
            await renderCardAndOpenEditDialog()

            expect(
                await screen.findByText(
                    dummyConnections().connections[0].connectionName
                )
            ).toBeVisible()
        }, 10_000)

        it('should show error if connection deletion fails', async () => {
            server.use(
                http.delete(
                    'https://api.dev2.cloud.mantik.ai/projects/:projectId/data/:dataRepositoryId/connection/:connectionId',
                    async () => {
                        await delay(100)
                        return HttpResponse.json(
                            { detail: 'Data repository was not found' },
                            { status: 404 }
                        )
                    }
                )
            )

            await renderCardAndOpenEditDialog()
            await selectConnectionByText('None selected')
            await clickButton('Update')

            await waitFor(
                () => {
                    expect(screen.getByText(/Not found/i)).toBeVisible()
                },
                { timeout: 3000 }
            )
        }, 20_000)

        it('should show error if connection creation fails', async () => {
            server.use(
                http.post(
                    'https://api.dev2.cloud.mantik.ai/projects/:projectId/data/:dataRepositoryId/connection',
                    async () => {
                        await delay(500)
                        return HttpResponse.json(
                            { detail: 'Configured incorrectly' },
                            { status: 400 }
                        )
                    }
                )
            )

            await renderCardAndOpenEditDialog()
            await selectConnectionByText(
                dummyConnections().connections[2].connectionName
            )
            await clickButton('Update')

            await waitFor(
                () => {
                    expect(
                        screen.getByText(/Configured incorrectly/i)
                    ).toBeVisible()
                },
                { timeout: 4000 }
            )
        }, 10_000)

        it('should show error if update repository data fails', async () => {
            server.use(
                http.put(
                    'https://api.dev2.cloud.mantik.ai/projects/:projectId/data/:dataRepositoryId',
                    async () => {
                        await delay(300)

                        return HttpResponse.json(
                            { detail: 'Data repository was not found' },
                            { status: 404 }
                        )
                    }
                )
            )

            await renderCardAndOpenEditDialog()
            await clickButton('Update')
            await waitFor(
                () => {
                    expect(screen.getByText(/Not found/i)).toBeVisible()
                },
                { timeout: 3000 }
            )
        }, 10_000)

        describe('Edit data repository with existing dvc connection', () => {
            const DATAREPO_CONNECTIONS_API_URL = `${BASE_API_URL}/projects/:projectId/data/:dataRepositoryId/connection`
            const CONNECTION_API_URL = `${BASE_API_URL}/projects/:projectId/data/:dataRepositoryId/connection/:connectionId`

            beforeEach(() => {
                server.use(
                    getDataRepositoryMockHandler({
                        ...dummyDataRepositoryData(),
                        dvcConnectionId:
                            dummyConnections().connections[1].connectionId,
                        isDvcEnabled: true,
                    })
                )
            })

            it('should set dvc connection and checkbox if set', async () => {
                await renderCardAndOpenEditDialog()

                const isDvcCheckbox = await screen.findByRole('checkbox', {
                    name: /Is DVC/i,
                })
                expect(isDvcCheckbox).toBeChecked()

                expect(
                    screen.getByText(
                        dummyConnections().connections[1].connectionName
                    )
                ).toBeVisible()
            })

            it('should delete dvc connection when checkbox is unchecked and there was dvc connection set before', async () => {
                const deleteFn = jest.fn(async () => {
                    await delay(100)
                    return new HttpResponse(null, {
                        status: 204,
                        headers: {
                            'Content-Type': 'application/json',
                        },
                    })
                })
                server.use(http.delete(CONNECTION_API_URL, deleteFn))

                await renderCardAndOpenEditDialog()

                const user = userEvent.setup()
                const checkboxLabel = await screen.findByText(/Is DVC/i)
                await user.click(checkboxLabel)
                expect(checkboxLabel).not.toBeChecked()
                await clickButton('Update')

                expect(
                    await screen.findByText(/updated successfully/)
                ).toBeVisible()
                expect(deleteFn).toHaveBeenCalled()
            }, 10_000)

            it('should delete dvc connection and add dvc connection when dvc connection changed', async () => {
                const deleteFn = jest.fn(async () => {
                    await delay(100)
                    return new HttpResponse(null, {
                        status: 204,
                        headers: {
                            'Content-Type': 'application/json',
                        },
                    })
                })
                const addFn = jest.fn(async () => {
                    await delay(100)
                    return new HttpResponse(null, {
                        status: 201,
                        headers: {
                            'Content-Type': 'application/json',
                        },
                    })
                })
                server.use(http.delete(CONNECTION_API_URL, deleteFn))
                server.use(http.post(DATAREPO_CONNECTIONS_API_URL, addFn))

                await renderCardAndOpenEditDialog()
                await selectDvcConnectionByText(
                    dummyConnections().connections[3].connectionName
                )
                await clickButton('Update')

                expect(
                    await screen.findByText(/updated successfully/)
                ).toBeVisible()
                expect(deleteFn).toHaveBeenCalled()
                expect(addFn).toHaveBeenCalled()
            }, 10_000)
        })
    })
})
