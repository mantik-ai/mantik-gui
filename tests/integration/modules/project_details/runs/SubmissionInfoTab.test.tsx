import { render, screen, waitFor } from '@testing-library/react'
import { server } from '../../../../../jest.setup'
import { SubmissionInfoTab } from '../../../../../src/modules/project_details/runs/SubmissionInfoTab'
import { queryWrapper } from '../../../../utils/queryWrapper'
import {
    getProjectsProjectIdRunsRunIdInfoGetProjectsProjectIdRunsRunIdInfoGetMockHandler,
    getProjectsProjectIdRunsRunIdInfoGetProjectsProjectIdRunsRunIdInfoGetResponseMock,
} from '../../../../../src/queries/runs/runs.msw'
import { NextRouter, useRouter } from 'next/router'
import { delay, http, HttpResponse } from 'msw'
import { Run } from '../../../../../src/queries'
import { ComponentProps } from 'react'
import { faker } from '@faker-js/faker'

const exampleRun: Run = {
    runId: 'b73f8c83-c698-4426-83dd-ada06aa970fc',
    name: 'some name',
    user: {
        name: 'user',
        userId: 'id',
        createdAt: new Date().toISOString(),
    },
    createdAt: new Date().toISOString(),
    entryPoint: 'blank',
    mlflowRunId: 'blank',
    backendConfig: {},
    experimentRepository: {
        createdAt: new Date().toISOString(),
        name: 'experiment repo',
        mlflowExperimentId: 123,
        experimentRepositoryId: '1235',
    },
    mlflowMlprojectFilePath: 'path',
    modelRepository: {
        createdAt: new Date().toISOString(),
        modelRepositoryId: '456',
    },
}

async function renderFinishedSubmissionInfo(
    props: ComponentProps<typeof SubmissionInfoTab>
    // props: NonNullable<ComponentProps<typeof SubmissionInfoTab>>
) {
    render(queryWrapper(<SubmissionInfoTab runData={props.runData} />))
    expect(await screen.findByText('Loading Data...')).toBeInTheDocument()
    await waitFor(
        () => {
            expect(screen.queryByText('Loading Data...')).toBeNull()
        },
        { timeout: 3000 }
    )
}

describe('SubmissionInfoTab', () => {
    beforeEach(() => {
        jest.mocked(useRouter).mockReturnValue({
            asPath: '/path',
            push: jest.fn(),
            query: { id: 'foo' },
        } as unknown as NextRouter)
    })

    it('should render error message if submission data requires connection', async () => {
        const errorMessage = faker.word.words(5)
        server.use(
            http.get(
                'https://api.dev2.cloud.mantik.ai/projects/:projectId/runs/:runId/info',
                async () => {
                    await delay(1000)
                    return HttpResponse.json(
                        { detail: errorMessage },
                        { status: 422 }
                    )
                }
            )
        )

        await renderFinishedSubmissionInfo({ runData: exampleRun })

        expect(await screen.findByText(errorMessage)).toBeInTheDocument()
    })

    it('should render submission data', async () => {
        const testData =
            getProjectsProjectIdRunsRunIdInfoGetProjectsProjectIdRunsRunIdInfoGetResponseMock(
                { name: 'test name' }
            )
        server.use(
            getProjectsProjectIdRunsRunIdInfoGetProjectsProjectIdRunsRunIdInfoGetMockHandler(
                testData
            )
        )

        await renderFinishedSubmissionInfo({ runData: exampleRun })

        expect(await screen.findByText('test name')).toBeInTheDocument()
    })
})
