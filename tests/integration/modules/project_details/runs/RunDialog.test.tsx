import { faker } from '@faker-js/faker'
import {
    render,
    screen,
    waitFor,
    waitForElementToBeRemoved,
    within,
} from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import { http, HttpResponse } from 'msw'
import { server } from '../../../../../jest.setup'
import { RunDialog } from '../../../../../src/modules/project_details/runs/RunDialog'
import { getProjectsProjectIdDataGetProjectsProjectIdDataGetMockHandler } from '../../../../../src/queries/data-repository/data-repository.msw'
import { queryWrapper } from '../../../../utils/queryWrapper'
import { getProjectsProjectIdCodeGetProjectsProjectIdCodeGetMockHandler } from '../../../../../src/queries/code-repository/code-repository.msw'

const dummyProjectId = faker.string.uuid()
jest.mock('next/router', () => ({
    useRouter: jest.fn(() => ({ query: { id: dummyProjectId } })),
}))

jest.mock('../../../../../src/hooks/useAuthentication', () => ({
    useAuthentication: jest.fn(() => 'authenticated'),
}))

function renderRunDialog() {
    render(
        queryWrapper(
            <RunDialog
                open={true}
                setOpen={jest.fn()}
                setEditItem={jest.fn()}
            />
        )
    )
}

async function selectRepository(repoType: 'code' | 'data') {
    const testId =
        repoType === 'code'
            ? 'codeRepositorySelector'
            : 'dataRepositorySelector'

    const user = userEvent.setup()
    const selectCompoEl = (await screen.findAllByTestId(testId)).at(0)
    if (!selectCompoEl) {
        throw new Error('Expect selectCompoEl not to be null')
    }
    const button = within(selectCompoEl).getByRole('combobox')
    await user.click(button)
    const listbox = await screen.findByRole('listbox')

    await waitFor(() => {
        expect(within(listbox).getAllByRole('option')).not.toHaveLength(1)
    })
    await user.click(within(listbox).getAllByRole('option')[0])
}

async function selectRepoOption(
    repoType: 'code' | 'data',
    optionType: 'branch' | 'commitHash'
) {
    const testId =
        repoType === 'code' ? 'codeRepositoryInputs' : 'dataRepositoryInputs'

    const optionLabel = optionType === 'branch' ? 'Branch' : 'Commit Hash'

    const inputs = await screen.findByTestId(testId)
    within(inputs).getByText(optionLabel).click()
}

function interceptRepositories() {
    server.use(
        getProjectsProjectIdDataGetProjectsProjectIdDataGetMockHandler({
            dataRepositories: [
                {
                    dataRepositoryId: faker.string.uuid(),
                    dataRepositoryName: faker.word.sample(),
                    createdAt: faker.date.past().toISOString(),
                    platform: 'GitLab',
                    uri: faker.internet.url(),
                },
                {
                    dataRepositoryId: faker.string.uuid(),
                    dataRepositoryName: faker.word.sample(),
                    createdAt: faker.date.past().toISOString(),
                    platform: 'GitLab',
                    uri: faker.internet.url(),
                },
            ],
            pageRecords: 1,
            totalRecords: 2,
        }),

        getProjectsProjectIdCodeGetProjectsProjectIdCodeGetMockHandler({
            codeRepositories: [
                {
                    codeRepositoryId: faker.string.uuid(),
                    codeRepositoryName: faker.word.sample(),
                    createdAt: faker.date.past().toISOString(),
                    platform: 'GitLab',
                    uri: faker.internet.url(),
                },
                {
                    codeRepositoryId: faker.string.uuid(),
                    codeRepositoryName: faker.word.sample(),
                    createdAt: faker.date.past().toISOString(),
                    platform: 'GitLab',
                    uri: faker.internet.url(),
                },
            ],
            pageRecords: 1,
            totalRecords: 2,
        })
    )
}

function interceptInvalidRepo() {
    server.use(
        http.post('**/api/runs/get-repository', () => {
            return HttpResponse.json({
                data_response: false,
            })
        })
    )
}

function interceptInvalidRepoPath() {
    server.use(
        http.post('**/api/runs/get-repository', async (req) => {
            const postData = (await req.request.json()) as { url: string }
            const submittedUrl = new URL(postData.url)

            if (submittedUrl.pathname.length > 1) {
                return HttpResponse.json({
                    data_response: false,
                })
            }

            return HttpResponse.json({
                data_response: true,
            })
        })
    )
}

describe('Run Dialog', () => {
    describe('Data Repository', () => {
        beforeEach(() => {
            server.use(
                http.post('**/api/runs/get-repository', () => {
                    return HttpResponse.json({
                        data_response: true,
                    })
                })
            )
        })

        it('should render field for branch name if this option is selected', async () => {
            renderRunDialog()

            const inputs = await screen.findByTestId('dataRepositoryInputs')
            expect(
                within(inputs).getByLabelText('Branch', {
                    selector: 'input[type="radio"]',
                })
            ).toBeChecked()
            const branchName = within(inputs).getByLabelText(/Branch Name/)
            expect(branchName).toBeVisible()
        })

        it('should disable branch name field if data repository is not selected', async () => {
            renderRunDialog()

            const inputs = await screen.findByTestId('dataRepositoryInputs')
            const branchName = within(inputs).getByLabelText(/Branch Name/)

            expect(branchName).toBeDisabled()
        })

        it('should enable branch name field if data repository is selected', async () => {
            interceptRepositories()
            renderRunDialog()

            await selectRepository('data')

            const inputs = screen.getByTestId('dataRepositoryInputs')
            const branchName = within(inputs).getByLabelText(/Branch Name/)
            expect(branchName).toBeEnabled()
        })

        it('should render field for commit hash if this option is selected', async () => {
            const user = userEvent.setup()

            interceptRepositories()
            renderRunDialog()
            const progressbar = await screen.findAllByRole('progressbar')
            await waitForElementToBeRemoved(progressbar)

            await selectRepository('data')
            const inputs = screen.getByTestId('dataRepositoryInputs')
            await user.click(within(inputs).getByText(/Commit Hash/i))

            expect(
                await within(inputs).findByRole('textbox', {
                    name: /Commit Hash/,
                })
            ).toBeVisible()
        })

        it('should render enabled field for commit hash if data repository is selected', async () => {
            const user = userEvent.setup()
            interceptRepositories()
            renderRunDialog()
            await selectRepository('data')

            const inputs = screen.getByTestId('dataRepositoryInputs')
            await user.click(within(inputs).getByText('Commit Hash'))

            expect(
                within(inputs).getByRole('textbox', { name: /Commit Hash/ })
            ).toBeEnabled()
        })

        it('should set main as default branch for data repository', async () => {
            interceptRepositories()
            renderRunDialog()
            const progressbar = await screen.findAllByRole('progressbar')
            await waitForElementToBeRemoved(progressbar)

            await selectRepository('data')

            const inputs = screen.getByTestId('dataRepositoryInputs')
            const branchName = within(inputs).getByLabelText(/Branch Name/)
            expect(branchName).toHaveValue('main')
        })

        describe('mantik-gui#444 data and code repository branch/commit validation is done on reselect', () => {
            function givenIHaveReposConfigured() {
                interceptRepositories()
            }
            function givenIHaveValidReposConfigured() {
                interceptInvalidRepoPath()
                interceptRepositories()
            }

            async function whenIOpenTheRunDialog() {
                renderRunDialog()
                const progressbar = await screen.findAllByRole('progressbar')
                await waitForElementToBeRemoved(progressbar)
            }
            async function whenISelectAnInvalidCodeRepository() {
                interceptInvalidRepo()
                await selectRepository('code')
            }
            async function whenISelectACodeRepositoryWithInvalidBranch() {
                interceptInvalidRepoPath()
                await selectRepository('code')
                await screen.findByText('Invalid URL')
            }
            async function whenISelectAnInvalidDataRepository() {
                interceptInvalidRepo()
                await selectRepository('data')
            }
            async function whenISelectADataRepositoryWithInvalidBranch() {
                interceptInvalidRepoPath()
                await selectRepository('data')
                await screen.findByText('Invalid URL')
            }
            async function whenISelectACodeRepositoryCommitHashOption() {
                await selectRepoOption('code', 'commitHash')
            }
            async function whenISelectCodeRepositoryBranchOption() {
                await selectRepoOption('code', 'branch')
            }
            async function whenISelectADataRepositoryCommitHashOption() {
                await selectRepoOption('data', 'commitHash')
            }
            async function whenISelectDataRepositoryBranchOption() {
                await selectRepoOption('data', 'branch')
            }

            async function thenISeeCodeRepoError() {
                await waitFor(() => {
                    expect(
                        screen.getByText('Code repository could not be loaded')
                    ).toBeVisible()
                })
            }
            async function thenISeeDataRepoError() {
                await waitFor(() => {
                    expect(
                        screen.getByText('Data repository could not be loaded')
                    ).toBeVisible()
                })
            }
            async function thenISeeRepoBranchError() {
                await waitFor(() => {
                    expect(screen.getByText('Invalid URL')).toBeVisible()
                })
            }

            it('should display error for invalid code repo', async () => {
                givenIHaveReposConfigured()
                await whenIOpenTheRunDialog()
                await whenISelectAnInvalidCodeRepository()
                await thenISeeCodeRepoError()
            })
            it('should display error for invalid code repo branch', async () => {
                givenIHaveValidReposConfigured()
                await whenIOpenTheRunDialog()
                await whenISelectACodeRepositoryWithInvalidBranch()
                await thenISeeRepoBranchError()
            })
            it('should display error for reselect of invalid code repo branch', async () => {
                givenIHaveReposConfigured()
                await whenIOpenTheRunDialog()
                await whenISelectACodeRepositoryWithInvalidBranch()
                await whenISelectACodeRepositoryCommitHashOption()
                await whenISelectCodeRepositoryBranchOption()
                await thenISeeRepoBranchError()
            })

            it('should display error for invalid data repo', async () => {
                givenIHaveReposConfigured()
                await whenIOpenTheRunDialog()
                await whenISelectAnInvalidDataRepository()
                await thenISeeDataRepoError()
            })
            it('should display error for invalid data repo branch', async () => {
                givenIHaveReposConfigured()
                await whenIOpenTheRunDialog()
                await whenISelectADataRepositoryWithInvalidBranch()
                await thenISeeRepoBranchError()
            })
            it('should display error for reselect of invalid data repo branch', async () => {
                givenIHaveReposConfigured()
                await whenIOpenTheRunDialog()
                await whenISelectADataRepositoryWithInvalidBranch()
                await whenISelectADataRepositoryCommitHashOption()
                await whenISelectDataRepositoryBranchOption()
                await thenISeeRepoBranchError()
            })
        })
    })
})
