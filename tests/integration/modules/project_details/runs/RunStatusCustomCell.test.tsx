import * as React from 'react'
import { render, screen } from '@testing-library/react'
import { userEvent } from '@testing-library/user-event'
import RunStatusCustomCell, {
    RunStatusComponent,
} from '../../../../../src/modules/project_details/runs/RunStatusCustomCell'
import { StatusType } from '../../../../../src/components/TablesLayout/statusType'
import { UserRole } from '../../../../../src/types/UserRole'
import { queryWrapper } from '../../../../utils/queryWrapper'

describe('RunStatusCustomCell', () => {
    it('renders Chip with label FAILED', () => {
        render(
            queryWrapper(
                <RunStatusCustomCell
                    userRole={UserRole.Owner}
                    runId={'b73f8c83-c698-4426-83dd-ada06aa970fc'}
                    runStatus={StatusType.FAILED}
                    setCurrentRunStatus={() => {}}
                    setResetErrorRunId={() => {}}
                />
            )
        )
        expect(screen.getByText('FAILED')).toBeInTheDocument()
    })
})

describe('RunStatusComponent', () => {
    it('renders refresh icon when status is UNKNOWN', () => {
        render(
            <RunStatusComponent
                data={StatusType.UNKNOWN}
                handleRefreshClick={() => {}}
                userRole={UserRole.Researcher}
                isRefetching={false}
            />
        )
        expect(screen.getByTestId('RefreshIcon')).toBeInTheDocument()
    })
    it('does not render refresh icon when status is FINISHED', async () => {
        render(
            <RunStatusCustomCell
                userRole={UserRole.Owner}
                runId={'b73f8c83-c698-4426-83dd-ada06aa970fc'}
                runStatus={StatusType.FINISHED}
                setCurrentRunStatus={() => {}}
                setResetErrorRunId={() => {}}
            />
        )
        expect(screen.queryByTestId('RefreshIcon')).not.toBeInTheDocument()
    })
    it('calls the refetch function when refresh icon button is clicked', async () => {
        const user = userEvent.setup()
        const mockRefetch = jest.fn()
        render(
            <RunStatusComponent
                data={StatusType.UNKNOWN}
                handleRefreshClick={mockRefetch}
                userRole={UserRole.Researcher}
                isRefetching={false}
            />
        )
        await user.click(screen.getByTestId('RefreshIcon'))
        expect(mockRefetch).toHaveBeenCalled()
    })
})
