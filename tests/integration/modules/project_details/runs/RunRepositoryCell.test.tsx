import { render, screen } from '@testing-library/react'
import { RunRepositoryCell } from '../../../../../src/modules/project_details/runs/RunRepositoryCell'
import { queryWrapper } from '../../../../utils/queryWrapper'

describe('RunRepositoryCell', () => {
    describe('mantik-api#318 Empty repository fields are not displayed', () => {
        it('User reviews code repository of a run without a branch', () => {
            const repoName = 'Test Repo'
            render(queryWrapper(<RunRepositoryCell name={repoName} />))
            expect(screen.getByText('Name')).toBeInTheDocument()
            expect(screen.getByText(repoName)).toBeInTheDocument()
            expect(screen.queryByText('Branch')).not.toBeInTheDocument()
        })
    })
})
