import * as React from 'react'
import { render, screen, waitFor } from '@testing-library/react'
import { PlatformSelect } from '../../../../../src/modules/project_details/code/platform/PlatformSelect'
import { Platform } from '../../../../../src/queries/models'
import userEvent from '@testing-library/user-event'

describe('PlatformSelect', () => {
    it('renders field with label "Platform" if no platform is passed as props', async () => {
        render(
            <PlatformSelect
                setPlatform={() => {
                    /* noop */
                }}
            />
        )
        const select = screen.getByLabelText(/platform/i)
        expect(select).toBeInTheDocument()
        expect(select).toHaveTextContent('')
    })
    it('renders platform value if platform is passed as props', async () => {
        render(
            <PlatformSelect
                platform={Platform.GitLab}
                setPlatform={() => {
                    /* noop */
                }}
            />
        )
        const select = screen.getByLabelText(/platform/i)
        expect(select).toBeInTheDocument()
        expect(select).toHaveTextContent('GitLab')
    })
    it('renders selected platform value', async () => {
        render(
            <PlatformSelect
                setPlatform={() => {
                    /* noop */
                }}
            />
        )
        const select = screen.getByLabelText(/platform/i)
        const user = userEvent.setup()
        await user.click(select)
        await waitFor(() => {
            // TODO: Why is this here? what is meant to be tested?
            // to do: find out why select options are not being rendered in the test
        })
    })
})
