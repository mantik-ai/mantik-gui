import { render, screen, waitFor } from '@testing-library/react'
import { userEvent } from '@testing-library/user-event'
import * as React from 'react'
import {
    ActionProps,
    TablesLayout,
} from '../../../../src/components/TablesLayout/TablesLayout'
import { toSemanticUserRole } from '../../../../src/utils/userRole'
import { MantikApiDatabaseProjectProjectRole } from '../../../../src/queries'

const data = [
    {
        createdAt: '2023-12-07T09:40:46.802794+00:00',
        id: 'b73f8c83-c698-4426-83dd-ada06aa970fc',
        name: 'Run Name',
        experiment: 'Experiment Repository Name',
    },
    {
        createdAt: '2023-12-07T09:40:46.802794+00:00',
        id: 'b73f8c83-c698-4426-83dd-ada06aa970fc',
        name: 'Run Name 2',
        experiment: 'Experiment Repository Name 2',
    },
    {
        createdAt: '2023-12-07T09:40:46.802794+00:00',
        id: 'b73f8c83-c698-4426-83dd-ada06aa970fc',
        name: 'Run Name 3',
        experiment: 'Experiment Repository Name 3',
    },
]
describe('TableLayout', () => {
    const tableData = data.map(({ createdAt, id, name, experiment }) => ({
        data: {
            'Created At': { data: createdAt },
            ID: {
                data: id,
                hiddenTableColumn: true,
                component: <div data-testid="custom-component-1">{id}</div>,
            },
            Name: {
                data: name,
                component: <div data-testid="custom-component-2">{name}</div>,
            },
            'Experiment Repository': {
                data: experiment,
            },
        },
    }))
    const props = {
        data: tableData,
        action: {
            delete: () => {},
            edit: () => {},
        } as ActionProps,
        page: {
            rowsPerPage: 10,
            page: 1,
            totalRecords: data.length,
            handleRowsChange: () => {},
        },
    }

    it('renders TableLayout with 3 rows and 3 columns where one column has a custom component as a child', () => {
        render(<TablesLayout {...props} />)

        expect(screen.getByText('Created At')).toBeInTheDocument()
        expect(screen.getByText('Name')).toBeInTheDocument()
        expect(screen.getByText('Experiment Repository')).toBeInTheDocument()
        expect(screen.getAllByTestId('custom-component-2')).toHaveLength(3)
    })
    it('does not render column that has prop "hiddenTableColumn"', () => {
        render(<TablesLayout {...props} />)

        expect(
            screen.queryByTestId('custom-component-1')
        ).not.toBeInTheDocument()
        expect(screen.queryByText('ID')).not.toBeInTheDocument()
    })
    it('opens details dialog if "View Details" icon button is clicked', async () => {
        const user = userEvent.setup()
        render(<TablesLayout {...props} />)
        const actionButtonDetails = screen.getAllByTestId(
            'VisibilityOutlinedIcon'
        )[0]
        await user.click(actionButtonDetails)
        await waitFor(() => {
            const dialog = screen.getByRole('dialog')
            expect(dialog).toBeInTheDocument()
        })
    })
    it('checks if RUD buttons are always displayed on TablesLayout', async () => {
        render(<TablesLayout {...props} />)
        const RUDButtons = screen.queryAllByTestId((id) =>
            id.startsWith('rud-button-')
        )
        const numberOfButtonsPerRow = 3 // we have 3 buttons-Read,Update,Delete buttons
        const numberOfRows = data.length
        const expectedButtonCount = numberOfButtonsPerRow * numberOfRows
        expect(RUDButtons).toHaveLength(expectedButtonCount)
    })
    it('checks if Edit and Delete buttons are not clickable for users with role guest and reporter', async () => {
        const rolesNotAllowedToClick = ['VISITOR', 'GUEST', 'REPORTER']
        const allowedToClickTooltipTexts = ['Details']
        // Make sure we're testing what the API actually tells us the roles are
        const userRole = toSemanticUserRole(
            MantikApiDatabaseProjectProjectRole.NUMBER_2
        )
        const newProps = {
            ...props,
            action: { ...props.action, delete: undefined, edit: undefined },
        }
        render(<TablesLayout {...newProps} />)
        const RUDButtons = screen.queryAllByTestId((id) =>
            id.startsWith('rud-button-')
        )
        RUDButtons.forEach((button) => {
            const tooltipText = button
                .getAttribute('data-testid')
                ?.replace('rud-button-', '')

            if (tooltipText) {
                const isButtonDisabled =
                    rolesNotAllowedToClick.includes(userRole) &&
                    !allowedToClickTooltipTexts.includes(tooltipText)

                if (isButtonDisabled) {
                    expect(button).toBeDisabled()
                } else {
                    expect(button).toBeEnabled()
                }
            }
        })
    })
    it('checks if Details, Edit, and Delete buttons are clickable for users with role RESEARCHER+', async () => {
        const rolesAllowedToClick = ['RESEARCHER', 'MAINTAINER', 'OWNER']
        const allowedToClickTooltipTexts = ['Details', 'Edit', 'Delete']
        const userRole = toSemanticUserRole(
            MantikApiDatabaseProjectProjectRole.NUMBER_3
        )
        render(<TablesLayout {...props} />)
        const RUDButtons = screen.queryAllByTestId((id) =>
            id.startsWith('rud-button-')
        )
        RUDButtons.forEach((button) => {
            const tooltipText = button
                .getAttribute('data-testid')
                ?.replace('rud-button-', '')

            if (tooltipText) {
                const isButtonEnabled =
                    rolesAllowedToClick.includes(userRole) &&
                    allowedToClickTooltipTexts.includes(tooltipText)

                if (isButtonEnabled) {
                    expect(button).toBeEnabled()
                }
            }
        })
    })
})
