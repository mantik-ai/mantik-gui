import { render, screen, waitFor, within } from '@testing-library/react'
import { queryWrapper } from '../../../utils/queryWrapper'
import { ConnectionSelection } from '../../../../src/components/forms/ConnectionSelection'
import { AuthMethod, ConnectionProvider } from '../../../../src/queries'
import { faker } from '@faker-js/faker'
import { server } from '../../../../jest.setup'
import { getUsersUserIdSettingsConnectionsGetUsersUserIdSettingsConnectionsGetMockHandler as getUserConnectionsMockHandler } from '../../../../src/queries/user-connections/user-connections.msw'
import userEvent from '@testing-library/user-event'

const dummyConnections = {
    connections: [
        {
            authMethod: {} as AuthMethod,
            connectionId: faker.string.uuid(),
            connectionName: 'Test Connection',
            connectionProvider: 'GitHub' as ConnectionProvider,
            createdAt: '',
            user: {
                createdAt: '',
                name: 'Test User',
                userId: faker.string.uuid(),
            },
        },
        {
            authMethod: {} as AuthMethod,
            connectionId: faker.string.uuid(),
            connectionName: 'Test Connection2',
            connectionProvider: 'S3' as ConnectionProvider,
            createdAt: '',
            user: {
                createdAt: '',
                name: 'Test User2',
                userId: faker.string.uuid(),
            },
        },
        {
            authMethod: {} as AuthMethod,
            connectionId: faker.string.uuid(),
            connectionName: 'Test Connection3',
            connectionProvider: 'GitHub' as ConnectionProvider,
            createdAt: '',
            user: {
                createdAt: '',
                name: 'Test User3',
                userId: faker.string.uuid(),
            },
        },
        {
            authMethod: {} as AuthMethod,
            connectionId: faker.string.uuid(),
            connectionName: 'Test Connection4',
            connectionProvider: 'S3' as ConnectionProvider,
            createdAt: '',
            user: {
                createdAt: '',
                name: 'Test User2',
                userId: faker.string.uuid(),
            },
        },
    ],
    pageRecords: 1,
    totalRecords: 1,
}

describe('ConnectionSelection', () => {
    beforeEach(() => {
        server.use(getUserConnectionsMockHandler(dummyConnections))
    })
    it('should show filtered connection list', async () => {
        render(
            queryWrapper(
                <ConnectionSelection
                    setCurrentConnection={jest.fn()}
                    platform="GitHub"
                />
            )
        )

        await waitFor(() => {
            expect(screen.queryByRole('progressbar')).toBeNull()
        })

        const user = userEvent.setup()
        const selectCompoEl = screen.getAllByTestId('connectionSelector').at(0)
        if (!selectCompoEl) {
            throw new Error('Expect selectCompoEl not to be null')
        }
        const button = within(selectCompoEl).getByRole('combobox')
        await user.click(button)
        const listbox = await screen.findByRole('listbox')
        const options = within(listbox).getAllByRole('option')
        const optionValues = options.map((li) => li.getAttribute('data-value'))

        const filteredConnectionsValues = dummyConnections.connections
            .filter((connection) => connection.connectionProvider === 'GitHub')
            .map((i) => i.connectionId)
        expect(optionValues).toEqual([
            'None selected',
            ...filteredConnectionsValues,
        ])
    })
})
