import { setupServer } from 'msw/node'
import {
    getProjectsProjectIdDataDataRepositoryIdGetProjectsProjectIdDataDataRepositoryIdGetMockHandler,
    getProjectsProjectIdDataDataRepositoryIdGetV02ProjectsProjectIdDataDataRepositoryIdGetMockHandler,
    getProjectsProjectIdDataGetV02ProjectsProjectIdDataGetMockHandler,
} from '../src/queries/data-repository/data-repository.msw'
import { getLabelsGetLabelsGetMockHandler } from '../src/queries/labels/labels.msw'
import {
    getProjectsProjectIdGetProjectsProjectIdGetMockHandler,
    getProjectsGetProjectsGetMockHandler,
    getProjectsUserUserIdGetProjectsUserUserIdGetMockHandler,
} from '../src/queries/projects/projects.msw'
import {
    getSignupResendConfirmationCodePostSignupResendConfirmationCodePostMockHandler,
    getSignupResendConfirmationCodePostSignupResendConfirmationCodePostResponseMock,
} from '../src/queries/signup/signup.msw'
import { getUsersUserIdDeleteUsersUserIdDeleteMockHandler } from '../src/queries/users/users.msw'

const server = setupServer(
    getSignupResendConfirmationCodePostSignupResendConfirmationCodePostMockHandler(
        getSignupResendConfirmationCodePostSignupResendConfirmationCodePostResponseMock(
            {
                userName: 'testuser',
            }
        )
    ),
    ...[
        getProjectsProjectIdDataDataRepositoryIdGetProjectsProjectIdDataDataRepositoryIdGetMockHandler,
        getProjectsProjectIdGetProjectsProjectIdGetMockHandler,
        getProjectsGetProjectsGetMockHandler,
        getLabelsGetLabelsGetMockHandler,
        getProjectsUserUserIdGetProjectsUserUserIdGetMockHandler,
        getProjectsProjectIdDataDataRepositoryIdGetV02ProjectsProjectIdDataDataRepositoryIdGetMockHandler,
        getProjectsProjectIdDataDataRepositoryIdGetProjectsProjectIdDataDataRepositoryIdGetMockHandler,
        getProjectsProjectIdDataGetV02ProjectsProjectIdDataGetMockHandler,
        getUsersUserIdDeleteUsersUserIdDeleteMockHandler,
    ].map((f) => f())
)

export default server
