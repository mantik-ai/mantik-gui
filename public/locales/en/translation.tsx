// At some point we will need to internationalize.
// We would convert the tsx to JSON
// and install i18next

// Best practice: keep hard-coded strings separate from code and in one place
// If we ever internationalize the app, this will make it easier
export type URL = {
    href: string
    text?: string
}

export const urls: Record<string, URL> = {
    externalDataTransferDocs: {
        href: 'https://mantik-ai.gitlab.io/mantik/remote-execution/remote-file-service.html',
        text: 'our documentation.',
    },
}

export const strings = {
    DataRepositories: {
        loadingDataRepositories: 'Loading Data Repositories...',
        privateProjectError:
            "The project you're trying to access is private and you don't have the required permissions!",
        externalDataTransferInfo:
            'Data can be transferred to external systems (e.g. HPC clusters, S3 buckets) using the Mantik CLI. For further details see ',
        tooltipDisabled:
            "You don't have the required permissions for this action",
        tooltip: 'Add a Data Repository',
        createSuccess: 'Data repository created successfully!',
        updateSuccess: 'Data repository updated successfully!',
        deleteSuccess: 'Data repository deleted successfully!',
        error: 'An error occurred',
    },
}
