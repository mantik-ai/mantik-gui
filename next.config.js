// eslint-disable-next-line @typescript-eslint/no-require-imports
const path = require('path')

const nextConfig = {
    reactStrictMode: true,
    publicRuntimeConfig: {
        apiBaseUrl: process.env?.NEXT_PUBLIC_API_URL,
        debounceTimerSearchQuery: Number(
            process.env?.DEBOUNCE_TIMER_SEARCH_QUERY
        ),
        mockDynamically: Boolean(process.env.MOCK_DYNAMICALLY),
        nextAuthUrl: process.env?.NEXTAUTH_URL,
        vercelUrl: process.env?.VERCEL_URL,
    },
    serverRuntimeConfig: {
        apiBaseUrl: process.env.NEXT_PUBLIC_API_URL,
    },
    env: {
        NEXT_PUBLIC_API_URL: process.env?.NEXT_PUBLIC_API_URL,
    },
}

/** @type {import('next').NextConfig} */
module.exports = {
    ...nextConfig,
    async redirects() {
        return [
            {
                source: '/settings',
                destination: '/settings/account',
                permanent: true,
            },
        ]
    },
    output: 'standalone',
    webpack: (config) => {
        config.resolve.alias['@'] = path.resolve(__dirname, './src')
        config.module.rules.push({
            test: /\.md$/,
            use: [
                {
                    loader: 'raw-loader',
                },
            ],
        })

        return config
    },
}
