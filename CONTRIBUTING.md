# Contributing to mantik GUI

## Table of Content

- [Contributing to mantik GUI](#contributing-to-mantik-gui)
    - [Table of Content](#table-of-content)
    - [Governance](#governance)
    - [How to Contribute](#how-to-contribute)
        - [Reporting bugs](#reporting-bugs)
        - [Suggesting improvements](#suggesting-improvements)
    - [Code contributions](#code-contributions)
        - [Tech Stack](#tech-stack)
            - [Linting and Code formatting](#linting-and-code-formatting)
        - [Development and testing](#development-and-testing)
            - [Create new mock endpoints](#create-new-mock-endpoints)
            - [Environment Variables required](#environment-variables-required)
            - [Generate client code from OpenAPI spec](#generate-client-code-from-openapi-spec)
            - [Integration Tests](#integration-tests)
        - [Merge requests](#merge-requests)
        - [App structure](#app-structure)
    - [Contributors](#contributors)

## Governance

Governance of the mantik GUI is conducted by [ambrosys GmbH](https://ambrosys.de) represented by

- [Fabian Emmerich](https://gitlab.com/fabian.emmerich) [(fabian.emmerich@4-cast.de)](mailto:fabian.emmerich@4-cast.de)
- [Amanda Maricia Raphael (lead frontend developer)](https://gitlab.com/amanda.maricia.raphael) [(amanda.maricia.raphael@ambrosys.de)](mailto:amanda.maricia.raphael@ambrosys.de)
- [Thomas Seidler](https://gitlab.com/thomas_ambrosys) [(thomas.seidler@ambrosys.de)](mailto:thomas.seidler@ambrosys.de)

## How to Contribute

### Reporting bugs

This section guides you through submitting a bug report for the mantik API.
Following these guidelines helps understand your report, reproduce the behavior, and find related reports.

**Notes:**

- Before submitting a bug report, check that your **issue does not already exist** in the [issue tracker](https://gitlab.com/mantik-ai/mantik-gui/issues).
- Make sure your issue **is really a bug**, and is not a support request.
- If you find a **closed** issue that seems like it is the same thing that you're experiencing, open a new issue and include a link to the original issue in the body of your new one.

Submit your bugs in the [issue tracker](https://gitlab.com/mantik-ai/mantik-gui/issues):

1. Use the bug issue template. This can be selected from the dropdown menu in the `Description` field.
1. Completely fill out the template.
1. Save the issue.

### Suggesting improvements

You may submit enhancement suggestion such as new features, improvements of existing functionality, or performance by submitting a ticket.
Before submitting a new suggestion, check that your **issue does not already exist** in the [issue tracker](https://gitlab.com/mantik-ai/mantik-gui/issues).

## Code contributions

### Tech Stack

- [NextJs](https://github.com/vercel/next.js/)
- [React Form Hook](https://github.com/react-hook-form/react-hook-form)
- [Material UI](https://github.com/mui/material-ui)
- [TypeScript](https://github.com/microsoft/TypeScript)
- [Axios](https://github.com/axios/axios)
- [Orval](https://github.com/anymaniax/orval)

#### Linting and Code formatting

- eslint
- prettier

### Development and testing

- Clone the repository
- Install dependencies with `npm install`
- Run `npm run dev` to start the development server locally (default port is 3000)

For local testing a mock server can be run with [prism](https://stoplight.io/open-source/prism) using the [OpenAPI spec from mantik-api](https://gitlab.com/mantik-ai/mantik-api/-/tree/main/spec).
This requires cloning the mantik-api repository and concatenting the current API specification (described [here](https://gitlab.com/mantik-ai/mantik-api/-/tree/main/spec#concatenate-api-specification)).

Then, the mock server can be served using

```bash
prism mock "<path to concatenated OpenAPI spec YAML>"
```

#### Create new mock endpoints

1. This requires cloning the mantik-api repository.
2. Create yaml files in the mantik-api repository according to the path needed.
3. Add examples for the response of the newly created endpoint according to the schema.
4. Add the path to the openapi.yaml file.
5. concatenate the current API specification by running the below docker commands from `mantik-api/spec`.

```bash
 docker build ./swagger -t swagger-cli
 docker run -v ${PWD}:/local swagger-cli bundle local/openapi.yaml --outfile local/concatenated_openapi.yaml --type yaml --dereference
```

(For more info click [here](https://gitlab.com/mantik-ai/mantik-api/-/tree/main/spec#concatenate-api-specification)).

6. Update `https://pcode.stoplight.io/docs/mantik-new/branches/main/0xh59fnf6p924-mantik-minimal-api` :-
    1. To update edit the project and add the newly created `concatenated_openapi.yaml` file by using `+`button.
    2. `Publish` it using the publish button and check the response.
    3. If response is ok, endpoint is ready to fetch the example data.
       Note : Create a ticket in `mantik-api` to implement the real api for the newly created mock api.

#### Environment Variables required

- `SECRET`: Hexadecimal string with 32 bytes
- `NEXTAUTH_URL`: Full domain in the form "https://{full_domain}"
- `COGNITO_CLIENT_ID`: Default Cognito Client ID
- `COGNITO_USERPOOL_ID`: User Pool ID
- `COGNITO_REGION`: AWS Region
- `NEXT_PUBLIC_MANTIK_MLFLOW_UI_URL`: MLflow UI baseURL

#### Generate client code from OpenAPI spec

We use `orval` to generate code for the mantik API calls. It is configured in `orval.config.js`. Note that the API spec is currently pulled from `api.dev2.cloud.mantik.ai`.

To generate code, do:

```bash
npm i orval -D
npx orval --config orval.config.js
```

Each time the api contract changes orval function has to be regenerated using the below commands:
To regenerate the code, do:

```bash
npx orval --clean
npx orval --config orval.config.js
npx orval --prettier
```

#### Integration Tests

We use Jest with the [React Testing Library](https://testing-library.com/docs/react-testing-library/intro/) for integration and component tests.
Integration tests allow us to choose a test driven development approach. When we write new components, we write an integration test that captures the expected behavior of the component or feature.

When we test components that make API calls, we need a service worker that intercepts the calls to the external API and replace it with mock data. We use the [Mock Service Worker](https://mswjs.io/) for the API call interception and [OpenAPI Stack](https://openapistack.co/) to create mock responses from our [OpenAPI Specification file](https://api.dev2.cloud.mantik.ai/mantik-api/openapi.json). The mockservice is started before the test execution and stopped after each test run in the [jest.setup file](jest.setup.ts). In order to get a response from the OpenApi specifications, we need to paste the json data into our local copy of the [openapi.json](__mockserver__/openapi.json) file.

We use Axios to make requests and React-Query (orval functions). Calls with both methods are intercepted and replaced. Components that make API calls with react-query need to be wrapped in a query client. This and other helper functions for integration and component tests are located in the [utils](tests/utils) folder.

Examples for testing components with different API calls and mocking requirements can be found [here](tests/unit/TestMSW.test.tsx).

### Merge requests

- Describe your changes as accurately as possible.
  The merge request body should be kept up to date as it will usually form the base for the changelog entry.
- Be sure that your merge request contains tests that cover the changed or added code.
  Tests are generally required for code be to be considered mergable, and code without passing tests will not be merged.
  Ideally, the test coverage should not decrease.
- Ensure your merge request passes the pre-commit checks.
  Remember that you can run these tools locally.
- If your changes warrant a documentation change, the merge request must also update the documentation.

### App structure

Check custom hooks here:

- `src > common > hooks > HOOKS_MAP.md

**Note:**
Make sure your branch is [rebased](https://docs.github.com/en/get-started/using-git/about-git-rebase) against the latest
main branch.

## Contributors

Markus Abel, Fabian Emmerich, Philipp Lehmann, Kevin Mohammed, Adhish Nigam, Christoph Pernsteiner, Thomas Seidler, Daniel Weidinger, Amanda Maricia Raphael, Zubaer Haque, Mirja Tschakarov.
